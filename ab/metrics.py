"""
Creation of a new column 'metrics' in data
"""

import numpy as np
import pandas as pd

from ab.types import MetricsNames


def add_metrics_by_date(data: pd.DataFrame,
                        metrics_name: MetricsNames = MetricsNames.AE
                        ) -> pd.DataFrame:
    """
    Adds column 'metrics' to the dataframe. The data in 'metrics' will be analized in AB tests of forecast quality.

    :param data: DataFrame with forecast results that must contain columns 'target' and 'fc'
    :param metrics: defines the type of metrics to be added to the data
            'AE' : metrics = abs(fc - target)

            Metrics types to be included in the function:
            'FA' : metrics = 1 - abs(fc - target) / mean(target); to get FA over the whole metrics for given key,
                   calculate mean(metrics)
            'FB' : metrics = (target - fc) / mean(fc); to get FB over the whole metrics for given key,
                   calculate mean(metrics)
            'MAPE' : metrics = 100 * abs(1 - fc / target), exists only if all target values are nonzero; to get MAPE
                     over the whole metrics for given key, calculate mean(metrics)
            'MSE' : metrics = (target - fc)**2; to get MSE over the whole metrics for given key, calculate mean(metrics)
    """

    # TODO: add other metrics, such as FA, FB, MAPE, MSE, and so on
    data['target'] = data['target'] if data['target'].dtype == 'float' else data.target.astype(float)
    data['fc'] = data['fc'] if data['fc'].dtype == 'float' else data.fc.astype(float)

    if metrics_name == MetricsNames.AE:
        data['metrics'] = np.abs(data['fc'] - data['target'])

        return data

    if metrics_name.value in ['FA', 'FB', 'MAPE', 'MSE']:
        raise NotImplementedError('This type of metrics cannot be handled yet.')

    return data
