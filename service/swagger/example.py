from io import StringIO
import json
import pandas as pd
from service.swagger.description import SwaggerDescription

description = SwaggerDescription()


class Examples:

    @staticmethod
    def test_1():
        test_df = pd.read_csv("./tests/tests/hecate/test_4/data.csv")
        return json.loads(test_df.to_json())

    @staticmethod
    def test_2():
        test_df = pd.read_csv("./tests/tests/hecate/test_1/data.csv")
        return json.loads(test_df.to_json())

    @staticmethod
    def test_3():
        test_df = pd.read_csv("./tests/tests/hecate/test_4/data.csv")
        test_df.drop(columns=['const'], inplace=True)
        test_df['partition'].replace({'fc': 'decompose'}, inplace=True)
        return json.loads(test_df.to_json())

    @staticmethod
    def test_1_csv():
        test_df = pd.read_csv("./tests/tests/hecate/test_4/data.csv")
        iostr = StringIO()
        test_df.to_csv(iostr, encoding='utf8', sep=';', index=False)
        return iostr.getvalue()

    @staticmethod
    def test_2_csv():
        test_df = pd.read_csv("./tests/tests/hecate/test_1/data.csv")
        iostr = StringIO()
        test_df.to_csv(iostr, encoding='utf8', sep=';', index=False)
        return iostr.getvalue()

    @staticmethod
    def test_3_csv():
        test_df = pd.read_csv("./tests/tests/hecate/test_4/data.csv")
        test_df.drop(columns=['const'], inplace=True)
        test_df['partition'].replace({'fc': 'decompose'}, inplace=True)

        iostr = StringIO()
        test_df.to_csv(iostr, encoding='utf8', sep=';', index=False)
        return iostr.getvalue()

    payload_type_examples = {
        "parquet": {
            "summary": "Parquet type",
            "description": "<p>Параметр <b>payload_type</b> формата \"parquet\".</p>" +
                           "<p>Пример для <b>Request body</b>, " +
                           "т.е. тела запроса необходимо установить <b>application/gzip</b></p>",
            "value": "parquet"
        },
        "json": {
            "summary": "Json type",
            "description": "<p>Параметр <b>payload_type</b> формата \"json\".</p>" +
                           "<p>Пример для <b>Request body</b>, т.е. " +
                           "тела запроса необходимо установить <b>application/json</b></p>",
            "value": "json"
        },
        "csv": {
            "summary": "CSV type",
            "description": "<p>Параметр <b>payload_type</b> формата \"csv\".</p>" +
                           "Ожидается, что файл будет иметь кодировку utf-8 и разделитель ';' ",
            "value": "csv"
        },
    }

    decompose_type_examples = {
        "all": {
            "summary": "All columns",
            "description": description.decompose_desc,
            "value": ["all"]
        },
        "Bl": {
            "summary": "Bl column",
            "description": description.decompose_desc,
            "value": ["bl"]
        },
        "Cn": {
            "summary": "Cn column",
            "description": description.decompose_desc,
            "value": ["cn"]
        },
        "Pc": {
            "summary": "Pc column",
            "description": description.decompose_desc,
            "value": ["pc"]
        },
        "Bl_Cn": {
            "summary": "Cn and Bl columns",
            "description": description.decompose_desc,
            "value": ["bl", "cn"]
        },
        "Bl_Pc": {
            "summary": "Bl and Pc columns",
            "description": description.decompose_desc,
            "value": ["bl", "pc"]
        },
        "Cn_Pc": {
            "summary": "Cn and Pc columns",
            "description": description.decompose_desc,
            "value": ["cn", "pc"]
        },
        "Bl_Cn_Pc": {
            "summary": "Bl, Cn and Pc columns",
            "description": description.decompose_desc,
            "value": ["bl", "cn", "pc"]
        },
    }
