from pydantic import BaseModel
from typing import Any
from service.swagger.example import Examples


class JsonDataResponse(BaseModel):
    """
    Schema json model of dataframe response 200 for FastApi documentation
    """
    key: dict | Any
    date: dict | Any
    fc: dict | Any


class TaskResponse(BaseModel):
    """
    Schema json model of dataframe response 200 for FastApi documentation
    """
    task_id: str | None


class NonAuth(BaseModel):
    """
    Schema json model of dataframe response 418 for FastApi documentation
    """
    msg: str | None


class ServerError(BaseModel):
    """
    Schema json model of dataframe response 500 for FastApi documentation
    """
    msg: str | None


class JsonData(BaseModel):
    """
    Schema json model of dataframe response for FastApi documentation
    """
    key: dict | Any
    date: dict | Any
    partition: dict | Any
    target: dict | Any

    class Config:
        schema_extra = {
            'examples': [
                {
                    'key': {"0": "key-1", "1": "key-1", "2": "key-1", "3": "key-1"},
                    'date': {"0": "2022-07-04", "1": "2022-07-11", "2": "2022-07-18", "3": "2022-07-25"},
                    'partition': {"0": "train", "1": "train", "2": "train", "3": "fc"},
                    'target': {"0": 100.01, "1": 1.0, "2": 1.0, "3": 0.0}
                },
                Examples.test_1(),
                Examples.test_2(),
                Examples.test_3(),
            ]
        }


class ParquetData(BaseModel):
    """
    Schema parquet model of dataframe response for FastApi documentation
    """
    data: bytes


class CSVData(BaseModel):
    """
    Schema CSV model of dataframe response for FastApi documentation
    """
    data: bytes

    class Config:
        schema_extra = {
            'examples': [
                Examples.test_1_csv(),
                Examples.test_2_csv(),
                Examples.test_3_csv(),
            ]
        }


class SwaggerSchemas():
    payload_type = {
        "parquet": "application/gzip",
        "json": "application/json",
        "csv": "text/csv",
    }
