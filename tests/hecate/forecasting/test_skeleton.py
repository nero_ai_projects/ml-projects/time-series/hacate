import pytest

from hecate import hecate as hc
from hecate.features import FeatureType
from hecate.forecasting.features.generators.base import DummyFGenerator
from hecate.forecasting.features.generators.business import BusinessFGenerator
from hecate.forecasting.features.generators.target import TargetFGenerator
from hecate.forecasting.features.generators.time import TimeFGenerator
from hecate.forecasting.outliers.base import DummyOutlierMarkGenerator
from hecate.forecasting.outliers.target import TargetOutlierGenerator
from hecate.forecasting.postprocessings.fc import \
    FCAnnihilator, DummyFCAnnihilator, DummyFCRounder, SimpleRounder, RollingRounder
from hecate.forecasting.repartitions.base import DummyRepartitionExecutor
from hecate.forecasting.repartitions.repartition import RepartitionExecutor
from hecate.forecasting.skeleton import (
    ForecastingTaskStep,
    Preprocessor, DummyPreprocessor,
    Forecaster, DummyForecaster,
    Postprocessor, DummyPostprocessor,
    ForecastingTaskConfig, ForecastingTask,
    PreprocessorConfig, PreprocessingStep,
    PostprocessingStep, PostprocessorConfig
)
from tests.hecate.test_hecate import generate_df_two_promo_period_per_partition


def test_postprocessor_step():

    stuff = [
        ('target_outlier_generator',   FCAnnihilator, DummyFCAnnihilator),
    ]
    for name, cls, dummy_cls in stuff:
        step = PostprocessingStep(name=name,
                                  executor=cls(),
                                  is_on=True)

        assert step.name == name
        assert step.is_on
        assert step.is_on and isinstance(step.get_executor(), cls)
        step.is_on = False
        assert not step.is_on and isinstance(step.get_executor(), dummy_cls)


def test_postprocessing_support_toggles():

    task = Postprocessor()
    assert isinstance(task.fc_annihilator, FCAnnihilator)

    cfg = PostprocessorConfig(toggles={'fc_annihilator': False})
    task = Postprocessor(cfg)
    assert isinstance(task.fc_annihilator, DummyFCAnnihilator)

    assert isinstance(task.fc_rolling_rounder, RollingRounder)

    cfg = PostprocessorConfig(toggles={'fc_rolling_rounder': False})
    task = Postprocessor(cfg)
    assert isinstance(task.fc_rolling_rounder, DummyFCRounder)

    assert isinstance(task.fc_simple_rounder, SimpleRounder)

    cfg = PostprocessorConfig(toggles={'fc_simple_rounder': False})
    task = Postprocessor(cfg)
    assert isinstance(task.fc_simple_rounder, DummyFCRounder)


def test_postprocessing_config_raise_on_unknowns():

    with pytest.raises(ValueError):
        PostprocessorConfig(toggles={'unknown_step': False})

    PostprocessorConfig(toggles={'fc_annihilator': False})


def test_preprocessor_step():

    stuff = [
        ('target_outlier_generator',   TargetOutlierGenerator, DummyOutlierMarkGenerator),
        ('repartition_executor',       RepartitionExecutor,    DummyRepartitionExecutor),
        ('business_feature_generator', BusinessFGenerator,     DummyFGenerator),
        ('time_feature_generator',     TimeFGenerator,         DummyFGenerator),
        ('target_feature_generator',   TargetFGenerator,       DummyFGenerator),
    ]

    for name, cls, dummy_cls in stuff:
        step = PreprocessingStep(name=name,
                                 executor=cls(),
                                 is_on=True)

        assert step.name == name
        assert step.is_on
        assert step.is_on and isinstance(step.get_executor(), cls)
        step.is_on = False
        assert not step.is_on and isinstance(step.get_executor(), dummy_cls)


def test_preprocessing_support_toggles():

    task = Preprocessor()
    assert isinstance(task.target_outlier_generator, TargetOutlierGenerator)
    assert isinstance(task.repartition_executor, RepartitionExecutor)
    assert isinstance(task.business_feature_generator, BusinessFGenerator)
    assert isinstance(task.time_feature_generator, TimeFGenerator)
    assert isinstance(task.target_feature_generator, TargetFGenerator)

    cfg = PreprocessorConfig(toggles={'target_outlier_generator': False})
    task = Preprocessor(cfg)
    assert isinstance(task.target_outlier_generator, DummyOutlierMarkGenerator)
    assert isinstance(task.repartition_executor, RepartitionExecutor)
    assert isinstance(task.business_feature_generator, BusinessFGenerator)
    assert isinstance(task.time_feature_generator, TimeFGenerator)
    assert isinstance(task.target_feature_generator, TargetFGenerator)


def test_preprocessing_config_raise_on_unknowns():

    with pytest.raises(ValueError):
        PreprocessorConfig(toggles={'unknown_step': False})

    PreprocessorConfig(toggles={'repartition_executor': False})


def test_fc_task_step():

    stuff = [
        ('preprocessor',  Preprocessor,  DummyPreprocessor),
        ('forecaster',    Forecaster,    DummyForecaster),
        ('postprocessor', Postprocessor, DummyPostprocessor),
    ]

    for name, cls, dummy_cls in stuff:
        step = ForecastingTaskStep(name=name,
                                   executor=cls(),
                                   is_on=True)

        assert step.name == name
        assert step.is_on
        assert step.is_on and isinstance(step.get_executor(), cls)
        step.is_on = False
        assert not step.is_on and isinstance(step.get_executor(), dummy_cls)


def test_fc_task_step_support_toggles():

    task = ForecastingTask()
    assert isinstance(task.forecaster, Forecaster)
    assert isinstance(task.preprocessor, Preprocessor)
    assert isinstance(task.postprocessor, Postprocessor)

    cfg = ForecastingTaskConfig(toggles={'forecaster': False})
    task = ForecastingTask(cfg)
    assert type(task.forecaster) is DummyForecaster
    assert type(task.preprocessor) is Preprocessor
    assert type(task.postprocessor) is Postprocessor


def test_fc_task_config_init_raises():
    with pytest.raises(ValueError):
        ForecastingTaskConfig()

    with pytest.raises(ValueError):
        ForecastingTaskConfig(toggles={'unknown_step': False})

    with pytest.raises(ValueError):
        ForecastingTaskConfig(custom_executors={'unknown_step': Forecaster()})

    with pytest.raises(ValueError):
        ForecastingTaskConfig(toggles={'unknown_step': False},
                              custom_executors={'unknown_step': Forecaster()})


def test_fc_task_support_custom_executors():

    task = ForecastingTask()
    assert isinstance(task.forecaster, Forecaster)
    assert isinstance(task.preprocessor, Preprocessor)
    assert isinstance(task.postprocessor, Postprocessor)

    class OneMorePostprocessor(Postprocessor):
        ...

    cfg = ForecastingTaskConfig(toggles={'forecaster': False},
                                custom_executors={'postprocessor': OneMorePostprocessor()})
    task = ForecastingTask(cfg)
    assert type(task.forecaster) is DummyForecaster
    assert type(task.preprocessor) is Preprocessor
    assert type(task.postprocessor) is OneMorePostprocessor
    # TODO: add execution call


def test_fc_task_support_step_configs():

    postprocessor_cfg = PostprocessorConfig(toggles={'fc_annihilator': False})
    cfg = ForecastingTaskConfig(postprocessor_config=postprocessor_cfg)

    _ = ForecastingTask(cfg)

    sample = generate_df_two_promo_period_per_partition()
    sample['stop'] = (sample.partition == 'fc').astype(int)
    model_answer = hc.fc_sample(sample, feature_types={'stop': FeatureType.stop}, drivers=[], config=cfg)
    data = sample.merge(model_answer.data)
    fc_data = data[data.partition == 'fc']
    assert fc_data.fc.sum() > 0.0
