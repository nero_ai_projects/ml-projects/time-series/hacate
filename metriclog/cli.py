import click

from hecate.log import get_logger
from metriclog.constants import HORIZONS
from metriclog.database import Database
from metriclog.runner import run_hecate_on_data_from_db
from metriclog.extract_data import extract_dataset
from metriclog.types import HecateVersionsParams


logger = get_logger('hecate')


@click.command()
@click.option('--mode', default='run', help="enter the operation to be performed: \n"
              "\n'run' runs the forecast on the data from the database "
              "(particular values of the parameters can be set with other options, see below) \n"
              "\n'list_projects' returns a list of projects available in the database \n"
              "\n'list_horizons' returns a list of the available forecast time horizon values \n"
              "\n'extract_dataset' extracts a dataset from the database to .json and .parquet files in "
              "the folder --output_dir/[PROJECT NAME]/, rewrites the files if they already exist (particular values "
              "of the parameters can be set with other options, see below) \n"
              "\n ")
@click.option('--password', prompt=True, help='enter the database password here. Otherwise, the terminal '
              'will ask you for the password on the next line as a hidden input \n'
              '\n ', hide_input=True)
@click.option('--db_ip', '-i', help="enter the database ip here, default='185.130.115.92' \n"
              "\n ", default='185.130.115.92')
@click.option('--db_name', '-d', help="enter the database name here, default='dev-data-and-reports-pg' \n"
              "\n ", default='dev-data-and-reports-pg')
@click.option('--user_name', '-u', help="enter the user name in database here, default='hecate \n"
              "\n ", default='hecate')
@click.option('--bl', '-b', help="whether to calculate the baseline; to calculate it every discount feature "
              "will be zeroed in the forecast period, default=True \n"
              "\n ", default=True)
@click.option('--n_folds', '-n', help="the number of folds, for which the forecast is to be calculated, "
              "if it exceeds the number of available folds, the forecast will be calculated for all folds, default=3 \n"
              '\n ', default=3)
@click.option('--focus', '-f', help='flag to use to forecast only the most important products and customers '
              'that are predefined in the dataset, default=False \n'
              '\n ', default=False)
@click.option('--project', '-p', help="enter the name project to use, can be 'all' to choose all projects in the "
              "database, default='wings' \n"
              "\n ", default='Wings')
@click.option('--horizon', '-h', help="'st', 'mt', 'lt', or 'all' - time horizon to use in the forecast, \n"
              "\n'st' is a short term (the horizon is shorter or equal to 6 weeks), \n"
              "\n'mt' is a mid term (the horizon is longer than 'st' but shorter or equal to "
              "a half year (27 weeks)), \n"
              "\n'lt' is a long term (the horizon is longer than a half year (27 weeks)), \n"
              "\n'all' is applicable for --mode=run to make forecasts for every available horizon,"
              "\ndefault='st' \n"
              "\n ", default='st')
@click.option('--save_locally', '-s', help='if True the results will be saved locally in the output folder '
              '(see --output_dir) instead of written into the database, default=False \n'
              '\n ', default=False)
@click.option('--save_input_file_locally', help='if True the hecate input dataframe will be saved locally '
              'in the output folder (see --output_dir), default=False \n'
              '\n ', default=False)
@click.option('--fold', help="the number of the folds to extract as the forecast period, the train period will be "
              "considered up to the beginning of the fold, the data after the fold will be partitioned as non relevant,"
              " applicable when --mode=extract_dataset, 1 is the last fold in the time scale, default=1 \n"
              '\n ', default=1)
@click.option('--output_dir', help="the path to the output folder, default='./outputs/' \n"
              "\n ", default='./outputs/')
@click.option('--n_cores', help='the number of threads to run in parallel for data uploading to the database, '
              'default=1 \n'
              '\n ', default=1)
def main(mode: str, password: str, db_ip: str, db_name: str, user_name: str, project: str, focus: bool, fold: int,
         horizon: HORIZONS, bl: bool, n_folds: int, save_locally: bool, save_input_file_locally: bool, output_dir: str,
         n_cores: int):
    """
    Tool for running hecate forecast on data from Advanced projects \n
    \n
    Usage: python -m metriclog.cli [OPTION_1] [VALUE] [OPTION_N] [VALUE] \n
    In few cases, as for example in GitBash for OS Windows: \n
    Usage: winpy python -m metriclog.cli [OPTION_1] [VALUE] [OPTION_N] [VALUE]
    """
    db_connection = Database(password, db_ip, db_name, user_name)
    if mode == 'run':
        hecate_versions_params = HecateVersionsParams(bl=bl, n_folds=n_folds, focus=focus, project=project.lower(),
                                                      horizon=horizon, save_locally=save_locally, n_cores=n_cores,
                                                      save_input_file_locally=save_input_file_locally)
        run_hecate_on_data_from_db(hecate_versions_params, db_connection, ab_mode=False)
    elif mode == 'list_projects':
        projects = db_connection.extract_project_names()
        logger.info('projects: ' + ', '.join(projects))
    elif mode == 'list_horizons':
        horizons = db_connection.extract_horizons_list()
        logger.info('horizons: ' + ', '.join(horizons))
    elif mode == 'extract_dataset':
        extract_dataset(password, db_ip, db_name, user_name, fold, project.lower(), horizon, focus, output_dir)
    else:
        logger.info('no such mode')


if __name__ == "__main__":
    main()
