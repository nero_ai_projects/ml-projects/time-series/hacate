from itertools import chain
from typing import Literal, Final

from hecate.types import ColsMapDrivers


TIME_FEATURES_COLS: Final = ('time-year', 'time-month', 'time-week', 'time-dayofweek', 'time-weekofmonth')
REQUIRED_COLS: Final = ('key', 'date', 'partition', 'target')
REQUIRED_COLS_TYPES: Final[dict[str, list[type | str]]] = {
    'date': ['datetime64[ns]'],
    'target': ['float32', 'float64', 'int8', 'int16', 'int32', 'int64'],
}

ALL_DRIVERS: Final[ColsMapDrivers] = {
    'bl': ['bl', 'uplift'],
    'cn': ['cannibalized'],
    'pc': ['price_effect'],
}
TARGET_FC_COLS: Final[set[Literal['fc', 'target']]] = {'fc', 'target'}
COLS_TO_ROUND: Final = set(chain(*ALL_DRIVERS.values())).union(TARGET_FC_COLS)
HALF_YEAR_LEN: Final = 182

TARGET_ENCODING_COLS: Final = ('time-month', 'time-week')
SHORT_TRAIN_FLAGS: Final = (True, False)
