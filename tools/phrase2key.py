import hashlib
import argparse


def phrase2key(phrase: str) -> str:
    return hashlib.sha512(phrase.encode()).hexdigest()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Convert a phrase to a key')
    parser.add_argument('phrase', type=str, help='The phrase to convert')
    args = parser.parse_args()
    print(phrase2key(args.phrase))
