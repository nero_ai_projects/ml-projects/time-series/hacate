import json
from io import BytesIO
from pathlib import Path
from typing import NamedTuple

import pandas as pd
import pytest
from fastapi import Header
from fastapi.testclient import TestClient

from hecate.config import PROJECT_PATH
from service import app, utils

TESTS_DATA_DIR = PROJECT_PATH.joinpath('tests', 'tests')
CATEG_TEST_DATA = TESTS_DATA_DIR.joinpath('categ_cols_usage')


class EndpointCaseData(NamedTuple):
    init_data: pd.DataFrame
    endpoint: str
    expected_res: float | int
    feature_types: dict[str, str]


@pytest.fixture(scope='module', params=[('test1', '/fc_batch'), ('test2', '/fc')])
def categ_data_cases(
    request,
    testclient: TestClient
) -> EndpointCaseData:

    init_data, feature_types = get_data(CATEG_TEST_DATA.joinpath(request.param[0]))
    headers = {
        "api_key": "test",
        "payload_type": "parquet",
        "decompose": "all",
        "parallel": "true",
        "Feature-Types": json.dumps(feature_types)
    }

    case_data = EndpointCaseData(
        init_data,
        request.param[1],
        init_fc_sum_for_categ_test(init_data, testclient, headers, request.param[1]),
        feature_types
    )

    return case_data


@pytest.fixture(scope='module', params=[
    ('test1', '/get_can_pairs', 200),
    ('test1', '/decompose_act', 200)])
def categ_data_cases_can_decomp(request) -> EndpointCaseData:

    init_data, feature_types = get_data(CATEG_TEST_DATA.joinpath(request.param[0]))
    case_data = EndpointCaseData(
        init_data,
        request.param[1],
        request.param[2],
        feature_types
    )

    return case_data


def init_fc_sum_for_categ_test(
    categ_init_data: pd.DataFrame,
    testclient: TestClient,
    headers_categ: dict,
    endpoint: str
) -> float:

    buffer = BytesIO()
    categ_init_data.to_parquet(buffer, engine='pyarrow')
    init_fc_req = testclient.post(url=endpoint, headers=headers_categ, data=buffer.getvalue())
    init_fc_sum = pd.read_parquet(BytesIO(init_fc_req.content)).fc.sum()
    return init_fc_sum


def get_data(path: Path) -> tuple[pd.DataFrame, dict[str, str]]:
    """
    Read data from file.
    """
    feature_types = {}
    data = pd.DataFrame()
    for i_path in path.glob('**/*.*'):
        print(i_path)
        if i_path.is_file():
            if i_path.suffix == '.parquet':
                data = pd.read_parquet(i_path, engine='pyarrow')
            elif i_path.suffix == '.csv':
                data = pd.read_csv(i_path, delimiter=';')
            elif i_path.suffix == '.json' and i_path.stem == 'data':
                with open(i_path, 'r') as file:
                    data = json.load(file)
            elif i_path.suffix == '.json' and i_path.stem == 'dict':
                with open(i_path, 'r') as file:
                    feature_types = json.load(file)
            else:
                raise FileNotFoundError(f'unknown file type {path}')
    return data, feature_types


@pytest.fixture(scope='module')
def testclient() -> TestClient:
    async def override_dependency(api_key: str | None = Header(title="api_key", convert_underscores=False)):
        return ""

    client = TestClient(app.app)
    app.app.dependency_overrides[utils.is_authorized] = override_dependency
    return client
