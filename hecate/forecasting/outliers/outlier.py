import pandas as pd

from hecate.forecasting.outliers.target import TargetOutlierGenerator
from hecate.utils.decorators import check_datatypes


@check_datatypes
def add_target_outliers(data: pd.DataFrame) -> pd.DataFrame:
    """
    Add outliers marks for target
    :param data: pd.DataFrame : key, target, date, partition columns is needed
    :return: pd.DataFrame that contains columns with outlier marks
    """

    outcomes = []
    for key, sample in data.groupby('key', sort=False):
        tog = TargetOutlierGenerator()
        sample, col_names = tog.add_outlier_marks(sample)
        outcomes.append(sample)

    return pd.concat(outcomes, ignore_index=True)
