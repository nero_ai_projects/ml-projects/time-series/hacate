from metriclog.types import HecateVersionsParams
import pandas as pd
import pytest


def test_extract_project_names(db_connection):
    # condition project list
    assert {'apricot', 'test', 'wings'} == set(db_connection.extract_project_names())


def test_extract_horizons_list(db_connection):
    # condition horizon list
    assert {'lt', 'mt', 'st'} == set(db_connection.extract_horizons_list())


@pytest.mark.skip("sqlite3 does not support copy_expert()")
def test_extract_dataset(db_connection):
    # condition not use focus
    dataset = db_connection.extract_dataset('wings', False)
    assert 3 == dataset.shape[0]
    assert {'key', 'date', 'partition', 'target', 'time_freq', 'focus', 'project'} == set(dataset.columns)

    # condition use focus
    dataset = db_connection.extract_dataset('wings', True)
    assert 2 == dataset.shape[0]


def test_extract_horizon_and_folds(db_connection):
    # condition horizon_and_folds
    horizon_and_folds = db_connection.extract_horizon_and_folds()
    assert 5 == horizon_and_folds.shape[0]
    assert {'project', 'horizon', 'fold', 'forecast_start', 'forecast_end'} == set(horizon_and_folds.columns)


def test_extract_feature_types(db_connection):
    # condition horizon_and_folds
    feature_types = db_connection.extract_feature_types('wings')
    assert {'key': 'dim', 'date': 'dim'} == feature_types


def test_send_chunk(db_connection):
    df_to_send = pd.DataFrame([['tom', 10], ['nick', 15], ['juli', 14]], columns=['Name', 'Age'])
    db_connection.send_chunk(df_to_send, 'test_table')
    db_connection.cursor.execute("SELECT * FROM test_table;")
    df_in_db = db_connection.cursor.fetchall()
    column_names = [desc[0] for desc in db_connection.cursor.description]
    df_in_db = pd.DataFrame(df_in_db, columns=column_names)
    assert df_to_send.equals(df_in_db)


def test_send_df(db_connection):
    df_to_send = pd.DataFrame([['tom', 10], ['nick', 15], ['juli', 14], ['ted', 20]], columns=['Name', 'Age'])
    db_connection.send_df(df_to_send, 'test_table', 2)
    db_connection.cursor.execute("SELECT * FROM test_table;")
    df_in_db = db_connection.cursor.fetchall()
    column_names = [desc[0] for desc in db_connection.cursor.description]
    df_in_db = pd.DataFrame(df_in_db, columns=column_names)
    assert df_to_send.equals(df_in_db)


def test_create_result_table(db_connection):

    # condition create results table
    db_connection.create_result_table('another_test_table')
    db_connection.cursor.execute("SELECT * FROM another_test_table;")
    df_in_db = db_connection.cursor.fetchall()
    column_names = [desc[0] for desc in db_connection.cursor.description]
    assert [] == df_in_db

    assert {'key', 'date', 'fc', 'target', 'focus', 'fold',
            'partition', 'product_category', 'product_group', 'chain_of_stores',
            'horizon', 'commit_id', 'branch', 'project'} == set(column_names)


def test_index_results_table(db_connection):
    # condition index create
    db_connection.index_results_table('results_wings')
    db_connection.cursor.execute("SELECT * FROM sqlite_master WHERE type='index';")
    db_answer = db_connection.cursor.fetchall()
    assert 'result_id' == db_answer[0][1]
    assert 'results_wings' == db_answer[0][2]


def test_delete_results_if_exist(db_connection):
    # condition delete results
    hvp = HecateVersionsParams(branch='branch', commit_id='id_1', focus=False, fold=0, horizon='mt')
    db_connection.delete_results_if_exist(hvp, 'results_wings')
    db_connection.cursor.execute("SELECT * FROM results_wings;")
    df_in_db = pd.DataFrame(db_connection.cursor.fetchall())
    assert 2 == df_in_db.shape[0]


def test_send_results_to_db(db_connection, final_table):

    # condition send results
    dict_to_send = {"key": ["key1", "key1"], "date": ["2019-01-01", "2019-01-02"], "fc": [1.0, 5.0],
                    "target": [1.0, 5.0], "focus": [False, False], "fold": [0, 0], "horizon": ["mt", "mt"],
                    "commit_id": ["id_2", "id_2"], "branch": ["branch", "branch"], "project": ["apricot", "apricot"],
                    "product_group": ['pg_1', 'pg_1'], 'product_category': ['pc_1', 'pc_1'],
                    'chain_of_stores': ['ch_1', 'ch_1'], 'partition': ['train', 'train '],
                    }
    df_to_send = pd.DataFrame.from_dict(dict_to_send)
    hvp = HecateVersionsParams('branch', 'id_2', focus=False, fold=0, horizon='mt', project='apricot')
    db_connection.send_results_to_db(df_to_send, hvp)
    db_connection.cursor.execute("SELECT * FROM results_apricot;")
    df_in_db = db_connection.cursor.fetchall()
    column_names = [desc[0] for desc in db_connection.cursor.description]
    df_in_db = pd.DataFrame(df_in_db, columns=column_names)
    assert 2 == df_in_db.shape[0]


def test_select_ab_results_if_exist(db_connection, ab_results_test):

    test_data = ab_results_test
    table = 'ab_results_test'
    db_connection.send_df(test_data, table)
    hecate_versions_params_empty = HecateVersionsParams(branch_a='unexisting_branch', commit_a_id='unexisting_commit')
    hecate_versions_params = HecateVersionsParams(branch_a=test_data.branch_a[0],
                                                  commit_a_id=test_data.commit_a_id[0],
                                                  branch_b=test_data.branch_b[0],
                                                  commit_b_id=test_data.commit_b_id[0],
                                                  focus=test_data.focus[0],
                                                  fold=test_data.fold[0],
                                                  horizon=test_data.horizon[0]
                                                  )

    selected_empty_data = db_connection.select_ab_results_if_exist(hecate_versions_params_empty, table)
    assert len(selected_empty_data) == 0

    selected_data = db_connection.select_ab_results_if_exist(hecate_versions_params, table)
    assert selected_data.effect_abs.sum() == test_data.effect_abs.sum()
    assert set(selected_data.columns) == set(test_data.columns)
    assert set(selected_data.key.unique()) == set(test_data.key.unique())
    for key in selected_data.key.unique():
        assert (set(selected_data.loc[selected_data['key'] == key].values.tolist()[0]) ==
                set(test_data.loc[test_data['key'] == key].values.tolist()[0]))

    db_connection.cursor.execute(f'DELETE FROM {table} WHERE horizon = "lt";')


def test_send_ab_results_to_db(db_connection, ab_results_test):

    data_to_send = ab_results_test
    data_to_send['fold'] = 2
    hecate_versions_params = HecateVersionsParams(project='test',
                                                  branch_a=data_to_send.branch_a[0],
                                                  commit_a_id=data_to_send.commit_a_id[0],
                                                  branch_b=data_to_send.branch_b[0],
                                                  commit_b_id=data_to_send.commit_b_id[0],
                                                  focus=data_to_send.focus[0],
                                                  fold=data_to_send.fold[0],
                                                  horizon=data_to_send.horizon[0]
                                                  )
    table = 'ab_results_test'
    db_connection.send_ab_results_to_db(data_to_send, hecate_versions_params, rewrite_ab_results=False)
    data_in_db = db_connection.select_ab_results_if_exist(hecate_versions_params, table)
    data_in_db = data_in_db[~data_in_db.key.isin(['test_key'])]
    assert data_in_db.effect_abs.sum() == data_to_send.effect_abs.sum()
    assert set(data_in_db.columns) == set(data_to_send.columns)
    assert set(data_in_db.key.unique()) == set(data_to_send.key.unique())
    for key in data_in_db.key.unique():
        assert (set(data_in_db.loc[data_in_db['key'] == key].values.tolist()[0]) ==
                set(data_to_send.loc[data_to_send['key'] == key].values.tolist()[0]))

    new_data_to_send = data_to_send.copy()
    new_data_to_send['effect_abs'] = new_data_to_send['effect_abs'] * 1.01
    with pytest.raises(ValueError):
        db_connection.send_ab_results_to_db(new_data_to_send, hecate_versions_params, rewrite_ab_results=False)

    db_connection.send_ab_results_to_db(new_data_to_send, hecate_versions_params, rewrite_ab_results=True)
    data_in_db = db_connection.select_ab_results_if_exist(hecate_versions_params, table)
    data_in_db = data_in_db[~data_in_db.key.isin(['test_key'])]
    assert data_in_db.effect_abs.sum() == new_data_to_send.effect_abs.sum()
    assert set(data_in_db.columns) == set(new_data_to_send.columns)
    assert set(data_in_db.key.unique()) == set(new_data_to_send.key.unique())
    for key in data_in_db.key.unique():
        assert (set(data_in_db.loc[data_in_db['key'] == key].values.tolist()[0]) ==
                set(new_data_to_send.loc[new_data_to_send['key'] == key].values.tolist()[0]))

    db_connection.cursor.execute(f'DELETE FROM {table} WHERE horizon = "lt";')
