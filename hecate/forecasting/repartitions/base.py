import pandas as pd

from hecate.features import TypesOfFeatures
from hecate.forecasting.horizon import Horizon


class DummyRepartitionExecutor:
    def __init__(self) -> None:
        super().__init__()
        self.horizon = Horizon.UNDEFINED

    def update_partition(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) -> None:
        pass
