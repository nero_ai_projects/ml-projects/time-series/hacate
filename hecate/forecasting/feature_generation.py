from typing import Literal, Callable

import pandas as pd
import numpy as np
from scipy import stats

from sklearn.preprocessing import MinMaxScaler
from sklearn.base import TransformerMixin
from sklearn.cluster import DBSCAN

from hecate.features import FeatureType, TypesOfFeatures
from hecate.types import TupleDataAndCols
from hecate.utils.utils import FuncAndArgs
from hecate.constants import HALF_YEAR_LEN


def add_business_features(data: pd.DataFrame,
                          feature_types: TypesOfFeatures,
                          freq: str | None) -> TupleDataAndCols:
    """
    Function add new features for business feature according
    dictionary of feature types and function of processing features.
    Dict of feature_types must have the same value with dictionary with
    function of processing features.
    Data columns must contain feature_types dict indexes.
    Format of output of all feature_func have to be: func(data: pd.DataFrame, col: str) -> (pd.DataFrame, list)

    :param data: pd.DataFrame - input dataframe
    :param feature_types: dict - dictionary of features and types <feature>: <type of feature>
    :param freq: str - string for time frequency of the time series. Possible options: 'W', 'D'. In case of other
    frequencies (like month or year), prices won't be processed

    :return: data - data with added features
    :return: list - list of added features
    """

    feature_func: dict[FeatureType, dict[str, FuncAndArgs]] = {
        FeatureType.discount: {
            'add_promo_feature': FuncAndArgs(add_promo_feature),
            'add_mark_first_promo_slot': FuncAndArgs(add_mark_first_promo_slot),
            'add_mark_last_trained_slot_and_later': FuncAndArgs(add_mark_last_trained_slot_and_later),
            'add_promo_id_longer_then': FuncAndArgs(add_promo_id_longer_then),
            'add_week_before_promo': FuncAndArgs(add_week_before_promo),
        },
        FeatureType.promo_budget: {
            'add_promo_feature': FuncAndArgs(add_promo_feature)
        },
        FeatureType.stock: {
            'add_oos': FuncAndArgs(add_oos)
        }
    }

    if freq is None:
        freq = 'D'

    price_kwargs = {
        'D': {'price_change_freq': 31, 'price_effect_dur_days': 21,
              'promo_cols': [k for k, v in feature_types.items() if v == 'discount']},
        'W': {'price_change_freq': 7, 'price_effect_dur_days': 3, 'promo_cols': list}
    }

    if freq in ['W', 'D']:
        feature_func.update({
            FeatureType.price_reg: {
                'add_reg_price_features': FuncAndArgs(
                    add_reg_price_features,
                    kwargs=price_kwargs[freq]
                )
            }
        })

    # find features columns and generate new features according dictionary
    cols = [key for key, item in feature_types.items() if item in feature_func.keys()]

    business_features = []
    for col in cols:
        for func_details in feature_func[feature_types[col]].values():
            if func_details.args and func_details.kwargs:
                data, features_cols = func_details.func(data, col, *func_details.args, **func_details.kwargs)
            elif func_details.args:
                data, features_cols = func_details.func(data, col, *func_details.args)
            elif func_details.kwargs:
                data, features_cols = func_details.func(data, col, **func_details.kwargs)
            else:
                data, features_cols = func_details.func(data, col)
            business_features += features_cols

    return data, business_features


def add_promo_feature(data: pd.DataFrame, promo_feature: str) -> TupleDataAndCols:
    """
    Function add new features for promo feature list:
    - add promo_profile
    Data columns must contain promo_feature list.

    :param data: pd.DataFrame - input dataframe
    :param promo_feature: object - column with promo features

    :return: data - data with added features
    :return: list of string - list of added features
    """

    data, col = add_profile(data, promo_feature, profile_type='weighted')

    return data, col


def add_mark_first_promo_slot(sample: pd.DataFrame, col: str, min_slot_length: int = 7) -> TupleDataAndCols:
    """
    Function mark first promo slot via binary feature 'first_promo_slot'.
    If every slot shorter min_slot_length then will be added constant 0
    (or if there is only one promo slot).

    :param col: name of col that will be used to define promo slot
    :param sample: pd.DataFrame with data
    :param min_slot_length: minimal length of slot

    :return: sample with name of added column ('{col}_first_slot').
    """
    feature = f'business-{col}_first_slot'
    train_data = sample[sample.partition == 'train'].copy()
    train_data['business-promo_period'] = train_data[col] > 0.0
    train_data['business-promo_id'] = calc_promo_id(train_data['business-promo_period'])
    train_data['business-promo_length'] = \
        train_data.groupby('business-promo_id').date.transform('count')

    slots = train_data[train_data['business-promo_period'] * train_data['business-promo_length'] >= min_slot_length]
    unique_slots_id = slots['business-promo_id'].unique()
    if unique_slots_id.size < 2:
        sample[f'business-{col}_first_slot'] = 0
        return sample, [feature]

    first_promo_id = unique_slots_id.min()
    first_promo_dates = set(slots[slots['business-promo_id'] == first_promo_id].date)
    sample[feature] = np.where(sample.date.isin(first_promo_dates), 1, 0)

    return sample, [feature]


def add_mark_last_trained_slot_and_later(sample: pd.DataFrame, col: str, min_slot_length: int = 7,
                                         splitted_not_valid: bool = True) -> TupleDataAndCols:
    """
    The function marks promo slots starting from the last one in the train.
    If there is only one promo slot in the train, the markup will not happen.

    :param sample: input dataset.
    :param col: name of column contains discounts.
    :param min_slot_lenght: minimal lenght of promo-slot. Defaults 7.
    :param splitted_not_valid: flag indicating that the last slot cannot be split among partitions.

    :return: sample and name of added column ('{col}_last_and_later_slot').
    """

    feature = f'business-{col}_last_and_later_slot'

    sample['business-promo_period'] = sample[col] > 0.0
    sample['business-promo_id'] = calc_promo_id(sample['business-promo_period'])
    sample['business-promo_length'] = sample.groupby('business-promo_id').date.transform('count')

    train_data = sample[sample.partition == 'train']

    slots = train_data.loc[
        train_data['business-promo_period'] * train_data['business-promo_length'] >= min_slot_length,
        'business-promo_id'
    ]
    sorted_slots_id = np.sort(slots.unique())

    last_train_slot_idx = sorted_slots_id.max() if sorted_slots_id.size > 0 else np.nan
    is_splitted_slot = last_train_slot_idx in sample.loc[sample.partition != "train", 'business-promo_id'].unique()
    choose_before_last = is_splitted_slot and splitted_not_valid

    if (sorted_slots_id.size < 2) or (choose_before_last and sorted_slots_id.size < 3):
        sample[feature] = 0

        return sample, [feature]

    if choose_before_last:
        last_train_slot_idx = sorted_slots_id[-2]

    sample[feature] = sample['business-promo_id'] >= last_train_slot_idx

    return sample, [feature]


def add_promo_id_longer_then(sample: pd.DataFrame, col: str, minimal_length: int = 7) -> TupleDataAndCols:
    """
    Function add column '{col}_promo_id_longer_then' that contains promo id
    if promo slot's length greater or equal minimal_length.\

    To calculate promo id function calc_promo_id is used.

    :param sample: pd.DataFrame
    :param col: name of column
    :param minimal_length: minimal length for promo slot
    :return: sample dataframe, [created column name]
    """

    promo_id_col = f'business-{col}_promo_id_longer_then'

    sample[promo_id_col] = calc_promo_id(sample[col] > 0, minimal_length)

    return sample, [promo_id_col]


def calc_promo_id(discount: np.ndarray, minimal_length: int = 1) -> np.ndarray:
    """
    Numerates promo slots according to a given discount column (sets promo id)
    may be used with a 'transform' method of groupby
    """
    promo_id_array = np.zeros(discount.size, dtype=np.uint16)
    promo_id = 0
    prev_point = 0
    id_len = 0
    is_nan_index = np.isnan(discount)
    discount[is_nan_index] = 0
    for i, point in enumerate(discount):
        if point > 0 and point == prev_point:
            id_len += 1
            promo_id_array[i] = promo_id
        elif point > 0 and point != prev_point:
            promo_id += 1
            if 0 < id_len < minimal_length:
                promo_id_array[i-id_len:i] = 0
                promo_id -= 1
            id_len = 1
            promo_id_array[i] = promo_id
        elif point == 0 and prev_point > 0:
            if 0 < id_len < minimal_length:
                promo_id_array[i-id_len:i] = 0
                promo_id -= 1
            id_len = 0
            promo_id_array[i] = 0
        elif point == 0 and prev_point == 0:
            id_len = 0
            promo_id_array[i] = 0
        else:
            raise ValueError
        prev_point = point
    if prev_point > 0 and 0 < id_len < minimal_length:
        promo_id_array[-id_len:] = 0

    return promo_id_array


def get_week_of_month(dt_series: pd.Series) -> pd.Series:
    """
    Returns number of week in a month. Takes datetime formatted series as input.
    """
    return (dt_series.dt.day - 1) // 7 + 1


def get_half_year(dt_series: pd.Series) -> np.ndarray:
    """
    Returns number of a half year.
    """
    return np.where(dt_series.dt.quarter < 3, 1, 2)


def add_time_features(data: pd.DataFrame, time_features: list) -> TupleDataAndCols:
    """
    Function add time_features that passed in time_features.
    Support features that was implemented in pd.Series.dt

    :param data: pd.DataFrame - input dataframe
    :param time_features: list of strings - list of tim features
           (names of time features must be the same with pd.Series.dt functions)

    :return: data - data with added features
    :return: time_features_cols - list of added features
    """
    if data.date.dtype != 'datetime64[ns]':
        date_series = data.date.astype("datetime64[ns]")
    else:
        date_series = data.date

    custom_funcs: dict[str, Callable] = {
        'time-half_year': get_half_year,
        'time-weekofmonth': get_week_of_month,
    }

    for col in time_features:
        if col in {'time-week'}:
            data[col] = date_series.dt.isocalendar().week
        elif col in custom_funcs:
            data[col] = custom_funcs[col](date_series)
        else:
            data[col] = getattr(date_series.dt, col.split('-', 1).pop())

    return data, time_features


def add_profile(data: pd.DataFrame, col: str, profile_type: Literal['weighted', 'mean']) -> TupleDataAndCols:
    """
    The function adds the profile (historical behaviour during periods) using agg condition.
    Added columns: '<col>_profile'.
    Data columns must contain col value.

    :param data: pd.DataFrame (required columns: 'date', 'target', <promo_feature>, 'promo_day_num' 'partition')
    :param col: business feature to make profile
    :param profile_type: 'weighted' or 'mean'

    :return: pd.DataFrame - data with added features
    :return: added columns - - list of added features
    """

    condition = {
        'weighted': lambda x: np.average(x, weights=np.linspace(1, 10, x.size)),
        'mean': lambda x: x.mean()
    }

    cols: list[str] = []

    data, col_num = add_promo_period_num(data, col)
    profile_data = data[(data[col_num] != 0) & (data['partition'] == 'train')]
    col = f'business-{col}_profile'

    if not profile_data.empty:
        profile = profile_data['target'] \
            .groupby(profile_data[col_num], sort=False, observed=True) \
            .agg(condition[profile_type])
        profile.name = col
        data = data.set_index(col_num).join(profile, on=col_num).reset_index()
        data[col].fillna(0, inplace=True)
        cols += [col]

    data.drop([col_num], axis=1, inplace=True)
    return data, cols


def calc_promo_day(discount: np.ndarray) -> np.ndarray:
    """
    Numerates promo days of the promo slots for a given discount column
    may be used with a 'transform' method of groupby

    :param discount: the list of discounts in the dataset
    :return: the list of promo day
    """
    promo_day_feature = np.empty(discount.size, dtype=np.uint16)
    promo_day = 0
    prev_discount = None
    for i, element in enumerate(discount):
        if element == 0:
            promo_day_feature[i] = 0
            promo_day = 0
        else:
            if prev_discount != element:
                promo_day = 0
            promo_day += 1
            promo_day_feature[i] = promo_day
        prev_discount = element
    return promo_day_feature


def add_promo_period_num(data: pd.DataFrame, col: str) -> tuple[pd.DataFrame, str]:
    """
    The function transforms the information about promo in data frame with number of period promo.
    Added columns: '<col>_num'
    Data columns must contain col value.

    :param data: pd.DataFrame (required columns: 'data', <promo_feature>)
    :param col: string, name of promo feature column

    :return: pd.DataFrame - data with added column
    :return: object - name of added column
    """
    col_num = f'business-{col}_num'
    data[col_num] = calc_promo_day(data[col].to_numpy())

    return data, col_num


def add_oos(data: pd.DataFrame, col: str) -> TupleDataAndCols:
    """
    The function transforms the information about stocks in out of stocks data.
    If 'stock' == 0 and 'target' == 0 --> 'oos' = 1
    Added columns: 'oos'
    Data columns must contain col value.

    :param data: pd.DataFrame (required columns feature type: 'stock')
    :param col: string, name of stock feature column

    :return: pd.DataFrame - data with added column
    :return: list - list with name of added column
    """

    new_column = f'{col}-oos'
    data[new_column] = np.where((data[col] == 0) & (data['target'] == 0) & (data['partition'] == 'train'), 1, 0)

    return data, [new_column]


def add_week_before_promo(sample: pd.DataFrame, col: str, minimal_length: int = 7) -> TupleDataAndCols:
    """
    The function add mark about week (7 days) before promo slots.
    To define promo slots function add_promo_id_longer_then used.

    :param sample: pd.DataFrame
    :param col: column name
    :param minimal_length: minimal_length for add_promo_id_longer_then function

    :return: sample dataframe, [created column name]
    """

    feature_col = f'business-{col}_week_before_promo'
    promo_id_col: str | list[str] = f'business-{col}_promo_id_longer_then'
    if promo_id_col not in sample.columns:
        sample, promo_id_col = add_promo_id_longer_then(sample, col, minimal_length=minimal_length)

    sample['__col_id'] = sample[promo_id_col] > 0
    sample['__shifted_col_id'] = sample['__col_id'].shift(-7, fill_value=False)

    sample[feature_col] = sample['__shifted_col_id'] > sample['__col_id']

    del sample['__shifted_col_id']
    del sample['__col_id']

    return sample, [feature_col, ]


def remove_price_outliers(prices_val: pd.Series, scl: None | TransformerMixin = None,
                          prices_len_th: int = HALF_YEAR_LEN, eps: float = 0.1, min_samples: int = 4) -> pd.Series:
    """
    Remove price outliers, using DBSCAN, replace with previous values.

    :param prices_val: series with price data
    :param scl: class - Scaler (minmax by default)
    :param prices_len_th: minimum length of prices, required for the removal of price outliers. If actual length is
    less, returns input prices_val
    :param eps: float - DBSCAN parameter (0.1 by default)
    :param min_samples: int - DBSCAN parameter (4 by default)

    :return: series with corrected price data
    """
    if prices_val.notna().sum() <= prices_len_th:
        return prices_val

    if scl is None:
        scl = MinMaxScaler()

    ids = np.arange(prices_val.size)
    model = DBSCAN(eps=eps, min_samples=min_samples).fit(
        scl.fit_transform(np.vstack((prices_val.to_numpy(), ids)).T))
    prices_val_new = pd.Series(
        np.where(model.labels_ == -1, np.nan, prices_val),
        index=prices_val.index
    ).ffill().bfill()

    return prices_val_new


def cut_potential_pincs(column: pd.Series, window: int) -> pd.Series:
    """
    The function finds the periods of price changes and replaces them with empty values.

    :param column: input column
    :param window: window size to find the periods of price changes

    :return: column with nan values in potential price change periods
    """

    asc_max_col = column.rolling(window).max().bfill()
    desc_max_col = column.shift(-window).ffill().rolling(window).max().bfill()
    return asc_max_col.where(asc_max_col == desc_max_col)


def approximate_by_step_function(column: pd.Series, window: int, change_threshold: float = 0.8) -> np.ndarray:
    """
    Search for potential price changes and their approximation by a step function.

    :param column: the column containing prices
    :param window: window size to find the periods of price changes. Made after outliers removal in order to get rid of
    frequent variations
    :param change_threshold: price change that determines the position of the step

    :return: no return, input dataframe is changed
    """

    price_col_with_nans = cut_potential_pincs(column, window)

    ascending_bound = price_col_with_nans.ffill().bfill().to_numpy()
    descending_bound = price_col_with_nans.bfill().ffill().to_numpy()

    lower_bound = np.minimum(ascending_bound, descending_bound)
    higher_bound = np.maximum(ascending_bound, descending_bound)

    threshold = lower_bound + change_threshold * (higher_bound - lower_bound)
    return np.where(column.to_numpy() >= threshold, higher_bound, lower_bound)


def smooth_values(df: pd.DataFrame, col: str, change_freq: int):
    """
    Changes the initial values in a passed col. Includes the following transformations:
    1) removing outliers using DBSCAN
    2) removing frequent variations
    3) removing minor variations
    4) removing changes, if the period between changes too short (based on change_freq param)

    :param df: pd.DataFrame
    :param col: string, name of a column to be changed
    :param change_freq: int - minimal frequency of changes. If in initial dataframe changes are more
    frequent, they are removed

    :return: no return, the initial dataframe is transformed. Column values are changed.
    """
    change_col_name = f'{col}_change'

    # prep df
    df[col] = df[col].round(2)
    df[col] = df[col].mask(df[col] == 0).ffill().bfill()
    df[col] = remove_price_outliers(df[col])

    # compare max price on the left and on the right, if equal - take it
    window_for_max = change_freq // 2
    df[col] = approximate_by_step_function(df[col], window_for_max)
    df[change_col_name] = add_fractional_changes_of_col(df[col])

    # mark first and last period
    first_or_last_flag_col: pd.Series[bool] = (df.date == df.date.min()) | (df.date == df.date.max())
    change_col_cumsum: pd.Series[float] = df[change_col_name].cumsum()
    first_or_last_flag_col: pd.Series[bool] = first_or_last_flag_col.groupby(change_col_cumsum).transform('max')
    first_or_last_flag_col = first_or_last_flag_col.fillna(False)

    # check pricing periods to be long enough (not less than change_freq) and correct initial price
    period_count_col_total = df['date'].groupby(change_col_cumsum).transform('count')
    df[col].mask((period_count_col_total < change_freq) & ~first_or_last_flag_col, inplace=True)
    df[col] = approximate_by_step_function(df[col], window_for_max)

    # recalc price change after changing initial price
    df[change_col_name] = add_fractional_changes_of_col(df[col])
    df[col] = df[col].groupby(df[change_col_name].cumsum()) \
        .transform(lambda x: np.repeat(stats.mode(x.to_numpy(), keepdims=False).mode, x.size))
    df[col].fillna(0, inplace=True)
    df.drop([change_col_name], axis=1, inplace=True)


def add_fractional_changes_of_col(column: pd.Series, min_change: float = 0.01) -> pd.Series:
    """
    Add a column change and period num to the input dataframe.

    :param df: pd.DataFrame
    :param col: string, name of a column to calc changes for
    :param min_change: value of a change to be considered insignificant and removed

    :return: no return, changes applied to input dataframe
    """

    change_col_name = column.pct_change(fill_value=column.iloc[0])
    too_small_change = change_col_name.abs() < min_change

    return change_col_name.mask(too_small_change, 0)


def get_bound_indexes_of_n_points_after_pinc(price_array: np.ndarray, n_points: int) -> list[tuple[int, int]]:
    """
    The function helps to find areas (indexes) after changing prices of a given length from the input array of prices.
    Borders are included.

    :param price_array:an array of prices sorted by date
    :param n_points: how many points should take after the price change
    :return: List of index tuples (left, right) for each price change
    """
    bounds = []
    len_const_price = 0
    prev_item = price_array[0]
    left_bound = 0
    right_bound = 0
    max_i = price_array.size - 1
    before_first_pinc = True
    for i, item in enumerate(price_array):
        if item != prev_item:
            if i != 0 and not before_first_pinc:
                if right_bound < left_bound:
                    right_bound = i - 1
                bounds.append((left_bound, right_bound))
            left_bound = i
            len_const_price = 1
            before_first_pinc = False
        else:
            len_const_price += 1

        if (len_const_price == n_points or (len_const_price < n_points and i == max_i)) and not before_first_pinc:
            right_bound = i

        prev_item = item

    if not before_first_pinc:
        bounds.append((left_bound, right_bound))
    return bounds


def is_pinc_produce_target_changes(df: pd.DataFrame, col: str, price_effect_dur_days: int,
                                   promo_cols: list[str] | None = None,
                                   p_threshold: float = 0.1, threshold: float = 0.7) -> bool:

    train = df[df.partition == 'train']
    if promo_cols is None:
        promo_cols = list()

    is_promo = (train[promo_cols] != 0).sum(axis=1)
    target = np.where(is_promo, np.nan, train.target)
    prices = train[col].to_numpy()

    region_after_pinc = get_bound_indexes_of_n_points_after_pinc(prices, price_effect_dur_days)
    region_before_pinc = get_bound_indexes_of_n_points_after_pinc(prices[::-1], price_effect_dur_days)[::-1]

    # calculate p-value of the t-test
    p_vals = []
    for (ll, lr), (rr, rl) in zip(region_before_pinc, region_after_pinc):
        rl, rr = prices.size - rl, prices.size - rr
        target_1 = target[ll:lr+1]
        target_2 = target[rl:rr+1]
        if (np.nanstd(target_1) > 0.001) and (np.nanstd(target_2) > 0.001):
            p_vals.append(stats.ttest_ind(target_1, target_2, nan_policy='omit').pvalue)

    total_cases = len([p_val for p_val in p_vals if not pd.isna(p_val)])
    if total_cases:
        r = len([p_val for p_val in p_vals if p_val < p_threshold]) / total_cases
        return r > threshold

    return False


def check_if_to_add_prices(df: pd.DataFrame, col: str, price_effect_dur_days: int,
                           promo_cols: list[str] | None = None,
                           pc_train_count: int = 2, pc_fc_count: int = 1, mean_target_th: int = 10,
                           zero_periods_th: float = 0.3):
    """
    Checks if to add prices to the initial dataframe. Firstly, performs checks on price changes count in, mean target
    value and zero periods duration. Secondly, checks the difference of mean target value before and after price
    changes. Returns True, if all checks passed.

    :param df: pd.DataFrame with initial data
    :param col: string, name of regular price column
    :param price_effect_dur_days: int for a duration of price increase effect
    :param promo_cols: list of promo columns to help identify promo periods
    :param pc_train_count: count of price changes in train partition
    :param pc_fc_count: count of price changes in fc partition
    :param mean_target_th: minimum mean target sales
    :param zero_periods_th: threshold of the sum duration of zero sales periods

    :return: boolean, True, if all filters passed, False otherwise
    """
    change_col_name = f'{col}_change'

    df[change_col_name] = add_fractional_changes_of_col(df[col])

    is_train = df.partition == 'train'
    is_fc = df.partition == 'fc'
    pc_period_sbs = df[f'{col}_change'] != 0

    # price changes count not less than a constant both in train and fc
    train_pc_cond = (pc_period_sbs & is_train).sum() >= pc_train_count
    fc_pc_cond = (pc_period_sbs & is_fc).sum() >= pc_fc_count

    train_target = df.loc[is_train, 'target']

    # mean target is above a constant
    low_sales_cond = train_target.mean() >= mean_target_th

    # sum duration of zero sales periods takes less than a constant
    target_is_null = train_target == 0

    is_second_zero = target_is_null & target_is_null.shift(1, fill_value=False)
    two_or_more_zeros = is_second_zero | is_second_zero.shift(-1, fill_value=False)
    train_target_cond = two_or_more_zeros.sum() / train_target.size <= zero_periods_th

    if all((train_pc_cond, fc_pc_cond, low_sales_cond, train_target_cond)):
        return is_pinc_produce_target_changes(df, col, price_effect_dur_days, promo_cols)

    return False


def num_periods(df, col: str, price_effect_dur_days: int, fd: int = 2, conf: float = 0.01) -> None:
    """
    Function adds two columns with numeric values to the periods before and after the price increase. Numeric values
    are t-distributed, and have a maximum same as the price increase.

    :param df: initial dataframe
    :param col: string, name of regular price column
    :param price_effect_dur_days: length of the price increase effect. t-distribution would be applied across this
    number of periods.
    :param fd: degrees of freedom, a parameter affecting the shape of t-distribution
    :param conf: a parameter affecting the shape of t-distribution

    :return: no return. The initial dataframe is changed.
    """
    change_col = f'{col}_change'
    num_before = f'business-{col}_price_period_num_before'
    num_after = f'business-{col}_price_period_num_after'

    # add price change
    df[change_col] = add_fractional_changes_of_col(df[col])
    changes_cumsum = df[change_col].cumsum()
    # numerating forwards
    df[num_after] = df[change_col].groupby(changes_cumsum).cumcount()

    # numerating backwards
    df[num_before] = (-1) * (df[change_col].groupby(changes_cumsum).transform('count') - df[num_after])
    df[num_before] = np.where(df[change_col] != 0, 0, df[num_before])

    # restricting values
    df[num_after] = np.where(df[num_after] < price_effect_dur_days, df[num_after], price_effect_dur_days)
    df[num_before] = np.where(df[num_before] >= - price_effect_dur_days, df[num_before], price_effect_dur_days)

    # remake price_period_num into a t-distribution function
    # to account for non-linear effect for possible model change in future (not tree-based)
    pdf = np.linspace(stats.t.ppf(conf, fd), stats.t.ppf(1 - conf, fd), 2 * price_effect_dur_days + 1)
    mapping_val = {k - price_effect_dur_days: stats.t.pdf(pdf[k], fd) for k in range(2 * price_effect_dur_days + 1)}
    for column in [num_before, num_after]:
        df[column] = df[column].map(mapping_val)

    # add correction
    correct_max_val = df[change_col].mask(df[change_col] == 0)
    df[num_before] *= correct_max_val.bfill(limit=price_effect_dur_days).fillna(0)
    df[num_after] *= correct_max_val.ffill(limit=price_effect_dur_days).fillna(0)

    # drop obsolete columns
    df.drop(columns=[change_col], inplace=True)


def add_reg_price_features(df: pd.DataFrame, col: str, promo_cols: list[str] | None, price_change_freq: int,
                           price_effect_dur_days: int) -> TupleDataAndCols:
    """
    Corrects regular prices by removing outliers, then, in case preliminary checks are passed, adds regular price
    features, including price change and price period number. In case checking conditions are not satisfied, return
    the initial dataframe with corrected prices.
    NB! When doing calculations, the price is rounded down to 2 digits after the decimal point. Keep in mind, that the
    initial prices might require scaling.

    :param df: pd.DataFrame
    :param col: string, name of regular price column
    :param promo_cols: list of promo columns to help identify promo periods
    :param price_change_freq: int - minimal frequency of price changes. If in initial dataframe price changes more
    frequently, price is replaced with the latest price value over the period
    :param price_effect_dur_days: length of the price increase effect. t-distribution would be applied across this
    number of periods.

    :return: dataframe, [created column names]
    """
    smooth_values(df, col, price_change_freq)

    if check_if_to_add_prices(df, col, price_effect_dur_days, promo_cols):
        num_periods(df, col, price_effect_dur_days)
        return df, [f'business-{col}_price_period_num_before', f'business-{col}_price_period_num_after']

    return df, []
