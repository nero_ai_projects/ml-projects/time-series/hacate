version: "3.9"
services:
  web:
    build:
      context: .
      dockerfile: Dockerfile
      args:
        var_name: ${API_KEY}
        HEALTHCHECK_API_KEY: ${HEALTHCHECK_API_KEY}
    environment:
      - CELERY_BROKER_URL=redis://redis:6379/0
      - CELERY_RESULT_BACKEND=redis://redis:6379/0
      - CELERY_SERIALIZER=pickle
    command: ["uvicorn", "service.app:app", "--host", "0.0.0.0", "--port", "80", "--no-server-header"]
    env_file:
      - .env
    ports:
      - "42080:80"
    expose:
      - 80
    volumes:
      - ./logs:/app/logs
    networks:
      - hecnet
    restart: always
    healthcheck:
      test: ["CMD", "./healthcheck.sh"]
      interval: 1m
      timeout: 10s
      retries: 2

  redis:
    image: redis:6-alpine
    expose:
      - 6379
    networks:
      - hecnet

  worker:
    build: .
    command: ['celery', '--app=service.worker.celery_worker', 'worker', '-l', 'info']
    environment:
      - CELERY_BROKER_URL=redis://redis:6379/0
      - CELERY_RESULT_BACKEND=redis://redis:6379/0
      - CELERY_WORKER_SEND_TASK_EVENTS=True
      - CELERY_TASK_SEND_SENT_EVENT=True
    depends_on:
      - redis
      - web
    networks:
      - hecnet
    expose:
      - 6379

  loki:
    image: grafana/loki:2.8.0
    ports:
      - "3100:3100"
    expose:
      - 3100
    volumes:
      - ./configs/loki:/etc/loki
    command: -config.file=/etc/loki/loki-cfg.yaml
    networks:
      - loki

  promtail:
    image: grafana/promtail:2.8.0
    volumes:
      - /var/log:/var/log
      - ./logs:/var/log/hecate
      - ./configs/promtail:/etc/promtail
    command: -config.file=/etc/promtail/promtail-cfg.yaml
    networks:
      - loki

  grafana:
    env_file:
      - .env
    entrypoint:
      - sh
      - -euc
      - |
        mkdir -p /etc/grafana/provisioning/datasources
        cat <<EOF > /etc/grafana/provisioning/datasources/ds.yaml
        apiVersion: 1
        datasources:
        - name: Loki
          type: loki
          access: proxy
          orgId: 1
          url: http://loki:3100
          basicAuth: false
          isDefault: true
          version: 1
          editable: false
        EOF
        /run.sh
    image: grafana/grafana:latest
    ports:
      - "3000:3000"
    networks:
      - loki

networks:
  hecnet:
    driver: bridge
  loki:
