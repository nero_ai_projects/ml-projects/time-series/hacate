import json
import pandas as pd
from os import cpu_count
from joblib import Parallel, delayed

from hecate.log import get_logger
from hecate.features import TypesOfFeatures
from hecate.types import Decompositors, Drivers
import hecate.cannibalization.cannibalization as cannibalization
from hecate.forecasting.decompositions.decomposition import get_decomposition
from hecate.forecasting.decompositions.strategies import DecompositionStrategies
from hecate.forecasting.skeleton import FCSampleResult, ForecastingTask, ForecastingTaskConfig
from hecate.prediction_interval import calculate_prediction_interval, check_pi_conditions

from hecate.utils.decorators import (
    log_in_out_shape_wrapper,
    log_incoming_df_info,
    act_decompose_wrapper,
    extend_fc_sample_result,
    log_running_mode_wrapper,
    check_datatypes,
)

logger = get_logger('hecate')


@log_running_mode_wrapper
@log_in_out_shape_wrapper
@log_incoming_df_info
@act_decompose_wrapper
def fc_batch(
        data: pd.DataFrame,
        feature_types: TypesOfFeatures,
        decompose: Decompositors = 'all',
        dsd: DecompositionStrategies | None = None,
        config: ForecastingTaskConfig | None = None,
        add_columns: list[str] | None = None,
        parallel: bool = False,
        decimals: int | None = 2,
        prediction_interval: bool = False,
        pi_prob_list: list[float] | None = None,
        pi_method: str = 'statistical',
        pi_stat_mode: str = 'both',
        apply_check_pi_conditions: bool = True
) -> pd.DataFrame:

    """
    The function make prediction for a set of key in the one dataframe. The logic are same as for fc() function.

    :param data: pd.DataFrame
    :param feature_types: dict
    :param decompose: parameter for managing drivers to be calculated and returned in an output. Either a string ('all'
    or 'none'), or a list containing forecast drivers
    :param dsd: short for Decomposition Strategy Dictionary: dict for methods to use to calculate various figures
    :param config: instance of ForecastingTaskConfig to configurate fc task execution
    :param add_columns: additional columns to return in result
    :param parallel: if True joblib will be used for parallelization.
    :param decimals: number of decimal places for output data. Without rounding - None
    :param prediction_interval: if True, calculates prediction interval(s) for the forecast
    :param pi_prob_list: aplicable when prediction_interval is True; gives a list of probabilities for the prediction
                         interval(s). If None, the default value is 0.68.
    :param pi_method: aplicable when prediction_interval is True; the method for prediction interval(s) calculation;
                      can take values: 'statistical'.
    :param pi_stat_mode: aplicable when pi_method is 'statistical'; defines the way of of PI calculation, when 'norm
                         it's based on normal distribution, 'hdi' uses corresponding quantiles of real data, and
                         'both' yields the average of the two.
    :param apply_check_pi_conditions: If True, allows check that input params fit for prediction interval calculations

    :return: data

    """

    # TODO: check if we have at least one valid sample here and log others

    if prediction_interval:
        if check_pi_conditions(data, pi_method, pi_prob_list):
            apply_check_pi_conditions = False
        else:
            msg = "Cannot calculate prediction interval(s)."
            logger.error(msg)
            raise ValueError(msg)

    drivers = get_decomposition(decompose)
    logger.info(f'decompose {json.dumps(decompose)} produce drivers: {json.dumps(get_decomposition(decompose))}')

    if 'cn' in drivers:
        data, feature_types = cannibalization.add_cannibalization_as_feature(data, feature_types)

    outcomes = []
    if parallel:
        tasks = ({
            'data': sample, 'feature_types': feature_types, 'drivers': drivers, 'dsd': dsd, 'config': config,
            'add_columns': add_columns, 'decimals': decimals, 'prediction_interval': prediction_interval,
            'pi_prob_list': pi_prob_list, 'pi_method': pi_method, 'pi_stat_mode': pi_stat_mode,
            'apply_check_pi_conditions': apply_check_pi_conditions
        }
                 for _, sample in data.groupby('key')
                 if set(sample.partition.unique()).intersection({'fc', 'train'}) == {'fc', 'train'})

        n_jobs = cpu_count()
        if n_jobs == 1:
            logger.warning('n_jobs was defined as 1')

        res = Parallel(verbose=1, n_jobs=n_jobs)(delayed(fc_sample)(**task) for task in tasks)
        # TODO: log errors happened during fc_sample execution
        outcomes = [answer.data for answer in res] if res else []

    else:

        for key, sample in data.groupby('key', observed=True):

            if set(sample.partition.unique()).intersection({'fc', 'train'}) != {'fc', 'train'}:
                logger.warning(f'sample {key} missing fc or train partition')
                continue

            try:
                sample_answer = fc_sample(
                    sample, feature_types, drivers=drivers, dsd=dsd, config=config, add_columns=add_columns,
                    decimals=decimals, prediction_interval=prediction_interval, pi_prob_list=pi_prob_list,
                    pi_method=pi_method, pi_stat_mode=pi_stat_mode, apply_check_pi_conditions=apply_check_pi_conditions
                )
                outcomes.append(sample_answer.data)
            except Exception:
                logger.exception(f'sample {key} produce exception')

    if not outcomes:
        msg = 'every sample in batch was failed'
        logger.critical(msg)
        raise ValueError(msg)

    result = pd.concat(outcomes, ignore_index=True)

    return result


@check_datatypes
@extend_fc_sample_result
def fc_sample(
        data: pd.DataFrame,
        feature_types: TypesOfFeatures,
        drivers: Drivers | None = None,
        dsd: DecompositionStrategies | None = None,
        config: ForecastingTaskConfig | None = None,
        add_columns: list[str] | None = None,
        decimals: int | None = 2,
        prediction_interval: bool = False,
        pi_prob_list: list[float] | None = None,
        pi_method: str = 'statistical',
        pi_stat_mode: str = 'both',
        apply_check_pi_conditions: bool = True
) -> FCSampleResult:
    """
    Make transformation features and forecasting of sample.

    :param data: pd.DataFrame
    :param feature_types: dict
    :param drivers: a list containing forecast drivers to be calculated and returned in an
    output dataframe (possible list elements: 'bl', 'cn'). Default value is None (all drivers are calculated).
    :param dsd: short for Decomposition Strategy Dictionary: dict for methods to use to calculate various figures
    :param config: instance of ForecastingTaskConfig to configurate fc task execution
    :param add_columns: additional columns to return in result
    :param decimals: number of decimal places for output data. Without rounding - None
    :param prediction_interval: if True, calculates prediction interval(s) for the forecast
    :param pi_prob_list: aplicable when prediction_interval is True; gives a list of probabilities for the prediction
                         interval(s). If None, the default value is 0.68.
    :param pi_method: aplicable when prediction_interval is True; the method for prediction interval(s) calculation;
                      can take values: 'statistical'.
    :param pi_stat_mode: aplicable when pi_method is 'statistical'; defines the way of of PI calculation, when 'norm'
                         it's based on normal distribution, 'hdi' uses corresponding quantiles of real data, and
                         'both' yields the average of the two.
    :param apply_check_pi_conditions: If True, allows check that input params fit for prediction interval calculations

    :return: FCSampleResult that contains output data and meta information
    """

    fc_task = ForecastingTask(config=config)
    fc_result = fc_task.execute(data=data.copy(), feature_types=feature_types, drivers=drivers, dsd=dsd,
                                add_columns=add_columns, decimals=decimals)

    if prediction_interval:
        fc_result = calculate_prediction_interval(
            fc_task=ForecastingTask(config=config),
            fc_result=fc_result,
            data=data,
            feature_types=feature_types,
            drivers=drivers,
            dsd=dsd,
            decimals=decimals,
            pi_prob_list=pi_prob_list,
            pi_method=pi_method,
            pi_stat_mode=pi_stat_mode,
            apply_check_pi_conditions=apply_check_pi_conditions
        )

    return fc_result
