import pytest


@pytest.fixture()
def promo_details():
    return {'d1': {'disc': 10, 'dur': 7, 'interval': 45},
            'd2': {'disc': 30, 'dur': 14, 'interval': 180}}


@pytest.fixture()
def price_details():
    return {'p1': {'start_value': 100, 'interval': 180},
            'p2': {'start_value': 10, 'interval': 180}}
