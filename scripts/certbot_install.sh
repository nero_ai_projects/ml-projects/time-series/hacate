sudo apt remove certbot -y
sudo snap install --classic certbot -y
sudo ln -s /snap/bin/certbot /usr/bin/certbot
sudo certbot certonly -d "${DNS_ENV}" -m "${EMAIL}" --standalone --agree-tos
sudo certbot renew --dry-run
sudo ls /etc/letsencrypt/live/${DNS_ENV}/fullchain.pem
sudo ls /etc/letsencrypt/live/${DNS_ENV}/privkey.pem
