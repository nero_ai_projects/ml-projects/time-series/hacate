import pandas as pd
import numpy as np
import pytest

import hecate.forecasting.feature_selection.selection as selection
from hecate.features import FeatureType
from tools.generate_time_series import TSGenerator
from hecate.forecasting.feature_generation import add_time_features
from hecate.forecasting.feature_selection.selection import drop_non_looped_time_features


def create_template_for_test_df(time_features: list[str] | None = None, one_year: bool = False) -> pd.DataFrame:
    if time_features is None:
        time_features = ['time-year', 'time-month', 'time-week', 'time-dayofweek',
                         'time-weekofmonth', 'time-quarter']
    data = pd.DataFrame({'key': ['key1']*1195,
                         'date': pd.date_range(start="2021-09-09", periods=1195),
                         'partition': ['train']*1095 + ['fc']*100,
                         'target': [100]*500 + [120]*695,
                         })
    data, _ = add_time_features(data, time_features)
    if one_year:
        data = data.iloc[-464:]
    return data


def test_drop_constant_columns(hecate_input_df):

    data, feature_types, result = hecate_input_df
    feature_cols = [key for key, item in feature_types.items() if item not in [FeatureType.dim]]

    data_output, constant_cols = selection.drop_constant_columns(data.copy(), feature_cols)

    # condition for len of input and output dataframe
    assert len(data) == len(data_output)
    # condition for deleted features
    for col in constant_cols:
        assert data[col].nunique() == 1
    for col in [col for col in feature_cols if col not in constant_cols]:
        assert data[col].nunique() > 1


def test_drop_business_features():

    func = selection.drop_business_features

    df = TSGenerator(formula='2', bl_start_value=100, trend_angle=0.02).get('hc')

    # Test 1 (no changes in the original dataset)
    df_test = df.copy()
    df_output, _ = func(df_test.copy(), [])

    assert df_test.columns.to_list() == df_output.columns.to_list(), \
        "The function should not change the columns!"
    assert df_test.shape[0] == df_output.shape[0], \
        "The number of rows has changed!"
    assert all(df_test.dtypes == df_output.dtypes), \
        "Column types have changed!"
    assert all((df_test == df_output).all()), \
        "Cell values have changed!"

    # Test 2 (partition is not used except 'train')
    df_test = df.copy()
    df_test['feature_1'] = np.where(df.partition == 'train', df.target, -np.inf)
    _, features_to_drop = func(df_test.copy(), ['feature_1'])
    assert 'feature_1' in features_to_drop


def test_drop_high_corr_features():

    func = selection.drop_high_corr_features

    df = TSGenerator(formula='2', bl_start_value=100, trend_angle=0.02).get('hc') \
        .query("partition == 'train'")

    # Test 1.1 (deleting a copy of the target)
    df_test = df.copy()
    df_test['target_copy'] = df_test.target.copy()
    features_to_drop = func(df_test.copy(), ['target_copy'])
    assert "target_copy" in features_to_drop, \
        "A copy of the target was not deleted!"

    # Test 1.2 (deleting a reverse copy of the target)
    df_test = df.copy()
    df_test['target_copy'] = (-1) * df_test.target.copy()
    features_to_drop = func(df_test.copy(), ['target_copy'])
    assert "target_copy" in features_to_drop, \
        "The negative copy of the target was not deleted!"

    # Test 1.3 (deleting two copies of a target)
    df_test = df.copy()
    df_test['target_copy'] = df_test.target.copy()
    df_test['target_another_copy'] = df_test.target.copy()
    features_to_drop = func(df_test.copy(), ['target_copy', 'target_another_copy'])
    assert set(features_to_drop) == {'target_copy', 'target_another_copy'}, \
        "Both copies of the target were not deleted!"

    # Test 2.1 (removing one of two identical features)
    df_test = df.copy()
    df_test['feature_1'] = np.log(np.power(np.arange(df_test.shape[0]) + 1, 2))
    df_test['feature_2'] = df_test['feature_1'].copy()
    features_to_drop = func(df_test.copy(), ['feature_1', 'feature_2'])
    assert len({'feature_1', 'feature_2'}.intersection(features_to_drop)) == 1, \
        "One of the two identical features was not deleted!"

    # Test 2.2 (removing two of three identical features)
    df_test = df.copy()
    df_test['feature_1'] = np.log(np.power(np.arange(df_test.shape[0]) + 1, 2))
    df_test['feature_2'] = df_test['feature_1'].copy()
    df_test['feature_3'] = df_test['feature_1'].copy()
    features_to_drop = func(df_test.copy(), ['feature_1', 'feature_2', 'feature_3'])
    assert len({'feature_1', 'feature_2', 'feature_3'}.intersection(features_to_drop)) == 2, \
        "Two of the three identical features were not removed!"

    # Test 2.3 (ignoring two random features)
    df_test = df.copy()
    df_test['feature_1'] = np.random.random(df_test.shape[0])
    df_test['feature_2'] = np.random.random(df_test.shape[0])
    features_to_drop = func(df_test.copy(), ['feature_1', 'feature_2'])
    assert 'feature_1' not in features_to_drop and 'feature_2' not in features_to_drop, \
        "A random feature has been removed!"

    # Test 2.4 (removing one of two identical (random) features)
    df_test = df.copy()
    df_test['feature_1'] = np.random.random(df_test.shape[0])
    df_test['feature_2'] = df_test['feature_1'].copy()
    features_to_drop = func(df_test.copy(), ['feature_1', 'feature_2'])
    assert len({'feature_1', 'feature_2'}.intersection(features_to_drop)) == 1, \
        "One of the two identical features was not deleted!"

    # Test 3.1 (ignoring three constant features)
    df_test = df.copy()
    df_test['feature_1'] = 1
    df_test['feature_2'] = -30
    df_test['feature_3'] = True
    features_to_drop = func(df_test.copy(), ['feature_1', 'feature_2', 'feature_3'])
    assert {'feature_1', 'feature_2', 'feature_3'}.intersection(features_to_drop) == set(), \
        "Constant features were not ignored!"

    # Test 3.2 (ignoring features from NaN and inf)
    df_test = df.copy()
    df_test['feature_1'] = np.nan
    df_test['feature_2'] = np.inf
    df_test['feature_3'] = -np.inf
    features_to_drop = func(df_test.copy(), ['feature_1', 'feature_2', 'feature_3'])
    assert {'feature_1', 'feature_2', 'feature_3'}.intersection(features_to_drop) == set(), \
        "Features containing only NaN and inf were not ignored!"

    # Test 4.1 (launch without features)
    df_test = df.copy()
    features_to_drop = func(df_test.copy(), [])
    assert features_to_drop == [], \
        "The function should return an empty list if there are no features!"

    # Test 6.1 (ignoring categorical (str) features)
    df_test = df.copy()
    df_test['feature_cat'] = 'bla'
    features_to_drop = func(df_test.copy(), ['feature_cat'])
    assert 'feature_cat' not in features_to_drop, \
        "The string feature was not ignored!"

    # Test 7.1 (deleting a duplicate target when corr_rate=1)
    df_test = df.copy()
    df_test['target_copy'] = df_test.target.copy()
    features_to_drop = func(df_test.copy(), ['target_copy'], corr_rate=1)
    assert "target_copy" in features_to_drop, \
        "When corr_rate=1, duplicate of target should be removed!"

    # Test 7.2 (removing duplicate features when corr_rate=1)
    df_test = df.copy()
    df_test['feature_1'] = np.random.random(df_test.shape[0])
    df_test['feature_2'] = df_test['feature_1'].copy()
    features_to_drop = func(df_test.copy(), ['feature_1', 'feature_2'], corr_rate=1)
    assert len({'feature_1', 'feature_2'}.intersection(features_to_drop)) == 1, \
        "When corr_rate=1, duplicate features should be removed!"


def test_drop_features(hecate_input_df):

    data, feature_types, result = hecate_input_df
    data['date'] = pd.to_datetime(data['date'])
    business_features = [key for key, item in feature_types.items() if item not in [FeatureType.dim]]
    time_features = ['year', 'month', 'week', 'dayofweek']
    cols = selection.DefaultFeatureSelector().drop_features(data.copy(), business_features, time_features)

    # condition for features
    for col in cols:
        assert col in business_features + time_features


test_data_drop_non_looped_time_features_synthetic_data = [
        (
            {},
            ['time-year', 'time-month', 'time-week', 'time-dayofweek', 'time-weekofmonth', 'time-quarter'],
            [],
            "Test on full data",
        ),
        (
            {'one_year': True},
            ['time-year', 'time-month', 'time-week', 'time-dayofweek', 'time-weekofmonth', 'time-quarter'],
            ['time-week', 'time-month'],
            "Test on short data",
        ),
        (
            {'time_features': ['time-month', 'time-week', 'time-dayofweek', 'time-weekofmonth', 'time-quarter'],
             'one_year': True},
            ['time-month', 'time-week', 'time-dayofweek', 'time-weekofmonth', 'time-quarter'],
            ['time-week', 'time-month'],
            "Test on short data without time-year feature",
        ),
        (
            {'time_features': ['time-year', 'time-week', 'time-dayofweek', 'time-weekofmonth', 'time-quarter'],
             'one_year': True},
            ['time-year', 'time-week', 'time-dayofweek', 'time-weekofmonth', 'time-quarter'],
            ['time-week'],
            "Test on short data without time-month feature",
        ),
]


@pytest.mark.parametrize("""data_variable_dict, features, to_drop_answer, error_msg""",
                         test_data_drop_non_looped_time_features_synthetic_data)
def test_drop_non_looped_time_features_synthetic_data(data_variable_dict: dict, features: list, to_drop_answer: list,
                                                      error_msg: str):

    data = create_template_for_test_df(**data_variable_dict)

    df_output, to_drop = drop_non_looped_time_features(data, features)

    assert data.columns.to_list() == df_output.columns.to_list(), \
        "The function should not change the columns!"
    assert data.shape[0] == df_output.shape[0], \
        "The number of rows has changed!"
    assert all(data.dtypes == df_output.dtypes), \
        "Column types have changed!"
    assert all((data == df_output).all()), \
        "Cell values have changed!"
    assert set(to_drop) == set(to_drop_answer), error_msg
