import pandas as pd
import numpy as np
from typing import Literal

from hecate.metrics.custom import fa, fb
from metriclog.types import HecateVersionsParams


def get_metrics_dict(df: pd.DataFrame) -> dict[Literal["FA", "FB"], float]:
    """
    Calculate FA and FB metrics and return a dictionary.

    :param df: input dataframe to calculate metrics for

    :return: dictionary with the 'FA' and 'FB' as keys and corresponding metric values
    """
    fc_partition = df[df.partition == 'fc']
    metrics_dict = {'FB': fb(fc_partition.target, fc_partition.fc),
                    'FA': fa(fc_partition.target, fc_partition.fc)}
    metrics_dict = {k: np.round(v * 100, 2) for k, v in metrics_dict.items()}

    return metrics_dict


def create_metrics_table(results: pd.DataFrame, hecate_versions_params: HecateVersionsParams) -> pd.DataFrame:
    """
    Function creates table with all the metrics for the one run results table

    :param results: dataframe to calculate metrics for
    :param hecate_versions_params: hecate versions parameters of interest (branch, commit_id, project, fold, horizon)

    :return: table with metrics for the current run results
    """
    result_table_name = 'results_' + hecate_versions_params.project.lower()
    run_info = {'project': [hecate_versions_params.project],
                'commit_id': [hecate_versions_params.commit_id],
                'branch': [hecate_versions_params.branch],
                'horizon': [hecate_versions_params.horizon],
                'fold': [hecate_versions_params.fold],
                'result_table': result_table_name
                }

    return pd.DataFrame.from_dict(dict(run_info, **get_metrics_dict(results)))


def get_sql_filter(hecate_versions_params: HecateVersionsParams,
                   req_params: list,
                   excl_params: list | None = None,
                   add_params: list | None = None) -> str:
    """
    Get a filter string for SQL.

    :param hecate_versions_params: values of variables to filter
    :param req_params: list of columns to filter sql table on
    :param excl_params: list of params in req_params to be omitted from sql filter
    :param add_params: list of params to add to req_params for sql filtering

    :return: filter string for sql
    """
    sql_req_params = req_params.copy()

    if excl_params is not None:
        sql_req_params = [p for p in sql_req_params if p not in excl_params]

    if add_params is not None:
        sql_req_params += add_params

    params = []
    for prm in sql_req_params:
        prm_val = getattr(hecate_versions_params, str(prm))
        if isinstance(prm_val, str):
            params.append(f"{prm} = '{prm_val}'")
        elif prm == "fold":
            params.append(f"{prm} = '{int(prm_val)}'")
        else:
            params.append(f"{prm} = {prm_val}")
    string = ' AND '.join(params)

    return string
