try:
    from importlib.metadata import version

    __version__ = version("hecate")
except ImportError:
    import toml
    from pathlib import Path

    PROJECT_DIR = Path(__file__).parent.parent
    __version__ = toml.load(PROJECT_DIR.joinpath("pyproject.toml"))['tool']['poetry']['version']
