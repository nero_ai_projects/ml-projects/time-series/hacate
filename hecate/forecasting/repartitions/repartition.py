import numpy as np
import pandas as pd
from datetime import timedelta

from hecate.forecasting.repartitions.base import DummyRepartitionExecutor
from hecate.forecasting.horizon import Horizon
from hecate.forecasting.utils import mark_tiny_leading_values_non_relevant, HorizonMixin
from hecate.features import FeatureType
from hecate.features import TypesOfFeatures
from hecate.utils.utils import safe_div
from hecate.log import get_logger

logger = get_logger('hecate')


class DefaultRepartitionExecutor(DummyRepartitionExecutor):
    def update_partition(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) -> None:
        data = mark_tiny_leading_values_non_relevant(data)
        _ = mark_extreme_history_non_relevant(data, feature_types)


class STRepartitionExecutor(DefaultRepartitionExecutor):
    ...


class MTRepartitionExecutor(DefaultRepartitionExecutor):
    ...


class LTRepartitionExecutor(DefaultRepartitionExecutor):
    ...


class RepartitionExecutor(DummyRepartitionExecutor, HorizonMixin):

    def update_partition(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) -> None:

        if not isinstance(data, pd.DataFrame):
            raise ValueError

        repartition_from_horizon = {
            Horizon.ST: STRepartitionExecutor,
            Horizon.MT: MTRepartitionExecutor,
            Horizon.LT: LTRepartitionExecutor,
        }
        repartition = repartition_from_horizon.get(self.horizon, DefaultRepartitionExecutor)

        repartition().update_partition(data, feature_types)


def mark_extreme_history_non_relevant(sample: pd.DataFrame, feature_types: TypesOfFeatures | None) \
        -> pd.DataFrame:
    """
    Function mark parts of history as non-relevant if found extreme changes in historical data
    :param sample: pd.DataFrame
    :param feature_types: dict <feature: feature_type>
    :return: pd.DataFrame
    """
    is_extreme_changes = is_extreme_changes_in_target_yoy(sample, feature_types)
    if is_extreme_changes:
        first_train_date = sample.loc[sample.partition == "train", 'date'].max() - timedelta(365)
        sample['partition'] = np.where(sample.date < first_train_date, 'non_relevant', sample.partition)

    return sample


def is_extreme_changes_in_target_yoy(sample: pd.DataFrame, feature_types: TypesOfFeatures | None,
                                     threshold: float = 1.5) -> bool:
    """
    Function check if last 90 counts vs 90 days happened a year ago.
    If median value on regular target changes grater then threshold True will be returned.
    If is not possible to calculate will False will be return.

    :param sample:
    :param feature_types:
    :param threshold:
    :return: bool
    """

    if feature_types is None:
        feature_types = {}
    period_columns = {'target-outlier', 'target-zero_period'}
    condition = sample.partition == "train"
    if period_columns.intersection(set(sample.columns)) != period_columns:
        logger.warning('outliers and zero periods not passed when repartitioning')
    else:
        condition &= (sample['target-outlier'] == 0) & (sample['target-zero_period'] == 0)
    train = sample[condition]

    if (train.date.max() - train.date.min()).days < 365 + 90:
        return False

    train_max_date = train.date.max()
    year_n = int(((train.date.max() - train.date.min()).days - 90) / 365) + 1

    discount_cols = [col for col, feature_type in feature_types.items() if feature_type == FeatureType.discount]

    meds = []
    for i in range(year_n):
        train_part = train[(train.date > train_max_date + timedelta(-90 - 365 * i)) &
                           (train.date <= train_max_date + timedelta(-365 * i))].copy()
        indexer = train_part.target > -np.inf
        for col in discount_cols:
            indexer &= ~(np.abs(train_part[col].to_numpy()) > 0)
        meds.append(train_part[indexer].target.median())

    answer = all([max(safe_div(med, meds[0]), safe_div(meds[0], med)) > threshold for med in meds[1:]])

    return answer
