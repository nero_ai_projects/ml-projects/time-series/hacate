FROM python:3.10-slim AS development_build

ARG UID=1000 \
  # Needed for fixing permissions of files created by Docker
  GID=1000

ENV \
  # python:
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PYTHONDONTWRITEBYTECODE=1 \
  # pip:
  PIP_NO_CACHE_DIR=1 \
  PIP_DISABLE_PIP_VERSION_CHECK=1 \
  PIP_DEFAULT_TIMEOUT=100 \
  PIP_ROOT_USER_ACTION=ignore \
  # poetry:
  POETRY_VERSION=1.5.1 \
  POETRY_NO_INTERACTION=1 \
  POETRY_VIRTUALENVS_IN_PROJECT=1 \
  POETRY_HOME='/usr/local'

RUN apt-get update && apt-get upgrade -y \
  && apt-get install gettext -y \
  && apt-get install --no-install-recommends -y curl \
  && curl -sSL 'https://install.python-poetry.org' | python - \
  && poetry --version \
  # Cleaning cache:
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && apt-get clean -y && rm -rf /var/lib/apt/lists/*

WORKDIR /app

ARG HEALTHCHECK_API_KEY
ENV HEALTHCHECK_KEY $HEALTHCHECK_API_KEY

RUN groupadd -g "${GID}" -r web \
  && useradd -d '/app' -g web -l -r -u "${UID}" web \
  && chown web:web -R '/app'

COPY --chown=web:web ./poetry.lock ./pyproject.toml /app/

RUN poetry run pip install -U pip \
  && poetry install --no-interaction --no-ansi --only main --no-root

ENV VIRTUAL_ENV=/app/.venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
ENV HECATE_LOG_LOCATION=/app/logs/

# Running as non-root user:
USER web

EXPOSE 80

FROM development_build AS production_build
COPY --chown=web:web . /app
RUN envsubst '\$HEALTHCHECK_KEY' < scripts/healthcheck.sh > healthcheck.sh\
    && chmod +x healthcheck.sh
