import pandas as pd


def fa(target: pd.Series, forecast: pd.Series) -> float:
    """
    FA metric.

    :param target: pd.Series
    :param forecast: pd.Series
    :return: float
    """
    ae = sum(abs(target - forecast))
    if ae == 0:
        return 1

    act_sum = sum(target)
    if act_sum == 0:
        return 0

    return 1 - ae / act_sum


def fb(target: pd.Series, forecast: pd.Series) -> float:
    """
    FB metric.

    :param target: pd.Series
    :param forecast: pd.Series
    :return: float
    """
    e = sum(target - forecast)
    fc_sum = sum(forecast)
    if fc_sum == 0:
        if sum(target) != 0:
            return 1
        else:
            return 0

    return e / fc_sum
