import pandas as pd

from hecate.forecasting.utils import HorizonMixin
from hecate.forecasting.horizon import Horizon
from hecate.forecasting import trend
from hecate.features import FeatureType, get_cols_from_types
from hecate.features import TypesOfFeatures
from hecate.types import TupleDataAndCols


class DummyTrendGenerator:
    """
    Dummy trend generator, adds a zero-filled trend column. Has to do so, the forecasting model will be searching
    for it.
    """

    def __init__(self) -> None:
        super().__init__()
        self.horizon = Horizon.UNDEFINED

    def add_trend(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) \
            -> tuple[pd.DataFrame, list]:
        data['trend'] = 0
        return data, ['trend']


class DefaultTrendGenerator(DummyTrendGenerator):
    """
    Dummy trend generator, adds a zero-filled trend column. Has to do so, the forecasting model will be searching
    for it.
    """

    def add_trend(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) -> TupleDataAndCols:
        """
        Default logics to apply to extract trend.
        """

        if feature_types is None:
            promo_cols = []
        else:
            promo_cols = get_cols_from_types(feature_types,
                                             [FeatureType.discount, FeatureType.promo, FeatureType.promo_budget])

        data, added_col = trend.add_trend(data, promo_cols=promo_cols)
        return data, added_col


class STFGenerator(DefaultTrendGenerator):
    """
    Logics to apply to extract trend for short-term forecasting.
    """
    ...


class MTFGenerator(DefaultTrendGenerator):
    """
    Logics to apply to extract trend for midterm forecasting.
    """
    ...


class LTFGenerator(DefaultTrendGenerator):
    """
    Logics to apply to extract trend for long-term forecasting.
    """
    ...


class TrendGenerator(DummyTrendGenerator, HorizonMixin):
    """
    Generates trend based on the forecasting horizon.
    """

    def add_trend(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) -> TupleDataAndCols:
        generator_from_horizon = {
            Horizon.ST: STFGenerator,
            Horizon.MT: MTFGenerator,
            Horizon.LT: LTFGenerator
        }
        generator = generator_from_horizon.get(self.horizon, DefaultTrendGenerator)
        data, added_col = generator().add_trend(data, feature_types)

        return data, added_col
