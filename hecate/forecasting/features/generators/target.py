import pandas as pd

from hecate.forecasting.features.generators.base import DummyFGenerator
from hecate.forecasting.features.target import add_rolling
from hecate.forecasting.utils import HorizonMixin
from hecate.forecasting.horizon import Horizon
from hecate.features import FeatureType
from hecate.features import TypesOfFeatures


class DefaultFGenerator(DummyFGenerator):

    def add_features(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) \
            -> tuple[pd.DataFrame, list]:

        return data, []


class STFGenerator(DefaultFGenerator):

    def add_features(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) \
            -> tuple[pd.DataFrame, list]:

        if feature_types is None:
            discount_cols = []
        else:
            discount_cols = [key for key, item in feature_types.items()
                             if item in {FeatureType.discount, FeatureType.promo_budget}]

        data = add_rolling(data, discount_cols=discount_cols)
        return data, ['rolling']


class MTFGenerator(DefaultFGenerator):
    ...


class LTFGenerator(DefaultFGenerator):
    ...


class TargetFGenerator(DummyFGenerator, HorizonMixin):

    def add_features(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) \
            -> tuple[pd.DataFrame, list]:
        generator_from_horizon = {
            Horizon.ST: STFGenerator,
            Horizon.MT: MTFGenerator,
            Horizon.LT: LTFGenerator
        }
        generator = generator_from_horizon.get(self.horizon, DefaultFGenerator)
        data, added_features = generator().add_features(data)

        return data, added_features
