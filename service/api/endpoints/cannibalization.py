import json
from fastapi import APIRouter, Query, Header, Request, Response, Depends

from hecate.log import get_logger, set_up_user_name
import hecate.cannibalization.cannibalization as cn
from hecate.validations import quality
from hecate.features import TypesOfFeatures
from service.utils import body_to_df, get_binary_outcome, is_authorized
from service.swagger.description import SwaggerDescription
from service.swagger.example import Examples
from service.swagger.response import SwaggerResponse
from service.swagger.request import SwaggerRequest


desc = SwaggerDescription()
examp = Examples()
resp = SwaggerResponse()
reqs = SwaggerRequest()


router = APIRouter()
can_docs = None
logger = get_logger("hecate", service_kind="app")


@router.post(
    "",
    description=can_docs,
    openapi_extra=reqs.fc_openapi_extra,
    responses=resp.can_response
)
async def get_can_pairs(
    request: Request,
    payload_type: str = Query(default="parquet", examples=examp.payload_type_examples),
    feature_types: str = Header(title="Feature-Types", default="{}",
                                description=desc.headers_desc["feature_types"]),
    dependencies=Depends(is_authorized)
) -> Response:
    """
    Route get_can_pairs for forecast.

    :param **payload_type**: tuple of requests data ["parquet", "json"]\n
    :param **api_key**: key for authorization, set into header\n
    :param **Feature_Types**: types for feature\n
    """
    set_up_user_name(logger, dependencies)
    logger.info("Start Retrieving cannibalization pairs")

    body = await request.body()
    try:
        data = body_to_df(body, payload_type)
    except Exception as e:
        logger.exception('df building from request body failed:')
        return Response(content=str(e), status_code=400)

    feat_types: TypesOfFeatures = json.loads(feature_types)

    missed_columns = quality.get_missed_columns(data, feat_types)
    if missed_columns:
        logger.info(f"Columns in data: {data.columns}, feature types: {feat_types}, missed: {missed_columns}")
        return Response(status_code=400, content=f"missed columns: {missed_columns}")

    can_data = cn.get_cannibalization_pairs(data, feat_types, parallel=False)

    if payload_type is not None and payload_type == "json":
        binary_outcome = str(json.loads(can_data.to_json()))
    else:
        binary_outcome = get_binary_outcome(can_data)

    logger.info("Finish Retrieving cannibalization pairs")

    return Response(content=binary_outcome)


can_docs = get_can_pairs.__doc__
