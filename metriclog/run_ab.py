"""
Introduces AB tests of forecast by two different hecate versions from hecate.ab into metriclog
"""

import pandas as pd

from metriclog.types import HecateVersionsParams
from metriclog.constants import MAIN_AB_HECATE_PARAMS
from ab import ab
from ab.types import ABParams


def add_hecate_versions_params(ab_results: pd.DataFrame, hecate_versions_params: HecateVersionsParams) -> pd.DataFrame:
    """
    Add columns to ab_results table with given parameters of hecate versions compared in the AB test

    :param ab_results: the DataFrame with AB test results to be supplemented by hecate versions and parameters
    :param hecate_versions_params: hecate versions and parameters to be added to ab_results (branch_a, commit_a_id,
                                   branch_b, commit_b_id, fold, focus, horizon)

    :return: updated ab_results table
    """

    ab_results['project'] = f"{hecate_versions_params.project}"
    for param in MAIN_AB_HECATE_PARAMS:
        ab_results[param] = hecate_versions_params.__getattribute__(param)

    return ab_results


def prepare_data_for_compose_ab_summary(data: pd.DataFrame) -> pd.DataFrame:
    """
    Replaces all values by 'total' in columns 'key', 'product_group' and 'product_category',
    sums up 'target' and 'fc' within a single date, deletes 'metrics' if necessary

    :param data: the data to be preprocessed

    :return: the preprocessed data
    """
    columns = ['target', 'fc', 'key', 'product_group', 'product_category', 'metrics']
    data_gr = data.loc[data.partition == 'fc'] \
        .groupby(data.columns.difference(columns).tolist(), as_index=False) \
        .agg({'fc': sum, 'target': sum})
    for column in ['key', 'product_group', 'product_category']:
        data_gr[column] = 'total'

    return data_gr


def compose_ab_summary(ab_results: pd.DataFrame, data_a: pd.DataFrame, data_b: pd.DataFrame, ab_params: ABParams
                       ) -> pd.DataFrame:
    """
    Creates a summary table of AB test results for a single project

    :param ab_results: a table with AB test results
    :param data_a: input data A used in the AB test
    :param data_b: input data A used in the AB test
    :param ab_params: parameters required for AB tests ('add_metrics', 'metrics_name', 'alpha_ab', 'beta_ab', 'ci_prob',
                      'distribution', 'alpha_dist', 'greater_is_better', 'zero_threshold', 'min_len_test',
                      'min_len_norm', 'n_signif_digits')

    :return: a DataFrame containing the summary on the given AB test results
    """

    data_a = prepare_data_for_compose_ab_summary(data_a)
    data_b = prepare_data_for_compose_ab_summary(data_b)
    ab_params.add_metrics = True
    ab_total = ab.ab_test_all_keys(data_a, data_b, ab_params)
    columns_to_drop = ['key', 'weight']

    columns = MAIN_AB_HECATE_PARAMS.copy()
    columns.insert(4, 'project')
    fc_params = pd.DataFrame([ab_results.loc[ab_results.index[0], columns].values], columns=columns)

    ab_summary = pd.concat([ab_total.drop(columns=columns_to_drop), fc_params], axis=1, join='outer')

    return ab_summary
