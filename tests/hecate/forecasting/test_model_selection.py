from hecate.forecasting import model_selection
from hecate.feature_types import FeatureType


def test_train_predict(hecate_input_df):
    data, feature_types, result = hecate_input_df
    feature_cols = [key for key, item in feature_types.items() if item not in [FeatureType.dim]]
    if len(feature_cols) > 0:
        data['forecast'] = model_selection.train_predict(data=data, features=feature_cols)
        assert False in list(data['forecast'].isna())


def test_forecast(hecate_input_df):
    data, feature_types, result = hecate_input_df
    feature_cols = [key for key, item in feature_types.items() if item not in [FeatureType.dim]]
    if len(feature_cols) > 0:
        data['forecast'] = model_selection.train_predict(data=data, features=feature_cols)
        assert False in list(data['forecast'].isna())
