import pandas as pd
import pytest

from hecate.features import FeatureType
from hecate.hecate import fc_batch
from hecate.cannibalization import cannibalization


def test_cannibalization_from_hecate(cannibalization_input_df):
    data, feature_types = cannibalization_input_df
    out = fc_batch(data, feature_types, decompose='all')

    assert 'cannibalized' in out.columns
    assert out.shape[0] == data.shape[0]


@pytest.mark.parametrize('cannibalization_input_df', ['test_1'], indirect=True)
def test_cannibalization(cannibalization_input_df):
    data, _ = cannibalization_input_df
    feature_types = {
        "price_reg": FeatureType.price_reg,
        "discount": FeatureType.discount,
        "cann_product_level": FeatureType.can_level,
    }
    out, _ = cannibalization.add_cannibalization_as_feature(data.copy(), feature_types)

    assert isinstance(data, pd.DataFrame), 'result is not a dataframe'
    assert data.shape[0] == out.shape[0], 'input and output lengths do not match'
    assert data.shape[1] + 2 == out.shape[1], 'wrong number of output columns'
    assert out['discount_cannibalizator'].sum() != 0, 'cannibalizator discount is zero'

    for col in ['key', 'partition']:
        data[col] = data[col].astype("category")
    _ = cannibalization.add_cannibalization_as_feature(data.copy(), feature_types)


def test_add_cannibalization_as_feature_support_cats():
    """Faced case when cannibalization failed if key, partition passed as category
        So, here we emulate this case.
        Idea of this test just proof that addc
    """

    sample = {
        'key': ['product_11-customer_1'] * 8,
        'date': ['2020-01-01', '2020-01-02', '2020-01-03', '2020-01-04', '2020-01-05', '2020-01-06', '2020-01-07',
                 '2020-01-08'],
        'partition': ['train'] * 7 + ['fc'],
        'target': [50, 55, 55, 20, 20, 50, 25, 25],
        'discount': [0] * 8,
        'category': ['cat_1'] * 8,
    }
    test_data_sample = pd.DataFrame(sample)

    test_data_sample_2 = test_data_sample.copy()
    test_data_sample_2['category'] = 'cat_2'
    test_data_sample_2['key'] = 'product_22-customer_1'

    test_data = pd.concat([test_data_sample, test_data_sample_2])

    for col in ['key', 'partition']:
        test_data[col] = test_data[col].astype("category")

    test_data_sample_3 = test_data_sample.copy()
    test_data_sample_3['key'] = 'product_12-customer_1'
    test_data_sample_3['target'] = [100, 100, 100, 150, 150, 100, 150, 150]
    test_data_sample_3['discount'] = [0, 0, 0, .25, .25, 0, .25, 0.25]

    test_data = pd.concat([test_data_sample,
                           test_data_sample_2,
                           test_data_sample_3])

    for col in ['key', 'partition']:
        test_data[col] = test_data[col].astype("category")

    feature_types = {
        'discount': FeatureType.discount,
        'category': FeatureType.can_level
    }

    _ = cannibalization.add_cannibalization_as_feature(test_data.copy(), feature_types, parallel=False)


@pytest.mark.parametrize('cannibalization_input_df', ['test_1'], indirect=True)
def test_cannibalizator_selection(cannibalization_input_df):
    key = '62402_3'

    data, _ = cannibalization_input_df
    feature_types = {
        "price_reg": FeatureType.price_reg,
        "discount": FeatureType.discount,
        "cann_product_level": FeatureType.can_level,
    }

    discounts = data[['date', 'key', 'discount']].copy()
    discounts.rename({'key': 'cannibalizator', 'discount': 'discount_cannibalizator'},
                     axis=1, inplace=True)

    out = cannibalization.select_cannibalizator(data[data.key == key], discounts, feature_types)

    assert out['cannibalizator'].unique()[0] == '1K901_63', \
        'incorrect cannibalizator selected'


@pytest.mark.parametrize('cannibalization_input_df', ['test_2'], indirect=True)
def test_model_filter(cannibalization_input_df):
    data, _ = cannibalization_input_df
    feature_types = {
        "price_reg": FeatureType.price_reg,
        "discount": FeatureType.discount,
        "cann_product_level": FeatureType.can_level,
    }
    data, features_dict = cannibalization.get_features(data, feature_types)

    assert 'xgb_features' in features_dict
    assert [k for k, v in features_dict.items() if '_ext' in k] == \
        ['lm_features_ext', 'lm_cat_ext', 'lm_num_ext']
    assert [k[:-4] for k in features_dict.keys() if '_ext' in k] == \
        ['lm_features', 'lm_cat', 'lm_num']

    assert 'discount_cannibalizator' in features_dict['xgb_features']
    assert 'discount_cannibalizator' in features_dict['lm_features_ext']
    assert set(features_dict['lm_features_ext']) - {'discount_cannibalizator'} == \
        set(features_dict['lm_features'])
    assert set(features_dict['lm_num_ext']) - {'discount_cannibalizator'} == \
        set(features_dict['lm_num'])

    mf = cannibalization.ModelFilter()
    out = mf.fit(data, features_dict)

    assert isinstance(out, pd.DataFrame)
    assert mf.p < 0.01
    assert abs(mf.score_w_cann - .76) < .01
    assert abs(mf.score_no_cann - .753) < .01
    assert abs(mf.xgb_feat_imp - .088) < .01


@pytest.mark.parametrize('cannibalization_input_df', ['test_3'], indirect=True)
def test_hecate_output(cannibalization_input_df):
    """
    Test if hecate output returns drivers with expected signs.
    """
    data, ft = cannibalization_input_df
    res = fc_batch(data, feature_types=ft)

    assert res[res.bl < 0].shape[0] == 0
    assert res[res.fc < 0].shape[0] == 0
    assert res[res.cannibalized > 0].shape[0] == 0


@pytest.mark.parametrize('cannibalization_input_df', ['test_4'], indirect=True)
def test_long_fc_partition_data(cannibalization_input_df):
    data, _ = cannibalization_input_df
    feature_types = {
        "discount": FeatureType.discount,
        "category": FeatureType.can_level,
    }
    data, _ = cannibalization.add_cannibalization_as_feature(data, feature_types)

    assert abs(data.discount_cannibalizator.sum()) - 7.7 < 0.1
    assert set(data.cannibalizator.unique()) == {'nan', '5_2'}
