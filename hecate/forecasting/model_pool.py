import xgboost as xgb

import hecate.forecasting.models as models


def model_pool():
    """
    Collects all necessary models data to list to be passed to init_models.

    :return: list of named tuples containing models to be used in tournament, their parameters and constructors names.
    """
    pool_items = [models.ModelPoolItem(model_name='xgb_regressor',
                                       params={'eta': [0.3],
                                               'max_depth': [8],
                                               'n_estimators': [10],
                                               'min_child_weight': [2]},
                                       constructor=xgb.XGBRegressor)]
    pool = list()
    for pool_item in pool_items:
        pool = pool + pool_item.transform()

    return pool


def init_models(x_cols: list, cols_for_ohe: list, min_max_cols: list):
    """
    Initiates the model pool.

    :param x_cols: list - list of features to train
    :param cols_for_ohe: list of features which must transform to One-Hot_Encoding
    :param min_max_cols: list of features which must transform to MinMaxScaler

    :return: list of named tuples: model names and models itself.
    """
    models_list = []

    for model_name, params, constructor in model_pool():
        model = models.PipelineWrapper(model=constructor,
                                       params_model=params,
                                       features_cols=x_cols,
                                       ohe_cols=cols_for_ohe,
                                       min_max_cols=min_max_cols)
        models_list.append({
            'model_name': model_name,
            'model': model,
            'params': params,
            'params_model': {'general_models': False,
                             'x_cols': x_cols,
                             'cols_for_ohe': cols_for_ohe,
                             'min_max_cols': min_max_cols}})
    return models_list
