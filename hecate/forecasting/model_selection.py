from typing import List, Optional

import numpy as np
import pandas as pd

from hecate.forecasting.models import get_default_model, ModelAnswer
from hecate.utils.utils import add_trend, get_default_n_estimators, get_frequency


def get_sample_importance(data: pd.DataFrame, relaxation_coef: float = 1.2) -> np.ndarray:
    """
    Adds a column with a backward decreasing exponent that represents
    sample importance

    :param data: input dataset containing partition and date columns
    :param relaxation_coef: relaxation coefficient that affects speed of
    exponent decreasing, the bigger the coefficient, the smaller the
    influence of old samples on the forecast
    :return: sample importance array
    """
    train_last_date = data[data.partition == 'train'].date.max()
    return np.exp((data.date - train_last_date).dt.days / (365 * relaxation_coef))


def processing_trend_wrapper(func, *args, **kwargs):
    """
    Decorator for func (train_predict), which clears trend before train
    and adds it back after forecast is calculated.
    """

    def wrapper(**kwargs):
        """
        Wrapper for function.
        """
        data = kwargs['data']
        freq = kwargs['freq'] if kwargs.get('freq') else None
        col = 'fc' if not kwargs.get('col') else kwargs['col']

        # add trend if required, remove from target
        if col == 'fc':
            data, trend = add_trend(data, freq)
        kwargs['data']['target'] -= kwargs['data']['trend']

        # forecast
        func_res = func(**kwargs)
        data[col] = func_res.prediction

        # postprocessing trend
        data['target'] += data['trend']
        data[col] += data['trend']

        # postprocessing forecast
        data[col] = data[col].round(2).clip(lower=0)

        # updating func_res
        func_res.prediction = data[col]

        return func_res

    return wrapper


@processing_trend_wrapper
def train_predict(data: pd.DataFrame, features: list, freq: Optional[str] = None) -> ModelAnswer:
    """
    Train predict model function.
    :param data: pd.DataFrame - input dataframe
    :param features: list of string - list of features
    :param freq: frequency of timeline. If None, it would be inferred from data.date
    :return: instance of ModelAnswer class
    """
    features.sort()

    min_max_cols = features
    ohe_cols: List[str] = []

    if freq is None:
        freq = get_frequency(data['date'])
    n_est = get_default_n_estimators(freq)
    model = get_default_model(features, min_max_cols, ohe_cols, n_est)

    train_partition = data[data['partition'] == 'train']
    sample_importance = get_sample_importance(train_partition)

    model.fit(train_partition[features], train_partition['target'],
              model__sample_weight=sample_importance)

    result = ModelAnswer(prediction=model.predict(data[features]), model=model)

    return result
