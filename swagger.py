import json
import pandas as pd
from pydantic import BaseModel
from typing import Union, Any
from pydantic.schema import schema

from hecate.feature_types import FeatureType
from hecate.forecasting.decompositions.strategies import BLDecompositionStrategy


_TO_DICT_LINK = 'https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_dict.html'

tags_metadata = [
    {
        "name": "Forecast",
        "description": "Forecast Operation for binary data of pandas DataFrame",
    },
    {
        "name": "Cannibalizators",
        "description": "Get cannibalizator pairs from binary data of pandas DataFrame"
    }
]

headers_desc = {
    "feature_types": "" +
                     "<p>Information about type of business columns.</p>" +
                     "<p>Example:</p>" +
                     "<p>feature_types = {'dsc': 'discount', 'stc': 'stock'}</p>" +
                     "<p>We transmit information that the dsc column is a discount ('discount')," +
                     " and the stc column is the remains in the warehouse ('stock').</p>" +
                     "Available types:</p>" +
                     FeatureType.doc(),
    "decompose": "" +
                 "<p>Параметр регулирует декомпозицию прогноза на драйверы." +
                 "<p>Может принимать значения 'all' (декомпозировать на все доступные "
                 "драйверы), 'none' (не декомпозировать, вернуть только тотал прогноз), " +
                 "или может быть листом, содержащим драйвера ('bl', 'cn', 'pc').</p>",
    "calc_type": "" +
                 "<p>Information about type of calculation to be used for various figures.</p>" +
                 "<p>Example:</p>" +
                 "<p>feature_types = {'bl': 'zero_promo'}</p>" +
                 "<p>We transmit information that the baseline column is to be calculated by zeroing" +
                 " out promo features during promo periods.</p>" +
                 "Available types:</p>" +
                 BLDecompositionStrategy.doc()
}


class Examples:

    @staticmethod
    def test_1():
        test_df = pd.read_csv("./tests/tests/hecate/test_4/data.csv")
        return json.loads(test_df.to_json())

    @staticmethod
    def test_2():
        test_df = pd.read_csv("./tests/tests/hecate/test_1/data.csv")
        return json.loads(test_df.to_json())

    @staticmethod
    def test_3():
        test_df = pd.read_csv("./tests/tests/hecate/test_4/data.csv")
        test_df.drop(columns=['const'], inplace=True)
        test_df['partition'].replace({'fc': 'decompose'}, inplace=True)
        return json.loads(test_df.to_json())


class JsonDataResponse(BaseModel):
    """
    Schema json model of dataframe response 200 for FastApi documentation
    """
    key: Union[dict, Any]
    date: Union[dict, Any]
    fc: Union[dict, Any]


class NonAuth(BaseModel):
    """
    Schema json model of dataframe response 418 for FastApi documentation
    """
    msg: Union[str, None]


class ServerError(BaseModel):
    """
    Schema json model of dataframe response 500 for FastApi documentation
    """
    msg: Union[str, None]


class JsonData(BaseModel):
    """
    Schema json model of dataframe response for FastApi documentation
    """
    key: Union[dict, Any]
    date: Union[dict, Any]
    partition: Union[dict, Any]
    target: Union[dict, Any]

    class Config:
        schema_extra = {
            'examples': [
                {
                    'key': {"0": "key-1", "1": "key-1", "2": "key-1", "3": "key-1"},
                    'date': {"0": "2022-07-04", "1": "2022-07-11", "2": "2022-07-18", "3": "2022-07-25"},
                    'partition': {"0": "train", "1": "train", "2": "train", "3": "fc"},
                    'target': {"0": 100.01, "1": 1.0, "2": 1.0, "3": 0.0}
                },
                Examples.test_1(),
                Examples.test_2(),
                Examples.test_3(),
            ]
        }


class ParquetData(BaseModel):
    """
    Schema parquet model of dataframe response for FastApi documentation
    """
    data: bytes


class CSVData(BaseModel):
    """
    Schema CSV model of dataframe response for FastApi documentation
    """
    data: bytes


top_level_schema = schema([JsonData, ParquetData], title='Request Data Schema')

example_desc = "<p>Json, в случае при передече параметра payload_type = 'json'</p>" + \
               "<p>должен содержать необходимые колонки + признаки" + \
               "(например, глубину скидки, цену и пр.)</p>" + \
               "Ожидаемый json соответствует результату применения метода to_dict объекта " + \
               f"pandas.DataFrame <a href={_TO_DICT_LINK}>{_TO_DICT_LINK}</a>" + \
               "{0}"


fc_openapi_extra = {
    "header": {
        "accept": "application/gzip"
    },
    "requestBody": {
        "content": {
            "application/json": {
                "schema": JsonData.schema(),
                "examples": {
                    "json_1": {
                        "summary": "Json request data type example",
                        "description": example_desc.format(
                            "<p><span></span>: required columns: " +
                            "['key', 'date', 'partition', 'target']</p>" +
                            "<li>key - id временного ряда</li>" +
                            "<li>date - дата в формате 'YYYY-MM-dd'</li>" +
                            "<li>partition - разметка временного ряда" +
                            "<ul><li>'train' - данные для обучения модели</li>" +
                            "<li>'fc' - данные для построения прогноза</li></ul></li>" +
                            "<li>target - наша величина, которую нам нужно прогнозировать. " +
                            "Например, отгрузки</li>" +
                            "<p></p><p><b>Замечания:</b></p><p>* В данных не должно быть пропусков:</p>" +
                            "<p>Например, у нас не было отгрузок 1 января. " +
                            "В этом случае ожидается, что придет отгрузка равная нулю.</p>" +
                            "<p>* Даты должны быть сортированы по возрастанию.</p>" +
                            "<p>* Прогноз строится на все точки, где partition указан 'fc'</p>"
                        ),
                        "value": JsonData.Config().schema_extra["examples"][0],
                    },
                    "json_2": {
                        "summary": "Json request data with discount",
                        "description": example_desc.format(
                            "<p><span></span>: required columns: " +
                            "['key', 'date', 'partition', 'target', 'discount_promo']</p>" +
                            "<li>key - id временного ряда</li>" +
                            "<li>date - дата в формате 'YYYY-MM-dd'</li>" +
                            "<li>partition - разметка временного ряда</li>" +
                            "<li>'const' - колонка константного типа</li>" +
                            "<li>'discount_promo' - данные о скидках в процентах</li>" +
                            "<li>target - наша величина, которую нам нужно прогнозировать.</li>"
                        ),
                        "value": JsonData.Config().schema_extra["examples"][1],
                    },
                    "json_3": {
                        "summary": "Json request data with discount&stock",
                        "description": example_desc.format(
                            "<p><span></span>: required columns: " +
                            "['key', 'date', 'partition', 'target', 'stock', 'discount_promo']</p>" +
                            "<li>key - id временного ряда</li>" +
                            "<li>date - дата в формате 'YYYY-MM-dd'</li>" +
                            "<li>partition - разметка временного ряда</li>" +
                            "<li>'stock' - колонка stock</li>" +
                            "<li>'discount_promo' - данные о скидках в процентах</li>" +
                            "<li>target - наша величина, которую нам нужно прогнозировать.</li>"
                        ),
                        "value": JsonData.Config().schema_extra["examples"][2],
                    }
                },
            },
            "application/gzip": {
                "schema": ParquetData.schema(),
                "examples": {
                    "parquet": {
                        "summary": "Parquet request data type example",
                        "description": "Parquet request data type example. " +
                                       "Use this example, when payload_type = 'parquet'",
                        "value": str(JsonData.Config().schema_extra["examples"][0])
                    },
                },
            },
        },
        "required": True,
    },
}

act_decomposition_openapi_extra = {
    "header": {
        "accept": "application/gzip"
    },
    "requestBody": {
        "content": {
            "application/json": {
                "schema": JsonData.schema(),
                "examples": {
                    "json_1": {
                        "summary": "Json request example for actuals decomposition",
                        "description": example_desc.format(
                            "<p><span></span>: required columns: " +
                            "['key', 'date', 'partition', 'target']</p>" +
                            "<li>key - id временного ряда</li>" +
                            "<li>date - дата в формате 'YYYY-MM-dd'</li>" +
                            "<li>partition - разметка временного ряда" +
                            "<ul><li>'train' - данные для обучения модели</li>" +
                            "<li>'decompose' - данные для декомпозиции</li></ul></li>" +
                            "<li>target - величина, которую нужно декомпозировать. " +
                            "Например, отгрузки</li>" +
                            "<p></p><p><b>Замечания:</b></p><p>* В данных не должно быть пропусков:</p>" +
                            "<p>Например, у нас не было отгрузок 1 января. " +
                            "В этом случае ожидается, что придет отгрузка равная нулю.</p>" +
                            "<p>* В переданных feature_types обязательно должны быть указаны типы для " +
                            "декомпозиции. Например, если ни один feature_type, ассоциированный с промо, не " +
                            "указан, hecate не сможет сделать декомпозицию на baseline и uplift.</p>"
                            "<p>* Если передан пустой словарь feature_types, код упадет с ошибкой 400.</p>"
                            "<p>* Даты должны быть сортированы по возрастанию.</p>" +
                            "<p>* Декомпозиция строится на все точки, где partition указан 'decompose'.</p>" +
                            "<p>* Данные возвращаются только для точек, где partition указан 'decompose'.</p>"
                        ),
                        "value": JsonData.Config().schema_extra["examples"][3],
                    }
                },
            },
            "application/gzip": {
                "schema": ParquetData.schema(),
                "examples": {
                    "parquet": {
                        "summary": "Parquet request data type example",
                        "description": "Parquet request data type example. " +
                                       "Use this example, when payload_type = 'parquet'",
                        "value": str(JsonData.Config().schema_extra["examples"][0])
                    },
                },
            },
        },
        "required": True,
    },
}


fc_response = {
    200: {
        "description": "Return Result of forecast",
        "content": {
            "application/json": {
                "schema": JsonDataResponse.schema(),
                "examples": {
                    "json": {
                        "summary": "Json data",
                        "description": "Json response data type example. " +
                                       "Use this example, when payload_type = 'json'",
                        "value": {
                            'key': {'0': 'key-1', '1': 'key-1', '2': 'key-1', '3': 'key-1'},
                            'date': {'0': '2022-07-04', '1': '2022-07-11', '2': '2022-07-18', '3': '2022-07-25'},
                            'fc': {'0': 33.58, '1': 33.58, '2': 33.58, '3': 33.58}
                        },
                    },
                },
            },
            "text/csv": {
                "schema": CSVData.schema(),
                "examples": {
                    "csv": {
                        "summary": "CSV data",
                        "description": "CSV response data type example. " +
                                       "Use this example, when payload_type = 'csv'",
                        "value": "" +
                                 ";key;date;fc\n" +
                                 "0;key-1;2022-07-04;33.58\n" +
                                 "1;key-1;2022-07-11;33.58\n" +
                                 "2;key-1;2022-07-18;33.58\n" +
                                 "3;key-1;2022-04-25;33.58"
                    }
                }
            }
        },
    },
    418: {
        "description": "Return if non-authorized",
        "content": {
            "application/json": {
                "schema": NonAuth.schema(),
                "examples": {
                    "json": {
                        "summary": "Non-authorizated type",
                        "description": "Json response non-authorizated type example.",
                        "value": {
                            "msg": "You are not authorizated!"
                        },
                    },
                },
            }
        }
    },
    500: {
        "description": "Return if internal server error",
        "content": {
            "application/json": {
                "schema": ServerError.schema(),
                "examples": {
                    "json": {
                        "summary": "Json response example",
                        "description": "Json response with internal server error example.",
                        "value": {
                            "msg": "Done with internal server error!"
                        },
                    },
                },
            }
        }
    }
}

can_response = {
    200: {
        "description": "Return cannibalizator pairs",
        "content": {
            "application/json": {
                "schema": {
                    "$ref": ""
                },
                "examples": {
                    "json": {
                        "summary": "Json data",
                        "description": "Json response data type example. " +
                                       "Use this example, when payload_type = 'json'",
                        "value": {
                            'key': {'0': 'product_1', '1': 'product_2', '2': 'product_3'},
                            'cannibalizator': {'0': 'product_4', '1': 'product_5', '2': 'product_6'},
                        },
                    },
                },
            }
        },
    },
    418: {
        "description": "Return if non-authorized",
        "content": {
            "application/json": {
                "schema": {
                    "$ref": ""
                },
                "examples": {
                    "json": {
                        "summary": "Non-authorizated type",
                        "description": "Json response non-authorizated type example.",
                        "value": {
                            "msg": "You are not authorizated!"
                        },
                    },
                },
            }
        }
    }
}

act_decomposition_response = {
    200: {
        "description": "Return decomposed actuals",
        "content": {
            "application/json": {
                "schema": {
                    "$ref": ""
                },
                "examples": {
                    "json": {
                        "summary": "Json data",
                        "description": "Json response data type example. " +
                                       "Use this example, when payload_type = 'json'",
                        "value": {
                            "key": {"12": "key-1", "13": "key-1", "14": "key-1"},
                            "date": {"12": "2023-01-01", "13": "2023-02-01", "14": "2023-03-01"},
                            "fc": {"12": 10.98, "13": 16.1, "14": 16.1},
                            "bl": {"12": 10.98, "13": 16.1, "14": 16.1},
                            "uplift": {"12": 0, "13": 0, "14": 0},
                            "cannibalized": {"12": 0, "13": 0, "14": 0},
                            "price_effect": {"12": 0, "13": 0, "14": 0}
                        },
                    },
                },
            }
        },
    },
    418: {
        "description": "Return if non-authorized",
        "content": {
            "application/json": {
                "schema": {
                    "$ref": ""
                },
                "examples": {
                    "json": {
                        "summary": "Non-authorizated type",
                        "description": "Json response non-authorizated type example.",
                        "value": {
                            "msg": "You are not authorizated!"
                        },
                    },
                },
            }
        }
    }
}

payload_type_examples = {
    "parquet": {
        "summary": "Parquet type",
        "description": "<p>Параметр <b>payload_type</b> формата \"parquet\".</p>" +
                       "<p>Пример для <b>Request body</b>, " +
                       "т.е. тела запроса необходимо установить <b>application/gzip</b></p>",
        "value": "parquet"
    },
    "json": {
        "summary": "Json type",
        "description": "<p>Параметр <b>payload_type</b> формата \"json\".</p>" +
                       "<p>Пример для <b>Request body</b>, т.е. " +
                       "тела запроса необходимо установить <b>application/json</b></p>",
        "value": "json"
    },
    "csv": {
        "summary": "CSV type",
        "description": "<p>Параметр <b>payload_type</b> формата \"csv\".</p>"
                       "Ожидается, что файл будет иметь кодировку utf-8 и разделитель ';' ",
        "value": "csv"
    },
}

decompose_desc = "<p>decompose – параметр для контроля разбиения прогноза на драйвера.</p>" + \
                 "<p>Возможные типы: str (‘all’ или ‘none’), list (все возможные элементы листа: [‘bl’, ‘cn’]):</p>" + \
                 "<ul><li>При значении ‘none’ на выходе выдаётся только колонка fc, содержащая total forecast</li>" + \
                 "<li>При перечислении драйверов в листе рассчитываются только перечисленные драйвера:" + \
                 "<ul><li>Если лист содержит ‘bl’, на выходе к датафрейму добавляются колонки bl и uplift. " + \
                 "Сумма колонок должна равняться значению в колонке fc.</li>" + \
                 "<li>Если лист содержит ‘cn’, на выходе к датафрейму добавляется колонка cannibalized. </li>" + \
                 "<li>Если лист содержит ‘pc’, на выходе к датафрейму добавляется колонка price_effect. </li>" + \
                 "<li>Если лист содержит и ‘bl’, и ‘cn’, и ‘pc’" + \
                 "на выходе к колонке fc добавляются колонки bl, uplift, cannibalized и price_effect. " + \
                 "Сумма колонок должна равняться значению в колонке fc.</li></ul></li>" + \
                 "<li>При значении ‘all’ прогноз разбивается на все возможные драйвера ([‘bl’, ‘cn’, ‘pc’]).</li></ul>"
decompose_type_examples = {
    "all": {
        "summary": "All columns",
        "description": decompose_desc,
        "value": ["all"]
    },
    "Bl": {
        "summary": "Bl column",
        "description": decompose_desc,
        "value": ["bl"]
    },
    "Cn": {
        "summary": "Cn column",
        "description": decompose_desc,
        "value": ["cn"]
    },
    "Pc": {
        "summary": "Pc column",
        "description": decompose_desc,
        "value": ["pc"]
    },
    "Bl_Cn": {
        "summary": "Cn and Bl columns",
        "description": decompose_desc,
        "value": ["bl", "cn"]
    },
    "Bl_Pc": {
        "summary": "Bl and Pc columns",
        "description": decompose_desc,
        "value": ["bl", "pc"]
    },
    "Cn_Pc": {
        "summary": "Cn and Pc columns",
        "description": decompose_desc,
        "value": ["cn", "pc"]
    },
    "Bl_Cn_Pc": {
        "summary": "Bl, Cn and Pc columns",
        "description": decompose_desc,
        "value": ["bl", "cn", "pc"]
    },
}
