import json
from copy import deepcopy
from io import BytesIO
from itertools import chain

import numpy as np
import pandas as pd
import pytest
from fastapi import Header
from fastapi.testclient import TestClient

from hecate.constants import ALL_DRIVERS
from service import app, utils, worker
from tests.service.conftest import EndpointCaseData


async def override_dependency(api_key: str | None = Header(title="api_key", convert_underscores=False)):
    return ""


client = TestClient(app.app)
app.app.dependency_overrides[utils.is_authorized] = override_dependency

DATA_DICT = {
    'key': ['key-1', 'key-1', 'key-1', 'key-1'],
    'date': ['2022-07-04', '2022-07-11', '2022-07-18', '2022-07-25'],
    'discount': [10, 0, 0, 0],
    'partition': ['train', 'train', 'train', 'fc'],
    'target': [100.01, 1, 1, 0.0]
}
RESULT_DICT = {
    "key": {"0": "key-1", "1": "key-1", "2": "key-1", "3": "key-1"},
    "date": {"0": "2022-07-04", "1": "2022-07-11", "2": "2022-07-18", "3": "2022-07-25"},
    "fc": {"0": 34, "1": 34, "2": 34, "3": 34},
    "bl": {"0": 34, "1": 34, "2": 34, "3": 34},
    "uplift": {"0": 0, "1": 0, "2": 0, "3": 0},
    "cannibalized": {"0": 0, "1": 0, "2": 0, "3": 0},
    "price_effect": {"0": 0, "1": 0, "2": 0, "3": 0},
}
DATA_FOR_PI_DICT = {
    "key": {"0": "key-1", "1": "key-1", "2": "key-1", "3": "key-1", "4": "key-1", "5": "key-1"},
    "date": {
        "0": "2021-11-01", "1": "2021-11-02", "2": "2021-11-03", "3": "2021-11-04", "4": "2021-11-05", "5": "2021-11-06"
        },
    "target": {"0": 5, "1": 15, "2": 0, "3": 11, "4": 7, "5": 8},
    "partition": {"0": "train", "1": "train", "2": "train", "3": "train", "4": "fc", "5": "fc"},
    "discount_promo": {"0": 15, "1": 15, "2": 0, "3": 10, "4": 15, "5": 15},
    "stock": {"0": 11, "1": 3, "2": 4, "3": 0, "4": 0, "5": 0}
}
RESULT_PI_DICT = {
    "key": {"0": "key-1", "1": "key-1", "2": "key-1", "3": "key-1", "4": "key-1", "5": "key-1"},
    "date": {
        "0": "2021-11-01", "1": "2021-11-02", "2": "2021-11-03", "3": "2021-11-04", "4": "2021-11-05", "5": "2021-11-06"
        },
    "fc": {"0": 7.75, "1": 7.75, "2": 7.75, "3": 7.75, "4": 7.75, "5": 7.75},
    "PI_up_68.0%": {"0": 7.75, "1": 7.75, "2": 7.75, "3": 7.75, "4": 12.3547591788, "5": 12.3547591788},
    "PI_down_68.0%": {"0": 7.75, "1": 7.75, "2": 7.75, "3": 7.75, "4": 3.1452408212, "5": 3.1452408212}
}
NECESSARY_DIMS = ['key', 'date', 'partition']
PARAMS_DICT = [
    {"url": "/fc", "payload_type": "json"},
    {"url": "/fc", "payload_type": "parquet"},
    {"url": "/fc_batch", "payload_type": "json"},
    {"url": "/fc_batch", "payload_type": "parquet"},
    {"url": "/get_can_pairs", "payload_type": "json"},
    {"url": "/get_can_pairs", "payload_type": "parquet"},
    {"url": "/decompose_act", "payload_type": "json"},
    {"url": "/decompose_act", "payload_type": "parquet"},
]

for i in PARAMS_DICT:
    i["feature_types"] = {"discount": "discount"}

default_url_params = {"payload_type": "json", "prediction_interval": "false", "pi_prob_list": [0.68]}


def send_request(url, payload_type, feature_types, mocker, status_code, auth=False):
    data = pd.DataFrame.from_dict(DATA_DICT)
    req_data = dict()
    if payload_type == "json":
        data_io = data.to_json()
        data_io = json.loads(data_io)
        req_data["json"] = data_io if status_code == 200 else dict({"data": "test"})
    else:
        data_io = BytesIO()
        for dim in NECESSARY_DIMS:
            data[dim] = pd.Categorical(data[dim])
        data.to_parquet(data_io, index=False, compression='gzip')
        req_data["data"] = data_io.getvalue() if status_code == 200 else bytes("test", "utf-8")
    url_params = deepcopy(default_url_params)
    url_params["payload_type"] = payload_type
    response = client.post(url, headers={"api_key": "test", "Feature-Types": json.dumps(feature_types)},
                           params=url_params, **req_data)
    assert response.status_code == status_code


def test_routes(mocker):
    for param in PARAMS_DICT:
        send_request(mocker=mocker, status_code=200, **param)


def test_app_fail(mocker):
    for param in PARAMS_DICT:
        send_request(mocker=mocker, status_code=400, **param)


headers_test_params = [
    ("/fc", {"api_key": "test"}, 500, "The incomplete headers test failed!"),
    ("/fc_batch", {"api_key": "test"}, 500, "The incomplete headers test failed!"),
    ("/decompose_act", {"api_key": "test"}, 400, "The incomplete headers test failed!"),
    ("/fc", {"api_key": "test", "Feature-Types": '{"dim": "dim", "discount": "discount"}'}, 200,
     "The correct headers test failed!"),
    ("/fc_batch", {"api_key": "test", "Feature-Types": '{"dim": "dim", "discount": "discount"}'}, 200,
     "The correct headers test failed!"),
    ("/decompose_act", {"api_key": "test", "Feature-Types": '{"dim": "dim", "discount": "discount"}'}, 200,
     "The correct headers test failed!"),
    ("/fc", {"api_key": "test", "Feature-Types": "{'dim': 'dim', 'discount': 'discount'}"}, 400,
     "The single quotation mark test failed!"),
    ("/fc_batch", {"api_key": "test", "Feature-Types": "{'dim': 'dim', 'discount': 'discount'}"}, 400,
     "The single quotation mark test failed!"),
    ("/decompose_act", {"api_key": "test", "Feature-Types": "{'dim': 'dim', 'discount': 'discount'}"}, 400,
     "The single quotation mark test failed!")
]


@pytest.mark.parametrize("url, headers, expected, msg", headers_test_params)
def test_headers_correction(mocker, url, headers, expected, msg):
    data = pd.DataFrame.from_dict(DATA_DICT)
    data["dim"] = ["2 Jan", "3 Jan", "4 Jan", "5 Jan"]
    req_data = dict()
    data_io = data.to_json()
    data_io = json.loads(data_io)
    req_data["json"] = data_io

    response = client.post(url=url, headers=headers, params=default_url_params, **req_data)
    assert response.status_code == expected, msg


def test_create_fc_task():
    data = pd.DataFrame.from_dict(DATA_DICT)
    feature_types = {"discount": "discount"}

    result = worker.create_fc_task.__wrapped__(data.to_json(), feature_types)
    result = json.loads(result)

    for driver in list(chain(*ALL_DRIVERS.values())):
        assert np.allclose(list(result[driver].values()), list(RESULT_DICT[driver].values()), rtol=0.01)


def test_prediction_interval():
    data = pd.DataFrame.from_dict(DATA_FOR_PI_DICT)
    result = worker.create_fc_task.__wrapped__(data.to_json(), feature_types={}, decompose=['none'],
                                               prediction_interval=True, pi_prob_list=[0.68])
    result = json.loads(result)

    for col in ['PI_down_68.0%', 'PI_up_68.0%']:
        assert np.allclose(list(result[col].values()), list(RESULT_PI_DICT[col].values()), rtol=0.01)

    with pytest.raises(Exception):
        worker.create_fc_task.__wrapped__(data.loc[data.partition == 'fc'].to_json(), feature_types={},
                                          decompose=['none'], prediction_interval=True, pi_prob_list=[0.68])


@pytest.mark.parametrize("url", ["/fc", "/fc_batch", "/fc_batch/task"])
def test_pi_decompose_incompatibility(url):
    data = pd.DataFrame.from_dict(DATA_FOR_PI_DICT)
    url_params = deepcopy(default_url_params)
    url_params["prediction_interval"] = "true"
    url_params["decompose"] = "all"
    headers = {"api_key": "test", "Feature-Types": '{"discount_promo": "dsc", "stock": "stc"}'}
    response = client.post(url=url, data=data.to_json(), params=url_params, headers=headers)
    assert response.status_code == 400


@pytest.mark.parametrize(argnames='col', argvalues=['date', 'key', 'partition', 'business_direction', 'promo_type'])
@pytest.mark.parametrize(argnames='ordered', argvalues=[True, False])
def test_categ_cols_usage(
    col: str,
    ordered: bool,
    categ_data_cases: EndpointCaseData,
    testclient: TestClient
):
    headers = {
        "api_key": "test",
        "payload_type": "parquet",
        "decompose": "all",
        "parallel": "true",
        "Feature-Types": json.dumps(categ_data_cases.feature_types)
    }

    buffer = BytesIO()
    data = categ_data_cases.init_data

    data[col] = data[col].astype(pd.CategoricalDtype(ordered=ordered))
    data.to_parquet(buffer, engine='pyarrow')
    response = testclient.post(url=categ_data_cases.endpoint, headers=headers, data=buffer.getvalue())
    fc = pd.read_parquet(BytesIO(response.content)).fc.sum()

    assert response.status_code == 200, 'unexpected status code'

    assert fc == categ_data_cases.expected_res, 'fc result was changed'


@pytest.mark.parametrize(argnames='col', argvalues=['date', 'key', 'partition', 'business_direction', 'promo_type'])
@pytest.mark.parametrize(argnames='ordered', argvalues=[True, False])
def test_categ_cols_usage_can_decompose(
    col: str,
    ordered: bool,
    categ_data_cases_can_decomp: EndpointCaseData,
    testclient: TestClient
):

    headers = {
        "api_key": "test",
        "payload_type": "parquet",
        "decompose": "all",
        "parallel": "true",
        "Feature-Types": json.dumps(categ_data_cases_can_decomp.feature_types)
    }

    buffer = BytesIO()
    data = categ_data_cases_can_decomp.init_data
    data[col] = data[col].astype(pd.CategoricalDtype(ordered=ordered))
    data.to_parquet(buffer, engine='pyarrow')
    response = testclient.post(
        url=categ_data_cases_can_decomp.endpoint,
        headers=headers,
        data=buffer.getvalue()
    )

    assert response.status_code == categ_data_cases_can_decomp.expected_res, 'unexpected status code'
