from typing import Literal
import pandas as pd


Driver = Literal['bl', 'cn', 'pc']
Drivers = list[Driver]
Decompositors = Literal['all', 'none'] | list[Driver]
ColsMapDrivers = dict[Driver, list[Literal['bl', 'uplift', 'cannibalized', 'price_effect']]]
ColsOfDrivers = list[Literal['bl', 'uplift', 'cannibalized', 'price_effect']]
TupleDataAndCols = tuple[pd.DataFrame, list[str]]
