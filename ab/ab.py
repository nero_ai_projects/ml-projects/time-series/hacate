"""
Functions for AB tests in frequentist approach
"""

import pandas as pd
import numpy as np
from dataclasses import replace

from hecate.log import get_logger

from ab import statistics
from ab import metrics
from ab.types import (ABParams,
                      Distribution,
                      ABResult,
                      ABSingleReport,
                      ConfidenceInterval,
                      MetricsNames,
                      MetricSequence)
from ab.constants import ALL_KEYS_OUTPUT_COLUMNS


logger = get_logger('hecate')


def ab_single(metrics_a: MetricSequence,
              metrics_b: MetricSequence,
              ab_params: ABParams
              ) -> ABSingleReport:
    """
    Performs AB test for a single product key.

    :param metrics_a: the dataset A to be compared in the AB test
    :param metrics_b: the dataset B to be compared in the AB test
    :param ab_params: parameters required for AB tests ('add_metrics', 'metrics_name', 'alpha_ab', 'beta_ab', 'ci_prob',
                      'distribution', 'alpha_dist', 'greater_is_better', 'zero_threshold', 'min_len_test',
                      'min_len_norm', 'n_signif_digits')

    :return: namedtuple ABsingleReport with fields:
            result: 'A won' - mertics_a is statistically better than metrics_b in the sense defined by param lesser_wins
                    'B won' - mertics_b is statistically better than metrics_a in the sense defined by param lesser_wins
                    'tie' - there is no statistical difference between metrics_a and metrics_b
            effect_abs: obtained effect value in absolute units as np.mean(metrics_b) - np.mean(metrics_a)
            effect_percent: effect_abs / np.mean(metrics_a) * 100%
            ci_abs: (ci_prob * 100%)-confidence interval for the effect value in absolute units
            ci_percent: ci_abs / np.mean(metrics_a) * 100%
            p_value: p-value of the Student's t-test
    """
    # TODO: take beta_ab into account
    # TODO: think about median test. Problem: means are significantly different, but medians are equal

    ci_prob = ab_params.ci_prob
    n = ab_params.n_signif_digits

    if min(len(metrics_a), len(metrics_b)) > 0:
        match ab_params.distribution:
            case Distribution.NORM:
                a = np.mean(metrics_a)
                b = np.mean(metrics_b)
                effect_abs, p_value = statistics.effect_pvalue_norm(metrics_a, metrics_b, n=n)
                ci_abs = statistics.ci_norm(metrics_a, metrics_b, ci_prob=ci_prob, n=n)
                norm = a
            case Distribution.LOGNORM:
                a = np.mean(np.log(metrics_a))
                b = np.mean(np.log(metrics_b))
                effect_abs, p_value = statistics.effect_pvalue_lognorm(metrics_a, metrics_b, n=n)
                ci_abs = statistics.ci_lognorm(metrics_a, metrics_b, ci_prob=ci_prob, n=n)
                norm = np.exp(a + .5 * np.std(np.log(metrics_a))**2)
            case Distribution.BERNOULLI:
                a = np.mean(metrics_a)
                b = np.mean(metrics_b)
                effect_abs, p_value = statistics.effect_pvalue_bernoulli(metrics_a, metrics_b, n=n)
                ci_abs = statistics.ci_bernoulli(metrics_a, metrics_b, ci_prob=ci_prob, n=n)
                norm = (np.sum(metrics_a) + 1) / (len(metrics_a) + 2)
            case Distribution.UNDEFINED:
                a = np.median(metrics_a)
                b = np.median(metrics_b)
                effect_abs, p_value = statistics.effect_pvalue_mannwhitney(metrics_a, metrics_b, n=n)
                ci_abs = statistics.ci_mannwhitney(metrics_a, metrics_b, ci_prob=ci_prob, n=n)
                norm = a
            case _:
                raise ValueError(f'Incorrect distribution value: {ab_params.distribution}')

        if p_value >= ab_params.alpha_ab:
            result = ABResult.TIE
        elif np.abs(a - b) < ab_params.zero_threshold:
            result = ABResult.STRANGE_TIE
        elif ((a < b) and ab_params.greater_is_better) or ((a > b) and (not ab_params.greater_is_better)):
            result = ABResult.B
        elif ((a > b) and ab_params.greater_is_better) or ((a < b) and (not ab_params.greater_is_better)):
            result = ABResult.A
        if np.abs(norm) > ab_params.zero_threshold:
            effect_percent = statistics.n_significant_digits(float(effect_abs / norm * 100), n=n)
            ci_percent = ConfidenceInterval(
                statistics.n_significant_digits(float(ci_abs.lower_bound / norm * 100), n=n),
                statistics.n_significant_digits(float(ci_abs.upper_bound / norm * 100), n=n)
                )

    return ABSingleReport(result=result.value,
                          effect_abs=effect_abs,
                          effect_percent=effect_percent,
                          ci_abs_lower=ci_abs.lower_bound,
                          ci_abs_upper=ci_abs.upper_bound,
                          ci_percent_lower=ci_percent.lower_bound,
                          ci_percent_upper=ci_percent.upper_bound,
                          p_value=p_value
                          )


def ab_test_one_key(metrics_a: MetricSequence,
                    metrics_b: MetricSequence,
                    ab_params: ABParams,
                    ) -> pd.DataFrame:
    """
    Performs a single product key frequentist AB test of two hecate versions.
    The parameter to be compared is a single key metrics calculated for every day within
    the AB test time horizon with each hecate version.

    :param metrics_a: the older version of hecate forecast metrics_by_day
    :param metrics_b: the newer version of hecate forecast metrics_by_day to be compared with the older one
    :param ab_params: parameters required for AB tests ('add_metrics', 'metrics_name', 'alpha_ab', 'beta_ab', 'ci_prob',
                      'distribution', 'alpha_dist', 'greater_is_better', 'zero_threshold', 'min_len_test',
                      'min_len_norm', 'n_signif_digits')

    :return:
            result: 'A won' - mertics_a is statistically better than metrics_b in the sense defined by param lesser_wins
                    'B won' - mertics_b is statistically better than metrics_a in the sense defined by param lesser_wins
                    'tie' - there is no statistical difference between metrics_a and metrics_b
                    'zero tie' - both metrics are arrays of zeros (so, there is no statistical difference between them)
            effect_abs: difference between metrics centers (B - A) in absolute units.
                        The metrics center can be mean, median, and so on, depending on the metrics distribution.
            effect_percent: difference between metrics centers (B - A) in % of mean (or median at MW test) of metrics_a.
            ci_abs: (ci_prob * 100%)-confidence interval for the effect value in absolute units
            ci_percent: (ci_prob * 100%)-confidence interval for the effect value in % of mean (or median) of metrics_a.
            distrib: type of cumulative distribution function of both metrics_a and metrics_b
            mde: an estimation of minimal detectable effect in % of metrics_a' mean considering normal distribution
            p_value: p-value of corresponding test for equality of means (or medians) of metrics_a and metrics_b.
    """
    # TODO: add a case, when a part of metrics values are 0, and others follow lognorm distribution

    empty_report = ABSingleReport()

    if len(metrics_a) != len(metrics_b):
        raise ValueError('Metrics arrays must be of the same shape')

    mean_a = np.mean(metrics_a)
    len_a = len(metrics_a)

    mde = np.nan

    if len_a >= ab_params.min_len_test:
        if np.abs(mean_a) > ab_params.zero_threshold:
            mde = statistics.mde_norm(metrics_a,
                                      metrics_b,
                                      n=ab_params.n_signif_digits,
                                      alpha=ab_params.alpha_ab,
                                      beta=ab_params.beta_ab)

        if not ab_params.distribution:
            ab_params = replace(ab_params, distribution=statistics.distribution_check(metrics_a,
                                                                                      metrics_b,
                                                                                      ab_params.alpha_dist,
                                                                                      ab_params.min_len_norm)
                                )
            if not ab_params.distribution:
                report = pd.DataFrame(empty_report._asdict(), index=[0])
                report['distribution'] = ab_params.distribution
                report['mde'] = mde

                return report

        elif not isinstance(ab_params.distribution, Distribution):
            raise ValueError('Distribution value specified incorrectly. See docstring for correct values.')

        if isinstance(ab_params.distribution, Distribution):
            report = pd.DataFrame(ab_single(metrics_a=metrics_a, metrics_b=metrics_b, ab_params=ab_params)._asdict(),
                                  index=[0]
                                  )
            report['distribution'] = ab_params.distribution.value
            report['mde'] = mde

            return report

        else:
            raise ValueError("Distribution of nonempty metrics arrays takes an invalid value!")

    if isinstance(ab_params.distribution, Distribution):
        distrib = ab_params.distribution.value
    else:
        distrib = None
    report = pd.DataFrame(empty_report._asdict(), index=[0])
    report['distribution'] = distrib
    report['mde'] = mde

    return report


def prepare_input_data(data_a: pd.DataFrame,
                       data_b: pd.DataFrame,
                       add_metrics: bool = True,
                       metrics_name: MetricsNames = MetricsNames.AE,
                       ) -> pd.DataFrame:
    """
    Prepares a single dataset for AB test over all keys.

    :param data_a: the data with the older version of hecate forecast. Must contain columns: 'key', 'date',
                   'partition'; if add_metrics = True, must contain columns 'target' and 'forecast', if
                   add_metrics = False, must contain column 'metrics'.
    :param data_b: the newer version of hecate forecast to be compared with the older one. Must contain columns:
                   'key', 'date', 'partition'; if add_metrics = True, must contain columns 'target' and 'forecast',
                   if add_metrics = False, must contain column 'metrics'.
    :param add_metrics: if True metrics column to be added to the data by m.add_metrics_by_day(),
                        otherwise AB test will use metrics that already exist in both data_a and data_b.
    :param metrics_name: defines the type of metrics to be added to the data
            'AE' : metrics = abs(fc - target)

    :return: a dataframe with keys, dates and two metrics_by_date columns obtained from both datasets
    """

    if add_metrics:
        data_a, data_b = (metrics.add_metrics_by_date(df, metrics_name=metrics_name) for df in (data_a, data_b))

    data_a['metrics'], data_b['metrics'] = (
        df['metrics'] if df['metrics'].dtype == float else df['metrics'].astype(float) for df in (data_a, data_b)
    )

    cols = ['key', 'date', 'metrics']
    data_ab = data_a.loc[data_a.partition == 'fc', cols].merge(data_b.loc[data_b.partition == 'fc', cols],
                                                               on=['key', 'date'],
                                                               how='outer',
                                                               suffixes=['_a', '_b'])

    return data_ab


def detailed_report(data_a: pd.DataFrame,
                    data_ab: pd.DataFrame,
                    ab_params: ABParams
                    ) -> pd.DataFrame:
    """
    Forms detailed report for AB test over all keys.

    :param data_a: the data with the older version of hecate forecast.
    :param data_ab: a dataframe with keys, dates and two metrics_by_date columns
    :param ab_params: parameters required for AB tests ('add_metrics', 'metrics_name', 'alpha_ab', 'beta_ab', 'ci_prob',
                      'distribution', 'alpha_dist', 'greater_is_better', 'zero_threshold', 'min_len_test',
                      'min_len_norm', 'n_signif_digits')

    :return: test_report dataframe that contains key by key results of AB test: which commit is better,
             the effect size and confidence interval for it, distribution function of metrics, mde, weight.
             Weight is counted as sum target for given key devided by sum target for all keys.
    """

    sample_generator = data_ab.groupby('key')[['metrics_a', 'metrics_b']]
    temp_report = pd.DataFrame()
    failed_keys = []
    good_keys = []
    for key, sample in sample_generator:
        try:
            ABReport = ab_test_one_key(sample.metrics_a, sample.metrics_b, ab_params=ab_params)
            temp_report = pd.concat([temp_report, ABReport], ignore_index=True)
            good_keys.append(key)
        except Exception:
            failed_keys.append(key)
    logger.info(f'AB test finished successfully for {len(good_keys)} of {data_ab.key.nunique()} keys')

    if failed_keys:
        logger.error(f'AB test failed for {len(failed_keys)} following keys: {failed_keys}')

        if len(failed_keys) == data_ab.key.nunique():
            logger.critical('Ab test failed for all keys!')
            raise RuntimeError('Data is unsuitable for the AB test!')

    temp_report.insert(0, column='key', value=pd.Series(good_keys))

    weight = ((data_a.loc[data_a.key.isin(good_keys)].groupby('key')['target'].sum()
               / data_a.loc[data_a.key.isin(good_keys)]['target'].sum())
              .reset_index()
              .rename(columns={'target': 'weight'})
              .round(decimals=ab_params.n_signif_digits)
              )
    temp_report = temp_report.merge(weight, on=['key'], how='inner')

    return temp_report


def ab_test_all_keys(data_a: pd.DataFrame,
                     data_b: pd.DataFrame,
                     ab_params: ABParams,
                     ) -> pd.DataFrame:
    """
    Performs frequentist AB test of two versions of hecate forecast on the same dataset key by key.

    :param data_a: the data with the older version of hecate forecast. Must contain columns: 'key', 'date',
                   'partition'; if add_metrics = True, must contain columns 'target' and 'fc', if
                   add_metrics = False, must contain column 'metrics'.
    :param data_b: the newer version of hecate forecast to be compared with the older one. Must contain columns:
                   'key', 'date', 'partition'; if add_metrics = True, must contain columns 'target' and 'fc',
                   if add_metrics = False, must contain column 'metrics'.
    :param ab_params: parameters required for AB tests ('add_metrics', 'metrics_name', 'alpha_ab', 'beta_ab', 'ci_prob',
                      'distribution', 'alpha_dist', 'greater_is_better', 'zero_threshold', 'min_len_test',
                      'min_len_norm', 'n_signif_digits')

    :return: test_report dataframe that contains key by key results of AB test: which commit is better,
             the effect size and confidence interval for it, distribution function of metrics, mde, weight.
             Weight is counted as sum target for given key devided by sum target for all keys.
    """

    if ab_params.add_metrics is True:
        if not (set(['key', 'date', 'partition', 'target', 'fc'])
                .issubset(set(data_a.columns).intersection(set(data_b.columns)))):
            raise ValueError("""If add_metrics==True, both dataframes must contain columns:
                             'key', 'date', 'partition', 'target', 'fc'.""")
    else:
        if not (set(['key', 'date', 'partition', 'metrics'])
                .issubset(set(data_a.columns).intersection(set(data_b.columns)))):
            raise ValueError("""If add_metrics==False, both dataframes must contain columns:
                             'key', 'date', 'partition', 'metrics'.""")
        if np.any(np.isnan(data_a.metrics.to_numpy())):
            raise ValueError("NaN values found in 'metrics' column of the input data_a.")
        if np.any(np.isnan(data_b.metrics.to_numpy())):
            raise ValueError("NaN values found in 'metrics' column of the input data_b.")

    if len(data_a.loc[data_a.partition == 'fc']) != len(data_b.loc[data_b.partition == 'fc']):
        raise ValueError("Forecast period of both datafames must be of the same length.")

    data_a['date'], data_b['date'] = (
        df['date'] if df['date'].dtype == 'datetime64[ns]' else pd.to_datetime(df['date']) for df in (data_a, data_b)
    )
    if data_a.loc[data_a.partition == 'fc', 'date'].min() != data_b.loc[data_b.partition == 'fc', 'date'].min():
        raise ValueError("Forecast period of both dataframes must start at the same date.")
    if data_a.loc[data_a.partition == 'fc', 'date'].max() != data_b.loc[data_b.partition == 'fc', 'date'].max():
        raise ValueError("Forecast period of both dataframes must end at the same date.")

    data_ab = prepare_input_data(data_a=data_a,
                                 data_b=data_b,
                                 add_metrics=ab_params.add_metrics,
                                 metrics_name=ab_params.metrics_name
                                 )

    if np.any(np.isnan(data_ab[['metrics_a', 'metrics_b']].to_numpy())):
        raise ValueError("NaN values appered in 'metrics' columns of the preprocessed data.")

    temp_report = detailed_report(data_a=data_a, data_ab=data_ab, ab_params=ab_params)

    # add function adding product_group, product_category and project for each key
    # TODO: complete this part
    # data_columns = data_a.columns.union(data_b.columns)
    # if 'product_group' in data_columns:
    # if 'product_category' in data_columns:
    # if 'project' in data_columns:

    output_report = temp_report[list(ALL_KEYS_OUTPUT_COLUMNS)]

    return output_report
