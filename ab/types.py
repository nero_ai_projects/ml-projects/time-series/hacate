from dataclasses import dataclass
from enum import Enum
import numpy as np
import pandas as pd
from typing import Sequence, TypeAlias, NamedTuple


class Distribution(str, Enum):
    """
    Types of cumulative distribution function of a metrics that are available in AB test module
    """

    NORM = 'norm'
    LOGNORM = 'lognorm'
    BERNOULLI = 'bernoulli'
    UNDEFINED = 'undefined'


class ABResult(str, Enum):
    """
    Types of AB test result: which commit is better. It appears in ABReport (see below) as 'result'
    """
    desc: str

    def __new__(cls, name: str, desc: str):
        if not isinstance(name, str) or not isinstance(desc, str):
            raise TypeError(f"type of params {name=} or {desc=} is not str!")

        obj = str.__new__(cls, name)
        obj._value_ = name
        obj.desc = desc
        return obj

    A = ("A won", "mertics_a is statistically better than metrics_b in the sense defined by param greater_is_better")
    B = ("B won", "mertics_b is statistically better than metrics_a in the sense defined by param greater_is_better")
    TIE = ("tie", "there is no statistical difference between metrics_a and metrics_b")
    # ZERO_TIE = ("zero tie", "both metrics are arrays of zeros (so, there is no statistical difference between them)")
    STRANGE_TIE = ("strange tie",
                   """AB test shows difference, but A and B means (medians) are equal;
                   it may rise when the distribution function is wrongly set manually""")
    NO_DATA = ("not enough data", "the data is not enough for any AB test")


class ConfidenceInterval(NamedTuple):
    """
    class for confidence interval values
    """
    lower_bound: float = np.nan
    upper_bound: float = np.nan


class StatTestResult(NamedTuple):
    """
    class for statistics functions checking two datasets for equality and calculating effect size and p-value
    """
    effect: float
    pvalue: float


class ABSingleReport(NamedTuple):
    """
    class for the output of ab_single() in ab.py
    """
    result: ABResult = ABResult.NO_DATA
    effect_abs: float = np.nan
    effect_percent: float = np.nan
    ci_abs_lower: float = np.nan
    ci_abs_upper: float = np.nan
    ci_percent_lower: float = np.nan
    ci_percent_upper: float = np.nan
    p_value: float = np.nan


# Formats of data arrays that metrics can have to be treated in AB tests
MetricSequence: TypeAlias = list | pd.Series | np.ndarray | Sequence[float]


class MetricsNames(str, Enum):
    """
    Existing types:
        'AE' : metrics = abs(fc - target)

    Types to be implemented:
        'FA' : metrics = 1 - abs(fc - target) / mean(target); to get FA over the whole metrics for given key,
            calculate mean(metrics)
        'FB' : metrics = (target - fc) / mean(fc); to get FB over the whole metrics for given key,
            calculate mean(metrics)
        'MAPE' : metrics = 100 * abs(1 - fc / target), exists only if all target values are nonzero; to get MAPE
                over the whole metrics for given key, calculate mean(metrics)
        'MSE' : metrics = (target - fc)**2; to get MSE over the whole metrics for given key, calculate mean(metrics)
    """

    AE = "AE"
    FA = "FA"
    FB = "FB"
    MAPE = "MAPE"
    MSE = "MSE"


@dataclass
class ABParams:
    """
    Accumulates all AB test parameters in one object

    :param add_metrics: if True metrics column to be added to the data by m.add_metrics_by_day(),
                        otherwise AB test will use metrics that already exist in both data_a and data_b.
    :param metrics_name: defines the type of metrics to be added to the data
            'AE' : metrics = abs(fc - target)
    :param alpha_ab: AB test Type I Error (probability to omit significant difference of metrics_a and metrics_b)
    :param beta_ab: AB test Type II Error (probability to assume insignificant diffirence of metrics_a and metrics_b as
                    significant one)
    :param ci_prob: the confidence level that is the probability of the confidence interval to contain the truth value
                    of the compared parameter
    :param distribution: a cumulative distribution function of the parameters to be compared, defines the AB test type
                         to be performed:
                         Distribution.NORM - normal distribution, the test is Student's t-test
                         Distribution.LOGNORM - lognormal distribution, the test is Z-test for lognormally distributed
                                                data
                         Distribution.BERNOULLI - Bernoulli distribution, the test is Z-test for two binomial
                                                  proportions
                         Distribution.UNDEFINED - some other distribution, the test is Mahn-Whitney U test
    :param alpha_dist: significance level for tests on metrics' cumulative distribution function
    :param greater_is_better: if True greater value of metrics means better quality of the hecate version,
                              otherwise lesser metrics is better
    :param zero_threshold: required to define the equality ofany value to zero as
                           np.abs(value) < zero_threshold
    :param min_len_test: minimum metrics size required for AB testing.
    :param min_len_norm: minimum metrics size required for the distribution to be normal, or lognormal.
    :param n_signif_digits: number of significant digits for the output values to be rounded to

    """
    add_metrics: bool = True
    metrics_name: MetricsNames = MetricsNames.AE
    alpha_ab: float = 0.05
    beta_ab: float = 0.05
    ci_prob: float = 0.95
    distribution: Distribution | None = None
    alpha_dist: float = 0.01
    greater_is_better: bool = False
    zero_threshold: float = 1e-5
    min_len_test: int = 5
    min_len_norm: int = 30
    n_signif_digits: int = 4
