import traceback
from collections.abc import Callable
from functools import wraps
from io import StringIO
from typing import ParamSpec, TypeVar

import pandas as pd

from hecate.forecasting.skeleton import FCSampleResult
from hecate.forecasting.utils import EnvVar
from hecate.log import get_logger
from hecate.constants import REQUIRED_COLS_TYPES, REQUIRED_COLS
from hecate.features import FeatureType

logger = get_logger('hecate')

P = ParamSpec("P")
TSample = TypeVar("TSample", bound=FCSampleResult)
TBatch = TypeVar("TBatch", bound=pd.DataFrame)


def log_in_out_shape_wrapper(func: Callable[P, TBatch]) -> Callable[P, TBatch]:
    """
    The wrapper add record to log with information about shape and key amount in the first argument(if it is dataframe)
    and in the result
    :param func:
    :param args:
    :param kwards:
    :return:
    """
    @wraps(func)
    def wrapper(*args: P.args, **kwargs: P.kwargs) -> TBatch:
        if args and isinstance(args[0], pd.DataFrame):
            func_name = func.__name__
            shape = args[0].shape
            n_unique = args[0].key.nunique()

            msg = f'func {func_name} got input df with shape {shape} and unique key amount {n_unique}'
            logger.info(msg)
        result = func(*args, **kwargs)

        if isinstance(result, pd.DataFrame):
            func_name = func.__name__
            shape = result.shape
            n_unique = result.key.nunique()

            msg = f'func {func_name} returns df with shape {shape} and unique key amount {n_unique}'
            logger.info(msg)
        return result

    return wrapper


def log_incoming_df_info(func: Callable[P, TBatch]) -> Callable[P, TBatch]:
    """
    The wrapper add record to log with df.info()
    and in the result
    :param func:
    :param args:
    :param kwards:
    :return:
    """

    @wraps(func)
    def wrapper(*args: P.args, **kwargs: P.kwargs) -> TBatch:
        if args and isinstance(args[0], pd.DataFrame):
            buf = StringIO()
            args[0].info(buf=buf)
            logger.info(buf.getvalue())

        result = func(*args, **kwargs)

        return result

    return wrapper


def act_decompose_wrapper(func: Callable[P, TBatch]) -> Callable[P, TBatch]:
    """
    The wrapper changes partition of the input dataframe, if there's a keyword argument "change_partition".

    :param func: func to run (fc_batch)
    :param args: args for func
    :param kwargs: kwargs for func

    :return: result of running the passed func
    """
    @wraps(func)
    def wrapper(*args: P.args, **kwargs: P.kwargs) -> TBatch:
        """
        Wrapper for the func to change partition, and then return only fc partition result.
        """
        new_kwargs = {k: v for k, v in kwargs.items() if k != "act_decompose"}
        result = func(*args, **new_kwargs)

        if kwargs.get('act_decompose') and isinstance(args[0], pd.DataFrame) and isinstance(result, pd.DataFrame):
            args[0]['date'] = pd.to_datetime(args[0].date, format='%Y-%m-%d')
            result = result.merge(args[0][['key', 'date', 'partition']])
            result = result[result.partition == 'fc'].drop(columns=['partition'])

        return result

    return wrapper


def check_datatypes(func: Callable[P, FCSampleResult]) -> Callable[P, FCSampleResult]:
    """
    Checks to see if mandatory columns are of expected datatypes. Stops processing otherwise.
    NB! Columns are converted to the FIRST data type, listed for the column in hecate.constants.REQUIRED_COLS_TYPES.
    """

    @wraps(func)
    def wrapper(*args: P.args, **kwargs: P.kwargs) -> FCSampleResult:
        match args, kwargs:
            case (pd.DataFrame() as data, dict() as feature_types, *_), dict():
                pass
            case (pd.DataFrame() as data, *_), {"feature_types": feature_types}:
                pass
            case *_, {'data': data, "feature_types": feature_types}:
                pass
            case (pd.DataFrame() as data, *_), dict():
                pass
            case _:
                raise ValueError

        key = data.key.values[0]

        for col, available_dtypes in REQUIRED_COLS_TYPES.items():
            dtype = data[col].dtype
            if dtype not in available_dtypes:
                try:
                    data[col] = data[col].astype(available_dtypes[0])
                except ValueError:
                    trace = traceback.format_exc()
                    logger.exception(f'unexpected datatype for {col} column for key {key}')

                    return FCSampleResult(key, is_failed=True, trace=trace)

        for col in data.columns:
            if not (
                col in REQUIRED_COLS
                or (col in data.select_dtypes(include=['number', 'bool']).columns)
                or (feature_types.get(col) in [FeatureType.dim, FeatureType.label, FeatureType.can_level])
            ):
                try:
                    data[col] = pd.to_numeric(data[col])
                except ValueError:
                    trace = traceback.format_exc()
                    logger.exception(f'unexpected datatype for {col} column for key {key}')

                    return FCSampleResult(key, is_failed=True, trace=trace)

        return func(*args, **kwargs)

    return wrapper


def extend_fc_sample_result(func: Callable[P, FCSampleResult]) -> Callable[P, FCSampleResult]:
    @wraps(func)
    def wrapper(*args: P.args, **kwargs: P.kwargs) -> FCSampleResult:

        try:
            result = func(*args, **kwargs)

        except Exception:

            trace = traceback.format_exc()
            key = 'unknown'
            if args and isinstance(args[0], pd.DataFrame):
                key = args[0].key.values[0]

            logger.exception(f'fc_sample failed for key {key}')
            result = FCSampleResult(key, is_failed=True, trace=trace)

        return result

    return wrapper


def log_running_mode_wrapper(func: Callable[P, TBatch]) -> Callable[P, TBatch]:
    """
    The wrapper adds record to log with the HECATE_ENV setting.
    """
    if EnvVar.is_debug_environ():
        logger.info('HECATE_ENV set to dev, running in development mode')

    @wraps(func)
    def wrapper(*args: P.args, **kwargs: P.kwargs) -> TBatch:
        """
        Running fc_batch
        """
        result = func(*args, **kwargs)

        return result

    return wrapper
