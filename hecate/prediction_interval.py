"""
Performs calculation of a prediction interval (PI) for a given forecast.
Definition: X%-PI contains X% of the target values at the fc period
"""

import pandas as pd
import numpy as np
from scipy import stats

from hecate.log import get_logger
from hecate.features import TypesOfFeatures, FeatureType, get_cols_from_types
from hecate.forecasting.skeleton import FCSampleResult, ForecastingTask
from hecate.forecasting.decompositions.strategies import DecompositionStrategies
from hecate.types import Drivers

logger = get_logger('hecate')


class PredictionIntervalError(Exception):
    pass


def partition_mapper(dates: pd.Series, date_min: pd.Timestamp, date_max: pd.Timestamp) -> np.ndarray:
    """
    Maps 'train', 'fc' and 'non relevant' periods anew.

    :param dates: a series of dates to be mapped
    :param date_min: starting date of the 'fc' period
    :param date_max: ending date of the 'fc' period

    :return: an array of the new partition values
    """
    if date_min >= date_max:
        raise ValueError("date_max must be greater than date_min.")

    return np.select([dates < date_min, dates > date_max], ['train', 'non relevant'], 'fc')


def add_dummy_pi_columns(fc_result: FCSampleResult, pi_prob_list: list[float] | None = None) -> FCSampleResult:
    """
    Adds corresponding PI_up and PI_down columns to fc_result, which are equal to 'fc' column.

    :param fc_result: single key FCSampleResult that contains output data and meta information from fc_sample()
    :param pi_prob_list: a list of probabilities for the prediction interval(s). If None, the default value is 0.68

    :return: fc_result with added dummy columns of prediction interval(s) buondaries
    """
    if pi_prob_list is None:
        pi_prob_list = [0.68]

    for pi_prob in pi_prob_list:
        fc_result.data[f'PI_up_{pi_prob*100}%'] = fc_result.data.fc
        fc_result.data[f'PI_down_{pi_prob*100}%'] = fc_result.data.fc

    fc_result.output_columns = fc_result.data.columns.to_list()

    return fc_result


def add_promo_flag(data: pd.DataFrame, feature_types: TypesOfFeatures) -> tuple[pd.DataFrame, str]:
    """
    Adds promo flag column (bolean) to a dataframe

    :param data: pd.DataFrame
    :param feature_types: dict

    :return: the dataframe with the added column and the name of the promo flag column
    """
    promo_column = 'promo_flag_for_pi'
    if len(feature_types) == 0:
        data[promo_column] = False
    else:
        promo_cols = get_cols_from_types(feature_types,
                                         [FeatureType.discount, FeatureType.promo, FeatureType.promo_budget])
        promo_cols = [col for col in promo_cols if col in data.columns]
        if len(promo_cols) == 0:
            data[promo_column] = False
        else:
            data[promo_column] = data[promo_cols].fillna(0).prod(axis=1).astype(bool)

    return data, promo_column


def get_deviation_at_test(data: pd.DataFrame) -> pd.DataFrame:
    """
    Returns data with 'deviation_at_test' column that is deviation column (deviation = target - fc) at test period
    and 0 elsewhere instead of 'fc' column.
    The test period is the last fold at the train period with the duration of the fc period.

    :param data: a single key data containing 'date', 'target', 'partition_for_pi' and 'fc' columns

    :return: the dataframe with new column 'deviaion_pred' instead of 'fc'
    """

    data['deviation_at_test'] = np.where(data.partition_for_pi == 'fc', data.target - data.fc, 0)
    data.drop(columns=['fc'], inplace=True)

    return data


def split_deviation_by_promo(data: pd.DataFrame, promo_column: str) -> tuple[np.ndarray, np.ndarray]:
    """
    Splits 'deviation' column to two arrays: one for promo periods only, another for periods without promo.
    If non-promo period is too short, it will include promo periods as well.

    :param data: the folowwing columns are required: 'partition_for_pi', 'deviation_at_test', promo_column
    :param list_data_columns: the whole data columns list
    :param promo_column: a name of a column for mapping promo periods

    :return: a tuple of arrays of deviation at no promo and deviation at promo at the fc period
    """
    if promo_column in data.columns:
        is_fc_partition = data.partition_for_pi == 'fc'
        is_promo = data[promo_column] != 0
        array_no_promo = data.loc[is_fc_partition & ~is_promo, 'deviation_at_test'].to_numpy()
        array_promo = data.loc[is_fc_partition & is_promo, 'deviation_at_test'].to_numpy()
    else:
        raise ValueError(f"Get wrong name of promo_column: {promo_column}")

    if (array_no_promo.size + array_promo.size < 10) or (array_no_promo.size < 5):
        return np.hstack((array_no_promo, array_promo)), array_promo

    return array_no_promo, array_promo


def get_hdi_bounds(
    array_no_promo: np.ndarray, array_promo: np.ndarray, pi_prob: float, min_promo_len: int
) -> tuple[float, float, float, float]:
    """
    Calculates upper and lover bounds of deviation arrays for a given probability level for HDI approach

    :param array_no_promo: array of deviations at forecast periods without promo
    :param array_promo: array of deviations at forecast periods with promo
    :param pi_prob: the required probability level of the prediction interval to be calculated

    :return: a tuple of upper and lower bounds for non-promo and promo deviation arrays
    """

    failed_no_promo = False
    if array_no_promo.size > 0:
        lower_bound_no_promo, upper_bound_no_promo = np.quantile(
            array_no_promo, ((1 - pi_prob) / 2, (1 - (1 - pi_prob) / 2))
            )
    else:
        failed_no_promo = True

    if array_promo.size >= min_promo_len:
        lower_bound_promo, upper_bound_promo = np.quantile(
            array_promo, ((1 - pi_prob) / 2, (1 - (1 - pi_prob) / 2))
            )
        if failed_no_promo:
            lower_bound_no_promo, upper_bound_no_promo = (lower_bound_promo, upper_bound_promo)
    else:
        if failed_no_promo:
            lower_bound_no_promo = upper_bound_no_promo = lower_bound_promo = upper_bound_promo = np.nan
        else:
            lower_bound_promo, upper_bound_promo = (lower_bound_no_promo, upper_bound_no_promo)

    return (lower_bound_no_promo, upper_bound_no_promo, lower_bound_promo, upper_bound_promo)


def get_method_from_mode(mode: str) -> tuple[bool, bool]:
    """
    Get 'norm' and 'hdi' flags from the mode of statistical method for prediction interval calculation

    :param mode: mode of PI calculation, when 'norm' it's based on normal distribution,
                 'hdi' uses corresponding quantiles of real data, and 'both' yields the average of the two.

    :return: a tuple of bolean flags 'norm' and 'hdi'
    """

    if mode == 'norm':
        norm = True
        hdi = False
    elif mode == 'hdi':
        norm = False
        hdi = True
    elif mode == 'both':
        norm = True
        hdi = True
    else:
        raise ValueError("The 'mode' parameter can be 'norm', 'hdi', or 'both' only")

    return norm, hdi


def stat_pred_interval(data: pd.DataFrame, promo_column: str, mode: str = 'both', pi_prob: float = 0.68,
                       broadening: bool = False, bias: bool = False) -> pd.DataFrame:
    """
    Calculates prediction interval (PI) based on statistical approach and adds its lower and upper bounds to the
    input data as new columns.
    Promo periods have a particular treatment, since the distribution of forecast deviations from the target at promo
    differs from one at non-promo periods. But if the total promo period at train is too short, non-promo statistics is
    applied to the promo periods forecasting.

    :param data: a single key dataframe, must contain columns: ['date', 'partition_for_fc', 'partition_for_pi',
                 'deviation_at_test', 'fc']
    :param mode: mode of PI calculation, when 'norm' it's based on normal distribution,
                 'hdi' uses corresponding quantiles of real data, and 'both' yields the average of the two.
    :param pi_prob: the required probability level of the PI to be calculated
    :param promo_column: a name of a column for mapping promo periods. If given must be 0 for non-promo periods.
    :param broadening: if True takes into account the broadening of the PI with time as for a random value
    :param bias: if True takes into account the average bias of the forecast at the test fold

    :return: the dataframe with 2 new columns: f'PI_up_{pi_prob*100}%' and f'PI_down_{pi_prob*100}%'
    """

    mode = mode.lower()
    min_promo_len = 5
    pi_up = f'PI_up_{pi_prob*100}%'
    pi_down = f'PI_down_{pi_prob*100}%'

    date_min = data.loc[data.partition_for_fc == 'fc', 'date'].min()
    norm, hdi = get_method_from_mode(mode)

    array_no_promo, array_promo = split_deviation_by_promo(
        data=data[['partition_for_pi', 'deviation_at_test', promo_column]],
        promo_column=promo_column
    )

    len_no_promo, len_promo, total_len = (array_no_promo.size, array_promo.size, (data.partition_for_pi == 'fc').sum())
    mean_no_promo = np.mean(array_no_promo) if len_no_promo > 0 else 0
    mean_promo = np.mean(array_promo) if len_promo >= min_promo_len else mean_no_promo
    norm_days = ((1 + total_len) * total_len / 2) ** 0.5

    if norm:
        sigma_no_promo = np.std(array_no_promo) if len_no_promo > 0 else 0
        sigma_promo = np.std(array_promo) if len_promo >= min_promo_len else sigma_no_promo
        coef_sigma = stats.norm.ppf(1 - (1 - pi_prob)/2)
    else:
        sigma_no_promo = sigma_promo = coef_sigma = 0

    if hdi & (total_len > 0):
        lower_hdi_no_promo, upper_hdi_no_promo, lower_hdi_promo, upper_hdi_promo = get_hdi_bounds(
            array_no_promo, array_promo, pi_prob, min_promo_len
        )
    else:
        lower_hdi_no_promo = upper_hdi_no_promo = lower_hdi_promo = upper_hdi_promo = 0

    conds = [(data.date >= date_min) & (data[promo_column] == 0), (data.date >= date_min) & (data[promo_column] != 0)]
    pi_up_no_promo = (
        data.fc + bias * mean_no_promo
        + (norm * sigma_no_promo * coef_sigma + hdi * (upper_hdi_no_promo - mean_no_promo)) / (norm + hdi)
        * (1 + broadening * (data.date - date_min).dt.days / norm_days) ** 0.5
        )
    pi_up_promo = (
        data.fc + bias * mean_no_promo
        + (norm * sigma_promo * coef_sigma + hdi * (upper_hdi_promo - mean_promo)) / (norm + hdi)
        * (1 + broadening * (data.date - date_min).dt.days / norm_days) ** 0.5
        )
    pi_down_no_promo = (
        data.fc + bias * mean_no_promo
        + (hdi * (lower_hdi_no_promo - mean_no_promo) - norm * sigma_no_promo * coef_sigma) / (norm + hdi)
        * (1 + broadening * (data.date - date_min).dt.days / norm_days) ** 0.5
        )
    pi_down_promo = (
        data.fc + bias * mean_no_promo
        + (hdi * (lower_hdi_promo - mean_promo) - norm * sigma_promo * coef_sigma) / (norm + hdi)
        * (1 + broadening * (data.date - date_min).dt.days / norm_days) ** 0.5
        )
    data[pi_up] = np.select(conds, [pi_up_no_promo, pi_up_promo], data.fc)
    data[pi_down] = np.select(conds, [pi_down_no_promo, pi_down_promo], data.fc)

    data[pi_down].clip(lower=0, inplace=True)
    data[pi_up].clip(lower=0, inplace=True)

    return data


def pi_stat_preproc(data: pd.DataFrame, feature_types: TypesOfFeatures) -> tuple[pd.DataFrame, str]:
    """
    Preprocessing of a single key data for prediction interval calculations:
    1) add two partition columns 'partition_for_fc' that is the initial partition used for forecasting and
    'partition_for_pi' that is the partition used for calculations of fc deviations from target at the test period,
    which is right before the fc period.
    2) add promo flag column to treat promo and non-promo periods separately

    :param data: the single key data to be preprocessed
    :param feature_types: a dict of features used for the forecast

    :return: the preprocessed data and the name of promo flag column
    """

    date_min, date_max = data.loc[data.partition == 'fc', 'date'].agg([min, max])
    delta = date_max - date_min + pd.to_timedelta(1, 'D')
    if data.date.min() >= date_min - delta:
        raise PredictionIntervalError("Train period must be longer than forecast one.")

    data_preproc = data.copy()
    data_preproc['partition_for_fc'] = data_preproc.partition
    data_preproc['partition_for_pi'] = partition_mapper(data_preproc.date, date_min - delta, date_max - delta)
    data_preproc['partition'] = data_preproc.partition_for_pi

    data_preproc, promo_column = add_promo_flag(data_preproc, feature_types)

    return data_preproc, promo_column


def pi_stat_sample(
        fc_result: FCSampleResult,
        data_pi: pd.DataFrame,
        promo_column: str,
        pi_prob_list: list[float] | None = None,
        pi_stat_mode: str = 'both',
) -> FCSampleResult:
    """
    Calculates prediction interval(s) of given probability(ies) with 'statistical' method for single key data

    :param fc_result: single key FCSampleResult that contains output data and meta information from fc_sample()
    :param data_pi: single key data preprocessed for prediction interval calculations
    :param promo_column: the name of promo flag column
    :param pi_prob_list: a list of probabilities for the prediction interval(s). If None, the default value is 0.68
    :param pi_stat_mode: defines the way of of PI calculation, when 'norm' it's based on normal distribution,
                         'hdi' uses corresponding quantiles of real data, and 'both' yields the average of the two.

    :return: the input FCSampleResult with added columns of upper and lower boundaries of the calculated prediction
             interval(s)
    """
    if pi_prob_list is None:
        pi_prob_list = [0.68]

    columns_to_drop_from_data_pi = list(set(['target']) & set(fc_result.output_columns))
    data_pi = (
        get_deviation_at_test(data_pi)
        .drop(columns=columns_to_drop_from_data_pi)
    )
    fc_result.data = fc_result.data.merge(data_pi, on=['date'], how='inner')

    for pi_prob in pi_prob_list:
        fc_result.data = stat_pred_interval(
            data=fc_result.data, promo_column=promo_column, mode=pi_stat_mode, pi_prob=pi_prob, broadening=False
            )

    columns_to_drop_from_fc_result = list(
        set(['partition_for_pi', 'partition_for_fc', 'deviation_at_test', promo_column, 'target'])
        - set(columns_to_drop_from_data_pi)
        )
    fc_result.data.drop(columns=columns_to_drop_from_fc_result, inplace=True)
    fc_result.output_columns = fc_result.data.columns.to_list()

    return fc_result


def check_pi_conditions(data: pd.DataFrame, pi_method: str, pi_prob_list: list[float] | None) -> bool:
    """
    Checks that input data and arguments of fc_batch() and fc_sample() fit for prediction interval calculations

    :param data: single- or multiple-key pd.DataFrame
    :param pi_method: the method for prediction interval(s) calculation
    :param pi_prob_list: gives a list of probabilities for the prediction interval(s)

    :return: True if conditions are met
    """
    if pi_method != 'statistical':
        logger.warning(f'Prediction interval method {pi_method} not implemented yet.')

        return False

    if isinstance(pi_prob_list, list):
        for pi_prob in pi_prob_list:
            if (pi_prob < 0) or (pi_prob > 1):
                logger.warning("All probabilities in pi_prob_list must belong to the closed interval [0, 1]")

                return False

    number_of_keys = data.key.nunique()
    if number_of_keys == 1:
        if 'time_freq' in data.columns:
            cond = ((data.time_freq.unique()[0] != 'D') or (data.time_freq.nunique() > 1))
        else:
            cond = (pd.infer_freq(data.date) != 'D')
        if cond:
            logger.warning("Can calculate prediction interval for time_freq == 'D' only")

        return not cond

    elif number_of_keys > 1:
        if ('time_freq' in data.columns) & ((data.time_freq.unique()[0] != 'D') or (data.time_freq.nunique() > 1)):
            logger.warning("Can calculate prediction interval for time_freq == 'D' only")

            return False

    else:
        logger.warning(f"Got unexpected number of keys {number_of_keys}")

        return False

    return True


def calculate_prediction_interval(
        fc_task: ForecastingTask,
        fc_result: FCSampleResult,
        data: pd.DataFrame,
        feature_types: TypesOfFeatures,
        drivers: Drivers | None = None,
        dsd: DecompositionStrategies | None = None,
        decimals: int | None = 2,
        pi_prob_list: list[float] | None = None,
        pi_method: str = 'statistical',
        pi_stat_mode: str = 'both',
        apply_check_pi_conditions: bool = True
) -> FCSampleResult:
    """
    Performs the pipeline for predicion interval calculations within hecate.fc_sample()

    :param fc_task: ForecastingTask configured the same as for forecast
    :param fc_result: FCSampleResult that contains output data and meta information after forecast
    :param data: pd.DataFrame, input single key data for forecast
    :param feature_types: dict, types of features to be used in forecast
    :param drivers: a list containing forecast drivers to be calculated and returned in an output dataframe
                    (possible list elements: 'bl', 'cn'). Default value is None (all drivers are calculated).
    :param dsd: short for Decomposition Strategy Dictionary: dict for methods to use to calculate various figures
    :param decimals: number of decimal places for output data. Without rounding - None
    :param pi_prob_list: a list of probabilities for the prediction interval(s). If None, the default value is 0.68
    :param pi_method: the method for prediction interval(s) calculation; can take values: 'statistical'.
    :param pi_stat_mode: aplicable when pi_method is 'statistical'; defines the way of of PI calculation, when 'norm'
                         it's based on normal distribution, 'hdi' uses corresponding quantiles of real data, and
                         'both' yields the average of the two.
    :param apply_check_pi_conditions: If True, allows check that input params fit for prediction interval calculations

    :return: input fc_result with added data and meta information on calculated prediction interval(s).
    """

    if apply_check_pi_conditions:
        if not check_pi_conditions(data, pi_method, pi_prob_list):
            fc_result = add_dummy_pi_columns(fc_result, pi_prob_list)
            logger.warning(f"Prediction interval(s) failed for key '{fc_result.key}'; return dummy PI values.")

            return fc_result

    if pi_method == 'statistical':
        try:
            data_pi, promo_column = pi_stat_preproc(data, feature_types)

        except PredictionIntervalError:
            fc_result = add_dummy_pi_columns(fc_result, pi_prob_list)
            logger.warning(f"Prediction interval(s) failed for key '{fc_result.key}'; return dummy PI values.",
                           exc_info=True)

            return fc_result

        data_pi = data_pi[['date', 'partition_for_pi', 'partition_for_fc', 'target', promo_column]].merge(
            fc_task.execute(
                data=data_pi.drop(columns=['partition_for_pi', 'partition_for_fc', promo_column]),
                feature_types=feature_types,
                drivers=drivers,
                dsd=dsd,
                add_columns=None,
                decimals=decimals
                ).data[['date', 'fc']],
            how='left',
            on=['date']
            )

        fc_result = pi_stat_sample(fc_result, data_pi, promo_column, pi_prob_list, pi_stat_mode)

    return fc_result
