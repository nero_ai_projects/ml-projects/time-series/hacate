import pytest
import os
import pandas as pd

PATH_DIR = os.path.join(os.getcwd().split('tests')[0], 'tests', 'tests')
AB_DIR = os.path.join(PATH_DIR, 'ab')


@pytest.fixture(params=os.listdir(AB_DIR), ids=os.listdir(AB_DIR))
def ab_test_input(request):
    """
    Read test AB report from the folder.
    """
    data_a = pd.read_csv(os.path.join(AB_DIR, request.param, 'dataset_a.csv'), index_col=0)
    data_b = pd.read_csv(os.path.join(AB_DIR, request.param, 'dataset_b.csv'), index_col=0)
    report = pd.read_json(os.path.join(AB_DIR, request.param, 'result.json'))

    return data_a, data_b, report
