from dataclasses import replace
import os
import pandas as pd
import sys
import numpy as np

from hecate.hecate import fc_batch
from hecate.types import Decompositors
from hecate.log import get_logger
from ab.ab import ab_test_all_keys
from ab.types import ABParams
from metriclog.constants import MAIN_HECATE_FC_PARAMS, HORIZONS
from metriclog.types import HecateVersion, HecateVersionsParams
from metriclog import run_ab, repo
from metriclog.database import Database
from metriclog.utils import create_metrics_table


logger = get_logger('hecate')


def set_train_test(data: pd.DataFrame, fold: int, horizon: HORIZONS, project: str, horizon_and_folds: pd.DataFrame
                   ) -> pd.DataFrame:
    """
    Function sets partition for data as train/test/non relevant based on horizon and folds

    :param data: project data table
    :param fold: number of current fold
    :param horizon: current horizon
    :param project: current project
    :param horizon_and_folds: dataframe of dates for horizons and folds

    :return: data with requested partitions
    """
    if horizon == 'st':
        len_min = 1
        len_max = 7 * 6
    elif horizon == 'mt':
        len_min = 7 * 6
        len_max = 7 * 27
    elif horizon == 'lt':
        len_min = 7 * 27  # half year
        len_max = np.inf
    else:
        raise ValueError('Set unknown horizon name')

    start_date = horizon_and_folds.loc[(horizon_and_folds.project == project) &
                                       (horizon_and_folds.fold == fold) &
                                       (horizon_and_folds.horizon == horizon),
                                       'forecast_start'].values[0]
    end_date = horizon_and_folds.loc[(horizon_and_folds.project == project) &
                                     (horizon_and_folds.fold == fold) &
                                     (horizon_and_folds.horizon == horizon),
                                     'forecast_end'].values[0]
    conditions = [data['date'] < start_date,
                  (data['date'] >= start_date) & (data['date'] <= end_date),
                  data['date'] > end_date]
    choices = ["train", 'fc', 'non relevant']
    data["partition"] = np.select(conditions, choices)

    start_date = pd.to_datetime(start_date).normalize()
    end_date = pd.to_datetime(end_date).normalize()
    days_horizon = (end_date - start_date).days + 1
    if len_min <= days_horizon <= len_max:
        return data
    else:
        raise ValueError(f"The set horizon name {horizon} doesn't fit the dates range of the data: {days_horizon} days")


def set_output_table(results: pd.DataFrame, data_current: pd.DataFrame, hecate_versions_params: HecateVersionsParams
                     ) -> pd.DataFrame:
    """
    Function adds target column and columns with run info to the results table

    :param results: output of hecate run
    :param data_current: project data table
    :param hecate_versions_params: hecate versions parameters of interest (branch, commit_id, project, fold, focus,
                                   horizon)

    :return: results table to be saved
    """
    results['date'] = results['date'].dt.date
    results = results[['key', 'date', 'fc']]
    results = pd.merge(results, data_current[['key', 'date', 'target', 'product_group',	'product_category',
                                              'chain_of_stores', 'partition']], on=['key', 'date'], how='inner')
    results.loc[results.fc.isna(), 'fc'] = 0
    results['project'] = hecate_versions_params.project
    for param in MAIN_HECATE_FC_PARAMS:
        param_val = getattr(hecate_versions_params, str(param))
        results[param] = param_val

    return results


def create_output_folders(project: str, output_dir: str = "./outputs/") -> str:
    """
    Function creates folders for output saving if they not exist already

    :param project: current project
    :param output_dir: path to dir with output projects

    :return: output_dir
    """

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    output_project_dir = f"{output_dir}{project}/"
    if not os.path.exists(output_project_dir):
        os.mkdir(output_project_dir)

    return output_project_dir


def save_file_locally(df: pd.DataFrame, hecate_versions_params: HecateVersionsParams, file_type: str) -> str:
    """
    Function saves output file locally

    :param df: datframe to be saved
    :param hecate_versions_params: hecate versions parameters of interest (branch, commit_id, project, fold, horizon,
                                   output_dir)
    :param file_type: e.g. results or input

    :return: the path where the DataFrame was saved
    """
    output_project_dir = create_output_folders(hecate_versions_params.project, hecate_versions_params.output_dir + '/')
    branch = hecate_versions_params.branch
    commit_id = hecate_versions_params.commit_id
    horizon = hecate_versions_params.horizon
    fold = hecate_versions_params.fold
    file_name = f"{file_type}_branch_{branch}_commit_{commit_id}_horizon_{horizon}_fold_{fold}.parquet"
    output_path = output_project_dir + file_name
    output_path_old = output_project_dir + "." + file_name
    if os.path.exists(output_path):
        os.rename(output_path, output_path_old)
        logger.info('I will rewrite existing file')
    try:
        df.to_parquet(output_path, index=False)
    except KeyboardInterrupt:
        if os.path.exists(output_path):
            os.rename(output_path_old, output_path)
        sys.exit(1)
    else:
        logger.info(f'The {file_type} is saved to ' + hecate_versions_params.output_dir)
        if os.path.exists(output_path_old):
            os.remove(output_path_old)

    return output_path


def filter_project_horizon_and_fold(horizon_and_folds: pd.DataFrame, projects: list[str] | str,
                                    horizons: list[HORIZONS] | HORIZONS, n_folds: int) -> pd.DataFrame:
    """
    Function returns existing in the database combinations among given projects, horizons and folds.

    :param horizon_and_folds: dataframe of dates for projects, horizons and folds
    :param projects: a desired project, or a list of them
    :param horizons: a desired horizon, or a list of them
    :param n_folds: the number of desired folds. Use -1 to choose all available folds

    :return: dataframe of dates for desired projects, horizons and folds that exist in the DB
    """
    if type(projects) is str:
        projects = list(horizon_and_folds.project.unique()) if projects == 'all' else [projects]
    if type(horizons) is str:
        horizons = list(horizon_and_folds.horizon.unique()) if horizons == 'all' else [horizons]
    folds = list(horizon_and_folds.fold.unique()) if n_folds == -1 else list(range(n_folds))

    return (horizon_and_folds.query(f"project.isin({projects})")
            .query(f"horizon.isin({horizons})").query(f"fold.isin({folds})"))


def run_hecate_on_data_from_db(hecate_versions_params: HecateVersionsParams, db_connection: Database,
                               ab_mode: bool = False):
    """
    For each input project, horizon, and fold Function extracts data from database and
    runs hecate forecasting and saves the results in DB or locally

    :param hecate_versions_params: hecate parameters required for the forecast (branch, commit_id, project, fold, focus,
                                   horizon, bl, n_folds, n_cores, save_locally, save_input_file_locally, output_dir)
    :param db_connection: parameters of connection to a database (password, db_ip, db_name, user_name)
    :param ab_mode: True when running in AB tests

    :return: None
    """
    project = hecate_versions_params.project
    repo.check_unstaged_changes()
    horizon_and_folds = db_connection.extract_horizon_and_folds()
    horizon_and_folds = filter_project_horizon_and_fold(horizon_and_folds, project, hecate_versions_params.horizon,
                                                        hecate_versions_params.n_folds)
    if ab_mode:
        commit_id, branch = (hecate_versions_params.commit_id, hecate_versions_params.branch)
        horizon_and_folds = horizon_and_folds[horizon_and_folds.fold == hecate_versions_params.fold]
    else:
        commit_id, branch = repo.get_branch_and_last_commit_id()

    for project, mdf in horizon_and_folds.groupby('project'):
        print('\nProcessing project ' + project)
        data = db_connection.extract_dataset(project, hecate_versions_params.focus)
        if data.empty:
            logger.warning(f"No required for forecasting data found in the database for project == '{project}' "
                           f"and focus == {hecate_versions_params.focus}")
            if ab_mode:
                return pd.DataFrame()
            continue
        feature_types = db_connection.extract_feature_types(project)
        for ind, row in mdf.iterrows():
            hvp = replace(hecate_versions_params,
                          horizon=row.horizon, fold=row.fold, branch=branch, commit_id=commit_id)
            logger.info(f'Processing horizon {row.horizon} and fold {row.fold + 1}...')
            data_current = set_train_test(data, row.fold, row.horizon, project, horizon_and_folds)
            if hecate_versions_params.save_input_file_locally:
                save_file_locally(data_current, hvp, 'input')
            if hecate_versions_params.bl:
                decompose: Decompositors = ['bl']
            else:
                decompose: Decompositors = list()
            parallel = hecate_versions_params.n_cores > 1 if hecate_versions_params.n_cores is not None else False
            results = fc_batch(data_current, feature_types=feature_types, decompose=decompose, parallel=parallel)
            results = set_output_table(results, data_current, hvp)
            metrics_table = create_metrics_table(results, hvp)
            if ab_mode:
                return results
            if not hecate_versions_params.save_locally:
                error_send_results = db_connection.send_results_to_db(results, hvp)
                error_send_metrics = db_connection.send_metrics_to_db(metrics_table, hvp)
                if any([error_send_results, error_send_metrics]):
                    logger.info('I can not write to the database. I will save the results locally instead')
            if any([hecate_versions_params.save_locally, error_send_results, error_send_metrics]):
                save_file_locally(results, hvp, 'result')
                save_file_locally(metrics_table, hvp, 'metrics')


def get_data(db_connection: Database, commit_id: str, current_commit_id: str,
             hecate_versions_params: HecateVersionsParams) -> pd.DataFrame:
    """
    Auxillary function for run_ab_test() getting data from the database or from the forecast by corresponding hecate
    version

    :param db_connection: parameters of connection to a database (password, db_ip, db_name, user_name)
    :param commit_id: the commit id of the old hecate version required for the forecast
    :param current_commit_id: the commit id of the newest hecate version
    :param hecate_versions_params: hecate versions parameters of interest

    :return: the required data
    """

    data = db_connection.select_results_if_exist(hecate_versions_params=hecate_versions_params,
                                                 table='results_' + hecate_versions_params.project.lower())
    if data.empty:
        repo.switch_to_hecate_version(commit_id)
        data = run_hecate_on_data_from_db(hecate_versions_params, db_connection, ab_mode=True)
        repo.switch_to_hecate_version(current_commit_id)

    return data


def run_ab_test(hecate_versions_params: HecateVersionsParams, ab_params: ABParams, db_connection: Database,
                rewrite_ab_results: bool = False, rewrite_ab_summary: bool = False):
    """
    Pipeline for running AB test of forecast by two different hecate versions on a given project data.

    :param hecate_versions_params: hecate versions parameters of interest (branch_a, commit_a_id, branch_b, commit_b_id,
                                   project, fold, focus, horizon, bl, n_folds, n_cores,
                                   save_locally, save_input_file_locally, output_dir)
    :param ab_params: parameters required for AB tests ('add_metrics', 'metrics_name', 'alpha_ab', 'beta_ab', 'ci_prob',
                      'distribution', 'alpha_dist', 'greater_is_better', 'zero_threshold', 'min_len_test',
                      'min_len_norm', 'n_signif_digits')
    :param db_connection: parameters of connection to a database (password, db_ip, db_name, user_name)
    :param rewrite_ab_resuts: if True performs AB test with given params anew and rewrite the results to the database,
                              otherwise uses existing data
    :param rewrite_ab_summary: if True performs AB test with given params anew and rewrite the results to the database,
                              otherwise uses existing data

    :return: None
    """

    # TODO: consider the case bl = True
    hecate_versions_params = replace(hecate_versions_params, bl=False)

    project = hecate_versions_params.project.lower()
    branch_a, branch_b = hecate_versions_params.branch_a, hecate_versions_params.branch_b
    commit_a_id, commit_b_id = hecate_versions_params.commit_a_id, hecate_versions_params.commit_b_id
    hecave_versions_params_a = hecate_versions_params.set_main_version('a')
    hecave_versions_params_b = hecate_versions_params.set_main_version('b')
    hecave_versions_params_summ = replace(hecate_versions_params, project='ab_summary')

    for param in (project, hecate_versions_params.horizon):
        if param == 'all':
            raise ValueError(f"Cannot perform AB test for all {param}s. Please, choose one of them")
    for branch in [branch_a, branch_b]:
        if not repo.check_branch_exist(branch):
            raise ValueError(f"Branch '{branch}' not found in the local repository")
    for (commit_id, branch) in [(commit_a_id, branch_a), (commit_b_id, branch_b)]:
        if not repo.check_commit_exist(branch, commit_id):
            raise ValueError(f"Commit with the id '{commit_id}' not found in the branch '{branch}'")

    repo.check_unstaged_changes()
    current_commit_id, _ = repo.get_branch_and_last_commit_id()
    data_a_selected, data_b_selected = False, False

    ab_results = db_connection.select_ab_results_if_exist(hecate_versions_params,
                                                          'ab_results_' + hecate_versions_params.project)
    if rewrite_ab_results or ab_results.empty:

        data_a_selected, data_b_selected = True, True
        data_a = get_data(db_connection, commit_a_id, current_commit_id, hecave_versions_params_a)
        data_b = get_data(db_connection, commit_b_id, current_commit_id, hecave_versions_params_b)
        if data_a.empty or data_b.empty:
            logger.info(f"Cannot perform AB test on project == '{project}' and focus == "
                        f"{hecate_versions_params.focus} because of empty data.")

        ab_results = ab_test_all_keys(data_a, data_b, ab_params)
        ab_results = run_ab.add_hecate_versions_params(ab_results, hecate_versions_params)
        db_connection.send_ab_results_to_db(ab_results, hecate_versions_params, rewrite_ab_results)
        logger.info(f"New AB test results for project '{project}' have been added to the database.")

    else:
        logger.info(f"The desired AB test results for project '{project}' already exists in the database.")

    ab_summary = db_connection.select_ab_summary_if_exist(hecate_versions_params)

    if rewrite_ab_summary or ab_summary.empty:
        if not data_a_selected:
            data_a = get_data(db_connection, commit_a_id, current_commit_id, hecave_versions_params_a)
        if not data_b_selected:
            data_b = get_data(db_connection, commit_b_id, current_commit_id, hecave_versions_params_b)
        if data_a.empty or data_b.empty:
            logger.info(f"Cannot compose AB summary on project == '{project}' and focus == "
                        f"{hecate_versions_params.focus} because of empty data.")

        ab_summary = run_ab.compose_ab_summary(ab_results, data_a, data_b, ab_params)
        db_connection.send_ab_results_to_db(ab_summary, hecave_versions_params_summ, rewrite_ab_summary)
        logger.info(f"New AB test summary for project '{project}' have been added to the database.")

    else:
        logger.info(f"The desired AB test's summary for project '{project}' already exists in the database.")


def multiple_ab_test_runs(hecate_version_a: HecateVersion,
                          hecate_version_b: HecateVersion,
                          db_connection: Database,
                          ab_params: ABParams,
                          projects: list[str] | str = 'all',
                          n_folds: int = 3,
                          focuses: list[bool] = [False],
                          horizons: list[HORIZONS] | HORIZONS = 'all',
                          rewrite_ab_results: bool = False,
                          rewrite_ab_summary: bool = False
                          ):
    """
    Runs AB tests of two hecate versions on various datasets from the database.

    :param hecate_version_a: the first hecate version branch and commit id
    :param hecate_version_b: the second hecate version branch and commit id
    :param db_connection: parameters of connection to a database (password, db_ip, db_name, user_name)
    :param ab_params: parameters required for AB tests ('add_metrics', 'metrics_name', 'alpha_ab', 'beta_ab', 'ci_prob',
                      'distribution', 'alpha_dist', 'greater_is_better', 'zero_threshold', 'min_len_test',
                      'min_len_norm', 'n_signif_digits')
    :param projects: a project or a list of projects to pass hecate AB tests on
    :param n_folds: how many folds to pass hecate AB tests on, from 1 to 3.
    :param focuses: a list of focuses to pass hecate AB tests on
    :param horizons: a horizon or a list of horizons to pass hecate AB tests on
    :param rewrite_ab_resuts: if True performs AB test with given params anew and rewrite the results to the database,
                              otherwise uses existing data
    :param rewrite_ab_summary: if True performs AB test with given params anew and rewrite the results to the database,
                              otherwise uses existing data

    """
    for branch in [hecate_version_a.branch, hecate_version_b.branch]:
        if not repo.check_branch_exist(branch):
            raise ValueError(f"Branch '{branch}' not found in the local repository")
    for (branch, commit_id) in [hecate_version_a, hecate_version_b]:
        if not repo.check_commit_exist(branch, commit_id):
            raise ValueError(f"Commit with the id '{commit_id}' not found in the branch '{branch}'")

    horizon_and_folds = db_connection.extract_horizon_and_folds()
    horizon_and_folds = filter_project_horizon_and_fold(horizon_and_folds, projects, horizons, n_folds)
    count = 0
    for ind, row in horizon_and_folds[['project', 'horizon', 'fold']].iterrows():
        for focus in focuses:
            hvp = HecateVersionsParams(
                branch_a=hecate_version_a.branch,
                commit_a_id=hecate_version_a.commit_id,
                branch_b=hecate_version_b.branch,
                commit_b_id=hecate_version_b.commit_id,
                branch=hecate_version_a.branch,
                commit_id=hecate_version_a.commit_id,
                project=row.project,
                fold=row.fold,
                focus=focus,
                horizon=row.horizon
            )
            run_ab_test(hvp, ab_params, db_connection, rewrite_ab_results, rewrite_ab_summary)
            count += 1
            logger.info(f"Complete {count} AB tests.")
