import pytest

from hecate.forecasting.decompositions.strategies import BLDecompositionStrategy
from hecate.hecate import fc_sample
from hecate.metrics.custom import fa


@pytest.mark.parametrize('hecate_input_df', ['uplift'], indirect=True)
def test_bl_strategies(hecate_input_df):
    data, ft, _ = hecate_input_df
    data = data[['date', 'key', 'target', 'discount_promo', 'partition']].copy()

    # zero promo strategy
    model_answer = fc_sample(data.copy(), feature_types=ft, dsd={'bl': BLDecompositionStrategy.zero_promo})
    zero_promo_df = model_answer.data.merge(data, how='outer', on=['key', 'date'])
    zero_promo_fa = fa(zero_promo_df.target, zero_promo_df.fc)

    # remove promo strategy
    model_answer = fc_sample(data.copy(), feature_types=ft, dsd={'bl': BLDecompositionStrategy.remove_promo})
    remove_promo_df = model_answer.data.merge(data, how='outer', on=['key', 'date'])
    remove_promo_fa = fa(remove_promo_df.target, remove_promo_df.fc)

    assert zero_promo_df.bl.sum() > remove_promo_df.bl.sum()
    assert remove_promo_fa > zero_promo_fa

    # check regressing to zero promo strategy in case not enough data
    data['discount_promo'] = 0.1

    model_answer_fail_remove = \
        fc_sample(data.copy(), feature_types=ft, dsd={'bl': BLDecompositionStrategy.remove_promo})
    model_answer_zero = fc_sample(data.copy(), feature_types=ft, dsd={'bl': BLDecompositionStrategy.zero_promo})

    assert model_answer_fail_remove.data.equals(model_answer_zero.data)
