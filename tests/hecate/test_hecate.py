import pandas as pd
import numpy as np
import pytest
from typing import Sequence

import hecate.constants as constants
from hecate.forecasting.decompositions.strategies import DecompositionStrategies
from hecate.forecasting.horizon import Horizon
from hecate.hecate import fc_sample, fc_batch, extend_fc_sample_result
from hecate.forecasting.trend import trend_angle
from hecate.forecasting.utils import EnvVar
from hecate.metrics.custom import fa
from hecate.features import FeatureType, FeatureSet
from hecate.types import Drivers
from hecate.utils.utils import is_integer
from hecate.forecasting.skeleton import (
    FCSampleResult, ForecastingTaskConfig,
    Forecaster, ForecasterConfig, PreprocessorConfig, PostprocessorConfig
)


def test_fc_sample(hecate_input_df, columns_format, required_cols,
                   forecast_column, date_column):
    # test input dataframe
    data, feature_types, result = hecate_input_df

    # test output dataframe
    model_answer = fc_sample(data.copy(), feature_types, drivers=[])
    output_data = model_answer.data

    # condition for forecast column
    assert forecast_column in output_data.columns

    # condition for equals input and output dataframes
    assert set(output_data.columns) == {'key', 'date', 'fc'}

    # condition for nan forecast
    assert True not in list(output_data[forecast_column].isna())

    # condition for empty str forecast
    assert output_data[forecast_column].dtypes not in [object, bool]

    data['date'] = pd.to_datetime(data['date'], format='%Y-%m-%d')
    output_data = output_data.merge(data[['key', 'date', 'partition', 'target']],
                                    on=['key', 'date'], how='left')

    # condition for max forecast value
    assert output_data[output_data['partition'] == 'train']['target'].max() * 2 >= \
           output_data['fc'].max()

    # condition for non-zero forecast
    assert output_data[~(output_data['partition'] == 'train')]['fc'].sum() != 0

    # condition for promo and regular forecast
    promo_features = set(output_data.columns) & {'discount_promo', 'promo_budget'}
    if len(promo_features) > 0:
        feature = list(promo_features)[0]
        forecast_promo = output_data[~(output_data['partition'] == 'train') &
                                     (output_data[feature] > 0)]['fc'].mean()
        forecast_reg = output_data[~(output_data['partition'] == 'train') &
                                   (output_data[feature] == 0)]['fc'].mean()
        assert forecast_promo > forecast_reg

    # check promo uplift
    features = [key for key, item in feature_types.items()
                if item in [FeatureType.discount]]

    if len(features) > 0:
        output_data = output_data.merge(data[['key', 'date', features[0]]],
                                        on=['key', 'date'], how='left')
        if output_data[features[0]].sum() > 0:
            output_data['promo'] = output_data[features[0]].apply(lambda x: 1 if x > 0 else 0)
            uplift = output_data.groupby('promo', as_index=False)['fc'].mean()
            assert uplift[uplift['promo'] == 0]['fc'].sum() <= \
                   uplift[uplift['promo'] == 1]['fc'].sum()


def test_fc_sample_manage_growing_target(hecate_input_df):
    # test input dataframe
    data, feature_types, result = hecate_input_df

    # test output dataframe
    model_answer = fc_sample(data.copy(), feature_types, drivers=[])
    output_data = model_answer.data

    # condition for trend (delta - var of angle value)
    delta = 10
    tr_angle = 10

    data_trend_forecast = output_data[['date', 'fc']].set_index('date')
    angle_fc = trend_angle(data_trend_forecast['fc'])
    freq_circle = {'D': 365,
                   'W': 52,
                   'MS': 12,
                   'M': 12}
    angle = trend_angle(data['target'])
    # find time circle to make trend, needed two full circle
    freq = pd.infer_freq(pd.to_datetime(data['date'], format='%Y-%m-%d')).split('-')[0]
    freq = freq_circle[freq]
    if (angle > tr_angle) & (len(data[data['partition'] == 'train']) >= freq):
        assert (angle + delta) >= angle_fc >= (angle - delta)


@pytest.mark.parametrize('hecate_input_df', ['test_quality'], indirect=True)
def test_fc_sample_underfitting_by_case(hecate_input_df):
    data, _, _ = hecate_input_df
    model_answer = fc_sample(data, feature_types=dict())
    data = data.merge(model_answer.data)

    middle_2019_target = data.loc[(data.date < '2019-08-01') & (data.date > '2019-06-01'), 'target'].mean()
    middle_2019_fc = data.loc[(data.date < '2019-08-01') & (data.date > '2019-06-01'), 'fc'].mean()
    ratio = abs(middle_2019_fc - middle_2019_target) / middle_2019_target
    assert ratio < 0.2, \
        "Hecate is underfitted on a test dataset!"

    start_2022_target = data.loc[(data.date < '2022-03-01') & (data.date > '2022-01-01'), 'target'].mean()
    start_2022_fc = data.loc[(data.date < '2022-03-01') & (data.date > '2022-01-01'), 'fc'].mean()
    ratio = abs(start_2022_fc - start_2022_target) / start_2022_target
    assert ratio < 0.3, \
        "Hecate is underfitted on a test dataset!"

    nunique_fc = data['fc'].nunique()
    nunique_target = data['target'].nunique()
    ratio = abs(nunique_fc - nunique_target) / nunique_target
    assert ratio < 0.8, "Hecate is underfitted on a test dataset!"


def test_fc_sample_unknown_value_in_future():
    data = generate_df_with_unknown_value_in_future()
    model_answer = fc_sample(data, feature_types=dict(), drivers=['bl'])  # TODO: why drivers=bl
    data = data.merge(model_answer.data)

    totals = data[(data.partition == 'fc')
                  & (data['discount'])][['target', 'fc']].sum()
    ratio = totals['fc'] / totals['target']
    assert 0.90 < ratio < 1.10


def test_fc_sample_return_bl():
    data = generate_df_with_unknown_value_in_future()
    model_answer = fc_sample(data, feature_types=dict(), drivers=[])
    data = data.merge(model_answer.data)
    assert 'bl' not in data.columns

    model_answer = fc_sample(data, feature_types=dict(), drivers=['bl'])
    data = data.merge(model_answer.data)
    assert 'bl' in data.columns


def test_fc_sample_bl():
    sources = {
        'one_promo': generate_df_one_promo_period_per_partition,
        'two_promo': generate_df_two_promo_period_per_partition
    }
    for name, source in sources.items():
        data = source()
        model_answer = fc_sample(
            data,
            feature_types={'discount': FeatureType.discount},
            drivers=['bl']
        )
        data = data.merge(model_answer.data)

        act_regular_target = data[
            (data.partition == 'fc') &
            (data.discount == 0)].target.mean()

        promo_means = data[
            (data.partition == 'fc') &
            (data.discount > 0)
        ][['target', 'bl', 'fc']].mean()

        target_mean = promo_means['target']
        bl_mean = promo_means['bl']
        fc_mean = promo_means['fc']

        assert bl_mean < fc_mean, f'bl greater fc on promo period in case {name} in train'
        assert abs(target_mean - fc_mean) / target_mean < 0.10
        assert abs(bl_mean / act_regular_target - 1) < 0.10


def test_fc_sample_non_specified_feature_have_impact():
    data = generate_df_with_unknown_value_in_future()
    model_answer = fc_sample(data,
                             feature_types=dict(),
                             drivers=[])
    data = data.merge(model_answer.data)
    means = data[data.partition == 'fc'].groupby('discount').fc.mean()
    reg_fc, promo_fc = means.values

    assert (promo_fc - reg_fc) / reg_fc > 0.10


def test_fc_sample_leading_tiny_values_havent_impact():
    data = generate_df_with_tiny_leading_values()
    # expectation: we have tiny values in leading month and in
    # the fc period we have same month
    model_answer = fc_sample(data,
                             feature_types=dict(),
                             drivers=[])
    data = data.merge(model_answer.data)

    first_month = data.date.dt.month.values[0]
    target_fc_df = data[(data.partition == 'fc') &
                        (data.date.dt.month == first_month)]
    err_total = np.sum(target_fc_df.target - target_fc_df.fc)
    fc_total = target_fc_df.fc.sum()
    bias = err_total * 100 / fc_total

    assert abs(bias) < 5


def test_fc_sample_manage_empty_selected_features():
    # if we empty feature list to fc, we will fail,
    # so let test how it works with case that don't produce any features
    data_dict = {
        'key': ['key-1', 'key-1', 'key-1', 'key-1'],
        'date': ['2022-07-04', '2022-07-11', '2022-07-18', '2022-07-25'],
        'partition': ['train', 'train', 'train', 'fc'],
        'target': [100.01, 1, 1, 0.0]
    }

    data = pd.DataFrame(data_dict)
    data.date = pd.to_datetime(data.date)

    try:
        _ = fc_sample(data, feature_types={}, drivers=['bl'])

    except Exception:
        pytest.fail("fc_sample failed with case where no one "
                    "feature was selected")


def test_fc_sample_returns(monkeypatch):
    data = generate_df_one_promo_period_per_partition()

    result = fc_sample(data, {})
    assert isinstance(result, FCSampleResult)
    assert not result.is_failed

    def _failed_fc_sample(*args, **kwargs):
        raise NotImplementedError

    # weak approach. need to automate copying decorators from fc_sample function to _failed_fc_sample
    _failed_fc_sample = extend_fc_sample_result(_failed_fc_sample)
    result = _failed_fc_sample(data, feature_types={},)
    assert result.is_failed
    assert result.traceback


def test_fc_sample_batch_support_additional_cols():

    data = {
        'key':  ['k'] * 10,
        'date': [f'2022-01-{idx:02d}' for idx in range(1, 11, 1)],
        'dsc':  [0, 15, 15, ] + [0] * 6 + [15, ],
        'partition': ['train'] * 7 + ['fc'] * 3,
        'target': [10, 80, 75, 5, 10, 15, 10, 0, 0, 0],
    }

    data = pd.DataFrame(data)

    assert 'dsc' not in fc_sample(data, {}).data.columns
    assert 'dsc' not in fc_sample(data, {}, add_columns=[]).data.columns

    assert 'dsc' in fc_sample(data, {}, add_columns=['dsc']).data.columns

    assert 'dsc' not in fc_batch(data, {}).columns
    assert 'dsc' not in fc_batch(data, {}, add_columns=[]).columns

    assert 'dsc' in fc_batch(data, {}, add_columns=['dsc']).columns

    assert 'dsc' not in fc_batch(data, {}, parallel=True).columns
    assert 'dsc' not in fc_batch(data, {}, parallel=True, add_columns=[]).columns

    assert 'dsc' in fc_batch(data, {}, parallel=True, add_columns=['dsc']).columns


def test_fc_batch_support_parallel():
    data_dict = {
        'key': ['key-1', 'key-1', 'key-1', 'key-1'],
        'date': ['2022-07-04', '2022-07-11', '2022-07-18', '2022-07-25'],
        'partition': ['train', 'train', 'train', 'fc'],
        'target': [100.01, 1, 1, 0.0]
    }

    data = pd.DataFrame(data_dict)
    data.date = pd.to_datetime(data.date)

    data2 = data.copy()
    data2['key'] = 'k2'
    data = pd.concat([data, data2])

    _ = fc_batch(data, feature_types={}, decompose=[], parallel=True)


def test_fc_batch_one_exception_in_parallel(monkeypatch):
    data_dict = {
        'key': ['key-1', 'key-1', 'key-1', 'key-1'],
        'date': ['2022-07-04', '2022-07-11', '2022-07-18', '2022-07-25'],
        'partition': ['train', 'train', 'train', 'fc'],
        'target': [100.01, 1, 1, 0.0]
    }

    data = pd.DataFrame(data_dict)
    data.date = pd.to_datetime(data.date)

    data2 = data.copy()
    data2['key'] = 'failed_key'
    data2['target'] = ['must be failed here', 1, 1, 0.0]
    data = pd.concat([data, data2])

    batch_result = fc_batch(data, feature_types={}, decompose=[], parallel=True)

    assert data.key.nunique() == 2
    assert batch_result.key.nunique() == 1


def test_fc_sample_stop_fc():

    sample = generate_df_with_unknown_value_in_future()
    data = sample.copy()
    model_answer = fc_sample(data.copy(),
                             feature_types=dict(),
                             drivers=[])
    data = data.merge(model_answer.data)
    non_stop_fc_sum = data.fc.sum()

    # no impact
    data = sample.copy()
    data['wtf'] = 0
    model_answer = fc_sample(data.copy(),
                             feature_types=dict(),
                             drivers=[])
    assert model_answer.data.fc.sum() == non_stop_fc_sum

    model_answer = fc_sample(data.copy(),
                             feature_types={'wtf': FeatureType.stop},
                             drivers=[])
    assert model_answer.data.fc.sum() == non_stop_fc_sum

    # stop impact on fc
    data = sample.copy()
    data['wtf'] = 1
    model_answer = fc_sample(data.copy(),
                             feature_types={'wtf': FeatureType.stop},
                             drivers=[])
    data = data.merge(model_answer.data)
    assert data.fc.sum() == 0.0

    data = sample.copy()
    data['wtf'] = 1
    model_answer = fc_sample(data.copy(),
                             feature_types={'wtf': FeatureType.stop},
                             drivers=['bl'])
    data = data.merge(model_answer.data)
    assert data.fc.sum() == 0.0 and data.bl.sum() == 0.0

    # impact only for selected stop feature col
    data = sample.copy()
    data['wtf'] = 0
    data['wtf'] = data['wtf'].values[:-14].tolist() + [1] * 14
    model_answer = fc_sample(data.copy(), feature_types={'wtf': FeatureType.stop})
    assert model_answer.data.fc.values[-14:].sum() == 0.0
    assert model_answer.data.fc.values[:-14].sum() > 0.0


def test_get_decomposition():
    from hecate.forecasting.decompositions.decomposition import get_decomposition
    assert get_decomposition('all') == list(constants.ALL_DRIVERS.keys())
    assert get_decomposition([]) == []
    assert get_decomposition(['cn']) == ['cn']
    assert get_decomposition(['bl']) == ['bl']
    assert get_decomposition(['pc']) == ['pc']


def generate_df_one_promo_period_per_partition() -> pd.DataFrame:
    data = pd.DataFrame(pd.date_range(start='2022-01-01', end='2022-02-28', ),
                        columns=['date'])
    data['key'] = 'k'
    data['partition'] = np.where(data.date.dt.month == 2, 'fc', 'train')
    data['discount'] = np.where((data.date.dt.day > 10) &
                                (data.date.dt.day < 20), 15, 0)
    data['target'] = np.abs(np.sin(np.arange(data.shape[0]))) + 1
    data['target'] = np.where(data.discount > 0, data.target * 1.5, data.target)

    return data


def generate_df_two_promo_period_per_partition() -> pd.DataFrame:
    data = pd.DataFrame(pd.date_range(start='2022-01-01', end='2022-02-28'),
                        columns=['date'])
    data['key'] = 'k'
    data['partition'] = np.where(data.date.dt.month == 2, 'fc', 'train')
    data['discount'] = np.where((data.date.dt.month == 1) & (data.date.dt.day < 10), 10, 0)
    data['discount'] = np.where((data.date.dt.day > 10) &
                                (data.date.dt.day < 20), 15, 0)
    data['target'] = np.abs(np.sin(np.arange(data.shape[0]))) + 1
    data['target'] = np.where(data.discount > 0, data.target * 1.5, data.target)

    return data


def generate_df_with_unknown_value_in_future(ratio=2,
                                             deflate=True) -> pd.DataFrame:
    data = pd.DataFrame(pd.date_range(start='2022-01-01', end='2022-02-28'),
                        columns=['date'])
    data['key'] = 'k'
    data['partition'] = np.where(data.date.dt.month == 2, 'fc', 'train')
    data['discount'] = np.where((data.date.dt.day > 10) &
                                (data.date.dt.day < 20), 15, 0)
    if deflate:
        ratio = 1 / ratio
    data['discount'] = np.where(data['partition'] == 'train',
                                data['discount'],
                                data['discount'] * ratio)
    data['target'] = 20*np.abs(np.sin(np.arange(data.shape[0]))) + 80
    data['target'] = np.where(data.discount > 0, data['target'] *
                              (data['discount'] / 100 + 1), data['target'])

    return data


def generate_df_with_tiny_leading_values() -> pd.DataFrame:
    """
    Function return df like this:
       ____________
      /
     /
    /
    """
    sample = pd.DataFrame()
    sample['date'] = pd.date_range(start='2021-12-01', end='2022-12-31')
    sample['key'] = 'k'
    fc_from = '2022-09-01'
    sample['partition'] = np.where(sample.date < fc_from, 'train', 'fc')

    base_lvl = 100
    sample['target'] = base_lvl

    sample['target'] = np.where(
        (sample.date.dt.month == 12) & (sample.partition == 'train'),
        sample.date.dt.day * base_lvl / 31,
        sample.target
    )
    return sample


@pytest.mark.parametrize('prices_input_df', ['test_1'], indirect=True)
def test_changing_prices(prices_input_df):
    data, feature_types, _ = prices_input_df

    res = fc_sample(data.copy(), feature_types, drivers=['bl', 'pc'])
    res_dat = res.data
    assert 'price_effect' in res_dat.columns
    assert res_dat.price_effect.min() < -500
    assert res_dat.price_effect.sum() < -2000

    res_dat['target'] = data.target
    res_dat['partition'] = data.partition
    res_dat['fc_no_pc'] = res_dat.fc - res_dat.price_effect

    # check change in fa
    fc_a = fa(res_dat.target, res_dat.fc)
    fc_no_pc_a = fa(res_dat.target, res_dat.fc_no_pc)
    assert abs(fc_a - fc_no_pc_a) > .02


def test_categorical_input(hecate_input_df):
    """
    Test if having category as data type of input works.
    """
    data, ft, _ = hecate_input_df
    for col in ['date', 'key', 'partition']:
        data[col] = data[col].astype('category')
    model_answer = fc_sample(data, feature_types=ft)

    assert isinstance(model_answer.data, pd.DataFrame)
    assert len(model_answer.data)


@pytest.mark.parametrize('hecate_input_df', ['label_feature'], indirect=True)
def test_label_features(hecate_input_df, drivers, mock_dev_env):
    """
    Test if label feature is used for forecasting.
    """
    data, ft, _ = hecate_input_df
    model_answer = fc_sample(data.copy(), feature_types=ft, drivers=drivers)

    for col in ['discount', 'promo_type']:
        assert col in model_answer.feature_importance


def test_constant_label_feature(mock_dev_env) -> None:
    """
    A test for a real bug. The constant column placed as feature_type 'label'
    was deleted in feature selection, but passed to one-hot-encoder.
    """
    data = pd.DataFrame({
        'date': pd.date_range('2023-01-01', periods=100),
        'key': ['key1']*100,
        'partition': ['train']*80+['fc']*20,
        'target': np.random.random(100),
        'feature_label': ['label1']*100,
    })
    model_answer = fc_sample(data.copy(), feature_types={'feature_label': FeatureType.label})
    assert 'feature_label' not in model_answer.feature_importance


@pytest.mark.parametrize('hecate_input_df', ['test_quality'], indirect=True)
def test_dev_env_output(hecate_input_df, mock_dev_env):
    """
    Test if setting environment variable HECATE_ENV to 'dev' returns different results as compared to 'prod'.
    """

    data, _, _ = hecate_input_df
    model_answer = fc_sample(data, feature_types=dict())

    assert EnvVar().is_debug_environ()
    assert model_answer.total_from_partition
    assert model_answer.metrics
    for col in ['partition', 'fc', 'time-year', 'target']:
        assert col in model_answer.data.columns


@pytest.mark.parametrize('hecate_input_df', ['test_quality'], indirect=True)
def test_prod_env_output(hecate_input_df, mock_prod_env):
    """
    Test if setting environment variable HECATE_ENV to 'prod' returns different results as compared to 'prod'.
    """

    data, _, _ = hecate_input_df
    model_answer = fc_sample(data, feature_types=dict())

    assert not EnvVar.is_debug_environ()
    assert not model_answer.total_from_partition
    assert not model_answer.metrics
    assert set(model_answer.data.columns) == {'key', 'date', 'fc', 'bl', 'uplift', 'cannibalized', 'price_effect'}


def test_disable_preprocessing() -> None:
    """
    Disabling preprocessing should not break the calculation process.
    Case from real use.
    """
    sample = generate_df_one_promo_period_per_partition()
    cfg = ForecastingTaskConfig(toggles={'preprocessor': False})

    res = fc_sample(sample, feature_types={"discount": FeatureType.discount}, drivers=['bl'], config=cfg)

    assert res.data.loc[sample['discount'] > 0, 'uplift'].sum() > 0


def test_disable_feature_selection() -> None:
    """
    Add a copy of the target variable, with a zero forecast horizon.
    If the feature selector is not working,
    the forecast will press down to the lower boundary of the target variable.
    """
    sample = generate_df_one_promo_period_per_partition()
    sample['target_clone'] = np.where(sample.partition == 'train', sample['target'], 0)
    cfg = ForecastingTaskConfig(preprocessor_config=PreprocessorConfig(toggles={'feature_selector': False}))
    res = fc_sample(sample, feature_types={}, config=cfg)
    mean_fc = res.data.loc[sample.partition == 'fc', 'fc'].mean()
    lower_target_bound = sample['target'].min()

    assert (mean_fc - lower_target_bound) / lower_target_bound < 0.05

    # negative test
    res = fc_sample(sample, feature_types={})
    mean_fc = res.data.loc[sample.partition == 'fc', 'fc'].mean()
    lower_target_bound = sample['target'].min()

    assert (mean_fc - lower_target_bound) / lower_target_bound > 0.05


def test_disable_decompositor() -> None:
    """
    Disabling decomposition should not break the calculation process.
    """
    sample = generate_df_one_promo_period_per_partition()
    cfg = ForecastingTaskConfig(forecaster_config=ForecasterConfig(toggles={'decompositor': False}))

    res = fc_sample(sample, feature_types={"discount": FeatureType.discount}, drivers=['bl'], config=cfg)

    assert "uplift" not in res.data.columns and "bl" not in res.data.columns


def test_config_support():
    sample = generate_df_with_unknown_value_in_future()

    toggles = {
        'forecaster': False
    }
    cfg = ForecastingTaskConfig(toggles=toggles)

    res = fc_sample(sample, feature_types={}, config=cfg)
    assert res.data.fc.sum() == 0

    class SameForecaster(Forecaster):
        ...

    custom_executors = {
        'forecaster': SameForecaster()
    }
    cfg = ForecastingTaskConfig(custom_executors=custom_executors)
    _ = fc_sample(sample, feature_types={}, config=cfg)

    class MeanForecaster(Forecaster):
        """
        Gets mean forecast.
        """

        def execute(self,
                    data: pd.DataFrame,
                    horizon: Horizon,
                    feature_set: FeatureSet,
                    drivers: Drivers | None = None,
                    dsd: DecompositionStrategies | None = None):
            """
            Calculates forecast value as a mean target.
            """
            data['fc'] = sample.target.mean()
            return FCSampleResult(key=data.key.values[0], data=data, output_columns=['key', 'date', 'fc'])

    custom_executors = {
        'forecaster': MeanForecaster()
    }
    cfg = ForecastingTaskConfig(custom_executors=custom_executors)
    res = fc_sample(sample, feature_types={}, config=cfg, decimals=6)
    assert abs(sample.target.mean() - res.data.fc.mean()) < 0.000_001


@pytest.mark.parametrize('hecate_input_df', ['test_output'], indirect=True)
def test_hecate_output(hecate_input_df):
    """
    Test if hecate output returns drivers with expected signs.
    """
    data, ft, _ = hecate_input_df
    res = fc_batch(data, feature_types=ft)

    assert res[res.bl < 0].shape[0] == 0
    assert res[res.fc < 0].shape[0] == 0


def test_fc_batch_rounding() -> None:
    df = pd.DataFrame({
        'date': pd.date_range('2023-01-01', periods=20),
        'key': ['k'] * 20,
        'partition': ['train'] * 15 + ['fc'] * 5,
        'target': [123.4567890123] * 20,
    })

    # Test 0
    data = df.copy(deep=True)
    answer = df.copy(deep=True)

    data = fc_batch(data, feature_types={}, decompose='none', add_columns=['target'], decimals=None)
    assert np.all(data.target == answer.target)

    # Test 1.1
    data = df.copy(deep=True)
    answer = df.copy(deep=True)
    decimals = 6
    answer['target'] = answer['target'].round(decimals)

    data = fc_batch(data, feature_types={}, decompose='none', add_columns=['target'], decimals=decimals)
    assert np.all(data.target == answer.target)

    # Test 1.2
    data = df.copy(deep=True)
    answer = df.copy(deep=True)
    decimals = 0
    answer['target'] = answer['target'].round(decimals)

    data = fc_batch(data, feature_types={}, decompose='none', add_columns=['target'], decimals=decimals)
    assert np.all(data.target == answer.target)


def test_result_with_base_score() -> None:
    df = pd.DataFrame({
        'date': pd.date_range('2023-01-01', periods=20),
        'key': ['k'] * 20,
        'partition': ['train'] * 15 + ['fc'] * 5,
        'target': [0] * 20,
    })

    data = df.copy(deep=True)

    data = fc_batch(data, feature_types={}, decompose='none', decimals=None)
    assert max(data.fc.values) == 0


def test_both_roundings_work_together():
    """
    Test to check rolling fc
    """
    df = pd.DataFrame({
        'date': pd.date_range('2023-01-01', periods=20),
        'key': ['k'] * 20,
        'partition': ['train'] * 15 + ['fc'] * 5,
        'target': [23.4567890123] * 20,
    })

    res = fc_batch(df, feature_types={}, decompose='none', decimals=0)
    assert all(res['fc'].apply(is_integer))
    assert {23., 24.}.difference(res.fc.unique()) == set()

    res = fc_batch(df, feature_types={}, decompose='none', decimals=1)
    assert {23.4, 23.5}.difference(res.fc.unique()) == set()

    res = fc_batch(df, feature_types={}, decompose='none')
    assert {23.45, 23.46}.difference(res.fc.unique()) == set()


def test_fc_sample_prediction_interval(pi_input_df):
    data, feature_types, _ = pi_input_df
    data = data[data.key == '000'].drop(columns=['partition_for_pi', 'deviation_at_test', 'fc'])
    res = fc_sample(data.copy(), feature_types, prediction_interval=True, pi_prob_list=[0.5, 0.9],
                    add_columns=['partition'])
    for column in ['PI_down_90.0%', 'PI_down_50.0%', 'PI_up_50.0%', 'PI_up_90.0%']:
        assert column in res.data.columns
        assert column in res.output_columns
    res_data = res.data[res.data.partition == 'fc']
    assert (
        0 < res_data['PI_down_90.0%'].sum() < res_data['PI_down_50.0%'].sum()
        < res_data.fc.sum() < res_data['PI_up_50.0%'].sum() < res_data['PI_up_90.0%'].sum()
    )


def test_fc_sample_prediction_interval_simple_cases():
    # constant target means no deviation
    df = pd.DataFrame({
        'date': pd.date_range('2023-01-01', periods=30, freq='D'),
        'key': ['k'] * 30,
        'partition': ['train'] * 21 + ['fc'] * 9,
        'target': [10] * 30,
    })
    res0 = fc_sample(data=df.copy(), feature_types={}, prediction_interval=True, pi_prob_list=[0.5])
    assert (res0.data['PI_up_50.0%'] == res0.data['PI_down_50.0%']).all()
    # target at fc period affects nothing
    df1 = df.copy()
    df1.loc[df.partition == 'fc', 'target'] = [20] * 9
    res1 = fc_sample(data=df1, feature_types={}, prediction_interval=True, pi_prob_list=[0.5])
    for col in ['fc', 'PI_up_50.0%', 'PI_down_50.0%']:
        assert (res0.data[col] == res1.data[col]).all()
    # target at test period affects everithing
    df2 = df.copy()
    df2.loc[10:19, 'target'] = [20] * 10
    res2 = fc_sample(data=df2.copy(), feature_types={}, prediction_interval=True, pi_prob_list=[0.5])
    for col in ['fc', 'PI_up_50.0%', 'PI_down_50.0%']:
        assert (res0.data[col] != res2.data[col]).all()
    for col in ['PI_up_50.0%', 'PI_down_50.0%']:
        assert (
            (res0.data.fc - res0.data[col]).round().loc[21:] != (res2.data.fc - res2.data[col]).round().loc[21:]
            ).all()
    # parallel shift of target affects fc only
    df3 = df2.copy()
    df3['target'] = df2.target + 7
    res3 = fc_sample(data=df3, feature_types={}, prediction_interval=True, pi_prob_list=[0.5])
    for col in ['fc', 'PI_up_50.0%', 'PI_down_50.0%']:
        assert (res2.data.loc[21:, col] != res3.data.loc[21:, col]).all()
    for col in ['PI_up_50.0%', 'PI_down_50.0%']:
        assert ((res2.data.fc - res2.data[col]).round() == (res3.data.fc - res3.data[col]).round()).all()
    # train period is shorter than forecast gives dummy prediction interval
    df4 = df2.loc[15:]
    res4 = fc_sample(data=df4, feature_types={}, prediction_interval=True, pi_prob_list=[0.5])
    for col in ['PI_up_50.0%', 'PI_down_50.0%']:
        assert res4.data.fc.all() == res4.data[col].all()


def test_fc_batch_prediction_interval(pi_input_df):
    data, feature_types, _ = pi_input_df
    data = data.drop(columns=['partition_for_pi', 'deviation_at_test', 'fc'])
    res = fc_batch(data.copy(), feature_types, parallel=True, prediction_interval=True, pi_prob_list=[0.5, 0.9],
                   add_columns=['partition'])

    for column in ['PI_down_90.0%', 'PI_down_50.0%', 'PI_up_50.0%', 'PI_up_90.0%']:
        assert column in res.columns

    data = res[res.partition == 'fc']
    for key in data.key.unique():
        data1 = data[data.key == key]
        assert (
            0 < data1['PI_down_90.0%'].sum() < data1['PI_down_50.0%'].sum()
            < data1.fc.sum() < data1['PI_up_50.0%'].sum() < data1['PI_up_90.0%'].sum()
        )


@pytest.mark.parametrize(
        argvalues=[(False, 0, [23.4567890123]), (True, 0, [23.]),
                   (True, 1, [23.5]), (True, 2, [23.46])],
        argnames=['toggle_swithed_on', 'decimals', 'expected'])
def test_simple_rounding_toggle(toggle_swithed_on: bool, decimals: int, expected: Sequence[float]):
    """
    Test to check simple rounding works correctly
    """
    df = pd.DataFrame({
        'date': pd.date_range('2023-01-01', periods=20),
        'key': ['k'] * 20,
        'partition': ['train'] * 15 + ['fc'] * 5,
        'target': [23.4567890123] * 20,
    })

    config = ForecastingTaskConfig(postprocessor_config=PostprocessorConfig(toggles={
        'fc_simple_rounder': toggle_swithed_on, 'fc_rolling_rounder': False}))
    res = fc_batch(df.copy(), feature_types={}, decompose='none', decimals=decimals, config=config)

    flag = np.allclose(expected, res.fc.unique().tolist())
    assert flag, f'some numbers has unxpected number of decimals {res.fc.unique()}'


@pytest.mark.parametrize(
        argvalues=[(False, 0, [23.4567890123]), (True, 0, [23., 24.]),
                   (True, 1, [23.4, 23.5]), (True, 2, [23.46, 23.45])],
        argnames=['toggle_swithed_on', 'decimals', 'expected'])
def test_rolling_rounding_toggle(toggle_swithed_on: bool, decimals: int, expected: Sequence[float]):
    """
    Test to check rolling rounding works correctly
    """
    df = pd.DataFrame({
        'date': pd.date_range('2023-01-01', periods=20),
        'key': ['k'] * 20,
        'partition': ['train'] * 15 + ['fc'] * 5,
        'target': [23.4567890123] * 20,
    })

    config = ForecastingTaskConfig(postprocessor_config=PostprocessorConfig(toggles={
        'fc_simple_rounder': False, 'fc_rolling_rounder': toggle_swithed_on}))
    res = fc_batch(df.copy(), feature_types={}, decompose='none', decimals=decimals,
                   config=config, add_columns=['partition'])

    flag = np.allclose(expected, res.query('partition == "fc"').fc.unique().tolist())
    assert flag, f'some numbers has unxpected number of decimals {res.query("partition == fc").fc.unique()}'
