from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer, TransformedTargetRegressor
from typing import List, Optional
from collections import namedtuple
from itertools import product
import xgboost as xgb


class PipelineWrapper:
    """
    Wrapper class for converting features for models
    """
    def __init__(self, model, params_model, features_cols: List, ohe_cols: List, min_max_cols: List):
        """
        :param model: model constructor with sklearn API
        :param params_model: parameters for initializing model
        :param features_cols: list with feature column names
        :param ohe_cols: list with feature column names for one hot encoding
        :param min_max_cols: list with feature column names for min max scaling
        """
        other_columns = [col for col in features_cols if col not in ohe_cols and col not in min_max_cols]

        numeric_transformer = Pipeline(
            steps=[('scaler', MinMaxScaler())]
        )

        cat_transformer = OneHotEncoder(handle_unknown='ignore')

        preprocessor = ColumnTransformer(
            sparse_threshold=0,
            transformers=[
                ('num', numeric_transformer, min_max_cols),
                ('cat', cat_transformer, ohe_cols),
                ('other', 'passthrough', other_columns)
            ],
            # n_jobs=1,
        )

        pipeline = Pipeline(steps=[('preprocessor', preprocessor),
                                   ('model', model(**params_model))])

        self.model = TransformedTargetRegressor(regressor=pipeline, transformer=MinMaxScaler())

    def fit(self, *args, **kwargs):
        """
        Fit the model.
        """
        self.model.fit(*args, **kwargs)

    def get_feature_importance(self):
        """
        Return feature importance dictionary.
        """
        return self.model.regressor_.named_steps['model'].feature_importances_

    def predict(self, *args, **kwargs):
        """
        Return predictions of the model.
        """
        return self.model.predict(*args, **kwargs)


class ModelPoolItem:
    """
    ModelPoolItem class for make items for model pool
    """
    def __init__(self, model_name, params, constructor):
        """
        :param model_name: model name
        :param params: parameters for initializing model
        :param constructor: model constructor with sklearn API
        """
        self.model_name = model_name
        self.params = params
        self.constructor = constructor

    def transform(self):
        """ Transform model to tuple for model pool"""
        self_param = self.params
        # make flat param instead of dict
        list_param = [[*itm] for itm in product(*[self_param[key] for key in self_param])]
        flat_param = [dict(zip(self_param, lst)) for lst in list_param]
        model_dict = self.__dict__.copy()
        param = []
        for prm in flat_param:
            model_dict['params'] = prm
            param.append(namedtuple('ModelPoolItem', model_dict.keys())(**model_dict))
        return param


def get_default_model(features: list,
                      min_max_cols: Optional[list] = None,
                      ohe_cols: Optional[list] = None,
                      n_est: int = 20) -> PipelineWrapper:
    """
    Function return default model that ready to fit and predict
    :param features: list of features that model will use as x
    :param min_max_cols: features that will be scaled
    :param ohe_cols: features that will be one hot encoded
    :param n_est: n_estimators (the value depends on date frequency)

    :return: PipelineWrapper model
    """
    min_max_cols = features if min_max_cols is None else min_max_cols
    ohe_cols = [] if ohe_cols is None else ohe_cols

    constructor = xgb.XGBRegressor
    params = {'eta': 0.3,
              'max_depth': 2,
              'n_estimators': n_est,
              'min_child_weight': 2,
              'nthread': 1,
              'importance_type': 'gain',
              }
    model = PipelineWrapper(model=constructor,
                            params_model=params,
                            features_cols=features,
                            ohe_cols=ohe_cols,
                            min_max_cols=min_max_cols)
    return model


class ModelAnswer:
    """
    Class for storing various results of model fitting.
    """

    def __init__(self, prediction, model):
        """
        Initialize ModelAnswer class.
        """
        self.prediction = prediction
        self.model = model
