import pandas as pd
from hecate import constants


def is_no_missing_rows(data: pd.DataFrame) -> bool:
    raise NotImplementedError


def is_no_missing_values(data: pd.DataFrame) -> bool:
    raise NotImplementedError


def is_quality_data(data: pd.DataFrame) -> bool:
    return is_no_missing_rows(data) and is_no_missing_values(data)


def is_forecast_in_common_sense(data: pd.DataFrame) -> bool:
    if data.empty:
        raise ValueError('data dataframe must contain data')
    raise NotImplementedError


def get_missed_columns(data: pd.DataFrame, feature_types: dict) -> list:
    """
    Checking columns exiting in Dataframe
    :param data: pandas dataframe for checking
    :param feature_types: additional columns
    """
    required_cols = list(constants.REQUIRED_COLS)
    required_cols.extend(feature_types.keys())
    missed = [col for col in required_cols if col not in data.columns]
    return missed
