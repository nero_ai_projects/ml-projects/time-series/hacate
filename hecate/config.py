"""
The module contains environment settings and paths.
"""

import os

from pathlib import Path


def _get_logs_directory() -> Path:
    env_log_path = os.environ.get("HECATE_LOG_LOCATION")
    if env_log_path is None:
        home_dir = Path.home()
        log_dir = home_dir.joinpath(".hecate/logs")
    else:
        log_dir = Path(env_log_path)

    if not log_dir.exists():
        log_dir.mkdir(parents=True, exist_ok=True)

    return log_dir


LOGS_DIR: Path = _get_logs_directory()
FAILED_DF_PATH: Path = LOGS_DIR / 'failed.parquet'
LOGS_APP_PATH: Path = LOGS_DIR / "app.log"
LOGS_WORKER_PATH: Path = LOGS_DIR / "worker.log"
PROJECT_PATH = Path(__file__).parents[1]
