from hecate.features import FeatureType
from hecate.forecasting.decompositions.strategies import BLDecompositionStrategy


class SwaggerDescription():

    _TO_DICT_LINK = 'https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_dict.html'

    example_desc = "<p>Json, в случае при передече параметра payload_type = 'json'</p>" + \
                   "<p>должен содержать необходимые колонки + признаки" + \
                   "(например, глубину скидки, цену и пр.)</p>" + \
                   "Ожидаемый json соответствует результату применения метода to_dict объекта " + \
                   f"pandas.DataFrame <a href={_TO_DICT_LINK}>{_TO_DICT_LINK}</a>" + \
                   "{0}"

    decompose_desc = "<p>decompose – параметр для контроля разбиения прогноза на драйвера.</p>" + \
                     "<p>Возможные типы: str (‘all’ или ‘none’), " + \
                     "list (все возможные элементы листа: [‘bl’, ‘cn’]):</p>" + \
                     "<ul><li>При значении ‘none’ на выходе выдаётся только колонка fc, " + \
                     "содержащая total forecast</li>" + \
                     "<li>При перечислении драйверов в листе рассчитываются только перечисленные драйвера:" + \
                     "<ul><li>Если лист содержит ‘bl’, на выходе к датафрейму добавляются колонки bl и uplift. " + \
                     "Сумма колонок должна равняться значению в колонке fc.</li>" + \
                     "<li>Если лист содержит ‘cn’, на выходе к датафрейму добавляется колонка cannibalized. </li>" + \
                     "<li>Если лист содержит ‘pc’, на выходе к датафрейму добавляется колонка price_effect. </li>" + \
                     "<li>Если лист содержит и ‘bl’, и ‘cn’, и ‘pc’" + \
                     "на выходе к колонке fc добавляются колонки bl, uplift, cannibalized и price_effect. " + \
                     "Сумма колонок должна равняться значению в колонке fc.</li></ul></li>" + \
                     "<li>При значении ‘all’ прогноз разбивается на все " + \
                     "возможные драйвера ([‘bl’, ‘cn’, ‘pc’]).</li></ul>"

    prediction_interval_desc = "<p>prediction_interval – Если true, включает расчет одного или нескольких " + \
                               "прогнозных коридоров для каждого из заданных уровней вероятности.</p>" + \
                               "<p>Расчет возможен только для дневной гранулярности данных</p>"

    pi_prob_list_desc = "<p>pi_prob_list – список уровней вероятности прогнозных коридоров.</p>" + \
                        "<p>Уровень вероятности прогнозного коридора определяется как доля значений целевой " + \
                        "величины (target), которые должны будут попасть в этот коридор в прогнозный период. </p>" + \
                        "<p>Для каждого из значений списка pi_prob_list будет построен свой соотвествующий " +\
                        "прогнозный коридор.</p>"

    headers_desc = {
        "feature_types": "" +
                         "<p>Information about type of business columns.</p>" +
                         "<p>Example:</p>" +
                         '<p>feature_types = {"dsc": "discount", "stc": "stock"}</p>' +
                         "<p>We transmit information that the dsc column is a discount ('discount')," +
                         " and the stc column is the remains in the warehouse ('stock').</p>" +
                         "Available types:</p>" +
                         FeatureType.doc(),
        "decompose": "" +
                     "<p>Параметр регулирует декомпозицию прогноза на драйверы." +
                     "<p>Может принимать значения 'all' (декомпозировать на все доступные "
                     "драйверы), 'none' (не декомпозировать, вернуть только тотал прогноз), " +
                     "или может быть листом, содержащим драйвера ('bl', 'cn', 'pc').</p>",
        "calc_type": "" +
                     "<p>Information about type of calculation to be used for various figures.</p>" +
                     "<p>Example:</p>" +
                     "<p>feature_types = {'bl': 'zero_promo'}</p>" +
                     "<p>We transmit information that the baseline column is to be calculated by zeroing" +
                     " out promo features during promo periods.</p>" +
                     "Available types:</p>" +
                     BLDecompositionStrategy.doc()
    }
