from fastapi import APIRouter
from service.api.endpoints import forecast, cannibalization, utilities, decomposition, outliers

api_router = APIRouter()
api_router.include_router(forecast.router, prefix="", tags=["Forecast"])
api_router.include_router(cannibalization.router, prefix="/get_can_pairs", tags=["Cannibalization"])
api_router.include_router(utilities.router, prefix="/service", tags=["Service"])
api_router.include_router(decomposition.router, prefix="/decompose_act", tags=["Actuals Decomposition"])
api_router.include_router(outliers.router, prefix='/outliers', tags=['Outliers'])
