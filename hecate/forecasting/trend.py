import math
from typing import Callable

import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from scipy.stats import ttest_1samp
from statsmodels.tsa.filters.filtertools import convolution_filter
from datetime import timedelta

from hecate.utils.utils import get_frequency
from hecate.forecasting.feature_generation import get_week_of_month, get_half_year
from hecate.types import TupleDataAndCols
from hecate.log import get_logger

logger = get_logger('hecate')


def get_trend_params(ts: pd.Series) -> tuple:
    """
    Train model to predict trend

    :param ts: pd.Series() - target values to get a trend for

    :return: dictionary with trend line parameters (intercept and slope)
    """
    values_y = ts.to_numpy()
    values_x = np.arange(values_y.size)

    # find coefficients of linear equation
    args, _ = curve_fit(lambda x, a, b: a * x + b, values_x, values_y)

    return args


def trend_angle(ts: pd.Series) -> float:
    """
    Function find angle of trend_line.

    :param ts: pd.Series - X value to make trend

    :return alpha: float - angle of trend line
    """
    args = get_trend_params(ts)
    alpha = math.degrees(math.atan(args[0]))

    return alpha


def get_df_for_trend_extraction(data: pd.DataFrame, promo_cols: list) -> pd.DataFrame:
    """
    Prepare df for trend extraction. Includes removing promo periods and abnormal periods. Then adds
    number to the complete dataframe for periods which are included in trend calculation. NB! Initial dataset changed
    as well (new column 'time-period_num' added)

    :param data: initial dataframe
    :param promo_cols: list of promo cols to remove promo periods

    :return: pd.DataFrame for trend extraction
    """
    # get columns to exclude observations
    remove_cols = promo_cols + ['target-outlier', 'target-zero_period']
    remove_cols = [col for col in remove_cols if col in data.columns]

    # add num for trend periods to the initial dataframe
    not_remove_periods = data[remove_cols].sum(axis=1) == 0
    data['time-period_num'] = not_remove_periods
    data['time-period_num'] = data['time-period_num'].cumsum()
    data['time-period_num'].mask(~not_remove_periods, 0, inplace=True)

    train = data[(data.partition != 'fc') & (data['time-period_num'] != 0)]

    return train


def smoothen_ts(ts: pd.Series, period: int, two_sided: bool = False) -> pd.Series:
    """
    Use convolution filter to smoothen the target.

    :param ts: time series to smoothen
    :param period: filtering window
    :param two_sided: whether to center the filtered values

    :return: pandas series for smoothened trend
    """
    # split weights at ends
    if period % 2 == 0:
        f = np.array([0.5] + [1] * (period - 1) + [0.5]) / period
    else:
        f = np.repeat(1.0 / period, period)

    trend = convolution_filter(np.array(ts), f, int(two_sided) + 1)

    return trend


def add_seasonal_flag(df: pd.DataFrame,
                      freq: str,
                      recalc_trend: bool = False,
                      seasonality_col: str = 'seasonality_flag') -> None:
    """
    Function adds a column, where seasonal periods are marked as True.

    :param df: initial dataframe with target and fc partition
    :param freq: frequency of the dataframe
    :param recalc_trend: whether to recalculate trend for seasonality calculation. If False, uses pre-calculated
    trend, which is expected to be there in 'trend' column
    :param seasonality_col: string, name of the column to store seasonality flag in

    :return: no return. Makes changes to initial dataframe
    """
    # get dataframe for season component calculation
    periods = {
        'D': ['month', 'week_of_month'],
        'W': ['week'],
        'M': ['month'],
        'MS': ['month'],
        'Q': ['quarter'],
        'HY': ['half_year'],
    }

    custom_funcs: dict[str, Callable] = {
        'week_of_month': get_week_of_month,
        'half_year': get_half_year,
    }

    for period in periods[freq]:
        if period in ['week']:
            df[period] = df.date.dt.isocalendar().week
            continue
        try:
            df[period] = getattr(df.date.dt, period)
        except AttributeError:
            df[period] = custom_funcs[period](df.date)

    cols_to_use = ['date', 'time-period_num', 'target', 'partition'] + periods[freq] + \
        (['trend'] if not recalc_trend else [])
    target = df.loc[(df['time-period_num'] != 0) & (df.partition != 'fc'), cols_to_use].copy()

    # make long trend, if recalc_trend
    if recalc_trend:
        trend_params = get_trend_params(target.target)
        target['trend'] = make_trend_from_params(target['time-period_num'], trend_params)
    target['target'] -= target['trend']

    # get seasonality flag
    target_mean = target.target.mean()
    for period in periods[freq]:
        seasonality_flag = {
            period: ttest_1samp(mdf.target, target_mean).pvalue < 0.05
            if (np.std(mdf.target) > 0.001) else False
            for period, mdf in target.groupby(period)
        }
        df[period] = df[period].map(seasonality_flag)

    df[seasonality_col] = df[periods[freq]].any(axis=1)
    df.drop(columns=periods[freq], inplace=True)


def make_trend_from_params(period_num: pd.Series, trend_params: tuple) -> pd.Series:
    """
    Draws a trend based on trend_params and period num.
    """
    trend = trend_params[1] + trend_params[0] * period_num
    trend = np.where(period_num == 0, 0, trend)
    trend = trend.round(6)
    return trend


def add_trend(data: pd.DataFrame, promo_cols: list, corr_threshold: float = 0.4) -> TupleDataAndCols:
    """
    Make trend of timeseries

    :param data: pd.DataFrame() - input dataframe to decompose trend
    :param promo_cols: list of promo cols to remove promo periods prior to extracting trend
    :param corr_threshold: minimum correlation between period num and target to consider a potential trend present

    :return: pd.Series - trend of timeseries
    """
    trend_col = 'trend'
    seasonality_col = 'seasonality_flag'

    data[trend_col] = 0
    data[seasonality_col] = False
    # prepare train dataset for trend extraction
    train = get_df_for_trend_extraction(data, promo_cols)

    # check train data len to be long enough
    freq = get_frequency(data.date)
    freq_dict = {'Y': 1, 'Q': 4, 'MS': 12, 'W': 52, 'D': 365}
    if (freq_dict.get(freq) is None) or (train.shape[0] <= freq_dict[freq] * 1.1):
        logger.debug('time series too short for trend or seasonality extraction')
        return data, [trend_col, seasonality_col]

    # check correlation of date with target
    corr_matrix = train[['target', 'time-period_num']].corr()
    if np.abs(corr_matrix.loc['target', 'time-period_num']) < corr_threshold:
        logger.debug(f'correlation between date and target is less then {corr_threshold}, trend set to 0')
        add_seasonal_flag(data, freq)
        return data, [trend_col, seasonality_col]

    # smoothen target time series
    train['target'] = smoothen_ts(train['target'], period=freq_dict.get(freq))
    train.dropna(subset=['date', 'target'], inplace=True)

    # get train params, predict (for use by forecasting)
    trend_start_date = train.date.max() - timedelta(days=365)
    trend_params = get_trend_params(train.loc[train.date >= trend_start_date, 'target'])
    data['trend'] = make_trend_from_params(data['time-period_num'], trend_params)

    # add seasonality flag
    add_seasonal_flag(data, freq, recalc_trend=True)

    return data, [trend_col, seasonality_col]
