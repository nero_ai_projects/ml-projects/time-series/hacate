"""
Tests for module metriclog.run_ab
"""

from dataclasses import replace
import pandas as pd

from ab.constants import ALL_KEYS_OUTPUT_COLUMNS
from ab.types import ABParams
from metriclog.types import HecateVersionsParams
from metriclog.constants import MAIN_AB_HECATE_PARAMS
from metriclog.run_ab import add_hecate_versions_params, compose_ab_summary


def test_select_ab_results_if_exist(db_connection):

    columns = list(ALL_KEYS_OUTPUT_COLUMNS + ('project',) + tuple(MAIN_AB_HECATE_PARAMS))
    true_ab_results = pd.DataFrame(data=[["test_key", "A won", 1, 100, 0.5, 1.5, 50, 150, "norm", 50, 1.0, "test",
                                          "branch_a_test", "commit_a_test", "branch_b_test", "commit_b_test",
                                          "mt", False, 1
                                          ]],
                                   columns=columns
                                   )
    hecate_versions_params = HecateVersionsParams(branch_a='branch_a_test',
                                                  commit_a_id='commit_a_test',
                                                  branch_b='branch_b_test',
                                                  commit_b_id='commit_b_test',
                                                  project='test',
                                                  fold=1,
                                                  focus=False,
                                                  horizon='mt',
                                                  )
    extracted_ab_results = db_connection.select_ab_results_if_exist(hecate_versions_params,
                                                                    'ab_results_' + hecate_versions_params.project)
    assert extracted_ab_results.effect_abs.sum() == true_ab_results.effect_abs.sum()
    assert set(extracted_ab_results.columns) == set(true_ab_results.columns)
    assert set(extracted_ab_results.key.unique()) == set(true_ab_results.key.unique())
    for key in extracted_ab_results.key.unique():
        assert (set(extracted_ab_results.loc[extracted_ab_results['key'] == key].values.tolist()[0]) ==
                set(true_ab_results.loc[true_ab_results['key'] == key].values.tolist()[0]))

    assert db_connection.select_ab_results_if_exist(HecateVersionsParams(branch_a='unexisting', project='test'),
                                                    'ab_results_test').empty


def test_add_hecate_versions_params():

    df = pd.DataFrame(data=[[1, 2]], columns=['one', 'two'])
    df = add_hecate_versions_params(df, HecateVersionsParams(branch_a='br_a', focus=False))
    assert set(df.loc[0, ['one', 'branch_a', 'focus']]) == set([1, 'br_a', False])


def test_compose_ab_summary(ab_results_test, ab_datasets, ab_summary_test):

    ab_results = ab_results_test
    data_a, data_b = ab_datasets
    summary = ab_summary_test
    ab_params = ABParams()
    ab_params.beta_ab = 0.2
    ab_params.n_signif_digits = 3

    summary_composed = compose_ab_summary(ab_results=ab_results,
                                          data_a=data_a,
                                          data_b=data_b,
                                          ab_params=ab_params
                                          )
    assert summary_composed.effect_abs.sum() == summary.effect_abs.sum()
    assert set(summary_composed.columns) == set(summary.columns)
    assert set(summary_composed.project.unique()) == set(summary.project.unique())
    for column in summary_composed.columns:
        assert set(summary_composed[column].values.tolist()) == set(summary[column].values.tolist())


def test_select_ab_summary_if_exist(db_connection, ab_summary_test):

    ab_summary = ab_summary_test
    hecate_versions_params = HecateVersionsParams(branch_a=ab_summary.branch_a[0],
                                                  commit_a_id=ab_summary.commit_a_id[0],
                                                  branch_b=ab_summary.branch_b[0],
                                                  commit_b_id=ab_summary.commit_b_id[0],
                                                  project=ab_summary.project[0],
                                                  fold=ab_summary.fold[0],
                                                  focus=ab_summary.focus[0],
                                                  horizon=ab_summary.horizon[0],
                                                  )
    unexisting_hvp = replace(hecate_versions_params, project='unexisting_project')

    db_connection.send_ab_results_to_db(ab_summary, replace(hecate_versions_params, project='ab_summary'))
    ab_summary_extracted = db_connection.select_ab_summary_if_exist(hecate_versions_params)
    for column in ab_summary.columns:
        assert set(ab_summary[column].values.tolist()) == set(ab_summary_extracted[column].values.tolist())

    assert db_connection.select_ab_summary_if_exist(unexisting_hvp).empty
