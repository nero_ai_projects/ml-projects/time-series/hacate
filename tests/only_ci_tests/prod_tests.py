from collections.abc import Callable
from functools import wraps
from typing import ParamSpec
import requests
import pandas as pd
from io import BytesIO
import json
import argparse
from itertools import product
from time import sleep
from http import HTTPStatus

from hecate.log import get_logger
logger = get_logger('hecate')


P = ParamSpec("P")

DATA_DICT = {
    'key': ['key-1', 'key-1', 'key-1', 'key-1'],
    'date': ['2022-07-04', '2022-07-11', '2022-07-18', '2022-07-25'],
    'partition': ['train', 'train', 'train', 'fc'],
    'target': [100.01, 1, 1, 0.0],
    'discount': [10, 0, 0, 0],
    'category': ['c'] * 4,
    'price_reg': [10] * 4
}
FEATURE_TYPES = {
    'discount': 'discount',
    'category': 'can_level',
    'price_reg': 'price_reg'
}


def init_params(url: str, api_key: str) -> list:
    """
    Initialize parameters pool for forming a request.
    :param url: string in the format f"https://{IP}:{port}"
    :param api_key: API key to verify access
    :return: list of parameters (in form of a dictionary) for request URL
    """
    parameters = []
    for endpoint, payload in product(['fc', 'fc_batch', 'get_can_pairs', 'decompose_act'],
                                     ['json', 'parquet']):
        parameters.append({
            'url': f'{url}/{endpoint}',
            'payload_type': payload,
            'api_key': api_key,
            'feature_types': FEATURE_TYPES
        })
    return parameters


def send_request(url: str,
                 api_key: str,
                 payload_type: str,
                 feature_types: dict,
                 status_code: HTTPStatus) -> None:
    """
    Function to send request with specified parameters to check the response status. Raises ValueError, if unexpected
    response is received.

    :param url: full url of an endpoint to send request to
    :param api_key: str, API key to verify access
    :param payload_type: str, type of data to use to send dataframe
    :param feature_types: dictionary of feature types
    :param status_code: HTTP Status, expected response status

    :return: None
    """
    data = pd.DataFrame.from_dict(DATA_DICT)
    req_data = dict()

    if payload_type == 'json':
        data_io = data.to_json()
        data_io = json.loads(data_io)
        req_data['json'] = data_io if status_code == HTTPStatus.OK else dict({'data': 'test'})
    else:
        data_io = BytesIO()
        data.to_parquet(data_io, index=False, compression='gzip')
        req_data['data'] = data_io.getvalue() if status_code == HTTPStatus.OK else bytes('test', 'utf-8')

    response = requests.post(f'{url}?payload_type={payload_type}',
                             headers={'api_key': api_key, 'Feature-Types': json.dumps(feature_types)},
                             **req_data)
    if response.status_code != status_code.value:
        raise ValueError(f'url: {url} and payload: {payload_type}. ' +
                         f'Expected status code: {status_code}, got {response.status_code}')


def retry(n: int) -> Callable[[Callable[P, None]], Callable[P, None]]:
    """
    Wrapper to perform multiple runs in the amount of n.
    :param n: number of times to repeat the code in case of failure.
    :return: function
    """

    def _retry(func: Callable[P, None]) -> Callable[P, None]:
        """
        The wrapper to perform multiple runs of a specified function.
        :param func: function to do multiple runs for
        :return: function
        """

        @wraps(func)
        def wrapper(*args: P.args, **kwargs: P.kwargs) -> None:
            """
            Wrapper to run a specified function with given arguments several times.
            """
            for i in range(n):
                try:
                    func(*args, **kwargs)
                except Exception as e:
                    logger.info(f'Failed run {i+1} with the arguments: {e}')
                    sleep(10)
                    continue
                else:
                    logger.info(f'Successfully ran {func.__name__}')
                    return
            raise ValueError('All runs have failed!')

        return wrapper

    return _retry


@retry(3)
def test_routes(parameters: list) -> None:
    """
    Function tests whether status code 200 is received, when dataframe is sent to a specified endpoint.
    :param parameters: list, containing dictionaries with info required to construct a correct request
    :return: None
    """
    for param in parameters:
        send_request(status_code=HTTPStatus.OK, **param)


@retry(3)
def test_app_fail(parameters: list) -> None:
    """
    Function tests whether status code 400 is received, when dataframe is sent to a specified endpoint.
    :param parameters: list, containing dictionaries with info required to construct a correct request
    :return: None
    """
    for param in parameters:
        send_request(status_code=HTTPStatus.BAD_REQUEST, **param)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('DOMAIN', type=str, help='Domain')
    parser.add_argument('PORT', type=str, help='Port')
    parser.add_argument('API_KEY', type=str, help='API key')
    args = parser.parse_args()

    params_list: list[dict] = init_params(f'https://{args.DOMAIN}:{args.PORT}', args.API_KEY)

    test_routes(params_list)
    test_app_fail(params_list)
