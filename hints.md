# Hecate 

## Framework:
    FastApi

# how to run app in dev mode

    export API_KEY=user1:key1;user2:key2
    uvicorn app:app

# Deploy

## on dev server

1. Upload files:

        hecate/
        logs/.gitkeep
        tests/
        app.py
        setup.py
        requirements.txt

2. install python3 (need to lock version)
3. install pip3

        sudo apt-get update
        sudo apt-get -y install python3-pip

4. install unicorn

        sudo apt-get install -y unicorn

5. install libs

        python3 -m pip install -r requirements.txt

6. add api_key to env:

        export API_KEY=user1:key1;user2:key2

7. (tmux) run unicorn:

        uvicorn app:app --host 0.0.0.0 --port 80

## Docs:
    http://127.0.0.1:8000/docs


## Father readings:
* Documentation: https://fastapi.tiangolo.com