import pandas as pd

from hecate.validations.formatting import is_data_support_format


def main(data: pd.DataFrame) -> bool:
    is_ok = is_data_support_format(data)
    return is_ok
