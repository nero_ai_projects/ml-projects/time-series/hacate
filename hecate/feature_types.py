"""
A module containing the logic of working with the feature_types parameter.
"""

from enum import Enum
from typing import Dict, List, Sequence


class FeatureType(str, Enum):
    desc: str

    def __new__(cls, name: str, desc: str):

        if not isinstance(name, str) or not isinstance(desc, str):
            raise TypeError(f"type of params {name=} or {desc=} is not str!")

        obj = str.__new__(cls, name)
        obj._value_ = name
        obj.desc = desc
        return obj

    can_level = ("can_level", "name of a column containing product level inside which to search for cannibalization")
    discount = ("discount", "info about promo discount (it is also used to generate auxiliary promo features)")
    promo = ("promo", "promo data, other than discount (e.g. promo type)")
    stock = ("stock", "info about stocks (zero values are used for the Out-Off-Stock feature)")
    promo_budget = ("promo_budget", "info about promo in volume (to generate a promo profile feature)")
    price_reg = ("price_reg", "regular price")
    dim = ("dim", "descriptive information (not used for forecasting)")
    stop = ("stop", "making up a zero forecast and chained columns(bl, uplift, etc) if stop is 1")

    @staticmethod
    def doc() -> str:
        return ''.join([f"<li>{ftype} - {ftype.desc}</li>" for ftype in FeatureType])


def get_cols_from_types(feature_types: Dict[str, FeatureType], types: Sequence[FeatureType]) -> List[str]:

    return [col for col, feature_type in feature_types.items() if feature_type in types]
