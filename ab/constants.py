"""
Contains constants that required in module ab
"""

ALL_KEYS_OUTPUT_COLUMNS = ('key',
                           'result',
                           'effect_abs',
                           'effect_percent',
                           'ci_abs_lower',
                           'ci_abs_upper',
                           'ci_percent_lower',
                           'ci_percent_upper',
                           'distribution',
                           'mde',
                           'weight')

TEST_KEYS = ('diff_norm', 'diff_lognorm', 'diff_bernoulli', 'undefined', 'uniform')
