import pytest
import os
import json
import pandas as pd

PATH_DIR = os.path.join(os.getcwd().split('tests')[0], 'tests')
CANNIBALIZATION_DIR = os.path.join(PATH_DIR, 'cannibalization', 'tests')


@pytest.fixture(params=os.listdir(CANNIBALIZATION_DIR),
                ids=os.listdir(CANNIBALIZATION_DIR))
def cannibalization_input_df(request):
    """
    Read data from the folder.
    """
    return get_data(os.path.join(CANNIBALIZATION_DIR, request.param))


def get_data(abs_path_to_folder):
    """
    Get test from file. Tests` directory is 'tests'.
    Each test save in separate directory.
    Name of directory is the future name of test.
    Name of data file: data.csv.
    Name of dictionary file: dict.json.
    Name of result file: result.csv (default)
    """
    data = pd.read_csv(os.path.join(abs_path_to_folder, 'data.csv'),
                       parse_dates=['date'])

    try:
        with open(os.path.join(abs_path_to_folder, 'dict.json'), 'r') as file:
            feature_types = json.load(file)
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        feature_types = {}

    return data, feature_types
