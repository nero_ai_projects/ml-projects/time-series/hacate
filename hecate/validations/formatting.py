import pandas as pd


def is_data_support_format(data: pd.DataFrame) -> bool:
    raise NotImplementedError
