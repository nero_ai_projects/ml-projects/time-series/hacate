import pandas as pd
import numpy as np
import datetime as dt
import re
import random
import string
from dateutil.relativedelta import relativedelta
from collections import OrderedDict
from typing import Literal, Tuple

import hecate.forecasting.feature_generation as feature_generation


class TSGenerator:
    """
    Class for generation of time series for tests.
    By default, generates 2 years of daily observations.
    """

    def __init__(self, formula, bl_start_value: float, trend_angle: float,
                 seasonality: list = None, season_profile: dict = None,
                 start_date: str = '2023-01-01', freq: Literal['D', 'W', 'M', 'Y'] = 'D', dur: str = '2Y',
                 promo_details: dict = None, price_details: dict = None, white_noise_sd: float = 9,
                 train_share: float = 0.8):
        """
        Initialize the class.
        :param formula: formula with coefficients of dependent variables - "true" dependency. May include a various
        number of variables of type d(iscount) and p(rice). Based on variable type, the data for it would be generated
        in a specific manner.
        Coefficients values are:
        1) for discount variable a coefficient shows target uplift, associated with discount change by +1% (discount
        percentages are supposed to be expressed in % form, not as a fraction of unit)
        2) for price variables a coefficient shows (negative) target uplift, associated with price change by +1%. The
        effect on target is not linear, it is a concave function with maximum value around the price change date and
        lower impact n days before and after. You can set a parameter to the function, generating target based on price
        changes, to correct price elasticity prior to price increase.
        NB! 1) The formula string operations HAVE to be separated by whitespaces.
        NB! 2) Available operations for variables: ['*', '/'], available operations between variables: ['+', '-'].
        NB! 3) The variables HAVE to have distinct names.

        :param bl_start_value: mean value of baseline
        :param trend_angle: angle of the added trend-line
        :param seasonality: a list with indication on types of seasonality to be added. Options are ['Y', 'M', 'W'].
        Seasonality in this case is generated using random values of a specified normal distribution. These values
        define whether target for a specific period should be lower or higher, than the mean
        :param season_profile: overrides the basic method applied for generating seasonality. In this parameter,
        a deviation from the mean is defined explicitly, no random generation is used.
        :param cann_promo_col: columns with cannibalizator discount
        :param start_date: start_date of the timescale
        :param freq: frequency of generated time series ('D', 'W', 'M', 'Y')
        :param dur: duration expressed in a string, an int or float, followed by duration ('D', 'W', 'M', 'Y')
        :param promo_details: dictionary of promo properties of the following structure {promo_var_id: {'dur': 7,
        'disc': 0.1, 'interval': 30}}. Param 'interval' is measured as the amount of days between promos of this type.
        According to the logics of forming a promo calendar, the most frequent promos would be replaced by less
        frequent promos according to their frequency. So, if promo type 1 is said to happen once in 30 days, and promo
        type 2 - twice a year (once in 180 days), the output promo calendar would contain 12 promos in  total, with
        each 6th promo being of the type 2.
        :param price_details: dictionary of price properties of the following structure: {price_var_id: {'start_value':
        100, 'interval': 90}}. Param 'inverval' is measured as the amount of days between price changes. Generated
        price changes may be decreasing, as well as increasing.
        :param white_noise_sd: standard deviation of the white noise
        :param train_share: share of observations to be marked as 'train'
        """
        self.formula = formula
        self.bl_start_value = bl_start_value
        self.trend_angle = trend_angle
        self.seasonality = seasonality if seasonality else ['Y', 'M', 'W']
        self.season_profile = season_profile if season_profile else {}
        self.start_date = pd.to_datetime(start_date, format='%Y-%m-%d')
        self.freq = freq
        self.dur = dur
        self.promo_details = promo_details
        self.price_details = price_details
        self.white_noise_sd = white_noise_sd
        self.train_share = train_share

        self.var_namings = {'d': 'discount', 'p': 'price'}
        self.vars = self._parse_formula()
        self.vars_by_type = {var_type: [k for k, v in self.vars.items() if v['type'] == var_type]
                             for var_type in self.var_namings.values()}

        self.target_components = list()
        self.data = pd.DataFrame()

    def _parse_dur(self):
        """
        Transform input duration in a relativedelta form to be used in _generate_timeline.
        """
        dur_list = re.split(r'([a-zA-Z])', self.dur)
        # so that the function could accept dur values with no defined number of periods (suggest 1)
        if dur_list[0] == '':
            dur_list[0] = 1
        # l is re-made to int, as relativedelta won't accept floats. Still, the floats are parsed as floats.
        l, f = int(float(dur_list[0])), dur_list[1]

        func_dict = {'D': relativedelta(days=l),
                     'W': relativedelta(weeks=l),
                     'M': relativedelta(months=l),
                     'Y': relativedelta(years=l)}

        return func_dict[f]

    def _parse_formula(self):
        """
        Get variables and their coefficients from input formula.
        """
        formula_long = self.formula.split(' ')

        # move '-' sign to a following float
        for ind in [i for i, v in enumerate(formula_long) if v in ['-']]:
            formula_long[ind + 1] = str(-1 * float(formula_long[ind + 1]))

        var_d = {'*': 'direct', '/': 'reverse'}
        v = []
        t = []
        c = []
        d = []
        for var in formula_long:
            if var in ['*', '/']:
                d.append(var_d[var])
                continue
            if var in ['-', '+']:
                continue
            try:
                c.append(float(var))
            except ValueError:
                v.append(var)
                t.append(re.split(r'(\d+)', var)[0])
        return {var: {'type': self.var_namings[var_type], 'coef': coef, 'direction': direction}
                for var, var_type, coef, direction in zip(v, t, c, d)}

    def _generate_timeline(self):
        """
        Generates timeline based on input variables.
        """
        end_date = self.start_date + self._parse_dur()
        self.year_n = int((end_date - self.start_date).days / 365) + 1
        self.data = pd.DataFrame(pd.date_range(self.start_date, end_date, freq=self.freq), columns=['date'])

    @staticmethod
    def _generate_random_percent_changes(unchanged_percent: float = 0.8) -> Tuple:
        """
        Generates a list for random.choices to pick percent changes from, as well as probability distribution for the
        values.

        :param unchanged_percent: probability of percent change to equal zero

        :return: a tuple with a list for random.choices selection, and probabilities for respective values to be picked
        """
        var = [round(-0.10 + 0.05 * i, 2) for i in range(8)]
        var.remove(0)
        prob = [abs(1 / p ** 1.5) for p in var]

        # add zeroes and define its probability
        var.append(0)
        prob.append(unchanged_percent / (1 - unchanged_percent) * sum(prob))

        return var, prob

    def _generate_promo(self, start_date_var: int = 5, discount_var: float = 0.5, promo_dur_var: tuple = (3, 5)):
        """
        Generates promo timeline based on input variables. 'promo_id' and 'discount' columns are merged to self.data.

        :param start_date_var: an integer defining variability of an interval between promos when forming a promo
        calendar
        :param discount_var: a float, controlling the variability of discount. When set to maximum value (1), the
        variability is absent, meaning the promo discounts are equal to the ones defined in promo_details param
        :param promo_dur_var: a tuple, mean and standard deviation defining variability of promo period start

        #TO-DO all the promo types are treated as one, forming one output discount columns. It should be split by types
        of dependency instead - cannibalizator discounts have reverse effect on target variable.
        """
        # define promo start dates based on base_promo frequency
        # base_promo defined as the most frequent promo
        base_promo_interval = min([v['interval'] for v in self.promo_details.values()])
        base_promo = [i for i, v in self.promo_details.items() if v['interval'] == base_promo_interval][0]
        base_promo_disc = self.promo_details[base_promo]['disc']

        dates = [self.start_date + dt.timedelta(days=random.randint(0, start_date_var * 30))]
        for i in range(int(365 / base_promo_interval) * self.year_n - 1):
            dates.append(dates[i] +
                         dt.timedelta(days=base_promo_interval + random.randint(-start_date_var, start_date_var)))

        # create promo_cal with base promos
        cal = OrderedDict([(date, [
            base_promo,
            base_promo_disc * (1 + random.choices(*self._generate_random_percent_changes(discount_var))[0]),
            date + dt.timedelta(days=self.promo_details[base_promo]['dur'] + np.random.normal(*promo_dur_var))
        ]) for date in dates])

        # replace some promos with other promo types
        for promo in set(self.promo_details.keys()).difference(base_promo):
            interval = self.promo_details[promo]['interval']
            freq = interval / base_promo_interval if interval != base_promo_interval else 2
            for ind, date in enumerate(cal.keys()):
                if ind % freq == 0:
                    cal[date] = [
                        promo,
                        self.promo_details[promo]['disc'] *
                        (1 + random.choices(*self._generate_random_percent_changes(discount_var))[0]),
                        date + dt.timedelta(days=self.promo_details[promo]['dur'] + np.random.normal(*promo_dur_var))
                    ]
        cal = pd.DataFrame.from_dict(cal, orient='index').reset_index()
        cal.columns = ['promo_start', 'promo_id', 'discount', 'promo_end']

        # make long form
        cal_long = {}
        for ind, row in cal.iterrows():
            dates = pd.date_range(row['promo_start'], row['promo_end'])
            cal_long.update({date: [row['promo_id'], row['discount'], ind] for ind, date in enumerate(dates)})
        cal_long = pd.DataFrame.from_dict(cal_long, orient='index').reset_index()
        cal_long.columns = ['date', 'promo_id', 'discount', 'profile_num']

        self.data = self.data.merge(cal_long, how='left', on='date')
        self.data.fillna({'promo_id': '', 'discount': 0, 'profile_num': 0}, inplace=True)

    def _generate_price(self, price_col: str, start_date_var: int = 15):
        """
        Generates prices based on input variables. column named as price_col param merged to self.data.

        :param price_col: name of the price variable
        :param start_date_var: an integer defining variability of an interval between price changes
        """
        start_value = self.price_details[price_col]['start_value']
        interval = self.price_details[price_col]['interval']

        # generate price change periods
        dates = [self.start_date + dt.timedelta(days=interval + random.randint(-start_date_var, start_date_var))]
        for i in range(int(365 / interval) * self.year_n - 1):
            dates.append(dates[i] + dt.timedelta(days=interval + random.randint(-start_date_var, start_date_var)))

        # make into dataframe
        changes = {date: 1 + random.choices(*self._generate_random_percent_changes(0))[0] for date in dates}
        changes = pd.DataFrame.from_dict(changes, orient='index').reset_index()
        changes.columns = ['date', price_col]

        # transform from price increase to prices
        changes[price_col] = start_value * changes[price_col].cumprod()
        changes[price_col] = changes[price_col].apply(lambda x: int(x))

        # merge with self.data
        self.data = self.data.merge(changes, how='left', on='date')
        self.data[price_col].ffill(inplace=True)
        self.data[price_col].fillna(start_value, inplace=True)

    def _generate_x(self):
        """
        Generates independent variables based on input variables.
        """
        if self.vars_by_type['discount']:
            self._generate_promo()

        for price_var in self.vars_by_type['price']:
            self._generate_price(price_var)

    def _generate_baseline(self):
        """
        Adds a 'bl' column to the data, values of which are equal to bl_start_value input parameter.
        """
        self.data['bl'] = self.bl_start_value
        self.target_components.append('bl')

    def _generate_discount_uplift(self):
        """
        Generates target uplift based on discount value and formula coefficient.
        """
        data = self.data
        data['disc_coef'] = data.promo_id.replace({var: details['coef'] for var, details in self.vars.items()
                                                   if details['type'] == 'discount'})
        data['disc_coef'].replace({'': 0}, inplace=True)

        data['disc_dependency'] = data.promo_id.replace({var: details['direction'] for var, details in self.vars.items()
                                                         if details['type'] == 'discount'})
        data['disc_uplift'] = np.where(data.disc_dependency == 'reverse', data.disc_coef / data.discount,
                                       data.disc_coef * data.discount)
        data.drop(['disc_coef', 'disc_dependency'], axis=1, inplace=True)

        self.data = data
        self.target_components.append('disc_uplift')

    def _generate_promo_profile(self, mean: float = 0, sd: float = 0.2):
        """
        Remakes profile_num column in self.data to profile values.

        :param mean: mean of the normal distribution to get profile values from
        :param sd: standard deviation of the normal distribution to get profile values from
        """
        data = self.data

        out = [data[data.profile_num == 0]]
        for promo, mdf in data[data.profile_num != 0].groupby('promo_id'):
            sdf = mdf.copy()
            mapping = zip(sdf.profile_num.unique(), np.random.normal(mean, sd, sdf.profile_num.nunique()))
            sdf['profile_num'].replace({p: v for p, v in mapping}, inplace=True)
            out.append(sdf)

        data = pd.concat(out).sort_values(by='date')
        data['profile_effect'] = data.disc_uplift * (1 + data.profile_num)

        self.data = data
        self.target_components.append('profile_effect')

    def _generate_price_effect(self, price_col: str, fd: int = 30, price_effect_dur_days: int = 21,
                               pre_price_increase_sensitivity: float = 0.4, promo_price_sensitivity: float = 0.1):
        """
        Generates target change based on price values and formula coefficients.

        :param price_col: name of the price variable in question
        :param fd: degrees of freedom, a parameter affecting the shape of t-distribution
        :param price_effect_dur_days: length of the price increase effect. t-distribution would be applied across this
        number of periods
        :param pre_price_increase_sensitivity: correction of pre price increase elasticity. Target change before price
        increase would be multiplied by this value
        :param promo_price_sensitivity: correction of price increase elasticity during promo periods. Target change
        during promo periods would be multiplied by this value
        """
        effect_col = f'price_effect_{price_col}'

        data = self.data
        coef = self.vars[price_col]['coef']

        # add period value
        feature_generation.num_periods(data, price_col, fd=fd, price_effect_dur_days=price_effect_dur_days)

        # calc associated target uplift
        data[effect_col] = coef * 100 * data[f'business-{price_col}_price_period_num_after'] - \
            pre_price_increase_sensitivity * 100 * coef * data[f'business-{price_col}_price_period_num_before']
        data.drop([f'business-{price_col}_price_period_num_before',
                   f'business-{price_col}_price_period_num_after'], axis=1, inplace=True)

        # correct for direction
        if self.vars[price_col]['direction'] == 'reverse':
            data[effect_col] *= -1

        # correct uplift for promo periods
        if 'discount' in data.columns:
            data[effect_col] = data[effect_col] * np.where(data.discount != 0, promo_price_sensitivity, 1)

        self.data = data
        self.target_components.append(effect_col)

    def _generate_trend(self, promo_angle_correction: float = 0.4):
        """
        Generates trend and adds it to the target variable.

        :param promo_angle_correction: a parameter for correcting promo angle during promo periods
        """
        data = self.data

        data['trend'] = 1 + self.trend_angle * data.index

        if 'discount' in data.columns:
            data['promo_trend'] = 1 + self.trend_angle * promo_angle_correction * data.index
            data['trend'] = np.where(data.discount != 0, data.promo_trend, data.trend)
            data.drop('promo_trend', axis=1, inplace=True)

        self.data = data
        self.target_components.append('trend')

    def _generate_seasonality(self, freq: str, mean: float = 0, sd: float = 0.05, promo_correction: float = 0.8):
        """
        Add to data a separate column for the specified type of seasonality.

        :param freq: type of seasonality to add.
        :param mean: mean of the normal distribution to get seasonality values from
        :param sd: standard deviation of the normal distribution to get seasonality values from
        :param promo_correction: correction of seasonality during promo periods
        """
        col = freq + '_seasonality'

        data = self.data

        if freq in self.season_profile:
            s_dict = {'Y': 'year', 'M': 'month', 'W': 'week'}
            data[col] = getattr(data.date.dt, s_dict[freq])
            data[col].replace({p: v for p, v in zip(data[col].unique(), self.season_profile[freq])}, inplace=True)

        else:
            s_dict = {'Y': 'month', 'M': 'day', 'W': 'weekday'}
            data[col] = getattr(data.date.dt, s_dict[freq])
            data[col].replace({p: v for p, v in zip(data[col].unique(),
                                                    np.random.normal(mean, sd, data[col].nunique()))},
                              inplace=True)

        if 'discount' in data.columns:
            data[col] *= np.where(data.discount != 0, promo_correction, 1)

        self.data = data

    def _calculate_final_target(self):
        """
        1) sums up all target components
        2) adds seasonality on top
        3) adds white noise
        """
        data = self.data
        data['target'] = data[self.target_components].sum(axis=1)
        for freq in self.seasonality:
            data['target'] *= 1 + data[freq + '_seasonality']
        self.data = data

    def _generate_white_noise(self):
        """
        Generate white noise (added column 'white_noise'), according to input white_noise_sd.
        """
        self.data['white_noise'] = np.random.normal(0, self.white_noise_sd, len(self.data))
        self.data['target'] += self.data.white_noise

    def _postprocess(self):
        """
        Removes negative values, adds key column.
        """
        self.data['target'] = np.where(self.data.target < 0, 0, self.data.target)
        self.data['key'] = ''.join(random.choices(string.ascii_uppercase + string.digits, k=16))

    def _add_partition(self):
        """
        Add partition based on input variable train_share.
        """
        self.data['partition'] = np.where(self.data.index < self.train_share * self.data.shape[0], 'train', 'fc')

    def _generate_y(self):
        """
        Generates dependent variable based on input variables.
        """
        # generate baseline
        self._generate_baseline()

        # generate values based on x
        if self.vars_by_type['discount']:
            self._generate_discount_uplift()
            self._generate_promo_profile()

        for price_var in self.vars_by_type['price']:
            self._generate_price_effect(price_var)

        # generate trend-line
        self._generate_trend()

        # generate seasonality
        for freq in set(self.seasonality + list(self.season_profile.keys())):
            self._generate_seasonality(freq)

        # calculate final target
        self._calculate_final_target()

        # add white noise
        self._generate_white_noise()

        # postprocess
        self._postprocess()

        # add partition
        self._add_partition()

    def get(self, usecols: Literal['hc', 'all'] = 'hc'):
        """
        Return the final generated data.

        :param usecols: which columns to return. If 'hc' - returns columns for running hecate, if 'all' - returns all.
        """
        self._generate_timeline()
        self._generate_x()
        self._generate_y()

        if usecols == 'hc':
            usecols = ['key', 'date', 'partition', 'target']
            if self.vars_by_type['discount']:
                usecols.append('discount')
            for price_var in self.vars_by_type['price']:
                usecols.append(price_var)
        else:
            usecols = self.data.columns

        return self.data[usecols]
