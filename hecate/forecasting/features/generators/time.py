import pandas as pd

from hecate.forecasting.features.generators.base import DummyFGenerator
from hecate.forecasting.feature_generation import add_time_features
from hecate.forecasting.utils import HorizonMixin
from hecate.forecasting.horizon import Horizon
from hecate.features import TypesOfFeatures
from hecate.types import TupleDataAndCols


class DefaultFGenerator(DummyFGenerator):

    def add_features(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None,
                     time_features: list[str] | None = None) -> TupleDataAndCols:
        if time_features is not None:
            data, added_features = add_time_features(data, time_features=time_features)
        else:
            added_features = []
        return data, added_features


class STFGenerator(DefaultFGenerator):
    ...


class MTFGenerator(DefaultFGenerator):
    ...


class LTFGenerator(DefaultFGenerator):
    ...


class TimeFGenerator(DummyFGenerator, HorizonMixin):

    def add_features(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None,
                     time_features: list[str] | None = None) -> TupleDataAndCols:
        generator_from_horizon = {
            Horizon.ST: STFGenerator,
            Horizon.MT: MTFGenerator,
            Horizon.LT: LTFGenerator
        }
        generator = generator_from_horizon.get(self.horizon, DefaultFGenerator)
        data, added_features = generator().add_features(data,
                                                        time_features=[] if time_features is None else time_features)

        return data, added_features
