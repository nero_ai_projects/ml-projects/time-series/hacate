"""
Tests of ab.py
"""
import pytest
import numpy as np
import pandas as pd

from ab import ab
from ab.types import ABResult, ABParams
from ab.statistics import n_significant_digits
from ab.constants import TEST_KEYS


def test_ab_single(ab_test_input):

    data_a, data_b, _ = ab_test_input

    def effect_and_dist(set_a, set_b, key):
        """
        Returns effect_percent and distribution for the given key
        """
        if key == 'diff_norm':
            effect_percent = n_significant_digits(
                n_significant_digits((np.mean(set_b) - np.mean(set_a))) / np.mean(set_a) * 100
                )
            dist = 'norm'
        elif key == 'diff_bernoulli':
            effect_percent = n_significant_digits(
                n_significant_digits((np.sum(set_b) + 1) / (len(set_b) + 2) - (np.sum(set_a) + 1) / (len(set_a) + 2))
                / ((np.sum(set_a) + 1) / (len(set_a) + 2)) * 100
                )
            dist = 'bernoulli'
        elif key == 'diff_lognorm':
            effect_percent = n_significant_digits(
                n_significant_digits(np.exp(np.mean(np.log(set_b)) + 0.5 * np.std(np.log(set_b))**2)
                                     - np.exp(np.mean(np.log(set_a)) + 0.5 * np.std(np.log(set_a))**2))
                / np.exp(np.mean(np.log(set_a)) + 0.5 * np.std(np.log(set_a))**2) * 100
                )
            dist = 'lognorm'
        elif key == 'undefined':
            effect_percent = n_significant_digits(
                n_significant_digits((np.median(set_b) - np.median(set_a))) / np.median(set_a) * 100
                )
            dist = 'undefined'

        return effect_percent, dist

    for key in ['diff_norm', 'diff_lognorm', 'diff_bernoulli', 'undefined']:
        a = data_a.loc[data_a.key == key, 'metrics']
        b = data_b.loc[data_b.key == key, 'metrics']
        # setting shuffled data a
        a1 = a.values.copy()
        np.random.shuffle(a1)
        a1 = pd.Series(a1)
        effect, dist = effect_and_dist(set_a=a, set_b=b, key=key)

        ab_params = ABParams(distribution=dist)
        ab_params1 = ABParams(distribution=dist, greater_is_better=True)

        result1 = ab.ab_single(metrics_a=a, metrics_b=a1, ab_params=ab_params)
        assert result1.result == 'tie'
        assert result1.ci_abs_lower * result1.ci_abs_upper < 0  # check for CI_abs boundaries have diffrent sign
        assert np.abs(result1.effect_percent) < 0.01  # check for effect_percent

        result2 = ab.ab_single(metrics_a=a, metrics_b=b, ab_params=ab_params)
        assert result2.result == 'A won'
        assert result2.ci_abs_lower * result2.ci_abs_upper > 0  # check for CI_abs boundaries have the same sign

        result3 = ab.ab_single(metrics_a=a, metrics_b=b, ab_params=ab_params1)
        assert result3.result == 'B won'
        assert np.abs(result3.effect_percent - effect) < 0.01  # check for effect_percent


def test_ab_test_one_key(ab_test_input):

    data_a, data_b, report = ab_test_input
    # unequal data size teatment
    a_long = np.random.random(size=100)
    b_short = np.random.random(size=50)
    ab_params = ABParams()
    with pytest.raises(ValueError):
        ab.ab_test_one_key(a_long, b_short, ab_params)

    # empty data treatment
    a_empty = data_a.loc[data_a.key == 'all_nans', 'metrics'].dropna()
    b_empty = data_b.loc[data_b.key == 'all_nans', 'metrics'].dropna()
    empty_res = ABResult.NO_DATA
    assert ab.ab_test_one_key(a_empty, b_empty, ab_params).loc[0, 'result'] == empty_res

    data_a1 = data_a[data_a.partition == 'fc']
    data_b1 = data_b[data_b.partition == 'fc']
    report = report.set_index('key', drop=True).drop(columns='weight')
    test_keys = list(TEST_KEYS)
    test_keys.remove('diff_lognorm')
    ab_params1 = ABParams(beta_ab=0.2, n_signif_digits=3)
    for key in test_keys:
        a = data_a1.loc[data_a1.key == key, 'metrics'].values
        b = data_b1.loc[data_b1.key == key, 'metrics'].values
        assert (report.loc[report.index == key].reset_index([0], drop=True)
                .equals(ab.ab_test_one_key(a, b, ab_params1)[report.columns.to_list()]))

    a = data_a1.loc[data_a1.key == 'diff_lognorm', 'metrics'].values
    b = data_b1.loc[data_b1.key == 'diff_lognorm', 'metrics'].values
    assert (report
            .loc[report.index == 'diff_lognorm']
            .reset_index([0], drop=True)
            .drop(columns=['ci_abs_lower', 'ci_abs_upper'])
            .equals(ab.ab_test_one_key(a, b, ab_params1)[report.columns.to_list()]
                    .drop(columns=['ci_abs_lower', 'ci_abs_upper'])
                    )
            )
    assert 6.3 < ab.ab_test_one_key(a, b, ab_params1).loc[0, 'ci_abs_lower'] < 7.3
    assert 18.2 < ab.ab_test_one_key(a, b, ab_params1).loc[0, 'ci_abs_upper'] < 19.2


def test_prepare_input_data(ab_test_input):

    data_a, data_b, _ = ab_test_input
    cols = ['key', 'date', 'partition', 'target', 'fc', 'metrics']
    a = data_a[cols]
    a['metrics'] = a.metrics.astype(str)
    data_ab = ab.prepare_input_data(a, data_b, add_metrics=False)
    assert data_ab.metrics_a.dtype == data_ab.metrics_b.dtype == 'float'
    assert np.sum(np.abs(ab.prepare_input_data(data_a, data_b, add_metrics=False).metrics_a -
                         ab.prepare_input_data(data_a.drop(columns='metrics'),
                                               data_b.drop(columns='metrics'),
                                               add_metrics=True).metrics_a)) < 1e-5


def test_ab_test_all_keys(ab_test_input):

    data_a, data_b, result = ab_test_input
    result = result.set_index('key', drop=True)
    result['weight'] = n_significant_digits(1 / len(result), n=3)
    ab_params = ABParams()

    with pytest.raises(ValueError):
        ab.ab_test_all_keys(data_a.loc[data_a.key == 'some_nans'].fillna(0),
                            data_b.loc[data_b.key == 'some_nans'],
                            ab_params
                            )
    with pytest.raises(ValueError):
        ab.ab_test_all_keys(data_a.loc[data_a.key == 'some_nans'],
                            data_b.loc[data_b.key == 'some_nans'].fillna(0),
                            ab_params
                            )
    with pytest.raises(ValueError):
        ab.ab_test_all_keys(data_a.loc[data_a.key == 'some_nans'].dropna(),
                            data_b.loc[data_b.key == 'some_nans'].dropna(),
                            ab_params
                            )

    cols = ['key', 'date', 'partition', 'target']
    with pytest.raises(ValueError):
        ab.ab_test_all_keys(data_a.loc[data_a.key.isin(TEST_KEYS)][cols],
                            data_b.loc[data_b.key.isin(TEST_KEYS)].drop(columns='metrics'),
                            ab_params,
                            )
    ab_params = ABParams(add_metrics=False)
    with pytest.raises(ValueError):
        ab.ab_test_all_keys(data_a.loc[data_a.key.isin(TEST_KEYS)][cols],
                            data_b.loc[data_b.key.isin(TEST_KEYS)],
                            ab_params,
                            )

    data_a1 = data_a.copy()
    data_a1['date'] = data_a.date.min()
    with pytest.raises(ValueError):
        ab.ab_test_all_keys(data_a1.loc[data_a1.key.isin(TEST_KEYS)],
                            data_b.loc[data_b.key.isin(TEST_KEYS)],
                            ab_params,
                            )
    data_a1['date'] = data_a.date.max()
    with pytest.raises(ValueError):
        ab.ab_test_all_keys(data_a1.loc[data_a1.key.isin(TEST_KEYS)],
                            data_b.loc[data_b.key.isin(TEST_KEYS)],
                            ab_params,
                            )

    data_a1['date'] = data_a.date
    data_a1.loc[1000, 'key'] = 'diff_norm'
    with pytest.raises(ValueError):
        ab.ab_test_all_keys(data_a1.loc[data_a1.key.isin(TEST_KEYS)],
                            data_b.loc[data_b.key.isin(TEST_KEYS)],
                            ab_params,
                            )

    ab_params = ABParams(add_metrics=True, beta_ab=0.2, n_signif_digits=3)
    report = ab.ab_test_all_keys(data_a.loc[data_a.key.isin(TEST_KEYS)].drop(columns='metrics'),
                                 data_b.loc[data_b.key.isin(TEST_KEYS)].drop(columns='metrics'),
                                 ab_params
                                 ).set_index('key', drop=True)[result.columns.to_list()]

    assert (report.drop(columns=['ci_abs_lower', 'ci_abs_upper'])
            .equals(result.drop(columns=['ci_abs_lower', 'ci_abs_upper']))
            )

    test_keys_ci = list(TEST_KEYS)
    test_keys_ci.remove('diff_lognorm')
    for key in test_keys_ci:
        assert report.loc[key, 'ci_abs_lower'] == result.loc[key, 'ci_abs_lower']
        assert report.loc[key, 'ci_abs_upper'] == result.loc[key, 'ci_abs_upper']
    assert 6.3 < report.loc['diff_lognorm', 'ci_abs_lower'] < 7.3
    assert 18.2 < report.loc['diff_lognorm', 'ci_abs_upper'] < 19.2
