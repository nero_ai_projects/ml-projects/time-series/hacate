import pandas as pd

from hecate.forecasting import model_pool
from hecate.feature_types import FeatureType


def test_model_pool(hecate_input_df):
    data, feature_types, result = hecate_input_df
    data['date'] = pd.to_datetime(data['date'])
    features = [key for key, item in feature_types.items() if item not in [FeatureType.dim]]
    models = model_pool.model_pool()
    features = data[features]
    target = data['target']
    for model_name, params, constructor in models:
        model = constructor(**params)
        if len(features.columns) > 0:
            model.fit(features, target)
            predict = model.predict(features)
            assert len(predict) == len(target)
            assert False in list(pd.DataFrame(predict).isna())


def test_init_model(hecate_input_df):
    data, feature_types, result = hecate_input_df
    data['date'] = pd.to_datetime(data['date'])
    feature_cols = [key for key, item in feature_types.items() if item not in [FeatureType.dim]]
    features = data[feature_cols]
    target = data['target']
    min_max_cols = feature_cols
    ohe_cols = []
    x_cols = feature_cols
    models_list = model_pool.init_models(min_max_cols, ohe_cols, x_cols)
    for dict_params in models_list:
        model = dict_params['model']
        if len(features.columns) > 0:
            model.fit(features, target)
            predict = model.predict(features)
            assert len(predict) == len(target)
            assert False in list(pd.DataFrame(predict).isna())
