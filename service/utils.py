import os
import uuid
import json
from io import BytesIO, StringIO
import hashlib
import pandas as pd
from fastapi import Header, HTTPException

API_KEYS_DICT = None


def set_up_api_keys():
    """
    Load API KEY from env file and parse it

    :return: dict of api_keys if API KEY exist else empty dict
    """
    api_key = os.getenv('API_KEY')
    api_keys_dict = {}
    if not api_key:
        return api_keys_dict
    list_keys = api_key.split(";")
    for item in list_keys:
        temp_list = item.split(":")
        api_keys_dict[temp_list[0]] = temp_list[1]
    return api_keys_dict


def generate_request_id() -> str:

    return str(uuid.uuid4()).replace("-", "")


async def is_authorized(api_key: str | None = Header(title="api_key",
                                                     convert_underscores=False)) -> str:
    """
    Check authorization by api_key
    :param api_key: hash that will be used to authorization
    :return: (True, user) if authorized else (False, None)
    """
    if not api_key:
        raise HTTPException(status_code=418, detail="Not authorized")

    global API_KEYS_DICT
    if API_KEYS_DICT is None:
        API_KEYS_DICT = set_up_api_keys()

    for user in API_KEYS_DICT:
        expected_hash = hashlib.sha512(
            API_KEYS_DICT[user].encode()
        ).hexdigest()

        if api_key == expected_hash:
            return user

    raise HTTPException(status_code=418, detail="Not authorized")


def body_to_df(body: bytes,
               payload_type: str | None) -> pd.DataFrame:
    """
    Convert request body to pandas DataFrame
    :param payload_type: define what is payload. Support "json", "csv". If None works with data as a parquet bytes.
    :param data: request body data for get DataFrame
    :return: pandas DataFrame for forecast
    """
    if payload_type is not None and payload_type == "json":
        data = pd.DataFrame(json.loads(body))

    elif payload_type is not None and payload_type == "csv":
        data = pd.read_csv(StringIO(body.decode(encoding='utf8')), sep=';')

    else:
        data = pd.read_parquet(BytesIO(body))

    return data


def get_binary_outcome(outcome: pd.DataFrame) -> bytes:
    """
    Convert pandas DataFrame to bytes
    :param outcome: output pandas dataframe for binary converting
    :return: binary pandas dataframe
    """
    byte_io = BytesIO()
    outcome.to_parquet(byte_io, index=False, compression='gzip')
    binary_outcome = byte_io.getvalue()

    return binary_outcome


def build_outcome_content(outcome: pd.DataFrame, payload_type: str) -> str | bytes:
    """
    Builds the content for response according to paylod_type

    :param outcome: pd.DataFrame that will be converted to content for transmission
    :param payload_type: json, csv, None. If None parquet will be used
    :return: str if payload_type is json else bytes
    """
    if payload_type == "json":
        content = outcome.to_json()

    elif payload_type == "csv":
        iostr = StringIO()
        outcome.to_csv(iostr, encoding='utf8', sep=';', index=False)
        content = iostr.getvalue().encode('utf8')

    else:
        content = get_binary_outcome(outcome)

    return content
