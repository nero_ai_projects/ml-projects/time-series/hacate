import json
from fastapi import APIRouter, Query, Header, Request, Response, Depends
from starlette.responses import JSONResponse

import hecate.hecate as hc
from hecate.log import get_logger, get_request_id, set_up_user_name
from hecate.utils.utils import datetime_converting
from hecate.validations import quality
from hecate.config import FAILED_DF_PATH
from hecate.features import TypesOfFeatures
from hecate.forecasting.decompositions.decomposition import get_decomposition
from service import worker
from service.utils import body_to_df, build_outcome_content, is_authorized
from service.swagger.description import SwaggerDescription
from service.swagger.example import Examples
from service.swagger.response import SwaggerResponse
from service.swagger.request import SwaggerRequest
from service.swagger.schemas import SwaggerSchemas


desc = SwaggerDescription()
examp = Examples()
resp = SwaggerResponse()
reqs = SwaggerRequest()
scheme = SwaggerSchemas()
router = APIRouter()
fc_new_docs = None
logger = get_logger("hecate", service_kind="app")


@router.post(
    "/fc",
    description=fc_new_docs,
    openapi_extra=reqs.fc_openapi_extra,
    responses=resp.fc_response
)
async def fc(
    request: Request,
    payload_type: str = Query(default="parquet", examples=examp.payload_type_examples),
    decompose: list[str] = Query(default=["all"], examples=examp.decompose_type_examples),
    add_columns: list[str] = Query(default=[], description='Existed columns to add in result dataframe'),
    fc_only: bool = Query(default=False, description='If True only fc horizon will be returned'),
    decimals: int = Query(default=2, ge=0, description="Number of decimal places"),
    feature_types: str = Header(title="Feature-Types", default="{}",
                                description=desc.headers_desc["feature_types"]),
    prediction_interval: bool = Query(default=False, description=desc.prediction_interval_desc),
    pi_prob_list: list[float] | None = Query(default=[0.68], gt=0, lt=1, description=desc.pi_prob_list_desc),
    dependencies=Depends(is_authorized)
) -> Response:
    """
    Route fc for forecast.

    :param **payload_type**: type of request data ["parquet", "json", "csv"]\n
    :param **api_key**: key for authorization, set into header\n
    :param **Feature_Types**: types for feature\n
    :param **Decompose**: which drivers to return along with the forecast\n
    :param **add_columns**: input columns to pass in the result\n
    :param **fc_only**: If True only fc horizon will be returned\n
    :param **decimals**: Number of decimal places\n
    :param **prediction_interval**: If True calcultes prediction interval(s)\n
    :param **pi_prob_list**: List of probabilities for prediction interval(s). Values must be from 0 to 1 exclusive\n

    """
    set_up_user_name(logger, dependencies)
    logger.info("Start Forecast")

    body = await request.body()
    try:
        data = body_to_df(body, payload_type)
    except Exception as e:
        logger.exception('df building from request body failed:')
        return Response(content=str(e), status_code=400)

    try:
        feat_types: TypesOfFeatures = json.loads(feature_types)
    except json.decoder.JSONDecodeError:
        emsg = \
            """error during feature_types reading. feature_types format: {"feature": "dim"},
            keys and values must be in double quoters"""
        logger.exception(emsg)
        return Response(content=emsg, status_code=400)

    drivers = get_decomposition(decompose[0] if decompose[0] in ['all', 'none'] else decompose)

    missed_columns = quality.get_missed_columns(data, feat_types) + list(
                        filter(lambda col: col not in data.columns, add_columns))
    if missed_columns:
        logger.info(f"Columns in data: {data.columns}, feature types: {feat_types}, missed: {missed_columns}")
        return Response(status_code=400, content=f"missed columns: {missed_columns}")

    added_partition = fc_only and 'partition' not in add_columns
    if added_partition:
        add_columns.append('partition')

    if prediction_interval and decompose[0] != 'none':
        logger.error(f"Wrong input: prediction_interval = {prediction_interval} and decompose = {decompose}")
        return Response(content="Cannot perform decomposition and prediction interval calculations simultaneously",
                        status_code=400)

    fc_result = hc.fc_sample(data, feat_types, drivers=drivers, add_columns=add_columns, decimals=decimals,
                             prediction_interval=prediction_interval, pi_prob_list=pi_prob_list)

    if fc_result.is_failed:
        logger.error("done with internal server error")
        return Response(content=fc_result.traceback, status_code=500)

    outcome = fc_result.data
    if fc_only:
        outcome = outcome.query('partition == "fc"')
        if added_partition:
            del outcome['partition']

    outcome = datetime_converting(outcome)

    binary_outcome = build_outcome_content(outcome, payload_type)

    logger.info("Finish Forecast")

    return Response(content=binary_outcome,
                    media_type=scheme.payload_type[payload_type])


@router.post(
    "/fc_batch",
    description=fc_new_docs,
    openapi_extra=reqs.fc_openapi_extra,
    responses=resp.fc_response
)
async def fc_batch(
    request: Request,
    payload_type: str = Query(default="parquet", examples=examp.payload_type_examples),
    decompose: list[str] = Query(default=["all"], examples=examp.decompose_type_examples),
    add_columns: list[str] = Query(default=[], description='Existed columns to add in result dataframe'),
    fc_only: bool = Query(default=False, description='If True only fc horizon will be returned'),
    decimals: int = Query(default=2, ge=0, description="Number of decimal places"),
    parallel: bool = True,
    feature_types: str = Header(title="Feature-Types", default="{}",
                                description=desc.headers_desc["feature_types"]),
    prediction_interval: bool = Query(default=False, description=desc.prediction_interval_desc),
    pi_prob_list: list[float] | None = Query(default=[0.68], gt=0, lt=1, description=desc.pi_prob_list_desc),
    dependencies=Depends(is_authorized)
) -> Response:
    """
    Route fc_batch for batch forecast.

    :param **payload_type**: type of request data ["parquet", "json", "csv"]\n
    :param **Decompose**: which drivers to return along with the forecast\n
    :param **add_columns**: input columns to pass in the result\n
    :param **fc_only**: If True only fc horizon will be returned\n
    :param **decimals**: Number of decimal places\n
    :param **parallel**: type of forecasting True if parallel else False\n
    :param **api_key**: key for authorization, set into header\n
    :param **Feature_Types**: types for feature\n
    :param **prediction_interval**: If True calcultes prediction interval(s)\n
    :param **pi_prob_list**: List of probabilities for prediction interval(s). Values must be from 0 to 1 exclusive\n

    """
    set_up_user_name(logger, dependencies)
    logger.info(f"Start Forecast decompose:{decompose}")

    body = await request.body()
    try:
        data = body_to_df(body, payload_type)
    except Exception as e:
        logger.exception('df building from request body failed:')
        return Response(content=str(e), status_code=400)

    try:
        feat_types: TypesOfFeatures = json.loads(feature_types)
    except json.decoder.JSONDecodeError:
        emsg = \
            """error during feature_types reading. feature_types format: {"feature": "dim"},
            keys and values must be in double quoters"""
        logger.exception(emsg)
        return Response(content=emsg, status_code=400)

    missed_columns = quality.get_missed_columns(data, feat_types) + list(
                        filter(lambda col: col not in data.columns, add_columns))
    if missed_columns:
        logger.info(f"Columns in data: {data.columns}, feature types: {feat_types}, missed: {missed_columns}")
        return Response(status_code=400, content=f"missed columns: {missed_columns}")

    if prediction_interval and decompose[0] != 'none':
        logger.error(f"Wrong input: prediction_interval = {prediction_interval} and decompose = {decompose}")
        return Response(content="Cannot perform decomposition and prediction interval calculations simultaneously",
                        status_code=400)

    try:
        added_partition = fc_only and 'partition' not in add_columns
        if added_partition:
            add_columns.append('partition')

        outcome = hc.fc_batch(data, feat_types, decompose=decompose, parallel=parallel, add_columns=add_columns,
                              decimals=decimals, prediction_interval=prediction_interval, pi_prob_list=pi_prob_list)
        outcome = datetime_converting(outcome)

        if fc_only:
            outcome = outcome.query('partition == "fc"')
            if added_partition:
                del outcome['partition']

    except Exception:
        logger.exception('forecasting failed:')
        data.to_parquet(FAILED_DF_PATH.as_posix())
        raise

    binary_outcome = build_outcome_content(outcome, payload_type)

    logger.info("Forecast Batch is done")

    return Response(content=binary_outcome,
                    media_type=scheme.payload_type[payload_type])


@router.post(
    '/fc_batch/task',
    status_code=201,
    description=fc_new_docs,
    openapi_extra=reqs.fc_openapi_extra,
    responses=resp.task_response
)
async def create_fc_task(
    request: Request,
    payload_type: str = Query(default="parquet", examples=examp.payload_type_examples),
    decompose: list[str] = Query(default=["all"], examples=examp.decompose_type_examples),
    decimals: int = Query(default=2, ge=0, description="Number of decimal places"),
    parallel: bool = True,
    feature_types: str = Header(title="Feature-Types", default="{}",
                                description=desc.headers_desc["feature_types"]),
    prediction_interval: bool = Query(default=False, description=desc.prediction_interval_desc),
    pi_prob_list: list[float] | None = Query(default=[0.68], gt=0, lt=1, description=desc.pi_prob_list_desc),
    dependencies=Depends(is_authorized)
) -> JSONResponse | Response:
    """
    Route to create fc task.

    :param request: \n
    :param payload_type:\n
    :param decompose:\n
    :param decimals:\n
    :param parallel:\n
    :param api_key:\n
    :param feature_types:\n
    :param prediction_interval: If True calcultes prediction interval(s)\n
    :param pi_prob_list: List of probabilities for prediction interval(s). Values must be from 0 to 1 exclusive\n

    :return: json {'task_id': here-should-be-task-id}\n
    """
    set_up_user_name(logger, dependencies)
    logger.info(f"Start create fc task. decompose: {decompose}")

    if prediction_interval and decompose[0] != 'none':
        logger.error(f"Wrong input: prediction_interval = {prediction_interval} and decompose = {decompose}")
        return Response(content="Cannot perform decomposition and prediction interval calculations simultaneously",
                        status_code=400)

    body = await request.body()
    try:
        data = body_to_df(body, payload_type)
    except Exception as e:
        logger.exception('df building from request body failed:')
        return Response(content=str(e), status_code=400)

    try:
        feat_types: TypesOfFeatures = json.loads(feature_types)
    except json.decoder.JSONDecodeError:
        emsg = \
            """error during feature_types reading. feature_types format: {"feature": "dim"},
            keys and values must be in double quoters"""
        logger.exception(emsg)
        return Response(content=emsg, status_code=400)

    missed_columns = quality.get_missed_columns(data, feat_types)
    if missed_columns:
        logger.info(f"Columns in data: {data.columns}, feature types: {feat_types}, missed: {missed_columns}")
        return Response(status_code=400, content=f"missed columns: {missed_columns}")

    request_id = get_request_id(logger)

    task = worker.create_fc_task.delay(jsoned_data=data.to_json(), feature_types=feat_types, decompose=decompose,
                                       parallel=parallel, request_id=request_id, decimals=decimals,
                                       prediction_interval=prediction_interval, pi_prob_list=pi_prob_list)
    logger.info(f"Task fc was created. id: {task.id}")

    return JSONResponse({'task_id': task.id}, status_code=201)


@router.get('/fc_batch/result')
async def get_fc_task_result(
    task_id: str,
    dependencies=Depends(is_authorized)
) -> dict:
    """
    Return a result of fc task by task_id

    :param api_key: auth key\n
    :param task_id: id of fc task\n
    :return: dataframe in json\n
    """
    set_up_user_name(logger, dependencies)
    logger.info("start extraction of fc task result")

    result = worker.celery_worker.AsyncResult(task_id)

    response = {
        'status': result.status,
        'result': None
    }
    if result.status == 'SUCCESS':
        response['result'] = result.get()

    logger.info(f"end extraction of fc task result. Task status: {result.status}")
    return response


fc_new_docs = fc.__doc__
