from enum import Enum
from typing import final


@final
class BLDecompositionStrategy(str, Enum):
    desc: str

    def __new__(cls, fig: str, calc_type: str):

        if not isinstance(fig, str) or not isinstance(calc_type, str):
            raise TypeError(f"type of params {fig=} or {calc_type=} is not str!")

        obj = str.__new__(cls, fig)
        obj._value_ = fig
        obj.desc = calc_type
        return obj

    zero_promo = ("zero_promo",
                  "method for calculating baseline, zeros promo features during promo periods")
    remove_promo = ("remove_promo",
                    "method for calculating baseline, removes promo periods completely. in case not " +
                    "enough data to calculate baseline, regresses to 'zero_promo' method")

    @staticmethod
    def doc() -> str:
        return ''.join([f"<li>{ctype} - {ctype.desc}</li>" for ctype in BLDecompositionStrategy])


DecompositionStrategies = dict[str, BLDecompositionStrategy]
