import numpy as np
import pandas as pd
import math
from typing import Iterable
from hecate.features import FeatureType, TypesOfFeatures
from hecate.forecasting.horizon import Horizon
from hecate.utils.utils import round_cols
from hecate.constants import COLS_TO_ROUND


class DummyFCAnnihilator:
    def __init__(self, horizon: Horizon = Horizon.UNDEFINED):
        self.horizon = horizon

    def postprocess(self, data: pd.DataFrame, output_cols: list[str],
                    feature_types: TypesOfFeatures | None = None) -> pd.DataFrame:
        return data


class DefaultFCAnnihilator(DummyFCAnnihilator):

    def postprocess(self, data: pd.DataFrame, output_cols: list[str],
                    feature_types: TypesOfFeatures | None = None) -> pd.DataFrame:
        fc_cols = [col for col in output_cols if col not in ('date', 'key')]
        if feature_types is None:
            feature_types = {}
        stop_columns = [col for col, ftype in feature_types.items() if ftype == FeatureType.stop]
        if stop_columns:
            for stop_col in stop_columns:
                stop_fc(data, fc_cols, stop_col)

        return data


class STFCAnnihilator(DefaultFCAnnihilator):
    ...


class MTFCAnnihilator(DefaultFCAnnihilator):
    ...


class LTFCAnnihilator(DefaultFCAnnihilator):
    ...


class FCAnnihilator(DefaultFCAnnihilator):

    def postprocess(self, data: pd.DataFrame, output_cols: list[str],
                    feature_types: TypesOfFeatures | None = None) -> pd.DataFrame:
        annihilator_from_horizon = {
            Horizon.ST: STFCAnnihilator,
            Horizon.MT: MTFCAnnihilator,
            Horizon.LT: LTFCAnnihilator,
        }
        annihilator = annihilator_from_horizon.get(self.horizon, DefaultFCAnnihilator)
        data = annihilator(horizon=self.horizon).postprocess(data, output_cols, feature_types)

        return data


def stop_fc(data: pd.DataFrame, fc_cols: list[str], stop_column: str = 'stop'):
    """
    Function zeros the every forecast columns.
    :param data: dataframe that must contain fc_cols and stop_column
    :param stop_column: column to define where to zero forecast. Must contain 1 where we need to stop forecast
    :param fc_cols: list of columns with forecasted data
    :return: None
    """

    for col in fc_cols:
        data[col] = np.where(data[stop_column], 0, data[col])


class DummyFCRounder:
    def __init__(self) -> None:
        pass

    def postprocess(self, data: pd.DataFrame, *args, **kwargs) -> pd.DataFrame:

        return data


class SimpleRounder(DummyFCRounder):
    def __init__(self) -> None:
        super().__init__()

    def postprocess(self, data: pd.DataFrame, decimals: int | None,
                    cols_to_round: Iterable | None = None) -> pd.DataFrame:
        """
        Simple rounding using pandas df round function for each col in cols_to_round
        :param cols_to_round: interable object with columns to round
        :param decimals: number of decimal places to round to

        :return: pandas dataframe with rounded cols
        """

        cols_to_round = COLS_TO_ROUND if cols_to_round is None else cols_to_round

        round_cols(df=data, cols_to_round=cols_to_round, decimals=decimals)

        return data


class RollingRounder(DummyFCRounder):
    def __init__(self) -> None:
        super().__init__()

    def postprocess(self, data: pd.DataFrame, decimals: int | None,
                    fc_column: str = 'fc', threshold: int = 50, *args, **kwargs) -> pd.DataFrame:
        """
        Create pipeline of postprocessing using rolling rounding
        :param data: pd.DataFrame
        :param decimals: number of decimal places to round the output values to
        :param fc_column: column with fc
        :param threshold: criterion for changing fc

        :return: data in pandas dataframe
        """

        self._roll_decimals_forward(data=data, decimals=decimals, fc_column=fc_column, threshold=threshold)

        return data

    def _roll_decimals_forward(self, data: pd.DataFrame, decimals: int | None,
                               fc_column: str = 'fc', threshold: int = 50) -> None:
        """
        Function replaces fc to rolling rounded fc
        :param data: pd.DataFrame
        :param decimals: number of decimal places to round the output values to
        :param fc_column: column with fc
        :param threshold: criterion for changing fc
        :return: data
        """
        if decimals is not None:
            max_fc = data.loc[data['partition'] == 'fc', fc_column].max()

            if max_fc < threshold:
                roll_fc = self._rolling_round_agg(data[fc_column], decimals=decimals)
                data[fc_column] = np.where(data['partition'] == 'fc', roll_fc, data[fc_column])

    def _rolling_round_agg(self, numbers: pd.Series, decimals: int) -> np.ndarray:
        """
        Support function to round list of numbers
        :param numbers: list of numbers
        :param decimals: number of decimal places to round the output values to
        :return: list of rounded integers
        """
        rounded = np.zeros(numbers.size)
        fractional_sum = 0
        for i, number in enumerate(numbers):
            if decimals == 0:
                floor = math.floor(number)
            else:
                floor = math.floor(number * 10 * decimals) / (10 * decimals)
            difference = number - floor
            fractional_sum += difference
            rounded[i] = floor + round(fractional_sum, decimals)
            fractional_sum -= round(fractional_sum, decimals)
        return rounded
