from collections import defaultdict
from typing import final

from sklearn.base import BaseEstimator, TransformerMixin
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler


class ModelAnswer:
    """
    Class for storing various results of model fitting.
    """

    def __init__(self, prediction, model):
        """
        Initialize ModelAnswer class.
        """
        self.prediction = prediction
        self.model = model


def get_sample_importance(train: pd.DataFrame, relaxation_coef: float = 1.2) -> np.ndarray | None:
    """
    Adds a column with a backward decreasing exponent that represents
    sample importance

    :param train: input dataset containing partition and date columns
    :param relaxation_coef: relaxation coefficient that affects speed of
    exponent decreasing, the bigger the coefficient, the smaller the
    influence of old samples on the forecast
    :return: sample importance array
    """
    train_size = train.shape[0]
    if train_size < 90:
        return None

    train_num = np.arange(train_size)
    sample_weight = np.exp((train_num / (365 * relaxation_coef)))
    return sample_weight / max(sample_weight)


def get_default_n_estimators(freq: str) -> int:
    """
    The function returns an integer with the number of estimators to be used
    in XGB. Depends on the time dimension granularity.

    :param freq: frequency of timeline

    :return: number of estimators for XGB. 20, if the series is daily, 10 - if weekly. Default value - 20.
    """
    freq_n_estimators = defaultdict(lambda: 10)
    freq_n_estimators['D'] = 20

    return freq_n_estimators[freq]


def get_base_score(data: pd.DataFrame) -> float:
    """
    The function returns the parameter base_score to be used in XGB.

    :param data: pd.DataFrame

    :return: base_score for XGB, which is computed as the mean value of the scaled target on the training set.
    """
    outlier_cols = data.columns.intersection(['target-outlier', 'target-zero_period']).to_list()
    train_target = data.loc[(data.partition == 'train') & (data[outlier_cols].sum(axis=1) == 0), 'target'].to_numpy()
    scaler = MinMaxScaler()
    target_array = train_target.reshape(-1, 1)
    target_scaled = scaler.fit_transform(target_array)
    base_score = np.mean(target_scaled)

    return base_score


@final
class TargetEncoder(BaseEstimator, TransformerMixin):
    """
    Class performs ordinal target encoding for defined columns.
    """

    def __init__(self):
        self.transform_rules = dict()

    def fit(self, X: pd.DataFrame, y: np.ndarray):
        """
        For every feature calculates reorder dictionary, stores it in class attributes.
        :param X: dataframe containing features to be transformed (train partition)
        :param y: target (train partition)
        :return: self
        """

        for col in X.columns:
            reorder_dict = pd.Series(y, name='target').groupby(X[col]).agg('mean')
            reorder_dict = {k: v + 1 for k, v in zip(reorder_dict.sort_values().index,
                                                     range(len(reorder_dict)))}
            self.transform_rules[col] = reorder_dict

        return self

    def transform(self, X: pd.DataFrame) -> pd.DataFrame:
        """
        Transforms passed dataframe according to the rules, calculated during fit.
        :param X: dataframe to transform
        :return: transformed dataframe
        """
        for col, reorder_dict in self.transform_rules.items():
            X[col] = X[col].map(reorder_dict)

        return X
