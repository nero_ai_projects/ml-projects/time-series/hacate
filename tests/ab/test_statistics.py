"""
Tests of ab/statistics.py
"""
import numpy as np

from ab.types import ConfidenceInterval
from ab.statistics import (
    effect_pvalue_norm,
    ci_norm,
    effect_pvalue_lognorm,
    ci_lognorm,
    effect_pvalue_bernoulli,
    ci_bernoulli,
    effect_pvalue_mannwhitney,
    ci_mannwhitney,
    distribution_check,
    mde_norm
)


def test_effect_pvalue_norm(ab_test_input):

    data_a, data_b, result = ab_test_input
    key = 'diff_norm'
    a = data_a.loc[(data_a.partition == 'fc') & (data_a.key == key), 'metrics']
    b = data_b.loc[(data_b.partition == 'fc') & (data_b.key == key), 'metrics']
    effect = result.loc[result.key == key, ['effect_abs']].values
    calc = effect_pvalue_norm(a, b, n=3)
    assert np.abs(effect - calc.effect) < 1e-5
    assert 0 <= calc.pvalue <= 1


def test_ci_norm(ab_test_input):

    data_a, data_b, result = ab_test_input
    key = 'diff_norm'
    a = data_a.loc[(data_a.partition == 'fc') & (data_a.key == key), 'metrics']
    b = data_b.loc[(data_b.partition == 'fc') & (data_b.key == key), 'metrics']
    [[lower, upper]] = result.loc[result.key == key, ['ci_abs_lower', 'ci_abs_upper']].values
    assert ci_norm(a, b, n=3) == ConfidenceInterval(lower, upper)


def test_effect_pvalue_lognorm(ab_test_input):

    data_a, data_b, result = ab_test_input
    key = 'diff_lognorm'
    a = data_a.loc[(data_a.partition == 'fc') & (data_a.key == key), 'metrics']
    b = data_b.loc[(data_b.partition == 'fc') & (data_b.key == key), 'metrics']
    effect = result.loc[result.key == key, ['effect_abs']].values
    calc = effect_pvalue_lognorm(a, b, n=3)
    assert np.abs(effect - calc.effect) < 1e-5
    assert 0 <= calc.pvalue <= 1


def test_ci_lognorm(ab_test_input):

    data_a, data_b, result = ab_test_input
    key = 'diff_lognorm'
    a = data_a.loc[(data_a.partition == 'fc') & (data_a.key == key), 'metrics']
    b = data_b.loc[(data_b.partition == 'fc') & (data_b.key == key), 'metrics']
    [[lower, upper]] = result.loc[result.key == key, ['ci_abs_lower', 'ci_abs_upper']].values
    ci = ci_lognorm(a, b, n=3)
    assert 0.9 * lower < ci.lower_bound < 1.1 * lower
    assert 0.9 * upper < ci.upper_bound < 1.1 * upper


def test_effect_pvalue_bernoulli(ab_test_input):

    data_a, data_b, result = ab_test_input
    key = 'diff_bernoulli'
    a = data_a.loc[(data_a.partition == 'fc') & (data_a.key == key), 'metrics']
    b = data_b.loc[(data_b.partition == 'fc') & (data_b.key == key), 'metrics']
    effect = result.loc[result.key == key, ['effect_abs']].values
    calc = effect_pvalue_bernoulli(a, b, n=3)
    assert np.abs(effect - calc.effect) < 1e-5
    assert 0 <= calc.pvalue <= 1


def test_ci_bernoulli(ab_test_input):

    data_a, data_b, result = ab_test_input
    key = 'diff_bernoulli'
    a = data_a.loc[(data_a.partition == 'fc') & (data_a.key == key), 'metrics']
    b = data_b.loc[(data_b.partition == 'fc') & (data_b.key == key), 'metrics']
    [[lower, upper]] = result.loc[result.key == key, ['ci_abs_lower', 'ci_abs_upper']].values
    assert ci_bernoulli(a, b, n=3) == ConfidenceInterval(lower, upper)


def test_effect_pvalue_mannwhitney(ab_test_input):

    data_a, data_b, _ = ab_test_input
    key = 'undefined'
    a = data_a.loc[(data_a.partition == 'fc') & (data_a.key == key), 'metrics']
    b = data_b.loc[(data_b.partition == 'fc') & (data_b.key == key), 'metrics']
    calc = effect_pvalue_mannwhitney(a, b, n=3)
    assert np.abs(19.1 - calc.effect) < 1e-5
    assert 0 <= calc.pvalue <= 1


def test_ci_mannwhitney(ab_test_input):

    data_a, data_b, _ = ab_test_input
    key = 'undefined'
    a = data_a.loc[(data_a.partition == 'fc') & (data_a.key == key), 'metrics']
    b = data_b.loc[(data_b.partition == 'fc') & (data_b.key == key), 'metrics']
    assert ci_mannwhitney(a, b, n=3) == ConfidenceInterval(10.2, 26.3)


def test_distribution_check(ab_test_input):

    data_a, data_b, _ = ab_test_input

    distributions = {'diff_norm': 'norm',
                     'diff_lognorm': 'lognorm',
                     'diff_bernoulli': 'bernoulli',
                     'undefined': 'undefined',
                     'uniform': 'undefined'
                     }
    dists_determined = {}
    for key in list(distributions.keys()):
        a = data_a.loc[data_a.key == key, 'metrics'].values
        b = data_b.loc[data_b.key == key, 'metrics'].values
        dist = distribution_check(a, b)
        if dist:
            dists_determined[str(key)] = dist.value
        else:
            dists_determined[str(key)] = dist
    assert distributions == dists_determined


def test_mde_norm(ab_test_input):

    data_a, data_b, result = ab_test_input

    mde_res = {}
    for key, mde in result[['key', 'mde']].values.tolist():
        mde_res[str(key)] = mde

    a = data_a.loc[data_a.partition == 'fc', ['key', 'metrics']]
    b = data_b.loc[data_b.partition == 'fc', ['key', 'metrics']]
    mde_calc = {}
    for key in result.key.values:
        mde_calc[str(key)] = mde_norm(a.loc[a.key == key, 'metrics'],
                                      b.loc[b.key == key, 'metrics'],
                                      n=3,
                                      beta=0.2
                                      )

    assert mde_res == mde_calc
