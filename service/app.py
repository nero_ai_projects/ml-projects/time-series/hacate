from fastapi import FastAPI, Request, Response
from fastapi.middleware.gzip import GZipMiddleware
from dotenv import load_dotenv

from hecate.log import get_logger, set_up_request_id
from service.swagger.swagger import SwaggerTagsMetadata
from service.utils import generate_request_id
from hecate import __version__ as hecate_version
from service.api.api import api_router

tags = SwaggerTagsMetadata()
load_dotenv()
fc_new_docs = ""

app = FastAPI(
    title=f"Hecate Forecast Engine version {hecate_version}",
    version="0.0.2",
    description="",
    openapi_tags=tags.tags_metadata,
)
app.add_middleware(GZipMiddleware, minimum_size=1024)
app.include_router(api_router)

logger = get_logger("hecate", service_kind="app")


@app.middleware("http")
async def request_middleware(request: Request, call_next) -> Response:
    set_up_request_id(logger, request_id=generate_request_id())
    logger.info("Request started")

    try:
        response = await call_next(request)
    except Exception as ex:
        logger.exception("Request failed:")
        return Response(content=f"Request failed: {ex}", status_code=500)
    finally:
        logger.info("Request ended")
    return response
