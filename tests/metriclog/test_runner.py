from dataclasses import replace
import pytest

from metriclog import runner, repo
from metriclog.types import HecateVersion, HecateVersionsParams
from hecate.config import LOGS_APP_PATH
from ab.types import ABParams
# import os
# from tempfile import TemporaryDirectory


def test_set_train_test(input_from_db, horizon_and_folds, partition_column):
    output_data = runner.set_train_test(data=input_from_db, horizon_and_folds=horizon_and_folds, fold=1,
                                        horizon='mt', project='apricot')
    # condition for partition column
    assert partition_column in output_data.columns

    # condition for partition types
    assert {'train', 'fc', 'non relevant'} == set(output_data[partition_column])

    # condition for equals input and output dataframes
    assert input_from_db.shape == output_data.shape


def test_set_output_table(hecate_input, hecate_output, final_table_columns, partition_column):
    ...
    # output_data = runner.set_output_table(hecate_output, hecate_input, commit_id='test_id', branch='test_branch',
    #                                       project='apricot', focus=False, fold=1, horizon='mt')
    # # condition for columns presence
    # assert final_table_columns == set(output_data.columns)
    #
    # # condition for partition column
    # assert hecate_input[hecate_input[partition_column] == 'fc'].shape[0] == output_data.shape[0]


def test_save_file_locally(hecate_output):
    ...
    # with TemporaryDirectory() as output_dir:
    #     output_dir_path = os.path.join(output_dir)
    #     print(output_dir_path)
    #     output_file_path = runner.save_file_locally(hecate_output, commit_id='test_id', branch='test_branch',
    #                                                 project='apricot', fold=1, horizon='mt', file_type='result',
    #                                                 output_dir=output_dir_path)
    #     # condition file is created in output project dir
    #     assert os.path.exists(output_file_path)


def test_filter_project_horizon_and_fold(horizon_and_folds):
    # condition fold exist
    hs_n_fs = runner.filter_project_horizon_and_fold(horizon_and_folds, projects='wings', horizons='st', n_folds=2)
    assert 2 == len(hs_n_fs)
    # condition fold not exist
    hs_n_fs = runner.filter_project_horizon_and_fold(horizon_and_folds, projects='wings', horizons='st', n_folds=10)
    assert 3 == len(hs_n_fs)
    # condition all projects
    hs_n_fs = runner.filter_project_horizon_and_fold(horizon_and_folds, projects='all', horizons='st', n_folds=1)
    assert {'wings', 'avocado', 'apricot'} == set(hs_n_fs.project.unique())
    # condition project not exists
    hs_n_fs = runner.filter_project_horizon_and_fold(horizon_and_folds, projects='???', horizons='st', n_folds=1)
    assert 0 == len(hs_n_fs)
    # condition all horizons
    hs_n_fs = runner.filter_project_horizon_and_fold(horizon_and_folds, projects='wings', horizons='all', n_folds=1)
    assert {'mt', 'st'} == set(hs_n_fs.horizon.unique())
    # condition horizon not exists
    hs_n_fs = runner.filter_project_horizon_and_fold(horizon_and_folds, projects='all', horizons='???', n_folds=1)
    assert 0 == len(hs_n_fs)
    # correct treatment of list inputs
    hs_n_fs = runner.filter_project_horizon_and_fold(horizon_and_folds, projects=['wings', 'avocado'],
                                                     horizons=['st', 'mt'], n_folds=1)
    assert 4 == len(hs_n_fs)


def test_get_forecast_from_db(db_connection):

    values = ['1', "2019-01-01", 1, 1, False, 0, "st", "id_3", "branch", "wings"]
    hvp_st = HecateVersionsParams(project='wings', commit_id='id_3', branch='branch',
                                  focus=False, fold=0, horizon='st')
    hvp_lt = replace(hvp_st, horizon='lt')
    hvp_all = replace(hvp_st, horizon='all')
    assert db_connection.select_results_if_exist(hecate_versions_params=hvp_st,
                                                 table='results_' + hvp_st.project).iloc[0].values.tolist() == values
    assert db_connection.select_results_if_exist(hecate_versions_params=hvp_lt,
                                                 table='results_' + hvp_lt.project).empty
    # test of the case, when the horizon name is not in ('st', 'mt', 'lt')
    with pytest.raises(ValueError):
        assert db_connection.select_results_if_exist(hecate_versions_params=hvp_all,
                                                     table='results_' + hvp_all.project.lower())


def test_run_ab_test(db_connection, ab_results_test, ab_datasets, ab_summary_test):

    def dummy_check(*args, **kwards):
        return True

    def dummy_none(*args, **kwards):
        pass

    def dummy_branch_commit(*args, **kwards):
        return 'commit_a_test', 'branch_a_test'

    with pytest.MonkeyPatch.context() as mp:
        mp.setattr(repo, name='check_branch_exist', value=dummy_check)
        mp.setattr(repo, name='check_commit_exist', value=dummy_check)
        mp.setattr(repo, name='check_unstaged_changes', value=dummy_none)
        mp.setattr(repo, name='get_branch_and_last_commit_id', value=dummy_branch_commit)

        ab_results = ab_results_test
        data_a, data_b = ab_datasets
        ab_summary = ab_summary_test

        hvp = HecateVersionsParams(
            branch_a='branch_a_test',
            commit_a_id='commit_a_test',
            branch='branch_a_test',
            commit_id='commit_a_test',
            branch_b='branch_b_test',
            commit_b_id='commit_b_test',
            project=ab_results.project[0],
            fold=ab_results.fold[0],
            focus=ab_results.focus[0],
            horizon=ab_results.horizon[0]
        )
        ab_params = ABParams()
        db_connection.send_ab_results_to_db(ab_results, hvp)
        db_connection.send_ab_results_to_db(ab_summary, replace(hvp, project='ab_summary'))

        data_a['branch'] = f"{hvp.branch_a}"
        data_a['commit_id'] = f"{hvp.commit_a_id}"
        data_a['project'] = f"{hvp.project}"
        data_a['fold'] = hvp.fold
        data_a['focus'] = hvp.focus
        data_a['horizon'] = f"{hvp.horizon}"
        data_b['branch'] = f"{hvp.branch_b}"
        data_b['commit_id'] = f"{hvp.commit_b_id}"
        data_b['project'] = f"{hvp.project}"
        data_b['fold'] = hvp.fold
        data_b['focus'] = hvp.focus
        data_b['horizon'] = f"{hvp.horizon}"
        table = 'results_' + hvp.project
        db_connection.send_chunk(df=data_a, table=table)
        db_connection.send_chunk(df=data_b, table=table)
        db_connection.send_chunk(df=data_a, table='test')

        # Case 1: ab_results exist, rewrite_ab_results==False, ab_summary exist, rewrite_ab_summary==False
        runner.run_ab_test(hvp, ab_params, db_connection, rewrite_ab_results=False, rewrite_ab_summary=False)
        true_line = f"The desired AB test's summary for project '{hvp.project}' already exists in the database."
        lines = []
        for line in open(file=LOGS_APP_PATH, mode='r'):
            lines.append(line)
        assert lines[-1].split(' - ')[-1][:-1] == true_line

        # Case 2: ab_results exist, rewrite_ab_results==False, rewrite_ab_summary==True
        runner.run_ab_test(hvp, ab_params, db_connection, rewrite_ab_results=False, rewrite_ab_summary=True)
        true_line = f"New AB test summary for project '{hvp.project}' have been added to the database."
        lines = []
        for line in open(file=LOGS_APP_PATH, mode='r'):
            lines.append(line)
        assert lines[-1].split(' - ')[-1][:-1] == true_line

        # Case 3: rewrite_ab_results==True
        runner.run_ab_test(hvp, ab_params, db_connection, rewrite_ab_results=True, rewrite_ab_summary=True,)
        true_line = f"New AB test results for project '{hvp.project}' have been added to the database."
        lines = []
        for line in open(file=LOGS_APP_PATH, mode='r'):
            lines.append(line)
        assert lines[-3].split(' - ')[-1][:-1] == true_line


def test_multiple_ab_test_runs(db_connection):

    hs_n_fs = db_connection.extract_horizon_and_folds()

    def dummy_check(*args, **kwards):
        return True

    def dummy_none(*args, **kwards):
        pass

    with pytest.MonkeyPatch.context() as mp:
        mp.setattr(repo, name='check_branch_exist', value=dummy_check)
        mp.setattr(repo, name='check_commit_exist', value=dummy_check)
        mp.setattr(runner, name='run_ab_test', value=dummy_none)

        hv_a = HecateVersion('br_a', 'comm_a')
        hv_b = HecateVersion('br_b', 'comm_b')

        # Case 1. Default run: testing all existing in the database projects with 'focus' == False
        runner.multiple_ab_test_runs(hv_a, hv_b, db_connection, ABParams())
        true_line = f"Complete {len(hs_n_fs)} AB tests."
        lines = []
        for line in open(file=LOGS_APP_PATH, mode='r'):
            lines.append(line)
        assert lines[-1].split(' - ')[-1][:-1] == true_line

        # Case 2. Testing param 'focuses'
        runner.multiple_ab_test_runs(hv_a, hv_b, db_connection, ABParams(), horizons='lt', focuses=[True, False])
        true_line = f"Complete {2} AB tests."
        lines = []
        for line in open(file=LOGS_APP_PATH, mode='r'):
            lines.append(line)
        assert lines[-1].split(' - ')[-1][:-1] == true_line
