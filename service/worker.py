import os

import pandas as pd
from celery import Celery
from dotenv import load_dotenv

import hecate.hecate as hc
from hecate.features import TypesOfFeatures
from hecate.types import Decompositors
from hecate.utils.utils import datetime_converting
from hecate.log import get_logger, set_up_request_id
from hecate.config import FAILED_DF_PATH
from service.utils import generate_request_id

load_dotenv()

celery_worker = Celery(__name__)
celery_worker.conf.broker_url = os.environ.get("CELERY_BROKER_URL", 'redis://localhost:6379')
celery_worker.conf.result_backend = os.environ.get("CELERY_RESULT_BACKEND", 'redis://localhost:6379')
celery_worker.conf.accept_content = ['application/json', ]

logger = get_logger('hecate', service_kind="worker")


@celery_worker.task(name='fc')
def create_fc_task(jsoned_data: str,
                   feature_types: TypesOfFeatures,
                   decompose: Decompositors = 'all',
                   parallel: bool = False,
                   decimals: int = 2,
                   prediction_interval: bool = False,
                   pi_prob_list: list[float] | None = None,
                   request_id: str = 'unknown',
                   ) -> str:

    if request_id == 'unknown':
        set_up_request_id(logger, request_id=generate_request_id())
        logger.warning('request id is unknown. New request id used.')
    else:
        set_up_request_id(logger, request_id=request_id)

    data: pd.DataFrame = pd.read_json(jsoned_data)

    try:
        outcome = hc.fc_batch(data, feature_types, decompose=decompose, parallel=parallel, decimals=decimals,
                              prediction_interval=prediction_interval, pi_prob_list=pi_prob_list)
        outcome = datetime_converting(outcome)
    except Exception:
        logger.exception('fc task failed')
        data.to_parquet(FAILED_DF_PATH.as_posix())
        raise

    logger.info("fc task is done")
    return outcome.to_json()
