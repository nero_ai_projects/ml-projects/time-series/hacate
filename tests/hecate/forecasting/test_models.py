import pytest
import numpy as np
import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import OneHotEncoder
from datetime import datetime as dt

from hecate.forecasting.horizon import get_horizon_from_sample
from hecate.forecasting.models.utils import TargetEncoder
from hecate.forecasting.models.models import DefaultForecastingPipeline
from hecate.forecasting.models import model_selection
from hecate.forecasting import trend
from hecate.features import FeatureType, FeatureSet, get_cols_from_types


def test_forecasting_pipeline(hecate_input_df):
    data, feature_types, result = hecate_input_df
    feature_cols = [key for key, item in feature_types.items() if item not in [FeatureType.dim, FeatureType.can_level]]

    promo_feature_types = [FeatureType.discount, FeatureType.promo, FeatureType.promo_budget]
    promo_features = get_cols_from_types(feature_types, promo_feature_types)

    label_features = get_cols_from_types(feature_types, [FeatureType.label])

    feature_set = FeatureSet()
    feature_set.selected_features = feature_cols
    feature_set.promo_features = promo_features
    feature_set.label_features = label_features

    trend.add_trend(data, promo_features)

    if len(feature_cols) > 0:
        data['forecast'] = model_selection.ForecastingMachine().forecast(
            data=data, horizon=get_horizon_from_sample(data), feature_set=feature_set).prediction
        assert False in list(data['forecast'].isna())


def test_default_pipeline(model_items):
    model_name, params, constructor, x_cols, cols_for_ohe, cols_for_min_max = model_items
    # Create model object
    model_wrap = DefaultForecastingPipeline(constructor=constructor,
                                            params_model=params,
                                            feature_cols=x_cols,
                                            ohe_cols=cols_for_ohe,
                                            min_max_cols=cols_for_min_max)
    attributes = {'constructor', 'model'}

    assert set(model_wrap.__dict__.keys()) == attributes


def test_pipeline_attributes():
    features = ['discount', 'price']
    model = DefaultForecastingPipeline(feature_cols=features, freq='D')

    assert hasattr(model, 'fit') and hasattr(model, 'predict') and hasattr(model, 'get_feature_importance')


def test_target_encoding():
    dates = pd.date_range(dt(2021, 1, 1), dt(2022, 2, 1), freq='M')

    # get initial dataframe
    df_init = pd.DataFrame(dates, columns=['date'])
    df_init['key'] = 'a'
    df_init['partition'] = ['train'] * 12 + ['fc']
    df_init['target'] = [i for i in np.arange(12, 0, -1)] + [12]
    df_init['time-month'] = [i for i in np.arange(1, 13)] + [1]

    # get expected output dataframe
    df_out = df_init.copy()
    df_out['time-month'] = df_init.target

    # check if target encoding does reordering as expected
    train_partition = df_init[df_init.partition == 'train']
    te_cols = ['time-month']

    answer_X = np.array(df_out['time-month']).reshape(-1, 1)

    preprocessor = ColumnTransformer(sparse_threshold=0,
                                     transformers=[('target_encoding', TargetEncoder(), te_cols)])

    preprocessor.fit(train_partition[te_cols], train_partition['target'])

    out = preprocessor.transform(df_init[te_cols])
    assert np.array_equal(out, answer_X)


@pytest.mark.parametrize('hecate_input_df', ['test_6'], indirect=True)
def test_ohe(hecate_input_df):
    # get initial dataframe
    df, _, _ = hecate_input_df

    # check if ohe works as expected
    train_partition = df[df.partition == 'train']
    ohe_cols = ['discount_type']

    preprocessor = ColumnTransformer(sparse_threshold=0,
                                     transformers=[('cat',
                                                    OneHotEncoder(handle_unknown='ignore'),
                                                    ohe_cols)])

    preprocessor.fit(train_partition[ohe_cols], train_partition['target'])

    out = preprocessor.transform(df[ohe_cols])
    out_cols = preprocessor.get_feature_names_out()
    out = pd.DataFrame(out, columns=out_cols)

    # get dataframe with expected answer
    expected_out = pd.DataFrame({
        'cat__discount_type_2in1': [1.] * 4 + [.0] * 8 + [1],
        'cat__discount_type_price_discount': [.0] * 7 + [1.] * 3 + [.0] * 3
    })
    expected_out['cat__discount_type_nan'] = np.where(expected_out.sum(axis=1) != 0, .0, 1.)

    assert expected_out[out_cols].equals(out)
