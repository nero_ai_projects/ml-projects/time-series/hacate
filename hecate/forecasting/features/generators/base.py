import pandas as pd

from hecate.features import TypesOfFeatures
from hecate.forecasting.horizon import Horizon


class DummyFGenerator:

    def __init__(self):
        super().__init__()
        self.horizon = Horizon.UNDEFINED

    def add_features(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None,
                     time_features: list[str] | None = None) \
            -> tuple[pd.DataFrame, list]:
        return data, []
