import traceback
from typing import Union, List
import uuid
import json
from io import BytesIO, StringIO
import hashlib
import pandas as pd
import logging
from fastapi import FastAPI, Request, Response, Header, Query
from dotenv import load_dotenv

import hecate.hecate as hc
import hecate.cannibalization.cannibalization as cn
import setup
import swagger
from hecate import version as hecate_version
from hecate.validations import quality

load_dotenv()
fc_new_docs = ""

app = FastAPI(
    title=f"Hecate Forecast Engine version {hecate_version}",
    version="0.0.2",
    description="",
    openapi_tags=swagger.tags_metadata,
)
app.api_keys_dict = setup.set_up_api_keys()
setup.set_up_logger()
logger = logging.getLogger('hecate')


def is_authorized(passed_hash: str) -> tuple:
    """
    Check authorization by api_key
    :param passed_hash: hash that will be used to authorization
    :return: (True, user) if authorized else (False, None)
    """
    if not passed_hash:
        return False, None

    for user in app.api_keys_dict:
        expected_hash = hashlib.sha512(
            app.api_keys_dict[user].encode()
        ).hexdigest()

        if passed_hash == expected_hash:
            return True, user

    return False, None


def body_to_df(data: bytes,
               payload_type: Union[str, None]) -> pd.DataFrame:
    """
    Convert request body to pandas DataFrame
    :param payload_type: define what is payload. Support "json", "csv". If None works with data as a parquet bytes.
    :param data: request body data for get DataFrame
    :return: pandas DataFrame for forecast
    """
    if payload_type is not None and payload_type == "json":
        data = pd.DataFrame(json.loads(data))

    elif payload_type is not None and payload_type == "csv":
        data = pd.read_csv(StringIO(data.decode(encoding='utf8')), sep=';')

    else:
        data = pd.read_parquet(BytesIO(data))

    return data


def get_binary_outcome(outcome) -> bytes:
    """
    Convert pandas DataFrame to bytes
    :param outcome: output pandas dataframe for binary converting
    :return: binary pandas dataframe
    """
    byte_io = BytesIO()
    outcome.to_parquet(byte_io, index=False, compression='gzip')
    binary_outcome = byte_io.getvalue()

    return binary_outcome


@app.middleware("http")
async def request_middleware(request, call_next):
    logger.filters.clear()
    logger.addFilter(setup.LoggerRequestIDFilter(
            request_id=str(uuid.uuid4()).replace("-", "")))
    logger.info("Request started")

    try:
        response = await call_next(request)
    except Exception as ex:
        logger.error(f"Request failed: {ex}")
        return Response(content=f"Request failed: {ex}", status_code=500)
    finally:
        logger.info("Request ended")
    return response


@app.post("/fc",
          tags=["Forecast"],
          description=fc_new_docs,
          openapi_extra=swagger.fc_openapi_extra,
          responses=swagger.fc_response,)
async def fc(request: Request,
             payload_type: Union[str, None] = Query(default="parquet",
                                                    examples=swagger.payload_type_examples),
             decompose: Union[List[str], None] = Query(default=["all"],
                                                       examples=swagger.decompose_type_examples),
             api_key: Union[str, None] = Header(title="api_key", convert_underscores=False),
             feature_types: Union[str, None] = Header(title="Feature-Types",
                                                      default="{}",
                                                      description=swagger.headers_desc["feature_types"])):
    """
    Route fc for forecast.

    :param **payload_type**: type of request data ["parquet", "json", "csv"]\n
    :param **api_key**: key for authorization, set into header\n
    :param **Feature_Types**: types for feature\n
    :param **Decompose**: which drivers to return along with the forecast\n
    """
    is_auth, current_user = is_authorized(api_key)
    if not is_auth:
        return Response(content="", status_code=418)

    logger.info(f"({current_user}): Start Forecast")

    data = await request.body()
    try:
        data = body_to_df(data, payload_type)
    except Exception as e:
        trace = traceback.format_exc()
        logger.error(f'df building from request body failed: \n{trace}')
        return Response(content=str(e), status_code=400)

    feature_types = json.loads(feature_types)
    drivers = decompose[0] if decompose[0] in ['all', 'none'] else decompose

    missed_columns = quality.get_missed_columns(data, feature_types)
    if missed_columns:
        logger.info(f"Columns in data: {data.columns}, feature types: {feature_types}, missed: {missed_columns}")
        return Response(status_code=400, content=f"missed columns: {missed_columns}")

    fc_result = hc.fc_sample(data, feature_types, drivers=drivers)
    if fc_result.is_failed:
        logger.error(f"({current_user}): done with internal server error")
        return Response(content=fc_result.traceback, status_code=500)

    outcome = fc_result.data
    outcome = hc.datetime_converting(outcome)

    binary_outcome = build_outcome_content(outcome, payload_type)

    logger.info(f"({current_user}): Finish Forecast")

    return Response(content=binary_outcome)


@app.post("/fc_batch",
          tags=["Forecast"],
          description=fc_new_docs,
          openapi_extra=swagger.fc_openapi_extra,
          responses=swagger.fc_response,)
async def fc_batch(request: Request,
                   payload_type: Union[str, None] = Query(default="parquet",
                                                          examples=swagger.payload_type_examples),
                   decompose: Union[List[str], None] = Query(default=["all"],
                                                             examples=swagger.decompose_type_examples),
                   parallel: bool = True,
                   api_key: Union[str, None] = Header(title="api_key", convert_underscores=False),
                   feature_types: Union[str, None] = Header(title="Feature-Types",
                                                            default="{}",
                                                            description=swagger.headers_desc["feature_types"])):
    """
    Route fc_batch for batch forecast.

    :param **payload_type**: type of request data ["parquet", "json", "csv"]\n
    :param **Decompose**: which drivers to return along with the forecast\n
    :param **parallel**: type of forecasting True if parallel else False\n
    :param **api_key**: key for authorization, set into header\n
    :param **Feature_Types**: types for feature\n
    """
    is_auth, current_user = is_authorized(api_key)
    if not is_auth:
        return Response(content="", status_code=418)

    logger.info(f"({current_user}): Start Forecast decompose:{decompose}")

    data = await request.body()
    try:
        data = body_to_df(data, payload_type)
    except Exception as e:
        trace = traceback.format_exc()
        logger.error(f'df building from request body failed: \n{trace}')
        return Response(content=str(e), status_code=400)
    feature_types = json.loads(feature_types)

    missed_columns = quality.get_missed_columns(data, feature_types)
    if missed_columns:
        logger.info(f"Columns in data: {data.columns}, feature types: {feature_types}, missed: {missed_columns}")
        return Response(status_code=400, content=f"missed columns: {missed_columns}")

    outcome = hc.fc_batch(data, feature_types, decompose=decompose, parallel=parallel)
    outcome = hc.datetime_converting(outcome)

    binary_outcome = build_outcome_content(outcome, payload_type)

    logger.info(f"({current_user}): Start Forecast Batch")

    return Response(content=binary_outcome)


@app.post("/decompose_act",
          tags=["Actuals Decomposition"],
          description=fc_new_docs,
          openapi_extra=swagger.act_decomposition_openapi_extra,
          responses=swagger.act_decomposition_response,)
async def decompose_act(request: Request,
                        payload_type: Union[str, None] = Query(default="parquet",
                                                               examples=swagger.payload_type_examples),
                        decompose: Union[List[str], None] = Query(default=["all"],
                                                                  examples=swagger.decompose_type_examples),
                        parallel: bool = True,
                        api_key: Union[str, None] = Header(title="api_key", convert_underscores=False),
                        feature_types: Union[str, None] = Header(title="Feature-Types",
                                                                 default="{}",
                                                                 description=swagger.headers_desc["feature_types"])):
    """
    Route fc_batch for actuals decomposition. NB! Uses fc_batch for forecasting, but you have to pass "decompose"
    partition instead of "fc". As an output you would receive decomposed data for that partition only.

    :param **payload_type**: type of request data ["parquet", "json", "csv"]\n
    :param **Decompose**: which drivers to return along with the forecast\n
    :param **parallel**: type of forecasting True if parallel else False\n
    :param **api_key**: key for authorization, set into header\n
    :param **Feature_Types**: types for feature\n
    """
    is_auth, current_user = is_authorized(api_key)
    if not is_auth:
        return Response(content="", status_code=418)

    logger.info(f"({current_user}): Start actuals decomposition: {decompose}")

    data = await request.body()
    try:
        data = body_to_df(data, payload_type)
    except Exception as e:
        return Response(content=str(e), status_code=400)

    feature_types = json.loads(feature_types)
    if not len(feature_types.keys()):
        return Response(content="No feature types for decomposition", status_code=400)

    missed_columns = quality.get_missed_columns(data, feature_types)
    if missed_columns:
        logger.info(f"Columns in data: {data.columns}, feature types: {feature_types}, missed: {missed_columns}")
        return Response(status_code=400, content=f"missed columns: {missed_columns}")

    outcome = hc.fc_batch(data, feature_types, decompose=decompose, parallel=parallel, act_decompose=True)

    outcome = hc.datetime_converting(outcome)
    binary_outcome = build_outcome_content(outcome, payload_type)

    logger.info(f"({current_user}): Finish actuals decomposition")

    return Response(content=binary_outcome)


def build_outcome_content(outcome: pd.DataFrame, payload_type: str) -> str | bytes:
    """
    Builds the content for response according to paylod_type

    :param outcome: pd.DataFrame that will be converted to content for transmission
    :param payload_type: json, csv, None. If None parquet will be used
    :return: str if payload_type is json else bytes
    """
    if payload_type == "json":
        content = outcome.to_json()

    elif payload_type == "csv":
        iostr = StringIO()
        outcome.to_csv(iostr, encoding='utf8', sep=';', index=False)
        content = iostr.getvalue().encode('utf8')

    else:
        content = get_binary_outcome(outcome)

    return content


fc_new_docs = fc.__doc__


@app.post("/get_can_pairs",
          tags=["Cannibalizators"],
          description=fc_new_docs,
          openapi_extra=swagger.fc_openapi_extra,
          responses=swagger.can_response,)
async def get_can_pairs(request: Request,
                        payload_type: Union[str, None] = "parquet",
                        api_key: Union[str, None] = Header(title="api_key", convert_underscores=False),
                        feature_types: Union[str, None] = Header(title="Feature-Types",
                                                                 default="{}",
                                                                 description=swagger.headers_desc["feature_types"])):
    """
    Route fc_new for forecast.

    :param **payload_type**: tuple of requests data ["parquet", "json"]\n
    :param **api_key**: key for authorization, set into header\n
    :param **Feature_Types**: types for feature\n
    """

    is_auth, current_user = is_authorized(api_key)
    if not is_auth:
        return Response(content="", status_code=418)

    logger.info("Start Retrieving cannibalization pairs")

    data = await request.body()
    try:
        data = body_to_df(data, payload_type)
    except Exception as e:
        trace = traceback.format_exc()
        logger.error(f'df building from request body failed: \n{trace}')
        return Response(content=str(e), status_code=400)

    feature_types = json.loads(feature_types)

    missed_columns = quality.get_missed_columns(data, feature_types)
    if missed_columns:
        logger.info(f"Columns in data: {data.columns}, feature types: {feature_types}, missed: {missed_columns}")
        return Response(status_code=400, content=f"missed columns: {missed_columns}")

    can_data = cn.get_cannibalization_pairs(data, feature_types, parallel=False)

    if payload_type is not None and payload_type == "json":
        binary_outcome = str(json.loads(can_data.to_json()))
    else:
        binary_outcome = get_binary_outcome(can_data)

    logger.info("Finish Retrieving cannibalization pairs")

    return Response(content=binary_outcome)
