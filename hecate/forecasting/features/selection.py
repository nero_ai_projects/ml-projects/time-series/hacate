import pandas as pd
import numpy as np
from typing import Tuple, Optional, Callable, Dict


def drop_business_features(data: pd.DataFrame, features: list | pd.Index, corr_rate=0.95) -> Tuple[pd.DataFrame, list]:
    """
    The function filters business features:
        * by exceeding the correlation threshold
        ...
    :param train: pd.DataFrame - input dataframe
    :param features: list of string - list of features
    :param corr_rate: float - correlation rate to drop features

    :return: tuple with dataset and list of drop features
    """

    features = data.columns.intersection(features)

    train = data.query("partition == 'train'")
    drop_cols = drop_high_corr_features(train, features, corr_rate)

    return data, drop_cols


def drop_high_corr_features(
        train: pd.DataFrame,
        features: list | pd.Index,
        corr_rate: float = 0.9
) -> list:

    """
    Function select and delete business features according correlation rate.
    Default correlation rate is 0.9.

    :param train: pd.DataFrame - input dataframe
    :param features: list of string - list of features
    :param corr_rate: float - correlation rate to drop features

    :return: list of string - list of drop features
    """

    features = train.select_dtypes(["number", "bool"]).columns.intersection(features)
    matrix_corr_abs = np.abs(train[features].corr().to_numpy())
    features_mask = np.full((features.size,), True, dtype=bool)

    for i in range(features.size):
        for j in range(i+1, features.size):
            if matrix_corr_abs[i, j] >= corr_rate and ~np.isnan(matrix_corr_abs[i, j]):
                features_mask[j] = False

    selected_columns = features[features_mask]
    corr_with_target = train[selected_columns].corrwith(train.target).to_dict()
    selected_columns = []

    for feature, val in corr_with_target.items():
        if np.isnan(val) or round(abs(val), 6) < corr_rate:
            selected_columns.append(feature)

    columns_to_drop = features.difference(selected_columns).to_list()

    return columns_to_drop


def drop_constant_columns(data: pd.DataFrame, features: list) -> Tuple[pd.DataFrame, list]:
    """
    Function select and delete constant features.
    All features drop which have unique one element in pd.Series.
    Data columns must contain features list.

    :param data: pd.DataFrame - input dataframe
    :param features: list of string - list of features

    :return: data - output dataframe with drop features
    :return: list - list of drop features
    """

    # select features only in column
    features = list(set(data.columns) & set(features))

    constant_cols = [col for col in features if data[data['partition'] == 'train'][col].nunique() == 1]
    # data.drop(columns=constant_cols, inplace=True)

    return data, constant_cols


def drop_non_looped_time_features(data: pd.DataFrame, features: list) -> Tuple[pd.DataFrame, list]:
    """
    The function drop month and week columns if data contain less one-year history.

    :param data: pd.DataFrame - input dataframe
    :param features: list of time features
    :return: tuple (data, list with removed features)
    """
    day_length = (data.query('partition == "train"').date.max() - data.query('partition == "train"').date.min()).days
    check_loop_for = {'time-month', 'time-week'}
    non_looped = [] if day_length >= 365 else list(check_loop_for.intersection(set(features)))

    to_drop = list(set(non_looped).intersection(set(data.columns)))
    if to_drop:
        # data.drop(columns=to_drop, inplace=True)
        ...

    return data, to_drop


def drop_features(data: pd.DataFrame,
                  business_features: list,
                  time_features: list,
                  period_features: Optional[list] = None,
                  target_features: Optional[list] = None) -> list:
    """
    Function select and delete constant features.
    All features drop which have unique one element in pd.Series.
    Data columns must contain features list.

    :param data: pd.DataFrame - input dataframe
    :param business_features: list of string - list of business features
    :param time_features: list of string - list of time features
    :param period_features: list of string - list of column names
    for outliers and zero-sales periods
    :param target_features: list of string - list of features generated
    from target

    :return: list - list of result features
    """
    period_features = [] if period_features is None else period_features
    target_features = [] if target_features is None else target_features

    feature_cols = business_features + time_features + period_features + \
        target_features
    # features selection
    drop_feature = []

    params_from_func: Dict[Callable, list] = {
        drop_constant_columns: feature_cols,
        drop_non_looped_time_features: time_features,
        drop_business_features: business_features,
    }
    for func, feature in params_from_func.items():
        data, drop_cols = func(data, feature)
        drop_feature += drop_cols

    # result features after feature generation and feature selection
    feature_cols = list(set(feature_cols) - set(drop_feature))

    return feature_cols
