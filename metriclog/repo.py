from git import Repo
import os
import sys

from hecate.log import get_logger


logger = get_logger('hecate')


def get_branch_and_last_commit_id() -> tuple[str, str]:
    """
    Function gets branch and last commit id from the current git repo
    :return: commit_id - id of last commit in the current branch
    :return: branch - current branch
    """
    repo = Repo(os.path.dirname(os.path.abspath(__file__)), search_parent_directories=True)
    commit_id = str(repo.head.commit)
    branch = repo.active_branch.name
    return commit_id, branch


def check_unstaged_changes():
    """
    Function checks if there are unstaged changes and exits the program if any
    """
    repo = Repo(os.path.dirname(os.path.abspath(__file__)), search_parent_directories=True)
    if repo.is_dirty():
        logger.warning('There are unstaged changes!')
        sys.exit(0)


def check_branch_exist(branch: str) -> bool:
    """
    Checks if the branch exists in the local repository

    :param branch: the branch name to be checked

    :return: True if the branch exists locally
    """

    repo = Repo(os.path.dirname(os.path.abspath(__file__)), search_parent_directories=True)
    branches = []
    for branch_name in repo.git.branch().split('\n'):
        branches.append(branch_name[2:])  # two droped symbols in the branch name are '  ' or '* '

    return branch in branches


def check_commit_exist(branch: str, commit_id: str) -> bool:
    """
    Checks if the commit exists in the given branch in the local repository

    :param branch: the branch name to be checked
    :param commit_id: the id of the commit to be checked

    :return: True if the commit exists locally in the given branch
    """

    repo = Repo(os.path.dirname(os.path.abspath(__file__)), search_parent_directories=True)
    commits = []
    for line in repo.heads[branch].log():
        commits.append(str(line).split(' ')[1])

    return commit_id in commits


def switch_to_hecate_version(commit_id: str):
    """
    Switches hecate to the desired version

    :param commit_id: commit id for the desired hecate version

    :return: None
    """

    check_unstaged_changes()
    repo = Repo(os.path.dirname(os.path.abspath(__file__)), search_parent_directories=True)
    repo.git.checkout(f'{commit_id}')
