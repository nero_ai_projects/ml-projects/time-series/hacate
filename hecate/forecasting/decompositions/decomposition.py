from itertools import chain
from typing import Literal

import pandas as pd
import numpy as np

from hecate.constants import ALL_DRIVERS
from hecate.log import get_logger
from hecate.types import Driver, Drivers, ColsOfDrivers, Decompositors
from hecate.features import FeatureSet, FeatureType, get_cols_from_types
from hecate.utils.utils import get_frequency, remove_duplicates, FuncAndArgs
from hecate.forecasting.models.model_selection import ForecastingMachine
from hecate.forecasting.horizon import Horizon
from hecate.forecasting.decompositions.strategies import BLDecompositionStrategy, DecompositionStrategies


logger = get_logger('hecate')


def get_decomposition(decompose: Decompositors) -> Drivers:
    """
    Return variables for forecast drivers to decompose forecast.

    :param decompose: either a string ('all', or 'none'), or a list containing forecast drivers
    to be calculated and returned in an output dataframe (possible list elements: 'bl', 'up',
    'cn')

    :return: tuple with a dictionary of forecast drivers variables
    """

    drivers: Drivers = []
    if isinstance(decompose, str) and decompose == 'all' or \
            isinstance(decompose, list) and decompose == ['all']:
        return list(ALL_DRIVERS.keys())

    if isinstance(decompose, list):
        return [k for k in ALL_DRIVERS.keys() if k in decompose]

    return drivers


def get_driver_output_cols(drivers: Drivers) -> ColsOfDrivers:
    """
    Return output cols to be added.

    :param drivers: list of drivers, that require additional output columns.

    :return: a list of additional output columns
    """

    add_out_cols = list(chain(*(v for k, v in ALL_DRIVERS.items() if k in drivers)))

    return add_out_cols


class DummyDecompositor:
    """
    Dummy decompositor, doing nothing.
    """

    def __init__(self,
                 drivers: Drivers | None = None,
                 dsd: DecompositionStrategies | None = None):
        """
        Initializing class attributes.
        """
        self.drivers = drivers
        self.dsd = dsd

    def decompose(self,
                  data: pd.DataFrame,
                  horizon: Horizon,
                  model: ForecastingMachine,
                  feature_set: FeatureSet,
                  drivers: Drivers | None = None,
                  dsd: DecompositionStrategies | None = None):
        """
        Function to get columns for driver values after decomposition.
        """
        return []


class Decompositor(DummyDecompositor):
    """
    Class for handling decomposition by demand drivers.
    """

    def decompose(self,
                  data: pd.DataFrame,
                  horizon: Horizon,
                  model: ForecastingMachine,
                  feature_set: FeatureSet,
                  drivers: Drivers | None = None,
                  dsd: DecompositionStrategies | None = None):
        """
        Get columns for driver values after decomposition.

        :param data: Data Frame that contain data original data and generated features
        :param horizon: forecasting horizon
        :param model: model after fit to use for prediction of other drivers
        :param feature_set: instance of FeatureSet class containing features details
        :param drivers: drivers to get decomposition for
        :param dsd: short for Decomposition Strategy Dictionary: dict for methods to use to calculate various figures

        :return: list of output columns that contain necessary cols ['key', 'date', 'fc'] and cols that was produced
        during decomposition
        """
        if drivers is None:
            drivers = list(ALL_DRIVERS.keys())
        self.horizon = horizon
        self.drivers = drivers
        self.dsd = dsd

        drivers_executor: dict[Driver, FuncAndArgs] = {
            'bl': FuncAndArgs(func=self.add_bl_uplift),
            'cn': FuncAndArgs(func=self.add_cannibalization_vol),
            'pc': FuncAndArgs(func=self.add_price_effect_vol)
        }
        for driver in self.drivers:
            drivers_executor[driver].func(data, model, feature_set)

        return get_driver_output_cols(self.drivers)

    @staticmethod
    def add_cannibalization_vol(data: pd.DataFrame, model: ForecastingMachine, feature_set: FeatureSet) -> None:
        """
        Function adds cannibalization volume in the dataframe.
        Dataframe must contain every necessary columns for forecasting and column with forecast 'fc'.
        Cannibalization volume calculated using the same model, as the forecast, but without cannibalizator discount as
        a feature. In the end, the negative difference between cannibalized forecast and forecast is considered to be
        cannibalization volume.

        :param data: pd.Dataframe
        :param model: model after fit to use to predict cannibalization volume
        :param feature_set: instance of FeatureSet class containing feature details

        :return: None
        """
        new_col = 'cannibalized'
        cann_col = ['discount_cannibalizator']

        if any([col in feature_set.selected_features for col in cann_col]):
            Decompositor.calc_vol_by_filling_features(data, feature_set, model, cann_col,
                                                      new_col, filling_values="zeros")
            data[new_col] = np.where((data[cann_col].sum(axis=1) != 0) & (data[new_col] < 0), data[new_col], 0)

        else:
            data[new_col] = 0

    def add_bl_uplift(self,
                      data: pd.DataFrame,
                      model: ForecastingMachine,
                      feature_set: FeatureSet) -> None:
        """
        Function adds baseline and uplift in the dataframe.
        Dataframe must contain every necessary columns for forecasting and column with forecast 'fc'.
        To calculate baseline we need to zero any discount related columns.
        Every column that contain 'discount' or 'promo_budget' will be filled zero in partition == fc

        :param data: pd.Dataframe
        :param model: model after fit to use to calc baseline
        :param feature_set: instance of FeatureSet class containing feature details

        :return: None
        """

        promo_cols = self._get_generated_promo_cols(feature_set)

        if promo_cols:
            self.add_bl(data, model, feature_set, promo_cols)

        else:
            data['bl'] = data['fc']

        self.add_uplift(data, promo_cols)

    @staticmethod
    def _get_generated_promo_cols(feature_set: FeatureSet) -> list[str]:
        """
        Function to get promo columns, initial (from users) and generated by hecate. Excludes those, which mark before /
        after promo effects.

        :param feature_set: instance of FeatureSet class

        :return: list of promo columns
        """
        # get initial (from users) features, associated with promo
        business_features = get_cols_from_types(
            feature_set.feature_types,
            (FeatureType.discount, FeatureType.promo_budget, FeatureType.promo)
        )

        # find every feature that was generated by hecate from parent promo columns, e.g.:
        # from business features ['discount'] get ['discount_profile', 'discount_week_before_promo']
        promo_cols = []
        for parent_col in business_features:
            promo_cols.extend([col for col in feature_set.selected_features
                               if col.split('-', 1)[-1].startswith(parent_col)])

        # drop features that mark something before and after
        promo_cols = remove_duplicates([col for col in promo_cols if 'before' not in col and 'after' not in col])

        return promo_cols

    def add_bl(self,
               data: pd.DataFrame,
               model: ForecastingMachine,
               feature_set: FeatureSet,
               promo_cols: list[str]):
        """
        Add baseline based on dsd (Decomposition Strategy Dictionary) passed. In case remove_promo strategy cannot be
        used, zero_promo strategy is used instead, while logging this case.

        :param data: initial dataframe
        :param model: model after fit to use to calc baseline
        :param feature_set: instance of FeatureSet class storing details about features
        :param promo_cols: list of features, associated with promo

        :return: None, changes applied to initial dataframe
        """

        if not self.dsd:
            self.dsd = {'bl': BLDecompositionStrategy.zero_promo}

        if self.dsd['bl'] == BLDecompositionStrategy.remove_promo:
            # prepare baseline data by removing promo periods
            bl_data = data.copy()
            bl_feature_cols = [col for col in feature_set.selected_features if col not in promo_cols]
            bl_data = bl_data[bl_data[promo_cols].sum(axis=1) == 0]

            # check if remove_promo strategy may be used
            bl_data_len = bl_data[bl_data.partition == 'train'].shape[0]
            len_cond = ((bl_data_len / data[data.partition == 'train'].shape[0]) >= 0.1) | (bl_data_len > 20)

            if len_cond and bl_feature_cols:
                self.calc_bl_by_removing_promo(data, bl_data, bl_feature_cols)
                data['fc'] = np.where(np.any(data[promo_cols] > 0, axis=1), data['fc'], data['bl'])

            else:
                # raise a warning if not
                logger.warning('not enough data to calculate bl by remove_promo, regressing to zero_promo')
                self.dsd['bl'] = BLDecompositionStrategy.zero_promo

        if self.dsd['bl'] == BLDecompositionStrategy.zero_promo:
            Decompositor.calc_vol_by_filling_features(data=data,
                                                      feature_set=feature_set,
                                                      model=model,
                                                      features_to_fill=promo_cols,
                                                      new_col='bl',
                                                      diff=False,
                                                      filling_values="zeros")

    def calc_bl_by_removing_promo(self,
                                  data: pd.DataFrame,
                                  bl_data: pd.DataFrame,
                                  bl_feature_cols: list[str]):
        """
        Try calculating baseline by removing promo periods. Only works if there's enough regular sales data in the
        initial dataframe. Adds 'bl' column, if there's enough data to use this method, no change to initial dataframe
        otherwise

        :param data: initial data with added features
        :param bl_data: bl data with promo periods removed
        :param bl_feature_cols: bl feature columns

        :return: no return, changes either applied to initial dataframe or not, depending on whether there's enough data
        """
        bl_feature_set = FeatureSet()
        bl_feature_set.selected_features = bl_feature_cols

        bl_model = ForecastingMachine().forecast(bl_data, self.horizon, bl_feature_set,
                                                 freq=get_frequency(data.date)).model
        data['bl'] = bl_model.predict(data[bl_feature_cols])

    @staticmethod
    def add_uplift(data: pd.DataFrame, promo_cols: list[str] | None = None) -> None:
        """
        Function calculate uplift on fc and baseline.
        Assumption: Uplift will be greater zero only for promo period. Promo period defines as intersection for
        promo_cols.
        Function requires columns 'fc' and 'bl' in the dataframe.
        Function add column 'uplift' with zero as default value.

        :param data: input dataframe
        :param promo_cols: list of promo columns

        :return: None
        """
        if promo_cols is None or not promo_cols:
            data['uplift'] = 0
            return

        promo_indexer = np.any(data[promo_cols] > 0, axis=1)
        data['uplift'] = np.where(promo_indexer, data['fc'] - data['bl'], 0)

    @staticmethod
    def add_price_effect_vol(data: pd.DataFrame, model: ForecastingMachine, feature_set: FeatureSet) -> None:
        """
        Function adds price effect to the dataframe.
        Dataframe must contain every necessary columns for forecasting and column with forecast 'fc'.
        Price effect calculated using the same model, as the forecast, but without price as a feature. In the end,
        the difference between forecast with price features and forecast without them is considered to be price effect
        volume.

        :param data: pd.Dataframe
        :param model: model after fit to use to predict price effect volume
        :param feature_set: instance of FeatureSet class containing feature details

        :return: None
        """
        new_col = 'price_effect'
        reg_price_cols = get_cols_from_types(feature_set.feature_types, [FeatureType.price_reg])

        if len(reg_price_cols):

            col = reg_price_cols[0]
            price_features = [col, f'business-{col}_price_period_num_before', f'business-{col}_price_period_num_after']
            price_features = [col for col in price_features if col in data.columns]

            if len(price_features):
                Decompositor.calc_vol_by_filling_features(data, feature_set, model, price_features, new_col,
                                                          filling_values="last_values")
            else:
                data[new_col] = 0
        else:
            data[new_col] = 0

    @staticmethod
    def calc_vol_by_filling_features(data: pd.DataFrame, feature_set: FeatureSet, model: ForecastingMachine,
                                     features_to_fill: list[str], new_col: str, diff: bool = True,
                                     filling_values: Literal["zeros", "last_values"] = "zeros"):
        """
        Calculate volume difference between total header forecast and another forecast, calculated by filling out
        selected features.

        :param data: initial dataframe with features
        :param feature_set: instance of FeatureSet class storing feature details
        :param model: trained model to use for forecasting
        :param features_to_zero: list of features to zero out before running forecasting again
        :param new_col: name of the column to write difference to
        :param diff: whether to write to a new_col delta between total header forecast and the new forecast, or just
        the new forecast
        """
        store_dat = data[features_to_fill]

        for col in features_to_fill:
            match filling_values:
                case "zeros":
                    fill_value = 0.0
                case "last_values":
                    fill_value = data.loc[data.partition == 'train', ['date', col]].sort_values("date") \
                        .tail(14)[col].mean()
                case _:
                    raise ValueError("Parameter 'filling_values' must be in {'zeros', 'last_values'}")

            data[col] = fill_value

        fc_no_zero_feat = model.predict(data[feature_set.selected_features]) + data['trend']
        fc_no_zero_feat = fc_no_zero_feat.clip(lower=0)

        if diff:
            data[new_col] = data.fc - fc_no_zero_feat
        else:
            data[new_col] = fc_no_zero_feat

        data[features_to_fill] = store_dat
