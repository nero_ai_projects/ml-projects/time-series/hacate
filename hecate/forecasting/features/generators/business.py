import pandas as pd

from hecate.forecasting.features.generators.base import DummyFGenerator
from hecate.forecasting.feature_generation import add_business_features
from hecate.forecasting.utils import HorizonMixin
from hecate.forecasting.horizon import Horizon
from hecate.features import TypesOfFeatures


class DefaultFGenerator(DummyFGenerator):

    def add_features(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) \
            -> tuple[pd.DataFrame, list]:

        freq = pd.infer_freq(data.date)

        if feature_types is not None:
            data, added_features = add_business_features(data, feature_types, freq)
        else:
            added_features = []

        return data, added_features


class STFGenerator(DefaultFGenerator):
    ...


class MTFGenerator(DefaultFGenerator):
    ...


class LTFGenerator(DefaultFGenerator):
    ...


class BusinessFGenerator(DummyFGenerator, HorizonMixin):

    def add_features(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) \
            -> tuple[pd.DataFrame, list]:
        generator_from_horizon = {
            Horizon.ST: STFGenerator,
            Horizon.MT: MTFGenerator,
            Horizon.LT: LTFGenerator
        }
        generator = generator_from_horizon.get(self.horizon, DefaultFGenerator)
        data, added_features = generator().add_features(data, feature_types)

        return data, added_features
