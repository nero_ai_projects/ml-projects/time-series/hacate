import pandas as pd


class DummyFeatureSelector:
    def __init__(self) -> None:
        self.horizon = None

    def drop_features(
            self,
            data: pd.DataFrame,
            business_features: list[str],
            time_features: list[str],
            period_features: list[str] | None = None,
            target_features: list[str] | None = None
    ) -> list[str]:

        if period_features is None:
            period_features = []
        if target_features is None:
            target_features = []

        return business_features + time_features + period_features + target_features
