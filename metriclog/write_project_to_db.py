import pandas as pd

from hecate.log import get_logger
logger = get_logger('hecate')


HORIZONS_DICT_ALL_FREQ = {
    'D': {
        'mt': {'granularity': 'M', 'len': 3, 'overlap': 1},
        'st': {'granularity': 'W', 'len': 6, 'overlap': 0}
    },
    'W': {
        'mt': {'granularity': 'W', 'len': 12, 'overlap': 4},
        'st': {'granularity': 'W', 'len': 6, 'overlap': 0}
    },
    'M': {
        'mt': {'granularity': 'M', 'len': 4, 'overlap': 2},
        'st': {'granularity': 'M', 'len': 2, 'overlap': 0}
    }
}


def set_focus(df: pd.DataFrame, n: int | None = None) -> None:
    """
    Add focus column to the initial dataframe. Equals 1 for the n largest keys, 0 otherwise.

    :param df: pandas dataframe to add focus column to
    :param n: integer, number of largest keys to mark as the focus keys

    :return: None, column focus added to the input dataframe
    """
    if n:
        top_keys = df[['key', 'target']].groupby('key').sum(numeric_only=True).nlargest(n, 'target').index.values
        df['focus'] = df.key.isin(top_keys).astype(int)
    else:
        df['focus'] = 1


def make_horizon_table(project: str,
                       df: pd.DataFrame,
                       freq: str | None = None,
                       horizons_dict: dict | None = None,
                       max_folds: int = 3) -> pd.DataFrame:
    """
    Create a horizon table based on input values.

    :param project: name of the project
    :param df: input dataframe containing equidistant timeline in the column 'date'
    :param freq: string indicating frequency of the timeline ('D' for day, 'W' for week, 'M' for month). If
    horizons_dict is explicitly defined, freq becomes not a mandatory parameter
    :param horizons_dict: dictionary describing folds to be created. Keys - horizon names. Values - another dictionary
    with keys:
        - 'granularity' - time frequency to consider when defining the length of forecast partition, e.g. if 'M' -
        the fold's length would be counted in months. NB! Has to have a name that could be used with pd.Grouper as a
        freq parameter
        - 'len' - length of the fold (granularity defined with 'granularity' key)
        - 'overlap' - number of periods that the folds might overlap. E.g. if 1 - folds will overlap, with 1 period
        being a part of several folds
    :param max_folds: number of folds to create. A fold with number 0 is closest to the end of timeline.

    :return: pd.DataFrame with horizons and folds, ready to be written to the database
    """

    if horizons_dict is None and freq:
        horizons_dict = HORIZONS_DICT_ALL_FREQ[freq]
    else:
        logger.info('Either explicitly define a horizons_dict, or set freq for a default horizons_dict.')

    horizon_and_folds = pd.DataFrame(columns=["project", "horizon", "fold", "forecast_start", "forecast_end"])
    end_date = df.date.max()
    # TODO: rewrite more efficiently
    for h, h_info in horizons_dict.items():
        for fold in range(max_folds):
            offset = (h_info['len'] - h_info['overlap']) * fold
            if h_info['granularity'] == 'M':
                fold_start = end_date - pd.DateOffset(months=h_info['len'] + offset) + pd.DateOffset(days=1)
                fold_end = end_date - pd.DateOffset(months=offset)
            elif h_info['granularity'] == 'W':
                fold_start = end_date - pd.DateOffset(weeks=h_info['len'] + offset) + pd.DateOffset(days=1)
                fold_end = end_date - pd.DateOffset(weeks=offset)
            else:
                raise ValueError(f"Undefined granularity {h_info['granularity']} for horizon {h}! Update the code.")

            n_points = df.loc[(df.date >= fold_start) & (df.date <= fold_end), ['date']] \
                .drop_duplicates() \
                .groupby(pd.Grouper(key='date', freq=h_info['granularity'])) \
                .agg('count').shape[0]
            if n_points < h_info['len']:
                logger.info(f'Not enough data to make fold {fold} for horizon {h} starting {fold_start}, ' +
                            f'ending {fold_end}')
            else:
                new_row = pd.DataFrame({
                    "project": project,
                    "horizon": h,
                    "fold": fold,
                    "forecast_start": fold_start,
                    "forecast_end": fold_end
                }, index=[0])
                horizon_and_folds = pd.concat([new_row, horizon_and_folds], axis=0, ignore_index=True)

    return horizon_and_folds


def create_table_query(df: pd.DataFrame, enum_vars: dict, table_name: str) -> str:
    """
    Function to create a table in a database. Structure and data types are parsed from passed df.

    :param df: dataframe to parse structure and data types from
    :param enum_vars: dictionary containing columns as keys and names of ENUM variables as values, would be used
    to assign ENUM data type to columns
    :param table_name: string, name of the table to create in the database. If such a table already exists, no changes
    to the database will be made

    :return: query string to be passed to the database after review
    """
    col_types: dict = df.dtypes.to_dict()
    smallint_thr: int = 32767

    for col, dtype in col_types.items():
        if col in enum_vars:
            col_types[col] = enum_vars[col]
        elif dtype == 'object':
            col_types[col] = f"VARCHAR({int(df[col].str.len().max())})"
        elif dtype == 'datetime64[ns]':
            col_types[col] = 'DATE'
        elif dtype == 'int':
            if (df[col].min() > -smallint_thr) & (df[col].max() < -smallint_thr):
                col_types[col] = 'SMALLINT'
            else:
                col_types[col] = 'INTEGER'
        elif dtype == 'float':
            col_types[col] = 'FLOAT4'
        elif dtype == 'bool':
            col_types[col] = 'BOOLEAN'
        else:
            logger.warning(f"Datatype {dtype} (col '{col}') has not yet been considered, correct the query manually.")

    cols_to_create: list[str] = [
        f'{k} {v}' + (' NOT NULL' if df.sku.isna().sum() == 0 else '') for k, v in col_types.items()
    ]

    query: str = f"CREATE TABLE IF NOT EXISTS {table_name} ({', '.join(cols_to_create)});"

    return query
