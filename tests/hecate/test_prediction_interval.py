
import pandas as pd
import numpy as np
import pytest

from hecate.hecate import fc_sample
from hecate.prediction_interval import (
    partition_mapper,
    add_promo_flag,
    get_deviation_at_test,
    get_hdi_bounds,
    split_deviation_by_promo,
    get_method_from_mode,
    stat_pred_interval,
    pi_stat_preproc,
    pi_stat_sample,
    check_pi_conditions,
    PredictionIntervalError
)


def test_partition_mapper():
    date_min = pd.to_datetime('2022-05-01', format='%Y-%m-%d')
    date_max = pd.to_datetime('2022-05-31', format='%Y-%m-%d')
    data = pd.DataFrame(
        np.array([pd.date_range('2022-01-01', '2023-01-01', freq='D').to_list(), ['non relevant'] * 366]).T,
        columns=['date', 'partition']
        )
    new_data = data.copy()
    new_data['partition'] = partition_mapper(data.date, date_min, date_max)
    assert new_data[new_data.partition == 'fc'].date.min() == date_min
    assert new_data[new_data.partition == 'fc'].date.max() == date_max


@pytest.mark.parametrize('hecate_input_df', ['test_1'], indirect=True)
def test_add_promo_flag(hecate_input_df):
    data, feature_types, _ = hecate_input_df
    data, promo_column = add_promo_flag(data, feature_types)
    assert data[promo_column].sum() == 5


@pytest.mark.parametrize('hecate_input_df', ['test_3'], indirect=True)
def test_add_promo_flag_empty(hecate_input_df):
    data, feature_types, _ = hecate_input_df
    data, promo_column = add_promo_flag(data, feature_types)
    assert data[promo_column].sum() == 0


def test_get_deviation_at_test(pi_input_df):
    data, _, _ = pi_input_df
    data = data[data.key == '000'][['date', 'target', 'partition_for_pi', 'fc']]
    data = get_deviation_at_test(data)
    assert data.columns.to_list() == ['date', 'target', 'partition_for_pi', 'deviation_at_test']


def test_split_deviation_by_promo(pi_input_df):
    data, _, _ = pi_input_df
    promo_column = 'discount_promo'
    cols = ['deviation_at_test', 'partition_for_pi', promo_column]
    test_data = data.loc[data.key == '000', cols]
    # standard case
    array_no_promo, array_promo = split_deviation_by_promo(test_data, promo_column)
    assert array_no_promo.size == 16
    assert array_promo.size == 15
    assert round(np.mean(array_no_promo)) == -2732
    assert round(np.mean(array_promo)) == -11303
    # case no promo
    test_data[promo_column] = False
    array_no_promo, array_promo = split_deviation_by_promo(test_data, promo_column)
    assert array_no_promo.size == 31
    assert round(np.mean(array_no_promo)) == -6880
    assert array_promo.size == 0
    # case promo only: array_no_promo takes values of array_promo
    test_data[promo_column] = True
    array_no_promo, array_promo = split_deviation_by_promo(test_data, promo_column)
    assert array_promo.size == array_no_promo.size == 31
    assert round(np.mean(array_no_promo)) == round(np.mean(array_promo)) == -6880
    # case wrong promo_column name
    with pytest.raises(ValueError):
        split_deviation_by_promo(test_data, 'unexisting_promo_column')
    # case empty data
    array_no_promo, array_promo = split_deviation_by_promo(pd.DataFrame(columns=cols), promo_column='discount_promo')
    assert array_no_promo.size == array_promo.size == 0


def test_get_hdi_bounds():
    array = np.linspace(0, 100, 101)
    pi_prob = .8
    # standard case
    lower_hdi_no_promo, upper_hdi_no_promo, lower_hdi_promo, upper_hdi_promo = get_hdi_bounds(
            array, array * 2, pi_prob, 5
        )
    assert round(lower_hdi_no_promo) * 2 == round(lower_hdi_promo) == 20
    assert upper_hdi_no_promo * 2 == upper_hdi_promo == 180
    # case too short promo
    for promo_len in [0, 4]:
        lower_hdi_no_promo, upper_hdi_no_promo, lower_hdi_promo, upper_hdi_promo = get_hdi_bounds(
                array, np.linspace(0, 100, promo_len), pi_prob, min_promo_len=5
            )
        assert round(lower_hdi_no_promo) == round(lower_hdi_promo) == 10
        assert upper_hdi_no_promo == upper_hdi_promo == 90
    # case emplty data
    lower_hdi_no_promo, upper_hdi_no_promo, lower_hdi_promo, upper_hdi_promo = get_hdi_bounds(
            np.empty(0), np.empty(0), pi_prob, 5
        )
    for x in [lower_hdi_no_promo, upper_hdi_no_promo, lower_hdi_promo, upper_hdi_promo]:
        assert isinstance(x, type(np.nan))


def test_get_method_from_mode():
    mode = 'norm'
    assert get_method_from_mode(mode) == (True, False)
    mode = 'hdi'
    assert get_method_from_mode(mode) == (False, True)
    mode = 'both'
    assert get_method_from_mode(mode) == (True, True)


def test_stat_pred_interval(pi_input_df):
    data, _, _ = pi_input_df
    data.rename(columns={'partition': 'partition_for_fc'}, inplace=True)
    data = stat_pred_interval(data[data.key == '000'], promo_column='discount_promo', mode='norm', pi_prob=0.5)
    data = stat_pred_interval(data[data.key == '000'], promo_column='discount_promo', mode='both', pi_prob=0.9)

    with pytest.raises(ValueError):
        stat_pred_interval(data, promo_column='discount_promo', mode='unexisting_mode')

    data = data[data.partition_for_fc == 'fc']
    assert (
        0 < data['PI_down_90.0%'].sum() < data['PI_down_50.0%'].sum()
        < data.fc.sum() < data['PI_up_50.0%'].sum() < data['PI_up_90.0%'].sum()
    )


def test_pi_stat_preproc(pi_input_df):
    data, feature_types, _ = pi_input_df
    date_pi_min, date_pi_max = data.loc[data.partition_for_pi == 'fc', 'date'].agg([min, max])
    date_fc_min, date_fc_max = data.loc[data.partition == 'fc', 'date'].agg([min, max])
    data = data[data.key == '000'].drop(columns=['deviation_at_test', 'partition_for_pi', 'fc'])
    data_pi, promo_column = pi_stat_preproc(data, feature_types)
    assert promo_column == 'promo_flag_for_pi'
    for col in [promo_column, 'partition_for_fc', 'partition_for_pi']:
        assert col in data_pi.columns
    assert date_pi_min, date_pi_max == data_pi.loc[data_pi.partition_for_pi == 'fc', 'date'].agg([min, max])
    assert date_fc_min, date_fc_max == data_pi.loc[data_pi.partition_for_fc == 'fc', 'date'].agg([min, max])
    assert date_pi_min, date_pi_max == data_pi.loc[data_pi.partition == 'fc', 'date'].agg([min, max])

    data = data.loc[data.date > date_pi_min + pd.to_timedelta(5, 'D')]
    with pytest.raises(PredictionIntervalError):
        pi_stat_preproc(data, feature_types)


def test_pi_stat_sample(pi_input_df):
    data, feature_types, _ = pi_input_df
    data = data[data.key == '000'].drop(columns=['deviation_at_test'])
    fc_result = fc_sample(data.drop(columns=['partition_for_pi', 'fc']), feature_types)
    data_pi = data.rename(columns={'partition': 'partition_for_fc'})

    fc_result1 = pi_stat_sample(fc_result, data_pi=data_pi.copy(), promo_column='discount_promo',
                                pi_prob_list=[0.5, 0.8])
    assert {'PI_up_50.0%', 'PI_down_50.0%', 'PI_up_80.0%', 'PI_down_80.0%'}.issubset(fc_result1.data.columns)
    assert {'PI_up_50.0%', 'PI_down_50.0%', 'PI_up_80.0%', 'PI_down_80.0%'}.issubset(fc_result1.output_columns)
    assert 'PI_up_68.0%' not in fc_result1.data.columns
    fc_result = fc_sample(data.drop(columns=['partition_for_pi', 'fc']), feature_types)
    fc_result2 = pi_stat_sample(fc_result, data_pi=data_pi, promo_column='discount_promo')
    assert 'PI_up_68.0%' in fc_result2.data.columns


def test_check_pi_conditions(pi_input_df):
    data, feature_types, _ = pi_input_df
    assert check_pi_conditions(data, pi_method='statistical', pi_prob_list=[0, 0.5, 1])
    assert check_pi_conditions(data, pi_method='statistical', pi_prob_list=None)
    assert not check_pi_conditions(data, pi_method='unexisting_method', pi_prob_list=None)
    assert not check_pi_conditions(data, pi_method='statistical', pi_prob_list=[1.1])

    bad_data = data.copy()
    bad_data['time_freq'] = 'M'
    assert not check_pi_conditions(bad_data, pi_method='statistical', pi_prob_list=None)
    bad_data.loc[data.key == '000', 'time_freq'] = 'D'
    assert bad_data.time_freq.nunique() > 1
    assert not check_pi_conditions(bad_data, pi_method='statistical', pi_prob_list=None)

    bad_data = bad_data[bad_data.key == '002']
    assert not check_pi_conditions(bad_data, pi_method='statistical', pi_prob_list=None)

    bad_data = bad_data.head(10).drop(columns=['time_freq'])
    bad_data['date'] = pd.date_range(start='2020-01-01', periods=10, freq='M')
    assert not check_pi_conditions(bad_data, pi_method='statistical', pi_prob_list=None)
