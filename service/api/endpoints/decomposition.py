import json
from fastapi import APIRouter, Request, Response, Header, Query, Depends

import hecate.hecate as hc
from hecate.log import get_logger, set_up_user_name
from hecate.utils.utils import datetime_converting
from hecate.features import TypesOfFeatures
from service.utils import is_authorized, body_to_df, build_outcome_content
from hecate.validations import quality
from service.swagger.example import Examples
from service.swagger.response import SwaggerResponse
from service.swagger.request import SwaggerRequest
from service.swagger.description import SwaggerDescription

desc = SwaggerDescription()
examp = Examples()
resp = SwaggerResponse()
reqs = SwaggerRequest()


router = APIRouter()
decompose_docs = None
logger = get_logger("hecate", service_kind="app")


@router.post(
    "",
    description=decompose_docs,
    openapi_extra=reqs.act_decomposition_openapi_extra,
    responses=resp.act_decomposition_response
)
async def decompose_act(
    request: Request,
    payload_type: str = Query(default="parquet", examples=examp.payload_type_examples),
    decompose: list[str] = Query(default=["all"], examples=examp.decompose_type_examples),
    decimals: int = Query(default=2, ge=0, description="Number of decimal places"),
    parallel: bool = True,
    feature_types: str = Header(title="Feature-Types", default="{}",
                                description=desc.headers_desc["feature_types"]),
    dependencies=Depends(is_authorized)
) -> Response:
    """
    Route fc_batch for actuals decomposition. NB! Uses fc_batch for forecasting, but you have to pass "decompose"
    partition instead of "fc". As an output you would receive decomposed data for that partition only.

    :param **payload_type**: type of request data ["parquet", "json", "csv"]\n
    :param **Decompose**: which drivers to return along with the forecast\n
    :param **decimals**: Number of decimal places\n
    :param **parallel**: type of forecasting True if parallel else False\n
    :param **api_key**: key for authorization, set into header\n
    :param **Feature_Types**: types for feature\n
    """
    set_up_user_name(logger, dependencies)
    logger.info(f"Start actuals decomposition: {decompose}")

    body = await request.body()
    try:
        data = body_to_df(body, payload_type)
    except Exception as e:
        return Response(content=str(e), status_code=400)

    try:
        feat_types: TypesOfFeatures = json.loads(feature_types)
    except json.decoder.JSONDecodeError:
        emsg = \
            """error during feature_types reading. feature_types format: {"feature": "dim"},
            keys and values must be in double quoters"""
        logger.exception(emsg)
        return Response(content=emsg, status_code=400)

    if not len(feat_types.keys()):
        return Response(content="No feature types for decomposition", status_code=400)

    missed_columns = quality.get_missed_columns(data, feat_types)
    if missed_columns:
        logger.info(f"Columns in data: {data.columns}, feature types: {feat_types}, missed: {missed_columns}")
        return Response(status_code=400, content=f"missed columns: {missed_columns}")

    outcome = hc.fc_batch(data, feat_types, decompose=decompose, parallel=parallel, act_decompose=True,
                          decimals=decimals)

    outcome = datetime_converting(outcome)
    binary_outcome = build_outcome_content(outcome, payload_type)

    logger.info("Finish actuals decomposition")

    return Response(content=binary_outcome)


decompose_docs = decompose_act.__doc__
