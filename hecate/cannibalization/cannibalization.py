"""
This module is used to search for cannibalization effects, and then add cannibalization as a separate feature.
It takes the same data as hecate as an input, and returns it with two additional columns for cannibalizator
discount and cannibalizator product name. In case no cannibalization effect is found, the input dataframe returned
as is.

Along the calculations the initial data gets grouped by the cannibalization product level (which has to be
provided in an input dataframe). Then all products are matched with all other products, and several models are used
to assess the cannibalization effect. Afterwards, for each product one cannibalizator is selected, this
cannibalizator's discount is added as a feature to an output dataframe.

More on the logics, technical details, shortcomings, and future enhancements:
https://www.notion.so/advancedx/124-87f93d91bfe94bffac45c190340818b8
"""
import pandas as pd
import numpy as np
from os import cpu_count, path
from scipy import stats
from joblib import Parallel, delayed
from itertools import combinations
from sklearn.linear_model import Ridge
from copy import deepcopy

from hecate import constants
from hecate.forecasting.models.model_selection import DefaultForecastingPipeline
from hecate.forecasting.features.generators.time import DefaultFGenerator
from hecate.features import FeatureType, get_cols_from_types
from hecate.utils.utils import get_frequency
from hecate.log import get_logger
from hecate.features import TypesOfFeatures

from hecate.forecasting.feature_selection.selection import DefaultFeatureSelector

logger = get_logger('hecate')


def get_features(sample: pd.DataFrame, feature_types: TypesOfFeatures) -> tuple[pd.DataFrame, dict]:
    """
    Function to get data and feature lists for running with linear model and XGB.

    :param sample: data
    :param feature_types: dictionary with features and their types

    :return: Tuple, containing data with generated features, and dictionary of feature lists for
    further use by linear model and XGB.
    """

    cann_disc = 'discount_cannibalizator'
    disc_col = get_cols_from_types(feature_types, [FeatureType.discount])[0]

    sample['date'] = pd.to_datetime(sample['date'], format='%Y-%m-%d')
    features = [col for col in sample.columns
                if all([col not in constants.REQUIRED_COLS, feature_types.get(col, '') != FeatureType.dim])]

    # add time features
    sample, time_features = DefaultFGenerator().add_features(sample, time_features=list(constants.TIME_FEATURES_COLS))

    # drop constant features
    feature_cols = DefaultFeatureSelector().drop_features(sample, features, time_features)
    if cann_disc not in feature_cols:
        logger.debug('not passed constant features test')

    # remove_discount_if_close
    feature_cols = remove_discount_if_close(sample, feature_cols, disc_col)
    if cann_disc not in feature_cols:
        logger.debug('not passed close discount test')

    # final features set for XGB
    feature_cols = feature_cols if feature_cols else time_features

    # drop columns with the same values for linear model
    lm_feature_cols = remove_features_if_duplicated(sample, feature_cols)
    if cann_disc not in lm_feature_cols:
        logger.debug('not passed duplicated features test')

    # get cols for linear model
    time_features = [col for col in time_features if col in lm_feature_cols]
    min_max_cols = [col for col in lm_feature_cols if col not in time_features]

    # add all feature lists to a dictionary
    lm_list_names = ['lm_features', 'lm_cat', 'lm_num']
    lm_feature_lists = [lm_feature_cols, time_features, min_max_cols]

    features_dict = {'xgb_features': feature_cols}
    features_dict.update({k + '_ext': v for k, v in zip(lm_list_names, lm_feature_lists)})
    features_dict.update({k: list(set(v).difference({cann_disc})) for k, v
                          in zip(lm_list_names, lm_feature_lists)})

    return sample, features_dict


def remove_features_if_duplicated(df: pd.DataFrame, feature_cols: list[str]) -> list[str]:
    """
    Function returns sample after removal of duplicate columns, as linear regression cannot be
    calculated otherwise.

    :param df: DataFrame to remove features from
    :param feature_cols: list of features to exclude from

    :return: list of features, values of which are not duplicated
    """

    cann_col = 'discount_cannibalizator'

    # place discount_cannibalizator at the end of feature list
    if cann_col in feature_cols:
        feature_cols.append(feature_cols.pop(feature_cols.index(cann_col)))

    df_columns = list(df.columns)
    lm_feature_cols = feature_cols.copy()

    for col_1, col_2 in combinations(feature_cols, 2):
        if (col_1 in df_columns) and (col_2 in df_columns) and \
                (col_1 != col_2) and (df[col_1].equals(df[col_2])):
            df_columns.remove(col_1)
            lm_feature_cols.remove(col_1)

    return lm_feature_cols


def remove_discount_if_close(df: pd.DataFrame, feature_cols: list[str], disc_col: str,
                             threshold: float = 0.9) -> list[str]:
    """
    The function removes discount_cannibalizator from feature_cols,if the cannibalizator promo
    period dates are matching in more than *threshold* days.

    :param df: DataFrame to assess. Has to contain 'discount_cannibalizator' columns
    :param feature_cols: list of features to exclude from
    :param disc_col: name of a column with discount data
    :param threshold: float for matching days threshold

    :return: list of features to be used for further calculations
    """

    cann_disc_col = 'discount_cannibalizator'

    if cann_disc_col not in feature_cols:
        return feature_cols

    for col in [disc_col, cann_disc_col]:
        df[col + '_flag'] = np.where(df[col] != 0, 1, 0)
    df['match_count'] = np.where(df[disc_col + '_flag'] + df[cann_disc_col + '_flag'] == 2, 1, 0)

    def _safe_div(x, y):
        if y == 0:
            return 0
        return x / y

    if _safe_div(df.match_count.sum(), df[disc_col + '_flag'].sum()) >= threshold:
        feature_cols.remove(cann_disc_col)

    return feature_cols


class ModelFilter:
    """
    Class performing filtering of potential product-cannibalizator pairs.
    """

    def __init__(self):
        self.score_w_cann = 0
        self.score_no_cann = 0
        self.p = 0
        self.xgb_feat_imp = 0

    @staticmethod
    def _get_ridge_coefs(df: pd.DataFrame, feature_cols: list[str], ohe_cols: list[str],
                         min_max_cols: list[str], get_coef: bool = False) -> dict:
        """
        Calculates Ridge regression stats for a given sample.

        :param df: pd.DataFrame with discount_cannibalizator column
        :param feature_cols: list with features
        :param ohe_cols: list with time_features for OHE discounts
        :param min_max_cols: list with columns to apply MinMaxScaler
        :param get_coef: whether to calculate and return coefficient and
        significance of discount_cannibalizator feature

        :return: dictionary with score and p-value
        """
        if not len(feature_cols):
            logger.debug('all linear model features dropped, skipping calculation')
            return {'score': 0, 'p': 1}

        # fit linear model
        x, y = df[feature_cols], df['target']
        lm = DefaultForecastingPipeline(
            feature_cols=feature_cols,
            constructor=Ridge,
            params_model={},
            ohe_cols=ohe_cols,
            min_max_cols=min_max_cols)
        lm.model.fit(x, y)

        # TODO add all the calls to lm model as CommonForecastingPipeline class methods
        # Need approve from Denis for this
        x_t = lm.model.regressor_.named_steps['preprocessor'].transform(x)
        y_t = lm.model.transformer_.transform(np.array(y)[:, np.newaxis])

        # get score and p-value
        lm_score = lm.model.regressor_.named_steps['model'].score(x_t, y_t)
        fd = x.shape[0] - x_t.shape[1]
        if fd < 10:
            return {'score': 0, 'p': 1}
        lm_score = 1 - (1 - lm_score) * (x_t.shape[0] - 1) / (fd - 1)

        lm_p = 0
        if get_coef:
            y_hat = lm.model.transformer_.transform(lm.predict(x)[:, np.newaxis])

            lm_coefs = lm.model.regressor_.named_steps['model'].coef_
            lm_sse = np.sum((y_hat - y_t) ** 2, axis=0) / fd
            try:
                lm_v = np.diagonal(lm_sse[0] * np.linalg.inv(np.dot(x_t.T, x_t)))
            except Exception:
                lm_p = 1
            else:
                lm_se = np.array([np.sqrt(np.clip(lm_v, 0, 10 ** 6))])
                with np.errstate(divide='ignore'):
                    lm_t_v = np.where(lm_se != 0, lm_coefs / lm_se, 0)
                lm_p_v = 2 * (1 - stats.t.cdf(np.abs(lm_t_v), fd))

                # get discount_cannibalizator index in columns after CT
                # NB! It's important that 'num' transformer is the first
                # one in ColumnTransformer of CommonForecastingPipeline
                lm_p = lm_p_v[0][len(min_max_cols) - 1]

        return {'score': lm_score, 'p': lm_p}

    def _filter_by_ridge(self, df: pd.DataFrame, features_dict: dict):
        """
        Runs Ridge twice: with and without discount_cannibalizator as a feature. Then updates
        properties of ModelFilter class instance, including score_w_cann, p, and score_no_cann.

        :param df: dataframe with features
        :param features_dict: dictionary with feature lists for different models run

        :return: None. Updates Ridge properties of ModelFilter class instance
        """

        # run Ridge with cannibalizator
        incl_cann = [v for k, v in features_dict.items() if '_ext' in k]
        ridge_incl_cann = self._get_ridge_coefs(df, *incl_cann, get_coef=True)
        self.score_w_cann = ridge_incl_cann['score']

        # run Ridge without cannibalizator
        excl_cann = [features_dict[k[:-4]] for k in features_dict.keys() if '_ext' in k]
        ridge_excl_cann = self._get_ridge_coefs(df, *excl_cann)
        self.p = ridge_incl_cann['p']
        self.score_no_cann = ridge_excl_cann['score']

    def _filter_by_xgb(self, df: pd.DataFrame, features_list: list):
        """
        Runs XGB to get feature importance for cannibalizator discount.

        :param df: initial data
        :param features_list: list with features

        :return: None. Updates self.xbg_feat_imp property of ModelFilter class
        instance
        """
        xgb = DefaultForecastingPipeline(feature_cols=features_list, freq=get_frequency(df['date']))
        xgb.fit(df[features_list], df['target'])

        imp_dict = {k: v for k, v in zip(features_list, xgb.get_feature_importance())}
        self.xgb_feat_imp = imp_dict['discount_cannibalizator']

    def fit(self, df: pd.DataFrame, features_dict: dict, logger_product_str: str = '',
            p_threshold: float = 0.15, score_threshold: float = 0.01,
            xgb_threshold: float = 0.02) -> pd.DataFrame:
        """
        Function performs filtering. Returns filtered dataframe.

        :param df: data
        :param features_dict: dictionary with feature lists for different types of models
        :param logger_product_str: string for logger
        :param p_threshold: threshold for p-value for a cannibalizator discount variable when
        running Ridge with it
        :param score_threshold: difference between scores of Ridge models with and without
        discount_cannibalizator as a variable
        :param xgb_threshold: threshold for feature importance value

        :return: initial data with additional column 'xgb_feature_imp', containing feature
        importance value, if it's more than threshold, empty dataframe - otherwise
        """

        self._filter_by_ridge(df, features_dict)
        p_val_condition = self.p < p_threshold
        score_condition = self.score_w_cann - self.score_no_cann > score_threshold

        if not (p_val_condition | score_condition):
            logger.debug(logger_product_str + 'discarded by Ridge filter')
            return pd.DataFrame()

        self._filter_by_xgb(df, features_dict['xgb_features'])

        if not self.xgb_feat_imp > xgb_threshold:
            logger.debug(logger_product_str + 'discarded by XGB filter')
            return pd.DataFrame()

        df['xgb_feature_imp'] = self.xgb_feat_imp
        return df


def prefilter_combinations(df: pd.DataFrame, mult: int = 10) -> pd.DataFrame:
    """
    Removes data for product-cannibalizator combinations, if mean key product sales is 10 times
    more than mean cannibalizator sales.

    :param df: initial data with 'key' and 'cannibalizator' columns

    :return: pd.DataFrame
    """

    df = df[df.groupby('key')['target'].transform('mean') <
            df.groupby('cannibalizator')['target'].transform('mean') * mult]

    return df


def get_max_cannibalizator(df: pd.DataFrame) -> pd.DataFrame:
    """
    Function receives dataframe with all the potential cannibalizators of a given
    product. Returns only one product-cannibalizator pair, for which the XGB feature
    importance is maximum.

    :param df: initial data. Has to have 'xgb_feature_imp' column.

    :return: pd.DataFrame with the initial columns, except
    'xgb_feature_imp'.
    """

    df_gr = df.groupby('cannibalizator').agg({'xgb_feature_imp': 'max'})
    cann = df_gr.nlargest(1, 'xgb_feature_imp').index[0]
    df = df.loc[df.cannibalizator == cann, ['key', 'cannibalizator']].drop_duplicates()

    return df


def select_cannibalizator(sample: pd.DataFrame, discounts: pd.DataFrame,
                          feature_types: TypesOfFeatures, write_res: bool = False) -> pd.DataFrame:
    """
    Runs cannibalizator search, then adds cannibalizator discount as a separate column.

    :param sample: initial product data
    :param discounts: cannibalizator discount data
    :param feature_types: dictionary of features and their types
    :param write_res: if True, the results are written in a file using auxiliary function

    :return: DataFrame containing cannibalizator discount. None, if
    cannibalizator discount is zero or equals product discount.
    """

    key = sample['key'].unique()[0]

    out = []
    for cannibalizator, cann_dat in discounts.groupby('cannibalizator'):

        logger_product_str = f'key {key}, cannibalizator {cannibalizator}: '
        df = sample.merge(cann_dat, how='left', on='date')
        df['discount_cannibalizator'].fillna(0, inplace=True)
        df['cannibalizator'] = cannibalizator

        df = prefilter_combinations(df)
        if df.empty:
            logger.debug(logger_product_str + 'cannibalizator sales too low')
            continue

        df, features_dict = get_features(df, feature_types)
        if 'discount_cannibalizator' not in features_dict['lm_features_ext']:
            logger.debug(logger_product_str + 'removed as a feature')
            continue

        mf = ModelFilter()
        df = mf.fit(df, features_dict, logger_product_str)

        if write_res:
            dump_to_file([key, cannibalizator] + [v for v in mf.__dict__.values()])

        if not df.empty:
            out.append(df)

    if len(out):
        return get_max_cannibalizator(pd.concat(out))

    else:
        logger.debug(f'for product {key} cannibalizators not found')
        return pd.DataFrame()


def dump_to_file(list_to_write: list,
                 file_path: str = 'data/check_cannibalization_stats.txt'):
    """
    Function to append a file with text.

    :param list_to_write: list of data to write
    :param file_path: path of file to write to

    :return: no return
    """

    if not path.exists(file_path):
        with open(file_path, 'w') as f:
            f.write('\t'.join(['product', 'cannibalizator', 'score_w_cann', 'score_no_cann', 'p',
                               'xgb_feat_imp']) + '\n')

    with open(file_path, 'a') as f:
        f.write('\t'.join([str(i) for i in list_to_write]) + '\n')


def get_cannibalization_pairs(df: pd.DataFrame,
                              feature_types: TypesOfFeatures,
                              parallel: bool = True,
                              write_res: bool = False) -> pd.DataFrame:
    """
    Returns pd.DataFrame with key-cannibalizator pairs, selected for all the products. The output dataframe
    contains cannibalizator discounts by dates as well.

    :param df: initial data for all the products
    :param feature_types: dictionary with features as keys
    :param parallel: whether to parallelize calculations
    :param write_res: if True, the intermediate filtering results are written in a file using auxiliary function

    :return: pd.DataFrame with selected cannibalizators' discounts. Empty dataframe, if no cannibalization
    found
    """
    discounts = get_discounts(df, feature_types)

    if parallel:
        tasks = ((sample.copy(),
                  discounts[discounts.cannibalizator != key],
                  feature_types,
                  write_res)
                 for key, sample in df.groupby('key', observed=True))
        n_jobs = cpu_count()
        res = Parallel(verbose=1, n_jobs=n_jobs)(delayed(select_cannibalizator)(*task)
                                                 for task in tasks)
        cannibalization_data = pd.concat(res if res is not None else [pd.DataFrame()], ignore_index=True)
    else:
        out = []
        for key, sample in df.groupby('key', observed=True):
            params = (sample.copy(),
                      discounts[discounts.cannibalizator != key],
                      feature_types,
                      write_res)
            out.append(select_cannibalizator(*params))
        cannibalization_data = pd.concat(out)

    return cannibalization_data


def get_discounts(df: pd.DataFrame, feature_types: TypesOfFeatures) -> pd.DataFrame:
    """
    Calculate dataframe, containing data on product discounts. Takes as an input a dataframe, aggregated
    by cannibalization product.

    :param df: data for all the products, aggregated by cannibalization product
    :param feature_types: dictionary with features as keys

    :return: tuple with two dataframes, first one contains aggregated data, second one contains product
    discounts
    """

    can_disc = 'discount_cannibalizator'
    discount_cols = get_cols_from_types(feature_types, [FeatureType.discount])
    if discount_cols:
        discount_col = discount_cols[0]
    else:
        logger.warning('No column with feature type discount, cannibalization calculation skipped')
        return pd.DataFrame(columns=['cannibalizator', can_disc])

    discounts = df[['date', 'key', discount_col]].copy()
    discounts.rename({'key': 'cannibalizator', discount_col: can_disc}, axis=1, inplace=True)
    discounts = discounts[discounts.groupby('cannibalizator')[can_disc].transform('sum') != 0]

    return discounts


def add_cannibalization_as_feature(df: pd.DataFrame,
                                   feature_types: TypesOfFeatures,
                                   parallel: bool = True) -> tuple[pd.DataFrame, TypesOfFeatures]:
    """
    Filters initial dataframe using Ridge, and then XGB by iterating over potential pairs of
    products and cannibalizators.
    1) Groups the data at cannibalization product level (should be in the input dataframe)
    2) Iterates over dataframe keys, adds discount of every other product as a cannibalizator
    discount feature, runs Ridge regression. Filters the initial dataframe based on Ridge results.
    3) Runs XGB to retrieve importance of cannibalizator discount as a feature.
    If the two filtering steps have been passed, initial dataframe is appended to contain
    cannibalizator discount column, as well as the name of cannibalizator product. If no
    cannibalizator effect is found, the discount column is zero, and cannibalizator product
    column contains empty strings.
    Output dataframe with added cannibalizator discount feature to be used by hecate.

    :param df: initial data for all the products
    :param feature_types: dictionary with features as keys
    :param parallel: whether to parallelize calculations

    :return: pd.DataFrame, ready to be used for forecasting by hecate
    """

    cannibalization_products: list[str] = get_cols_from_types(feature_types, [FeatureType.can_level])
    if cannibalization_products:
        cannibalization_product: str = cannibalization_products[0]
    else:
        logger.warning('No column with feature type can_level, cannibalization calculation skipped')
        return df, feature_types

    feature_types = deepcopy(feature_types)
    for col in cannibalization_products:
        feature_types[col] = FeatureType.dim

    can_data_list = []
    for _, mdf in df[df.partition == 'train'].groupby(cannibalization_product, observed=True):
        can_bit = get_cannibalization_pairs(mdf, feature_types, parallel)
        can_data_list.append(can_bit)
    can_data = pd.concat(can_data_list, ignore_index=True)

    # add cannibalizator data to initial dataframe
    if can_data.empty:
        return df, feature_types

    # merge cannibalization pairs with discounts data
    discounts = get_discounts(df[df.key.isin(can_data.cannibalizator)], feature_types)
    discounts = discounts.merge(can_data, on=['cannibalizator'])

    # merge initial dataframe with discounts to get cannibalizator discount
    df = df.merge(discounts, how='left', on=['key', 'date'])
    df['discount_cannibalizator'].fillna(0, inplace=True)
    df['cannibalizator'] = df.cannibalizator.astype(str).fillna('')

    # update feature_types
    feature_types['cannibalizator'] = FeatureType.dim

    return df, feature_types
