from service.swagger.schemas import TaskResponse, NonAuth, ServerError, JsonDataResponse, CSVData


class SwaggerResponse():

    response_code_418 = {
        "description": "Return if non-authorized",
        "content": {
            "application/json": {
                "schema": NonAuth.schema(),
                "examples": {
                    "json": {
                        "summary": "Non-authorizated type",
                        "description": "Json response non-authorizated type example.",
                        "value": {
                            "msg": "You are not authorizated!"
                        },
                    },
                },
            }
        }
    }

    response_code_500 = {
        "description": "Return if internal server error",
        "content": {
            "application/json": {
                "schema": ServerError.schema(),
                "examples": {
                    "json": {
                        "summary": "Json response example",
                        "description": "Json response with internal server error example.",
                        "value": {
                            "msg": "Done with internal server error!"
                        },
                    },
                },
            }
        }
    }

    task_response = {
        201: {
            "description": "Return task indificator",
            "content": {
                "application/json": {
                    "schema": TaskResponse.schema(),
                    "examples": {
                        "json": {
                            "summary": "Json data",
                            "description": "Json response data type example.",
                            "value": {
                                "task_id": "1"
                            },
                        },
                    },
                },
            },
        },
        418: response_code_418,
        500: response_code_500
    }

    fc_response = {
        200: {
            "description": "Return Result of forecast",
            "content": {
                "application/json": {
                    "schema": JsonDataResponse.schema(),
                    "examples": {
                        "json": {
                            "summary": "Json data",
                            "description": "Json response data type example. " +
                                           "Use this example, when payload_type = 'json'",
                            "value": {
                                'key': {'0': 'key-1', '1': 'key-1', '2': 'key-1', '3': 'key-1'},
                                'date': {'0': '2022-07-04', '1': '2022-07-11', '2': '2022-07-18', '3': '2022-07-25'},
                                'fc': {'0': 33.58, '1': 33.58, '2': 33.58, '3': 33.58}
                            },
                        },
                    },
                },
                "text/csv": {
                    "schema": CSVData.schema(),
                    "examples": {
                        "csv": {
                            "summary": "CSV data",
                            "description": "CSV response data type example. " +
                                           "Use this example, when payload_type = 'csv'",
                            "value": "" +
                                     ";key;date;fc\n" +
                                     "0;key-1;2022-07-04;33.58\n" +
                                     "1;key-1;2022-07-11;33.58\n" +
                                     "2;key-1;2022-07-18;33.58\n" +
                                     "3;key-1;2022-04-25;33.58"
                        }
                    }
                }
            },
        },
        418: response_code_418,
        500: response_code_500
    }

    can_response = {
        200: {
            "description": "Return cannibalizator pairs",
            "content": {
                "application/json": {
                    "schema": {
                        "$ref": ""
                    },
                    "examples": {
                        "json": {
                            "summary": "Json data",
                            "description": "Json response data type example. " +
                                           "Use this example, when payload_type = 'json'",
                            "value": {
                                'key': {'0': 'product_1', '1': 'product_2', '2': 'product_3'},
                                'cannibalizator': {'0': 'product_4', '1': 'product_5', '2': 'product_6'},
                            },
                        },
                    },
                }
            },
        },
        418: response_code_418
    }

    act_decomposition_response = {
        200: {
            "description": "Return decomposed actuals",
            "content": {
                "application/json": {
                    "schema": {
                        "$ref": ""
                    },
                    "examples": {
                        "json": {
                            "summary": "Json data",
                            "description": "Json response data type example. " +
                                           "Use this example, when payload_type = 'json'",
                            "value": {
                                "key": {"12": "key-1", "13": "key-1", "14": "key-1"},
                                "date": {"12": "2023-01-01", "13": "2023-02-01", "14": "2023-03-01"},
                                "fc": {"12": 10.98, "13": 16.1, "14": 16.1},
                                "bl": {"12": 10.98, "13": 16.1, "14": 16.1},
                                "uplift": {"12": 0, "13": 0, "14": 0},
                                "cannibalized": {"12": 0, "13": 0, "14": 0},
                                "price_effect": {"12": 0, "13": 0, "14": 0}
                            },
                        },
                    },
                }
            },
        },
        418: response_code_418
    }

    outliers_target = {
        200: {
            "description": "Return Result of adding target outliers marks",
            "content": {
                "application/json": {
                    "schema": JsonDataResponse.schema(),
                    "examples": {
                        "json": {
                            "summary": "Json data",
                            "description": "Json response data type example. "
                                           "Use this example, when payload_type = 'json'",
                            "value": {
                                'key': {'0': 'key-1', '1': 'key-1', '2': 'key-1', '3': 'key-1'},
                                'date': {'0': '2022-07-04', '1': '2022-07-11', '2': '2022-07-18', '3': '2022-07-25'},
                                'partition': {'0': 'train', '1': 'train', '2': 'train', '3': 'train'},
                                'target': {'0': 100.01, '1': 1, '2': 1, '3': 0},
                                'target-outlier': {'0': False, '1': False, '2': False, '3': False},
                                'target-zero_period': {'0': 0, '1': 0, '2': 0, '3': 0},
                            },
                        },
                    },
                },
                "text/csv": {
                    "schema": CSVData.schema(),
                    "examples": {
                        "csv": {
                            "summary": "CSV data",
                            "description": "CSV response data type example. "
                                           "Use this example, when payload_type = 'csv'",
                            "value":
                                "key;date;target;const_feat;partition;discount_promo;stock;target-outlier;target-zero_period\n"  # noqa: E501
                                "1;2021-11-01;5;3;train;15;11;False;0\n"
                                "1;2021-11-02;15;3;train;15;3;False;0\n"
                                "1;2021-11-03;0;3;train;0;4;False;0\n"
                                "1;2021-11-04;11;3;train;10;0;False;0\n"
                                "1;2021-11-05;7;3;fc;15;0;False;0\n"
                                "1;2021-11-06;8;3;fc;15;0;False;0"
                        }
                    }
                }
            },
        },
        418: response_code_418,
        500: response_code_500
    }
