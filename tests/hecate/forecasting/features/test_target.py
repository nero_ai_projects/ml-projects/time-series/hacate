import numpy as np
import pandas as pd

from hecate.forecasting.features.target import add_rolling


def test_add_rolling(rolling_input_df):
    test_sample, _, _ = rolling_input_df
    necessary_cols = ['key', 'date', 'target', 'partition']
    test_sample.sort_values('date', inplace=True)

    # formal checking
    updated = add_rolling(test_sample[necessary_cols].copy())
    assert 'target-rolling' in updated.columns

    updated = add_rolling(test_sample[necessary_cols + ['target-zero_period']])
    assert 'target-rolling' in updated.columns

    updated = add_rolling(test_sample[necessary_cols + ['target-outlier']])
    assert 'target-rolling' in updated.columns

    updated = add_rolling(test_sample[necessary_cols + ['target-outlier', 'target-zero_period']])
    assert 'target-rolling' in updated.columns

    test_sample['discount'] = 0
    updated = add_rolling(
        test_sample[necessary_cols + ['target-outlier', 'target-zero_period', 'discount']],
        discount_cols=['discount']
    )
    assert set(updated.columns) \
           - set(test_sample.columns) \
           - set(['target-rolling',
                  'target-zero_period',
                  'target-outlier']) == set(), \
           "There are unnecessary columns in the dataset!"

    # same mean values
    updated = add_rolling(test_sample.copy())
    train_2w_mean = test_sample[test_sample.partition == 'train'].target.values[-14:].mean()
    fc_2w_mean = updated[updated.partition == 'fc']['target-rolling'].values[:14].mean()
    assert 0.05 > round(abs(train_2w_mean - fc_2w_mean) / train_2w_mean, 2), \
        'means target and rolling diff more 5%'

    # need to check if it ignores outliers and discount
    col_mult = [
        ('target-zero_period', 0),
        ('target-outlier', 10),
        ('discount', 10),
    ]
    for col, mult in col_mult:
        case_sample = test_sample.copy()
        case_sample['discount'] = 0
        dates = case_sample[case_sample.partition == 'train'].date.values[-14:]
        indexer = case_sample.date.isin(dates)
        case_sample['target'] = np.where(indexer,
                                         case_sample.target * mult,
                                         case_sample.target)
        case_sample[col] = np.where(indexer, 1, 0)
        updated = add_rolling(
            case_sample[necessary_cols + ['target-outlier',
                                          'target-zero_period',
                                          'discount']],
            discount_cols=['discount']
        )

        train_2w_mean = updated[(updated.partition == 'train') &
                                ~updated[col]].target.values[-14:].mean()
        fc_2w_mean = updated[updated.partition == 'fc']['target-rolling'].values[:14].mean()

        assert 0.05 > round(abs(train_2w_mean - fc_2w_mean) / train_2w_mean, 2), \
            f"target-rolling doesn't ignore {col}"

    # tests on synthetic_data
    synthetic_data = pd.DataFrame(
        {
            'date': pd.date_range(start='2020-01-01', end='2020-01-31'),
            'key': ['key_1'] * 31,
            'target': [1000] * 31
        }
    )
    synthetic_data['partition'] = np.where(synthetic_data.date > '2020-01-20', 'fc', 'train')
    synthetic_data['target'] = np.where(synthetic_data['partition'] == 'fc', 0, synthetic_data['target'])

    # test for loss of values from the training sample
    test_sample = synthetic_data.copy()
    updated = add_rolling(test_sample.copy())
    assert test_sample.loc[test_sample.partition == 'train', 'target'].sum() \
        == updated.loc[updated.partition == 'train', 'target-rolling'].sum(), \
        "Data loss has occurred!"

    # zero_period leak test
    test_sample = synthetic_data.copy()
    test_sample['target-zero_period'] = \
        np.where(test_sample.date.isin(['2020-01-10', '2020-01-12']), 1, 0)
    test_sample['target'] = np.where(test_sample['target-zero_period'], 0.001, test_sample['target'])
    updated = add_rolling(test_sample.copy())
    assert round(test_sample.loc[test_sample.date == '2020-01-11', 'target'].values[0], 5) \
        == round(updated.loc[updated.date == '2020-01-11', 'target-rolling'].values[0], 5), \
        "The values inside the target-zero_period affected the target-rolling!"

    # leak test near the promo slot
    test_sample = synthetic_data.copy()
    test_sample['discount'] = np.where(test_sample.date.isin(['2020-01-10', '2020-01-12']), 20, 0)
    test_sample['target'] = np.where(test_sample['discount'] != 0, test_sample['target'] * 1.2, test_sample['target'])
    updated = add_rolling(test_sample.copy(), discount_cols=['discount'])
    assert round(test_sample.loc[test_sample.date == '2020-01-11', 'target'].values[0], 5) \
        == round(updated.loc[updated.date == '2020-01-11', 'target-rolling'].values[0], 5), \
        "The values inside the promo slot affected rolling!"

    # leak test near outliers
    test_sample = synthetic_data.copy()
    test_sample['target-outlier'] = np.where(test_sample.date.isin(['2020-01-10', '2020-01-12']), 1, 0)
    test_sample['target'] = np.where(test_sample['target-outlier'], 1e10, test_sample['target'])
    updated = add_rolling(test_sample.copy())
    assert round(test_sample.loc[test_sample.date == '2020-01-11', 'target'].values[0], 5) \
        == round(updated.loc[updated.date == '2020-01-11', 'target-rolling'].values[0], 5), \
        "The values inside the outlier affected the rolling!"

    # leak test in the forecast horizon
    test_sample = synthetic_data.copy()
    dates_promo = ['2020-01-18', '2020-01-19', '2020-01-20', '2020-01-21', '2020-01-22']
    test_sample['discount'] = np.where(test_sample.date.isin(dates_promo), 20, 0)
    test_sample['target'] = np.where(test_sample['discount'] != 0,
                                     test_sample['target'] * 1.2,
                                     test_sample['target'])
    updated = add_rolling(test_sample.copy(), discount_cols=['discount'])
    assert updated.loc[updated.partition == 'train', 'target-rolling'].mean() \
        == updated.loc[updated.partition == 'fc', 'target-rolling'].mean(), \
        "The promo slot on the border of partition affected rolling!"
