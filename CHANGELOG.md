## 3.11.2 (2024-04-01)

No changes.

## 3.11.1 (2024-03-14)

### Fixed (2 changes)

- [fix: :bookmark: Update CI/CD Pipeline with predeploy install stage](augmento/hecate@58548fa57a1eb8e52b096836e8870c4fbf943209) by  ([merge request](augmento/hecate!354))
- [fix: :bookmark: Update CI/CD Pipeline with predeploy install stage](augmento/hecate@92a5352d72acb6910fd15fcc95215ff0a84d2a43) by  ([merge request](augmento/hecate!352))

## 3.11.0 (2024-02-19)

### Added (1 change)

- [feat(API): add prediction interval calculations to API](augmento/hecate@9c70f78da7bd204344194c73cb12709fd07b0f6b) by @oleg.klimenko ([merge request](augmento/hecate!346))

## 3.10.2 (2024-02-15)

### Fixed (1 change)

- [refactor: transform rounding funcs to classes](augmento/hecate@7378769e5c1f004e85ebfc46be48b9a9875e03c7) by @rodion.kraskov ([merge request](augmento/hecate!343))

## 3.10.1 (2024-02-15)

### Fixed (1 change)

- [fix: :memo: Fix CICD in deploy stage + DNS_NAME for deploy prod stage](augmento/hecate@28ef3e7c1f52b13bc1b4cc36f05062e91359c1ec) by  ([merge request](augmento/hecate!349))

## 3.10.0 (2024-02-12)

### Added (2 changes)

- [feat(PI): introduce prediction interval calculations into pipeline of fc_sample and fc_batch](augmento/hecate@0a062866ebb0de97d916cc7e942de2d275305109) by @oleg.klimenko ([merge request](augmento/hecate!326))
- [feat: initial creation of module for prediction interval calculations](augmento/hecate@db3ebe808e69202abb978fa27377f063a4d4b5cb) by @oleg.klimenko ([merge request](augmento/hecate!326))

## 3.9.10 (2024-02-12)

### Fixed (1 change)

- [fix: :ambulance: Eable restart ing docker containers after reboot](augmento/hecate@bd96c65c9c29e6edc35b6a45348d1bed276cb22c) by  ([merge request](augmento/hecate!345))

## 3.9.9 (2024-02-09)

### Fixed (1 change)

- [fix: add error handling for feat_types json load](augmento/hecate@91cb9f6ff7ca9b565997523a9a0c9ca3fec4d7f4) by @rodion.kraskov ([merge request](augmento/hecate!342))

## 3.9.8 (2024-02-09)

### Fixed (2 changes)

- [fix: :bug: rewrite the is_pinc_produce_target_changes function](augmento/hecate@95c52db10eb12a0334913d0050b596691a8c02d5) by @konstantin.losev ([merge request](augmento/hecate!344))
- [fix: :bug: save the table sorting by date in the add_profile function](augmento/hecate@f508170a759d68fc2af1cd6b946725597cedc3ba) by @konstantin.losev ([merge request](augmento/hecate!344))

## 3.9.7 (2024-02-07)

### Fixed (1 change)

- [fix: :alien: Add ngnix logs to Grafana and update reverse proxy with ssl to Grafana](augmento/hecate@fbd9f62d6b46d71ddfa9afc1f0a97e50bcbbee60) by  ([merge request](augmento/hecate!341))

## 3.9.6 (2024-02-05)

### Fixed (1 change)

- [fix(outliers): :bug: exclude mixing of types (bool and int)](augmento/hecate@a6cb3e3b0e5779262fcee975cc151f65b6e932ae) by @konstantin.losev ([merge request](augmento/hecate!340))

## 3.9.5 (2024-02-01)

### Fixed (1 change)

- [fix(metriclog): :bug: the boundaries of chunks in extract_dataset are...](augmento/hecate@01f997f123419a94b39d439c4b32ad8dbef45c26) by @konstantin.losev ([merge request](augmento/hecate!330))

## 3.9.4 (2024-02-01)

### Fixed (1 change)

- [fix(feature_type): :bug: label feature type does not conflict with selected features](augmento/hecate@3f14befa71a670e7cd51e3eee1e6e693bb3c0def) by @konstantin.losev ([merge request](augmento/hecate!338))

## 3.9.3 (2024-01-31)

### Fixed (1 change)

- [fix: :egg: Healthcheck fix for docker container, add user to logger (for log filtering)](augmento/hecate@0707dc2b1245743ea26f89f6eaa39c85a11f8fe9) by  ([merge request](augmento/hecate!335))

## 3.9.2 (2024-01-25)

### fix (1 change)

- [feat: :boom: Replace proxy service from traefik to nginx](augmento/hecate@4b28b8e86d46ce515a4a7d94b94f6b875c2ca305) ([merge request](augmento/hecate!331))

## 3.9.1 (2024-01-23)

### fix (1 change)

- [refactor: :tada: Refactor CI Regestry Image from static to variable](augmento/hecate@1304ff999c0a7857497fe137e3f0b613c4aee7bd) ([merge request](augmento/hecate!329))

## 3.9.0 (2024-01-17)

### feat (1 change)

- [feat(service): add endpoint /outliers/target](augmento/hecate@8fb9d48f5b46cadca90e433cedf6baac9940c64c) ([merge request](augmento/hecate!327))

## 3.8.1 (2024-01-16)

### fix (1 change)

- [fix(metriclog): :bug: update some params for run_hecate_on_data_from_db](augmento/hecate@6284e692b9221d5c1155975dcba357006dbf1f70) ([merge request](augmento/hecate!317))

## 3.8.0 (2024-01-16)

### fix (1 change)

- [fix(metriclog): :bug: fix send_df_in_parallel](augmento/hecate@1a69d546a5d04ac6303152369c4d4be4a68fa953) ([merge request](augmento/hecate!312))

### feat (1 change)

- [feat: :sparkles: creating functions to use for writing new project to the database](augmento/hecate@6ad2d98ee26ccee74fe2545e09d07b86c0ca8af4) ([merge request](augmento/hecate!312))

## 3.7.13 (2023-12-25)

### fix (2 changes)

- [refactor: :tada: Set image tag configuration to CICD](augmento/hecate@19922b3ff615f4f44b1b6eb634731f363301a96f) ([merge request](augmento/hecate!320))
- [fix: :truck: Add version to poetry & refactor including parts to jobs](augmento/hecate@79e06b365356de33d9ec72d2f7d431ae1841d30c) ([merge request](augmento/hecate!318))

## 3.7.12 (2023-12-20)

### fix (1 change)

- [fix: :rocket: Add restart: always to docker services](augmento/hecate@474d32eb4256cf346de92ca7971e344eaec842de) ([merge request](augmento/hecate!315))

## 3.7.11 (2023-12-18)

### fix (1 change)

- [fix: fixed feature types for cases with custom preprocessor](augmento/hecate@4b3bc8b80248f749b361306c7f75a9655e6d665b) ([merge request](augmento/hecate!311))

## 3.7.10 (2023-12-14)

### fix (2 changes)

- [fix: bug with too short train datasets](augmento/hecate@e9071d5015b411205f9d27605f4584b28d6c30b1) ([merge request](augmento/hecate!309))
- [fix: bug with 0/0 median](augmento/hecate@97edfbafc059e4eaa98da6f13606fbf8949fa413) ([merge request](augmento/hecate!309))

## 3.7.9 (2023-11-30)

### fix (1 change)

- [fix(feature_gen): :bug: fixed a bug with incorrect calculation of promo_id](augmento/hecate@05e481551c4d66eac9fbd1052271a8a0b2d23022) ([merge request](augmento/hecate!308))

## 3.7.8 (2023-11-24)

### fix (1 change)

- [feat: :rocket: Add pre-merge test in CI/CD Pipeline](augmento/hecate@fd6484315a953e741bf2f0810deda195455d4af9) ([merge request](augmento/hecate!305))

## 3.7.7 (2023-11-21)

### fix (1 change)

- [fix(feature_gen): fixed unique_slots_id](augmento/hecate@cf44077b728d23e05e0f8469c24d3e7ef1d9bdab) ([merge request](augmento/hecate!302))

## 3.7.6 (2023-11-14)

### fix (1 change)

- [fix: :bug: replace str horizon to enum in model_selection](augmento/hecate@d1c77961c17386338055e71a71a496e23e2e5cb5) ([merge request](augmento/hecate!301))

## 3.7.5 (2023-11-09)

### fix (1 change)

- [refactor: :ambulance: Refactor CI/CD Pipeline](augmento/hecate@076aa9ba17b12bdd9bd0cd95fb5a3cd2eb1a3247) ([merge request](augmento/hecate!299))

## 3.7.4 (2023-11-08)

### fix (2 changes)

- [perf: :racehorse: optimize calculation trend and outliers](augmento/hecate@af7d3f2cab9809cb3927a0276ae9b832d020f2d1) ([merge request](augmento/hecate!295))
- [perf: :racehorse: optimize feature generation](augmento/hecate@0d62548e2791b5dd0fc38e6ee8c492cd30aa5a5f) ([merge request](augmento/hecate!295))

## 3.7.3 (2023-11-07)

### fix (3 changes)

- [fix(types): :bug: marking immutable objects as "final" and "Final"](augmento/hecate@cab4f182e1bb94f9613a2cc428613dd1c0295d90) ([merge request](augmento/hecate!272))
- [fix(types): :bug: changing the string definition of forecasting horizons to the Enum type](augmento/hecate@67ffb1d0e39552ad31ec7231421deb670c469c00) ([merge request](augmento/hecate!272))
- [fix(types): :bug: correction of types in the module skeleton.py](augmento/hecate@91eb7fc6d3163f3cfe52cdc3b0d027c51ad54493) ([merge request](augmento/hecate!272))

## 3.7.2 (2023-11-01)

### fix (5 changes)

- [fix(price_reg): :bug: fix error with calculation outliers contains NaN values](augmento/hecate@e94ffc8cbecd4d7fcd148e55388c539a2ecec041) ([merge request](augmento/hecate!291))
- [fix(trend): :bug: :bug: round trend col](augmento/hecate@664b13c5bde4ba11c51e16d8f571a3d39da94218) ([merge request](augmento/hecate!291))
- [fix(trend): :bug: deleting nans only in required columns](augmento/hecate@d2f1cc2b702738fb9518fd03dbc4dbc6300daeb4) ([merge request](augmento/hecate!291))
- [fix(decorators): :bug: checking column types before changing](augmento/hecate@f9038996e4bb030511528cf852c5697b4561e471) ([merge request](augmento/hecate!291))

## 3.7.1 (2023-10-27)

### fix (2 changes)

- [fix: :ambulance: replace traefik image to docker-compose and delete it's dockerfile](augmento/hecate@033034b499f3cd982eff51bbc9cfdc573f4e53c7) ([merge request](augmento/hecate!290))
- [ci(docker): :green_heart: Dockerfile improvement](augmento/hecate@b67d3f7ad6d2eebc2e4df7fd8426fcbd14a4dd95) ([merge request](augmento/hecate!274))

## 3.7.0 (2023-10-20)

### feat (1 change)

- [feat: added ab functionality as a separate running mode of metriclog](augmento/hecate@4431addad2b1e747b2dce9f519a201b660448eb3) ([merge request](augmento/hecate!270))

## 3.6.2 (2023-10-17)

### fix (1 change)

- [fix(skeleton): :bug: drop misprint that prevented disabling Decompositor](augmento/hecate@c388f29e79cf7482a50e5b0c14e7e28d8823afd3) ([merge request](augmento/hecate!280))

## 3.6.1 (2023-10-16)

### fix (1 change)

- [fix(customization): :bug: modification of DummyPreprocessor for correct...](augmento/hecate@58ed4509fc3c846362b1a0c4cff7085c072f1932) ([merge request](augmento/hecate!278))

## 3.6.0 (2023-10-11)

### feat (1 change)

- [feat: add rolling forward for decimal part of forecast, when rounding is due...](augmento/hecate@e6cf6ea4cd78a3ff3e87ff5c987e304acdf41409) ([merge request](augmento/hecate!254))

## 3.5.0 (2023-09-19)

### feat (1 change)

- [feat: add model parameter base_score](augmento/hecate@a4f04f569e70d553c7c9483531e7cc75e1713b22) ([merge request](augmento/hecate!241))

## 3.4.0 (2023-09-18)

### feat (1 change)

- [feat: add ShortKeyForecastingPipeline for keys with short history which makes...](augmento/hecate@12180da04767c6a338a73c676047c78620d0ec2a) ([merge request](augmento/hecate!255))

## 3.3.1 (2023-09-18)

### fix (1 change)

- [fix: :bug: :bug: change logic for decompose price driver](augmento/hecate@4b37489e7828992ebf0003f4483c6e238fbee128) ([merge request](augmento/hecate!271))

## 3.3.0 (2023-08-29)

### feat (1 change)

- [feat: added label feature type for cols to be OHEd in the forecasting pipeline](augmento/hecate@74bafc15bdc5b6b858284c5b65cf2ecab2ff7dd0) ([merge request](augmento/hecate!268))

## 3.2.0 (2023-08-22)

### feat (1 change)

- [feat: time feature encoding as a separate fc pipeline step](augmento/hecate@2cc735c88f0909e4c1f017014dffd751d448d132) ([merge request](augmento/hecate!263))

## 3.1.1 (2023-08-21)

### fix (1 change)

- [fix: added horizon as a parameter of forecaster](augmento/hecate@e77cb2e90d4f554d56a8361d8578f7262ece5655) ([merge request](augmento/hecate!239))

## 3.1.0 (2023-08-17)

### feat (1 change)

- [ci(changelog): :green_heart: auto changelog via ci/cd pipeline stage](augmento/hecate@6ac769eb8cc8ea8f85dcae105a52a2af71f475c0) ([merge request](augmento/hecate!267))

## 3.0.0 (2023-08-17)

### Added
- AB test module
- Loki for logging in Grafana
- Poetry as a package manager
- Commit hooks (Conventional Commits)
- A decorator, checking data type of target and date columns

### Changed
- Logs are now stored locally in a directory defined by an environment variable
- Split service endpoint directory files by endpoint
- Split service swagger file according to function purpose

### Fixed
- Price decomposition bug
- Several bugs, resulting in runtime warnings


## 2.0.0 (2023-07-09)

### Added

- Week of month as a feature
- Seasonality detection
- Parameter for rounding hecate output numbers: added `decimals` parameter to `fc` and `fc_batch` functions to control number of digits for the output data
- `fc_only` parameter to service.app `/fc` and `fc_batch` handles: if set to True, output only returns forecast partition data
- Celery to manage task queue when receiving a URL request
- Domains, timings, additional data to dev_api_notebook
- Several tests for adequate hecate output

### Changed

- Metriclog client commenting: now more user-friendly and explanatory
- Cannibalization tests: now cannibalization tests are in a separate directory completely
- Trend calculation: made linear
- Typing: major changes concerning function type annotations

### Fixed

- Cannibalization logging: to not give too detailed output (comments moved to debug runs) + fixed several warnings of linalg package
- Metriclog bug, due to which fold 0 data wasn't saving locally
- Bugs when processing categorical input
- FeatureSet initialization
- Target data for running with HECATE_ENV set to 'dev'

## 1.3.0 (2023-06-02)

### Added

-  Added option to pass incoming columns to fc outputs. It was implemented for `/fc` and `/fc_batch` via adding `add_columns` parameter.

## 1.2.0 (2023-05-29)

### Added

-  Added rotation of web server log

### Fixed

- *Bugfix*: Fixed failure of creation long execution task.

### Changed

- split forecasting step of ForecastingTask to trigger model and decompositor as separate classes.
    - Model sub-step deals with forecasting pipeline and prediction of total header forecast. It also manages trend.
    - Decompositor step does demand driver decomposition based on inputs.

## 1.1.0 (23-05-26)

### Added

- Added end-points that implement logic to create long execution tasks.

## 1.0.1 (2023-05-03)

### Fixed

- *Bugfix*: isolate adding cannibalization as a feature: in case any failure during adding can-specific features will fill up zeros and a record will be made

## 1.0.0 (2023-04-07)

### Added

- Demand Drivers Decomposition

    Adds a possibility to define methods of decomposition to be applied for calculation of various drivers. Currently, only
    applied for baseline calculation, but may be in future extended to other drivers.

- _ModelAnswer_ now returns not only prediction, but the trained model as well.

- _development_ environment behavior
    Default environment for Hecate is _production_. To set up hecate environment to _development_ mode set up `HECATE_ENV` env variable as _dev_.
    In the _development_ mode, you will get a complete dataframe used for train (all features added) as an output of hecate.fc_batch.
    Moreover, some additional properties added to the _FCSampleResult_ class, including:

    - `total_from_partition`: sum of target and fc for _train_ and _fc_ partition
    - `metrics`: forecast accuracy and bias for _train_ and _fc_ partitions

- Customizing
    Was created tools to customize forecasting flow. Overview example placed into `hecate/notebooks/examples/customizing/overview.ipynb`

### Changed

- `processing_trend_wrapper` decorator extended
    `processing_trend_wrapper` decorator changed to be used not only for total header forecast calculation, but for
    baseline as well.

### Fixed

- *Issue*: in possible cases when one feature name is included in another feature name, promo columns for baseline
calculation got duplicated, resulting in run failures.
