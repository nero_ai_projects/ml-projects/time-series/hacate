import numpy as np
import pandas as pd
from os import environ

from hecate.log import get_logger
from hecate.forecasting.horizon import Horizon


logger = get_logger('hecate')


class Singleton(type):
    """
    Singleton class
    """

    _instances: dict = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class HorizonMixin:
    """
    Mixin class for horizon.
    """

    def __init__(self) -> None:
        self.horizon: Horizon = Horizon.UNDEFINED

    def set_horizon(self, horizon: Horizon = Horizon.UNDEFINED) -> None:
        """
        Set horizon with a value passed as an argument.
        """
        self.horizon = horizon


class EnvVar(metaclass=Singleton):
    """
    Singleton class to process environment variables.
    """

    @staticmethod
    def is_debug_environ():
        """
        Return True, if HECATE_ENV is set to 'dev' (hence, forecast metadata will be calculated), False - otherwise.
        """
        debug_env = environ.get('HECATE_ENV')
        if debug_env == 'dev':
            return True
        elif debug_env == 'prod':
            return False
        else:
            logger.warning(f'Unknown value {debug_env} for HECATE_ENV variable. Set to "prod" by default')
            return False


def mark_tiny_leading_values_non_relevant(sample: pd.DataFrame, percent_threshold: float = 0.5) -> pd.DataFrame:
    """
    The function catch tiny values in the start of the time series and set partition as "non_relevant".
    Algorithm of defining leading period as non_relevant based on analisys cumulative sum of target.
    Every point where cumsum less  percent_threshold will be marked as non_relevant
    :param sample: dataframe with data
    :param percent_threshold: vva
    :return: input dataframe with updated partition
    """
    is_train = sample.partition == "train"
    total = sample.loc[is_train, 'target'].sum()

    sample['cumsum_percent'] = np.where(is_train,
                                        100 * sample.target.cumsum() / total,
                                        100
                                        )
    sample['cumsum_threshold'] = np.where(sample.cumsum_percent < percent_threshold, 0, 100)

    if sample.loc[is_train, 'cumsum_threshold'].nunique() == 2:
        means_by_threshold = sample.loc[is_train, ['target', 'cumsum_threshold']] \
            .groupby('cumsum_threshold').agg({'target': 'mean'})
        mean_non_relevant, mean_others = means_by_threshold.target.values

        if mean_others > mean_non_relevant * 2:
            sample['partition'] = np.where(sample.cumsum_threshold == 0, 'non_relevant', sample.partition)

    del sample['cumsum_percent']
    del sample['cumsum_threshold']

    return sample


def get_week_of_month(dt_series: pd.Series) -> pd.Series:
    """
    Returns number of week in a month. Takes datetime formatted series as input.
    """
    return (dt_series.dt.day - 1) // 7 + 1


def get_half_year(dt_series: pd.Series) -> np.ndarray:
    """
    Returns number of a half year.
    """
    return np.where(dt_series.dt.quarter < 3, 1, 2)


def get_short_train_flag(data: pd.DataFrame) -> bool:
    """
    The function calculate train length.
    param: data: pd.DataFrame

    return: For short keys returns 'short', otherwise 'other'
    """

    length = np.count_nonzero(data.partition.values == 'train')
    short_train_flag: bool = True if length < 5 else False

    return short_train_flag
