"""
Functions for (1) calculations of confidence intervals, p-values and mean diference between two data arrays for various
distributions of data, (2) classification of distribution function of data arrays, (3) MDE calculation
"""

from functools import wraps
from math import ceil, floor, log10
from typing import Callable, ParamSpec, TypeVar

import numpy as np
import scipy.stats as stats

from ab.types import ConfidenceInterval, Distribution, MetricSequence, StatTestResult


def n_significant_digits(x: float,
                         n: int = 4) -> float:
    """
    Round a number to n significant digits

    :param x: a number to be rounded
    :param n: number of significant digits of the rounded number
    """

    if np.abs(x) < 1e-10:
        return 0.0
    elif np.abs(x) >= 1e-10:
        return round(x, n - int(floor(log10(abs(x)))) - 1)

    return np.nan


P = ParamSpec("P")
T = TypeVar("T", StatTestResult, ConfidenceInterval)


def round_decorator(func: Callable[P, T]) -> Callable[P, T]:
    @wraps(func)
    def wraper(*args: P.args, **kwargs: P.kwargs) -> T:

        res = func(*args, **kwargs)
        if "n" in kwargs and isinstance(kwargs["n"], int):
            n = kwargs["n"]
        else:
            raise ValueError("Parameter 'n' setting count of significant digits is not specified")
        res = res._replace(**{attr: n_significant_digits(val, n=n) for attr, val in res._asdict().items()})
        return res

    return wraper


@round_decorator
def effect_pvalue_norm(a: MetricSequence,
                       b: MetricSequence,
                       n: int = 4
                       ) -> StatTestResult:
    """
    calculates the difference of means (b - a), and p-value for two normally distributed datasets a and b
    H0: mean values of a and b are equal

    :param a: the control dataset
    :param b: the test dataset
    :param n: number of significant digits for the output values to be rounded to

    :return: effect - the difference of means of the input datasets
             p_value - p-value for the test of the equality of means
    """

    effect = float(np.mean(b) - np.mean(a))
    p_value = stats.ttest_ind(a, b, equal_var=False).pvalue

    return StatTestResult(effect, p_value)


@round_decorator
def ci_norm(a: MetricSequence,
            b: MetricSequence,
            ci_prob: float = 0.95,
            n: int = 4
            ) -> ConfidenceInterval:
    """
    calculates ci_prob*100% confidence interval for Student's T-test for equality of means of a and b sets.

    :param a: the control dataset
    :param b: the test dataset
    :param ci_prob: ci_prob*100% is the level of confidence
    :param n: number of significant digits for the output values to be rounded to
    """

    alpha = 1 - ci_prob
    scale = np.sqrt(np.std(a)**2 / len(a) + np.std(b)**2 / len(b))
    md = np.mean(b) - np.mean(a)

    return ConfidenceInterval(md - stats.norm.ppf(1 - alpha / 2) * scale,
                              md + stats.norm.ppf(1 - alpha / 2) * scale)


@round_decorator
def effect_pvalue_lognorm(a: MetricSequence,
                          b: MetricSequence,
                          n: int = 4
                          ) -> StatTestResult:
    """
    calculates the difference of means (b - a), and p-value for two log-normally distributed datasets a and b
    H0: mean values of a and b are equal

    For details see: https://sci-hub.ru/https://doi.org/10.2307/2533570 (Zhou, X.-H., Gao, S., & Hui, S. L. (1997).
    Methods for Comparing the Means of Two Independent Log-Normal Samples. Biometrics, 53(3), 1129. doi:10.2307/2533570)

    :param a: the control dataset
    :param b: the test dataset
    :param n: number of significant digits for the output values to be rounded to

    :return: effect - the difference of means of the input datasets
             p_value - p-value for the test of the equality of means
    """
    # TODO: see scipy.stats.lognorm manual
    sa = np.std(np.log(a))
    sb = np.std(np.log(b))
    z = (
        (np.mean(np.log(b)) - np.mean(np.log(a)) + 0.5 * (sb**2 - sa**2))
        /
        (sa**2 / len(a) + sb**2 / len(b) + 0.5 * (sa**4 / (len(a) - 1) + sb**4 / (len(b) - 1)))**0.5
    )
    effect = np.exp(np.mean(np.log(b)) + 0.5 * sb**2) - np.exp(np.mean(np.log(a)) + 0.5 * sa**2)
    p_value = stats.norm.sf(np.abs(z))

    return StatTestResult(effect, p_value)


def t_for_ci_lognorm(a: MetricSequence,
                     gen_size: int = 20_000
                     ) -> float:
    """
    Intermediate function for calculation of confidence interval for lognormal distribution via ci_lognorm().

    :param a: a data array
    :param gen_size: the number of generated random quantities required for the confidence interval calculation

    :return: intermediate value T according to ref. https://www.measurement.sk/2007/S1/Cimermanova.pdf
    """
    y = np.mean(np.log(a))
    s = np.std(np.log(a))
    z = stats.norm.rvs(size=gen_size)
    q = np.sqrt(stats.chi2.rvs(len(a) - 1, size=gen_size))
    t = y - s * (len(a) - 1)**0.5 * z / q / len(a)**0.5 + 0.5 * s**2 * (len(a) - 1)**0.5 / q

    return t


@round_decorator
def ci_lognorm(a: MetricSequence,
               b: MetricSequence,
               ci_prob: float = 0.95,
               n: int = 4,
               gen_size: int = 20000
               ) -> ConfidenceInterval:
    """
    calculates ci_prob * 100% confidence interval for two log-normally distributed datasets
    a is the control dataset
    b is the test dataset
    gen_size is the number of generated random quantities required for (1-ci_prob)/2 and
    (1 - (1-ci_prob)/2) quantiles calculation at the final step of the CI calculation

    For the details see: https://www.measurement.sk/2007/S1/Cimermanova.pdf

    :param a: the control dataset
    :param b: the test dataset
    :param ci_prob: ci_prob*100% is the level of confidence
    :param n: number of significant digits for the output values to be rounded to
    :param gen_size: the number of generated random quantities required for the confidence interval calculation

    :return: the confidence interval
    """
    # TODO: see scipy.stats.lognorm manual for alternative math
    alpha = 1 - ci_prob
    ta = t_for_ci_lognorm(a=a, gen_size=gen_size)
    tb = t_for_ci_lognorm(a=b, gen_size=gen_size)
    td = np.exp(tb) - np.exp(ta)

    return ConfidenceInterval(np.quantile(td, alpha / 2), np.quantile(td, 1 - alpha / 2))


@round_decorator
def effect_pvalue_bernoulli(a: MetricSequence,
                            b: MetricSequence,
                            n: int = 4
                            ) -> StatTestResult:
    """
    calculates the difference of means (b - a), and p-value for two binonial proportions a and b
    H0: mean values of a and b are equal

    :param a: the control dataset
    :param b: the test dataset
    :param n: number of significant digits for the output values to be rounded to
    :return: effect - the difference of means of the input datasets
             p_value - p-value for the test of the equality of means
    TODO: add a reference to a math article
    TODO: check scipy.stats for existing solutions (or somewhere else)
    """
    na = len(a)
    nb = len(b)
    pa = np.sum(a) / na
    pb = np.sum(b) / nb
    p_hat = (pa * na + pb * nb) / (na + nb)
    z_bin2s = (pb - pa) / (p_hat * (1 - p_hat) * (1 / na + 1 / nb))**0.5
    p_value = stats.norm.sf(np.abs(z_bin2s))
    effect = (np.sum(b) + 1) / (nb + 2) - (np.sum(a) + 1) / (na + 2)

    return StatTestResult(effect, p_value)


@round_decorator
def ci_bernoulli(a: MetricSequence,
                 b: MetricSequence,
                 n: int = 4,
                 ci_prob: float = 0.95
                 ) -> ConfidenceInterval:
    """
    calculates ci_prob * 100% confidence interval for two binomial proportions

    :param a: the control dataset
    :param b: the test dataset
    :param ci_prob: ci_prob*100% is the level of confidence
    :param n: number of significant digits for the output values to be rounded to

    :return: the confidence interval
    """
    alpha = 1 - ci_prob
    na = len(a) + 2
    nb = len(b) + 2
    pa = (np.sum(a) + 1) / na
    pb = (np.sum(b) + 1) / nb
    sqrt = (pa * (1 - pa) / na + pb * (1 - pb) / nb)**0.5

    return ConfidenceInterval(pb - pa - stats.norm.ppf(1 - alpha / 2) * sqrt,
                              pb - pa + stats.norm.ppf(1 - alpha / 2) * sqrt)


@round_decorator
def effect_pvalue_mannwhitney(a: MetricSequence,
                              b: MetricSequence,
                              n: int = 4
                              ) -> StatTestResult:
    """
    calculates the difference of medians (b - a), and p-value for the Mann-Whitney U-test of datasets a and b
    H0: median values of a and b are equal

    :param a: the control dataset
    :param b: the test dataset
    :param n: number of significant digits for the output values to be rounded to

    :return: effect - the difference of means of the input datasets
             p_value - p-value for the test of the equality of means
    """

    effect = float(np.median(b) - np.median(a))
    p_value = stats.mannwhitneyu(a, b).pvalue

    return StatTestResult(effect, p_value)


@round_decorator
def ci_mannwhitney(a: MetricSequence,
                   b: MetricSequence,
                   n: int = 4,
                   ci_prob: float = 0.95
                   ) -> ConfidenceInterval:
    """
    calculates CI_prob * 100% confidence interval for the difference between the medians
    of set a and set b

    :param a: the control dataset
    :param b: the test dataset
    :param ci_prob: ci_prob*100% is the level of confidence
    :param n: number of significant digits for the output values to be rounded to

    :return: the confidence interval
    """
    na = len(a)
    nb = len(b)
    alpha = 1 - ci_prob
    t = stats.norm.ppf(1 - alpha / 2)

    # The confidence interval for the difference between the two population
    # medians is derived through the (n x m) differences.
    diffs = sorted([j-i for i in a for j in b])

    # the Kth smallest to the Kth largest of the n x m differences then determine
    # the confidence interval, where K is:
    k = ceil(na * nb / 2 - (t * (na * nb * (na + nb + 1) / 12)**0.5))

    return ConfidenceInterval(diffs[k - 1], diffs[len(diffs) - k])


def distribution_check(metrics_a: MetricSequence,
                       metrics_b: MetricSequence,
                       alpha_dist: float = 0.01,
                       min_len_norm: int = 30
                       ) -> Distribution | None:
    """
    Determines the cumulative distribution function that describes both metrics.
    The distribution type must be the same for both metrics for correct performance of AB tests. If distributions are
    different, the output will be 'undefined' that causes Mahn-Whitney test, which is unsusceptible to distribution type

    :param alpha_dist: significance level for tests on metrics' cumulative distribution function
                       the default value is relatively small, since the AB tests for normal and lognormal distributions
                       are quite tolerant for deviations from normality and lognormality, correspondingly.
    :param min_len_norm: defines minimum metrics size required for normal, or lognormal distribution.

    :return: distribution type:
             'norm' - normal distribution, for Student's t-test
             'lognorm' - lognormal distribution, for Z-test for lognormally distributed data
             'bernoulli' - Bernoulli distribution, for Z-test for two binomial proportions
             'undefined' - some other distribution, for Mahn-Whitney U test
    """
    mean_a, std_a = np.mean(metrics_a), np.std(metrics_a)
    mean_b, std_b = np.mean(metrics_b), np.std(metrics_b)
    distribution = None

    if min(len(metrics_a), len(metrics_b)) == 0:
        raise ValueError('At least one of input Series is empty')

    # TODO: add a case, when a part of metrics values are 0, and others follow lognorm distribution
    if (min(std_a, std_b) > 0) and (min(len(metrics_a), len(metrics_b) >= min_len_norm)):
        kolm_a = stats.kstest((metrics_a - mean_a) / std_a, 'norm').pvalue
        kolm_b = stats.kstest((metrics_b - mean_b) / std_b, 'norm').pvalue
        if min(kolm_a, kolm_b) > alpha_dist:
            distribution = Distribution.NORM

            return distribution

        if min(min(np.abs(metrics_a)), min(np.abs(metrics_b))) > 1e-10:
            a = np.log(metrics_a)
            b = np.log(metrics_b)
            if min(np.std(a), np.std(b)) > 0:
                kolm_a_log = stats.kstest((a - np.mean(a)) / np.std(a), 'norm').pvalue
                kolm_b_log = stats.kstest((b - np.mean(b)) / np.std(b), 'norm').pvalue
                if min(kolm_a_log, kolm_b_log) > alpha_dist:
                    distribution = Distribution.LOGNORM

                    return distribution

        array_a = np.asarray(metrics_a)
        size_a_filtered = np.size(array_a[(array_a == 0) | (array_a == 1)])
        array_b = np.asarray(metrics_b)
        size_b_filtered = np.size(array_b[(array_b == 0) | (array_b == 1)])
        if (size_a_filtered == np.size(array_a)) & (size_b_filtered == np.size(array_b)):
            distribution = Distribution.BERNOULLI
        else:
            distribution = Distribution.UNDEFINED

    return distribution


def mde_norm(metrics_a: MetricSequence,
             metrics_b: MetricSequence,
             n: int = 4,
             alpha: float = 0.05,
             beta: float = 0.05) -> float:
    """
    estimation of MDE (minimum detectable effect) in consideration of normal distributions of
    both metrics_a and metrics_b and Student's t-test. For some other distributions it can serve as
    top estimate of MDE. For proper use read books on statistics.

    :param metrics_a: the older version of hecate forecast metrics_by_day
    :param metrics_b: the newer version of hecate forecast metrics_by_day to be compared with the older one
    :param n: number of significant digits for the output value to be rounded to
    :param alpha: AB test Type I Error (probability to omit significant difference of metrics_a and metrics_b)
    :param beta: AB test Type II Error (probability to assume insignificant diffirence of metrics_a and metrics_b
                    as significant one)

    :return: MDE
    """
    mean_a = np.mean(metrics_a)
    std_a = np.std(metrics_a)
    std_b = np.std(metrics_b)
    len_a = len(metrics_a)

    mde = (100 / mean_a * np.sqrt((std_a**2 + std_b**2) / len_a) *
           (stats.norm.ppf(q=1-alpha/2) + stats.norm.ppf(q=1-beta))**2)

    return n_significant_digits(mde, n=n)
