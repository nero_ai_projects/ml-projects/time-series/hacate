from fastapi import APIRouter, Request, Response, Depends
from fastapi.responses import FileResponse

from hecate.config import FAILED_DF_PATH, LOGS_APP_PATH
from hecate.log import get_logger, set_up_user_name
from service.utils import is_authorized


router = APIRouter()
can_docs = None
logger = get_logger("hecate", service_kind="app")


@router.get(
    "/log",
    description="End-point to download the latest application log",
)
async def get_log(
    request: Request,
    dependencies=Depends(is_authorized)
) -> FileResponse | Response:
    """
        Service end-point to download the latest application log
    """
    set_up_user_name(logger, dependencies)
    logger.info("got request on /service/log")

    return FileResponse(path=LOGS_APP_PATH, media_type='text/plain',
                        headers={'Content-Disposition': f"attachment; filename='{LOGS_APP_PATH.name}'"})


@router.get(
    "/failed_df",
    description="End-point to download the latest failed data if it exists",
    responses={404: {"description": "Failed df not found"}}
)
async def get_failed_dataframe(
    request: Request,
    dependencies=Depends(is_authorized)
) -> Response | FileResponse:
    """
        Service end-point to download the latest failed data if it exists
    """
    set_up_user_name(logger, dependencies)
    logger.info("got request on /service/failed_df")

    if FAILED_DF_PATH.exists():
        logger.info("failed df found")
        return FileResponse(path=FAILED_DF_PATH.as_posix(), media_type='application/octet-stream',
                            headers={'Content-Disposition': 'attachment; filename="failed_df.parquet"'},
                            )
    logger.info("failed_df.parquet does not exist")
    return Response(status_code=404)
