import json

from metriclog.constants import HORIZONS
from metriclog.database import Database
from metriclog import runner
from metriclog.types import HecateVersionsParams


def save_dict_locally(feature_types: dict, commit_id: str, branch: str, project: str, fold: int, horizon: HORIZONS,
                      output_dir: str = './outputs/'):
    """
    Function saves feature types dict as json

    :param feature_types: feature types for current project
    :param commit_id: last commit id in current branch
    :param branch: current branch
    :param project: current project
    :param fold: number of current fold
    :param horizon: current horizon
    :param output_dir: path to dir with output projects
    """
    output_project_dir = runner.create_output_folders(project, output_dir + '/')
    file_name = f"branch_{branch}_commit_{commit_id}_horizon_{horizon}_fold_{fold}.json"
    output_path = output_project_dir + file_name
    with open(output_path, 'w') as fp:
        json.dump(feature_types, fp)


def extract_dataset(password: str, db_ip: str = '185.130.115.92',
                    db_name: str = 'dev-data-and-reports-pg', user_name: str = 'hecate',
                    fold: int = 1, project: str = 'Wings', horizon: HORIZONS = 'st', focus: bool = False,
                    output_dir: str = './outputs/'):
    """
    Function extracts dataset from database and sets the train/fc partition.
    Folds are enumerated from the end. E.g. fold 1 is the last fold in time

    :param output_dir: path to directory for saving files
    :param password: password for database
    :param db_ip: database ip
    :param db_name: database name
    :param user_name: username for database
    :param fold: fold number. Fold 1 is the last fold in time
    :param project: desired project. can use all
    :param horizon: desired horizon. can use all
    :param focus: flag to use only most important products
    """
    db_connection = Database(password, db_ip, db_name, user_name)
    horizon_and_folds = db_connection.extract_horizon_and_folds()
    horizon_and_folds = runner.filter_project_horizon_and_fold(horizon_and_folds, project, horizon, n_folds=-1)
    for ind, row in horizon_and_folds[horizon_and_folds.fold == fold].iterrows():
        data = db_connection.extract_dataset(row.project, focus)
        feature_types = db_connection.extract_feature_types(row.project)
        data_partitioned = runner.set_train_test(data, fold, row.horizon, row.project, horizon_and_folds)
        hvp = HecateVersionsParams(project=row.project, output_dir=output_dir, fold=fold, focus=focus,
                                   horizon=row.horizon)
        runner.save_file_locally(data_partitioned, hvp, 'input')
        save_dict_locally(feature_types, '_', '_', row.project, fold, row.horizon, output_dir)
