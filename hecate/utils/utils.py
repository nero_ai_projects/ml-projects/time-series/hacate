from typing import Iterable, NamedTuple, Callable, final

import pandas as pd
import numpy as np

from hecate.log import get_logger

logger = get_logger('hecate')


def safe_div(x, y):
    """
    Division which would not throw a ZeroDivisionError.
    """
    if y == 0:
        if x == 0:
            return 1.0
        else:
            return np.inf
    return x / y


def remove_duplicates(input: Iterable) -> list:
    """
    Function to remove duplicates from a list while preserving order.
    """
    return list(dict.fromkeys(input))


def get_frequency(dates: pd.Series) -> str | None:
    """
    Function returns dates frequency. If it cannot be inferred using pd.infer_freq, 'D' is returned.

    :param dates: pd.Series of type dt.datetime- series with dates to infer frequency from

    :return: str representing data frequency ('D', 'W', 'MS')
    """
    try:
        freq = pd.infer_freq(dates.sort_values())
    except ValueError:
        freq = None
    if freq is None:
        logger.warning('could not infer frequency from the timeline, check dataset for missing dates')
    else:
        freq = freq.split('-')[0]

    return freq


@final
class FuncAndArgs(NamedTuple):
    """
    Class for storing function and its arguments.
    """
    func: Callable
    args: list = []
    kwargs: dict = {}


def datetime_converting(df: pd.DataFrame):
    """
    Function pandas dataframe time format converting.

    :param df: input dataframe
    """
    df['date'] = pd.to_datetime(df['date'])
    df['date'] = df['date'].dt.strftime('%Y-%m-%d')
    return df


def round_cols(df: pd.DataFrame, cols_to_round: Iterable, decimals: int | None) -> None:
    """
    Function rounds cols_to_round to a specified number of decimals.

    :param df: dataframe to apply changes to
    :param cols_to_round: a list of columns to round
    :param decimals: number of decimal places to round to

    :return: None, changes applied to the input dataframe
    """

    cols_to_round = [] if decimals is None else cols_to_round

    for col in df.columns.intersection(cols_to_round):
        df[col] = df[col].round(decimals)


def is_integer(value):
    """
    Function checks if number is integer
    :param value: number that is needed to check
    """
    return isinstance(value, int) or (isinstance(value, float) and value.is_integer())
