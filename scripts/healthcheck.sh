#!/bin/sh
curl -X 'POST' 'http://localhost/fc?payload_type=json&decompose=all&fc_only=false&decimals=2' \
-H 'accept: application/json' \
-H 'feature-types: {}' \
-H 'api_key: ${HEALTHCHECK_KEY}' \
-H 'Content-Type: application/json' \
-d '{
  "key": {"0": "key-1", "1": "key-1", "2": "key-1", "3": "key-1"},
  "date": {"0": "2022-07-04", "1": "2022-07-11", "2": "2022-07-18", "3": "2022-07-25"},
  "partition": {"0": "train", "1": "train", "2": "train", "3": "fc"},
  "target": {"0": 100.01, "1": 1, "2": 1, "3": 0}
}'
