from collections.abc import Callable

import numpy as np
import pandas as pd

from hecate.log import get_logger

logger = get_logger('hecate')


def is_seasonal_key(data):
    """
    Returns True, if forecasted period contains periods with seasonal component. NB! Seasonal component is added
    with the trend, if seasonality_flag not present in columns, returns False.
    """
    if 'seasonality_flag' not in data.columns:
        return False

    if data.loc[data.partition == 'fc', 'seasonality_flag'].sum():
        logger.debug('seasonal component in forecast partition, skipping target rolling')
        return True

    return False


def add_rolling(
        data: pd.DataFrame,
        weeks: int = 2,
        window: int = 7,
        discount_cols: list[str] | None = None
) -> pd.DataFrame:
    """
    The function generate target based feature: rolling on a few last weeks.
    This feature will be helpful only for short term forecasting.

    The rolling generation ignore outlier periods, periods with zero periods
    and discount periods.
    Outlier marks expected to catch from column 'outlier'. If this column
    is not present, it is suggested that data doesn't contain any
    outliers.
    Zero periods marks expected to catch from column 'zero_period'. If this
    column is not present, it is suggested that data doesn't contain any
    zero periods.

    :param data: pd.DataFrame
    :param weeks: week counts
    :param window: window size
    :param discount_cols: discount columns which define periods to ignore them
    :return: copy of input data with column 'rolling'
    """

    discount_cols = [] if discount_cols is None else discount_cols

    if is_seasonal_key(data):
        data['target-rolling'] = 0
        return data

    if 'target-zero_period' not in data.columns:
        data['target-zero_period'] = 0
    if 'target-outlier' not in data.columns:
        data['target-outlier'] = 0

    discount_indexer = np.any(data[discount_cols] != 0, axis=1)

    data['target-unusual_periods'] = \
        ((data['target-zero_period'] != 0) |
         (data['target-outlier'] != 0) | discount_indexer) & \
        (data.partition == 'train')

    data.sort_values('date', ascending=False, inplace=True)

    train_non_outliers_indexer = (data.partition == 'train') & ~data['target-unusual_periods']
    data['target-rolling'] = data['target'] \
        .where(train_non_outliers_indexer) \
        .groupby(data.date.dt.day_of_week.to_numpy()) \
        .transform(calculate_profile(weeks))

    data['target-rolling'] = np.where(data.partition == 'train', data['target'], data['target-rolling'])
    data['target-rolling'] = np.where(data['target-unusual_periods'], np.nan, data['target-rolling'])
    data.sort_values('date', inplace=True)
    data['target-rolling'] = data['target-rolling'].bfill().ffill()
    data['target-rolling'] = data['target-rolling'] \
        .rolling(window=window,
                 center=False,
                 win_type="exponential",
                 min_periods=1) \
        .mean(center=6, tau=7, sym=None)
    # need to test. was defined on wing pilot
    data.drop(columns=['target-unusual_periods'], inplace=True)
    data['target-rolling'].fillna(0, inplace=True)

    return data


def calculate_profile(weeks: int) -> Callable:

    def executor(series: pd.Series) -> np.ndarray:
        nonlocal weeks
        x = series.to_numpy()
        indices = ~np.isnan(x)
        return np.full(x.size, np.mean(x[indices][:weeks]), float)

    return executor
