import os
import gzip

from io import StringIO
from math import ceil
from pathlib import Path
from tempfile import TemporaryDirectory

import numpy as np
import pandas as pd
import psycopg2
from joblib import Parallel, delayed

from hecate.features import TypesOfFeatures
from hecate.log import get_logger
from metriclog.constants import MAIN_AB_HECATE_PARAMS, MAIN_HECATE_FC_PARAMS
from metriclog.types import HecateVersionsParams
from metriclog.utils import get_sql_filter

logger = get_logger('hecate')


class Database:
    def __init__(self, password: str, db_ip: str = '185.130.115.92', db_name: str = 'dev-data-and-reports-pg',
                 user_name: str = 'hecate'):
        self.conn = psycopg2.connect(f"host={db_ip} dbname={db_name} user={user_name} password={password}")
        self.cursor = self.conn.cursor()
        self.db_ip = db_ip
        self.db_name = db_name
        self.user_name = user_name
        self.password = password
        self.attempts_limit = 3

    @staticmethod
    def check_and_restore_connection(method):
        """
        Method checks if connection with database exists and restores if it was closed

        :param method: method to be run

        :return: method
        """
        def inner(*args, **kwargs):
            _self = args[0]
            if not hasattr(_self.conn, 'closed'):
                return method(*args, **kwargs)
            else:
                if _self.conn.closed != 0:
                    _self.conn = psycopg2.connect(
                        f"host={_self.db_ip} dbname={_self.db_name} user={_self.user_name} password={_self.password}")
                if _self.cursor.closed != 0:
                    _self.cursor = _self.conn.cursor()
                return method(*args, **kwargs)
        return inner

    @check_and_restore_connection
    def extract_project_names(self) -> list:
        """
        Method extracts list of available project names from database

        :return: list of project names
        """
        self.cursor.execute("select DISTINCT project from folds;")
        list_projects = self.cursor.fetchall()
        projects = [project[0] for project in list_projects]
        return projects

    @check_and_restore_connection
    def extract_horizons_list(self) -> list:
        """
        Method extracts list of available horizons from database

        :return: list of horizons
        """
        self.cursor.execute("select DISTINCT horizon from folds;")
        list_horizons = self.cursor.fetchall()
        horizons = [horizon[0] for horizon in list_horizons]
        return horizons

    def extract_dataset(self, project: str, focus: bool = False, chunk_size_nrows: int = 10**6) -> pd.DataFrame:
        """
        Method extracts dataset from database

        :param project: project name
        :param focus: flag to use only most important products

        :return: project dataset
        """
        selected_nrows = self.get_number_of_rows_from_table(project, focus)
        logger.info(f"Extracting dataset of project '{project}' with {selected_nrows} rows...")
        total_nrows = self.get_number_of_rows_from_table(project, False)
        with TemporaryDirectory() as tmpdir:
            Parallel(n_jobs=-1, backend='loky')(delayed(self.__extract_one_chunk)(**{
                'project': project, 'tmpdir': Path(tmpdir), 'chunk_size_nrows': chunk_size_nrows,
                'chunk_number': chunk_number, 'focus': focus, 'db_ip': self.db_ip, 'db_name': self.db_name,
                'user_name': self.user_name, 'password': self.password})
                for chunk_number in range(ceil(total_nrows / chunk_size_nrows))
            )
            logger.info("The dataset has been downloaded successfully. Chunks are being concatenated.")
            data = pd.read_parquet(tmpdir, engine='pyarrow')
        logger.info("The dataset extraction was successful!")
        return data

    @staticmethod
    def __extract_one_chunk(
            project: str, tmpdir: Path, chunk_size_nrows: int, chunk_number: int, focus: bool,
            db_ip: str, db_name: str, user_name: str, password: str) -> None:
        query = f"select * from {project} " \
            f"WHERE id > {chunk_size_nrows}*{chunk_number} AND id <= {chunk_size_nrows}*{chunk_number+1}" \
            f"{' AND focus = 1' if focus else ''} ORDER BY id ASC "
        with (
            psycopg2.connect(f"host={db_ip} dbname={db_name} user={user_name} password={password}") as conn,
            conn.cursor() as cursor,
            StringIO() as output
        ):
            cursor.copy_expert("COPY (%s) TO STDOUT (FORMAT 'csv', HEADER true)" % query, output)
            output.seek(0)
            pd.read_csv(output, engine='pyarrow') \
                .to_parquet(tmpdir / f"{chunk_number}.parquet", engine='pyarrow')

    @check_and_restore_connection
    def get_number_of_rows_from_table(self, table: str, focus: bool = False) -> int:
        query = "select count(*) from %s %s" % (table, "where focus = 1" if focus else "")

        self.cursor.execute(query)
        nrows = self.cursor.fetchall().pop()[0]
        return nrows

    @check_and_restore_connection
    def extract_horizon_and_folds(self) -> pd.DataFrame:
        """
        Method extract dataframe of dates for horizons and folds from database

        :return: dataframe of dates for horizons and folds from database
        """
        query = "SELECT * FROM folds;"
        self.cursor.execute(query)
        horizon_and_folds = self.cursor.fetchall()
        column_names = [desc[0] for desc in self.cursor.description]
        horizon_and_folds = pd.DataFrame(horizon_and_folds, columns=column_names)
        return horizon_and_folds

    @check_and_restore_connection
    def extract_feature_types(self, project: str) -> TypesOfFeatures:
        """
        Method extract dict with features used in projects and their types
        :param project: project name

        :return: dict of features and their types
        """
        query = "SELECT feature, type FROM feature_types where project = '" + project + "';"
        self.cursor.execute(query)
        tuple_feature_types = self.cursor.fetchall()
        feature_types = dict(tuple_feature_types)
        return feature_types

    @check_and_restore_connection
    def send_chunk(self, df: pd.DataFrame, table: str) -> bool:
        """
        Method loads tuples made of input dataframe rows as a batch to the database

        :param df: dataframe to load
        :param table: name of table where the dataframe should be put

        :return: True if the load failed
        """
        tuples = [tuple(x) for x in df.to_numpy()]
        cols = ','.join(list(df.columns))
        n_cols = df.shape[1]
        query_start = "INSERT INTO %s(%s) VALUES(%%s" + ",%%s" * (n_cols - 1) + ");"
        query = query_start % (table, cols)
        failed = False
        for attempt in range(self.attempts_limit):
            failed = False
            try:
                self.cursor.executemany(query, tuples)
                self.conn.commit()
            except (Exception, psycopg2.DatabaseError):
                failed = True
                self.conn.rollback()
            else:
                break
        return failed

    @staticmethod
    def send_chunk_in_parallel(
            db_ip: str,
            db_name: str,
            user_name: str,
            password: str,
            table: str,
            columns: list[str],
            chunk_number: int,
            tmpdir: Path,
            attempts_limit: int
    ) -> bool:
        """
        Method loads tuples made of input dataframe rows as a batch to the database.
        It creates a separate connection and chunk

        :param df: dataframe to load
        :param table: name of table where the dataframe should be put

        :return: True if the load failed
        """
        failed = False
        with (
            psycopg2.connect(f"host={db_ip} dbname={db_name} user={user_name} password={password}") as conn,
            conn.cursor() as cursor,
            gzip.open(tmpdir / f"{chunk_number}.csv", "rb") as output,
        ):
            for attempt in range(attempts_limit):
                failed = False
                try:
                    cursor.copy_from(output, table=table, sep="|", columns=columns, null="")
                    conn.commit()
                except (Exception, psycopg2.DatabaseError) as e:
                    failed = True
                    conn.rollback()
                    logger.warning(f"Sending the table {table} with the chunk number {chunk_number} "
                                   f"failed with error {e} ({attempt=})")
                except KeyboardInterrupt:
                    logger.warning(f"Sending the table {table} with the chunk number {chunk_number} "
                                   "stopped with KeyboardInterrupt")
                    failed = True
                    conn.rollback()
                    break
                else:
                    break
        return failed

    def send_df(self, df: pd.DataFrame, table: str, n_rows: int = 1000) -> bool:
        """
        Method loads a dataframe to the database in chunks

        :param df: dataframe to load
        :param table: name of table where the dataframe should be put
        :param n_rows: number of rows in one chunk

        :return: True if the load failed for at least one chunk
        """
        if df.shape[0] < n_rows:
            n_rows = df.shape[0]
        n_chunks = round(df.shape[0] / n_rows)
        list_df = np.array_split(df, n_chunks)
        for chunk in list_df:
            failed = self.send_chunk(chunk, table)
            if failed:
                return failed
        self.conn.commit()

    def send_df_in_parallel(
            self, df: pd.DataFrame,
            table: str,
            hecate_versions_params: HecateVersionsParams,
            chunk_size_nrows: int = 5*10**5,
    ) -> bool | None:

        """
        Method loads a dataframe to the database in chunks in parallel

        :param df: dataframe to load
        :param table: name of table where the dataframe should be put
        :param hecate_versions_params: hecate version and parameters of interest (branch, commit_id, fold,
                                       focus, horizon, n_cores)
        :param n_rows: number of rows in one chunk

        :return: True if the load failed for at least one chunk
        """
        if df.shape[0] < chunk_size_nrows:
            chunk_size_nrows = df.shape[0]
        n_chunks = ceil(df.shape[0] / chunk_size_nrows)
        logger.info(f"Sending {n_chunks} chunks in parallel")
        n_jobs = hecate_versions_params.n_cores if hecate_versions_params.n_cores is not None else os.cpu_count()
        with TemporaryDirectory() as tmpdir:
            Parallel(n_jobs=n_jobs)(delayed(self.__save_one_chunk)(
                Path(tmpdir), df[np.arange(df.shape[0]) // chunk_size_nrows == chunk_number], chunk_number)
                for chunk_number in range(n_chunks))
            fails = Parallel(n_jobs=n_jobs)(
                delayed(self.send_chunk_in_parallel)(**{
                    "db_ip": self.db_ip,
                    "db_name": self.db_name,
                    "user_name": self.user_name,
                    "password": self.password,
                    "table": table,
                    "columns": df.columns.to_list(),
                    "chunk_number": chunk_number,
                    "tmpdir": Path(tmpdir),
                    "attempts_limit": self.attempts_limit,
                }) for chunk_number in range(n_chunks))
        if fails is not None and any(fails):
            logger.warning('Failed for some chunks, while sending chunks in parallel')
            self.delete_results_if_exist(hecate_versions_params, table)
            return True

    @staticmethod
    def __save_one_chunk(tmpdir: Path, df_chunk: pd.DataFrame, chunk_number: int) -> None:
        df_chunk.to_csv(tmpdir / f"{chunk_number}.csv", sep="|", header=False, index=False, compression='gzip')

    @check_and_restore_connection
    def create_result_table(self, table: str):
        """
        Method creates result table in database

        :param table: desired table name

        :return: None
        """
        sql_request = "CREATE TABLE IF NOT EXISTS " + table + \
                      " (" \
                      "key VARCHAR ( 30 ) NOT NULL, " \
                      "date date NOT NULL, " \
                      "fc FLOAT4 NOT NULL, " \
                      "target FLOAT4 NOT NULL, " \
                      "focus boolean NOT NULL, " \
                      "fold smallint NOT NULL, " \
                      "horizon VARCHAR NOT NULL, " \
                      "commit_id VARCHAR NOT NULL, " \
                      "branch VARCHAR NOT NULL, " \
                      "product_group VARCHAR NOT NULL, " \
                      "product_category VARCHAR NOT NULL, " \
                      "chain_of_stores VARCHAR NOT NULL, " \
                      "partition VARCHAR NOT NULL, " \
                      "project PROJECT NOT NULL);"
        self.cursor.execute(sql_request)
        self.conn.commit()

    @check_and_restore_connection
    def create_metrics_table(self):
        """
        Method creates metrics table in database

        :return: None
        """
        sql_request = "CREATE TABLE IF NOT EXISTS Metrics" \
                      " (" \
                      "project PROJECT NOT NULL," \
                      "result_table VARCHAR NOT NULL," \
                      "fold smallint NOT NULL, " \
                      "horizon VARCHAR NOT NULL, " \
                      "commit_id VARCHAR NOT NULL, " \
                      "branch VARCHAR NOT NULL," \
                      "FA FLOAT4 NOT NULL, " \
                      "FB FLOAT4 NOT NULL);"
        self.cursor.execute(sql_request)
        self.conn.commit()

    @check_and_restore_connection
    def index_results_table(self, table: str):
        """
        Method makes index of the result table based on unique id (key + date + run info)

        :param table: name of table to index

        :return: None
        """
        self.cursor.execute(f"CREATE INDEX IF NOT EXISTS result_id ON {table} (key, date, focus, fold, horizon, "
                            f"commit_id, branch)")
        self.conn.commit()

    @check_and_restore_connection
    def index_metrics_table(self):
        """
        Method makes index of the metrics table based on unique id (run info)

        :return: None
        """
        self.cursor.execute("CREATE INDEX IF NOT EXISTS metrics_id ON Metrics (project, fold, horizon, "
                            "commit_id, branch)")
        self.conn.commit()

    @check_and_restore_connection
    def delete_results_if_exist(self, hecate_versions_params: HecateVersionsParams, table: str):
        """
        Method checks if the results already present in database by unique id (key + date + run info)
        and deletes if found

        :param hecate_versions_params: hecate version and parameters of interest (branch, commit_id, fold,
                                       focus, horizon)
        :param table: table name in database

        :return: None
        """
        string = get_sql_filter(hecate_versions_params, MAIN_HECATE_FC_PARAMS)
        self.cursor.execute(f"DELETE FROM {table} WHERE {string};")
        self.conn.commit()

    @check_and_restore_connection
    def delete_metrics_results_if_exist(self, hecate_versions_params: HecateVersionsParams):
        """
        Method checks if the metrics results already present in database by unique id (run info)
        and deletes if found

        :param hecate_versions_params: hecate version and parameters of interest (branch, commit_id, project, fold,
                                       horizon)

        :return: None
        """
        string = get_sql_filter(
            hecate_versions_params,
            req_params=MAIN_HECATE_FC_PARAMS,
            excl_params=['focus'],
            add_params=['project']
        )
        self.cursor.execute(f"DELETE FROM Metrics WHERE {string};")
        self.conn.commit()

    def send_results_to_db(self, results: pd.DataFrame, hecate_versions_params: HecateVersionsParams) -> bool:
        """
        Method loads result table into database into table called results_'project_name'.
        The table is indexed by unique id (key + date + run info)
        If the results are already present they will be overwritten

        :param results: dataframe to load into database
        :param hecate_versions_params: hecate version and parameters of interest (branch, commit_id, project, fold,
                                       focus, horizon, n_cores)

        :return: True if the load failed
        """
        table = 'results_' + hecate_versions_params.project.lower()
        self.create_result_table(table)
        self.index_results_table(table)
        self.delete_results_if_exist(hecate_versions_params, table)
        if hecate_versions_params.n_cores == 1:
            error = self.send_df(results, table)
        else:
            error = self.send_df_in_parallel(results, table, hecate_versions_params)
        return error

    def send_metrics_to_db(self, metrics_table: pd.DataFrame, hecate_versions_params: HecateVersionsParams) -> bool:
        """
        Method loads result table into database into table called results_'project_name'.
        The table is indexed by unique id (key + date + run info)
        If the results are already present they will be overwritten

        :param metrics_table: dataframe with metrics of current run
        :param hecate_versions_params: hecate version and parameters of interest (branch, commit_id, project, fold,
                                       horizon)

        :return: True if the load failed
        """
        self.create_metrics_table()
        self.index_metrics_table()
        self.delete_metrics_results_if_exist(hecate_versions_params)
        error = self.send_df(metrics_table, 'Metrics', 1)
        return error

    def __del__(self):
        self.cursor.close()
        self.conn.close()

    @check_and_restore_connection
    def select_results_if_exist(self, hecate_versions_params: HecateVersionsParams, table: str):
        """
        Method checks if the results already present in database by unique id (key + date + run info)
        and deletes if found

        :param hecate_versions_params: hecate version and parameters of interest (branch, commit_id, fold,
                                       focus, horizon)
        :param table: table name in database

        :return: None
        """
        string = get_sql_filter(hecate_versions_params, MAIN_HECATE_FC_PARAMS)
        self.cursor.execute(f"SELECT * FROM {table} WHERE {string};")
        data = self.cursor.fetchall()
        column_names = [desc[0] for desc in self.cursor.description]
        data = pd.DataFrame(data, columns=column_names)
        return data

    @check_and_restore_connection
    def create_ab_result_table(self, table: str):
        """
        Method creates AB result table in database
        :param table: desired table name

        :return: None
        """
        sql_request = "CREATE TABLE IF NOT EXISTS " + table + \
                      " (" \
                      "project VARCHAR NOT NULL, " \
                      "horizon VARCHAR NOT NULL, " \
                      "focus boolean NOT NULL, " \
                      "fold smallint NOT NULL, " \
                      "commit_a_id VARCHAR NOT NULL, " \
                      "branch_a VARCHAR NOT NULL, " \
                      "commit_b_id VARCHAR NOT NULL, " \
                      "branch_b VARCHAR NOT NULL, " \
                      "key VARCHAR NOT NULL, " \
                      "result VARCHAR NOT NULL, " \
                      "effect_abs FLOAT4 NOT NULL, " \
                      "effect_percent FLOAT4 NOT NULL, " \
                      "ci_abs_lower FLOAT4 NOT NULL, " \
                      "ci_abs_upper FLOAT4 NOT NULL, " \
                      "ci_percent_lower FLOAT4 NOT NULL, " \
                      "ci_percent_upper FLOAT4 NOT NULL, " \
                      "distribution VARCHAR NOT NULL, " \
                      "mde FLOAT4 NOT NULL, " \
                      "weight FLOAT4 NOT NULL);"
        self.cursor.execute(sql_request)
        self.conn.commit()

    @check_and_restore_connection
    def create_ab_summary_table(self, table: str):
        """
        Method creates AB summary table in database
        :param table: desired table name

        :return: None
        """
        sql_request = "CREATE TABLE IF NOT EXISTS " + table + \
                      " (" \
                      "project VARCHAR NOT NULL, " \
                      "horizon VARCHAR NOT NULL, " \
                      "focus boolean NOT NULL, " \
                      "fold smallint NOT NULL, " \
                      "commit_a_id VARCHAR NOT NULL, " \
                      "branch_a VARCHAR NOT NULL, " \
                      "commit_b_id VARCHAR NOT NULL, " \
                      "branch_b VARCHAR NOT NULL, " \
                      "result VARCHAR NOT NULL, " \
                      "effect_abs FLOAT4 NOT NULL, " \
                      "effect_percent FLOAT4 NOT NULL, " \
                      "ci_abs_lower FLOAT4 NOT NULL, " \
                      "ci_abs_upper FLOAT4 NOT NULL, " \
                      "ci_percent_lower FLOAT4 NOT NULL, " \
                      "ci_percent_upper FLOAT4 NOT NULL, " \
                      "distribution VARCHAR NOT NULL, " \
                      "mde FLOAT4 NOT NULL);"
        self.cursor.execute(sql_request)
        self.conn.commit()

    @check_and_restore_connection
    def select_ab_results_if_exist(self, hecate_versions_params: HecateVersionsParams, table: str) -> pd.DataFrame:
        """
        Method checks if the AB test results already present in database by unique id (key + date + run info)

        :param hecate_versions_params: hecate versions and parameters of interest
        :param table: table name in the database

        :return: AB test results table
        """
        string = get_sql_filter(hecate_versions_params, MAIN_AB_HECATE_PARAMS)
        self.cursor.execute(f"SELECT * FROM {table} WHERE {string};")
        return pd.DataFrame(self.cursor.fetchall(), columns=[desc[0] for desc in self.cursor.description])

    def select_ab_summary_if_exist(self, hecate_versions_params: HecateVersionsParams) -> pd.DataFrame:
        """
        Checks if a summary of AB test with the given parameters already exist in the database

        :param hecate_versions_params: hecate versions and parameters
        :param db_connection: parameters of connection to a database (password, db_ip, db_name, user_name)

        :return: True if the summary exists
        """

        string = get_sql_filter(hecate_versions_params, MAIN_AB_HECATE_PARAMS, add_params=['project'])
        self.cursor.execute(f"SELECT * FROM ab_summary WHERE {string};")
        return pd.DataFrame(self.cursor.fetchall(), columns=[desc[0] for desc in self.cursor.description])

    @check_and_restore_connection
    def send_ab_results_to_db(self, ab_results: pd.DataFrame, hecate_versions_params: HecateVersionsParams,
                              rewrite_ab_results: bool = False):
        """
        Method loads AB test result table into database into table called ab_results_'project_name'.

        :param ab_results: the AB test result table to be loaded
        :param hecate_versions_params: hecate versions and parameters of interest. When sending AB summary table, need
                                       to set hecate_versions_params.project = 'ab_summary'
        :param rewrite_ab_results: If True,  the results are already present they will be overwritten
        """

        project = hecate_versions_params.project.lower()

        if project == 'ab_summary':
            table = project
            self.create_ab_summary_table(table)
        else:
            table = 'ab_results_' + project
            self.create_ab_result_table(table)

        if not self.select_ab_results_if_exist(hecate_versions_params, table).empty:
            if rewrite_ab_results:
                string = get_sql_filter(hecate_versions_params, MAIN_AB_HECATE_PARAMS)
                self.cursor.execute(f"DELETE FROM {table} WHERE " + string + ";")
            else:
                raise ValueError("Cannot send AB results: they already exist; 'rewrite_ab_results' must be True then")
        return self.send_df(ab_results, table)
