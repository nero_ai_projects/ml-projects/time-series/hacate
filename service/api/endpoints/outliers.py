from fastapi import APIRouter, Depends, Query, Request, Response

from hecate.forecasting.outliers.outlier import add_target_outliers
from hecate.log import get_logger, set_up_user_name
from hecate.utils.utils import datetime_converting
from hecate.validations import quality

from service.swagger.example import Examples
from service.swagger.request import SwaggerRequest
from service.swagger.schemas import SwaggerSchemas
from service.swagger.response import SwaggerResponse
from service.utils import is_authorized, body_to_df, build_outcome_content

router = APIRouter()
examples = Examples()
reqs = SwaggerRequest()
schemas = SwaggerSchemas()
response = SwaggerResponse()

logger = get_logger("hecate", service_kind="app")


@router.post('/target',
             description='End-point to add columns with target outliers marks',
             openapi_extra=reqs.target_outliers_openapi_extra,
             responses=response.outliers_target,
             )
async def get_target_outliers(request: Request,
                              payload_type: str = Query(default="parquet", examples=examples.payload_type_examples),
                              dependencies=Depends(is_authorized)
                              ) -> Response:
    """
    Route to add target outliers marks.
    Any features don't have any impact.

    :param **payload_type**: type of request data ["parquet", "json", "csv"]\n

    """
    set_up_user_name(logger, dependencies)
    logger.info("outliers/target")
    body = await request.body()
    try:
        data = body_to_df(body, payload_type)
    except Exception as e:
        logger.exception('df building from request body failed:')
        return Response(content=str(e), status_code=400)

    missed_columns = quality.get_missed_columns(data, {})
    if missed_columns:
        logger.info(f"Columns in data: {data.columns}, feature types: (), missed: {missed_columns}")
        return Response(status_code=400, content=f"missed columns: {missed_columns}")

    try:
        result = add_target_outliers(data)
        result = datetime_converting(result)
    except Exception:
        logger.exception('target outliers failed: ')
        raise

    outcome_content = build_outcome_content(result, payload_type)

    logger.info("outliers/target is done")

    return Response(content=outcome_content,
                    media_type=schemas.payload_type[payload_type])
