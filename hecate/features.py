"""
A module containing the logic of working with the feature_types parameter. Also a class for storing feature details.
"""

from enum import Enum
from typing import Sequence, final


@final
class FeatureType(str, Enum):
    """
    Class for storing possible values of feature types dictionary.
    """
    desc: str

    def __new__(cls, name: str, desc: str):
        if not isinstance(name, str) or not isinstance(desc, str):
            raise TypeError(f"type of params {name=} or {desc=} is not str!")

        obj = str.__new__(cls, name)
        obj._value_ = name
        obj.desc = desc
        return obj

    can_level = ("can_level", "name of a column containing product level inside which to search for cannibalization")
    discount = ("discount", "info about promo discount (it is also used to generate auxiliary promo features)")
    promo = ("promo", "binary mark (0, 1) for non-price promo without discount (e.g. promo stands, gifts, etc.)")
    stock = ("stock", "info about stocks (zero values are used for the Out-Off-Stock feature)")
    promo_budget = ("promo_budget", "info about promo in volume (to generate a promo profile feature)")
    price_reg = ("price_reg", "regular price")
    label = ("label", 'any textual information. For example, promo mechanics: disco, stand etc. OHE will be applied.')
    dim = ("dim", "descriptive information (not used for forecasting)")
    stop = ("stop", "making up a zero forecast and chained columns(bl, uplift, etc) if stop is 1")

    @staticmethod
    def doc() -> str:
        return ''.join([f"<li>{ftype} - {ftype.desc}</li>" for ftype in FeatureType])


TypesOfFeatures = dict[str, FeatureType]


def get_cols_from_types(feature_types: TypesOfFeatures, types: Sequence[FeatureType]) -> list[str]:
    """
    Function to retrieve column names of specified feature types.

    :param feature_types: dictionary of feature types provided by a user
    :param types: types of features to collect

    :return: column names of dataframe with the selected feature types
    """
    return [col for col, feature_type in feature_types.items() if feature_type in types]


@final
class FeatureSet:
    """
    Class for collecting information about features during forecasting.
    """

    def __init__(self, feature_types: TypesOfFeatures | None = None, features: list[str] | None = None,
                 added_business_features: list[str] | None = None, time_features: list[str] | None = None,
                 added_target_features: list[str] | None = None, period_features: list[str] | None = None,
                 selected_features: list[str] | None = None, promo_features: list[str] | None = None,
                 label_features: list[str] | None = None):

        self.feature_types = feature_types if feature_types is not None else dict()
        self.features = features if features is not None else list()
        self.added_business_features = added_business_features if added_business_features is not None else list()
        self.time_features = time_features if time_features is not None else list()
        self.added_target_features = added_target_features if added_target_features is not None else list()
        self.period_features = period_features if period_features is not None else list()
        self.selected_features = selected_features if selected_features is not None else list()
        self.promo_features = promo_features if promo_features is not None else list()
        self.label_features = label_features if label_features is not None else list()
