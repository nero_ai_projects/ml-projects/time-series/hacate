from pydantic.schema import schema

from service.swagger.schemas import JsonData, ParquetData
from service.swagger.description import SwaggerDescription

description = SwaggerDescription()
top_level_schema = schema([JsonData, ParquetData], title='Request Data Schema')


class SwaggerTagsMetadata:
    tags_metadata = [
        {
            "name": "Forecast",
            "description": "Forecast Operation for binary data of pandas DataFrame",
        },
        {
            "name": "Cannibalization",
            "description": "Get cannibalizator pairs from binary data of pandas DataFrame"
        }
    ]
