from hecate.forecasting.models.utils import ModelAnswer
from hecate.forecasting.horizon import Horizon


class DummyForecastingMachine:
    """
    Dummy forecasting pipeline, returning zero forecast.
    """

    def __init__(self, horizon: Horizon = Horizon.UNDEFINED):
        self.horizon = horizon

    @staticmethod
    def forecast(*args, **kwargs) -> ModelAnswer:
        """
        Fit method for dummy class
        """
        raise NotImplementedError("Disabling the model is not available.")
