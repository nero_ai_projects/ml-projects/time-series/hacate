from enum import Enum
from typing import final

import pandas as pd


@final
class Horizon(str, Enum):
    ST = "st"
    MT = "mt"
    LT = "lt"
    UNDEFINED = "undefined"


def get_horizon_from_sample(sample: pd.DataFrame) -> Horizon:
    """
    Function analyze sample partition to understand how long fc horizon.
    If horizon shorter or equal 6 weeks 'st' will be returned.
    If horizon longer st but shorter or equal half year (27 weeks) 'mt' will be returned.
    For other cases 'lt' will be returned.

    :param sample: pd.Dataframe with 'date' and 'partition' columns
    :return: string: 'st'|'mt'|'lt'
    """

    dates = sample.loc[sample.partition == "fc", 'date']
    first_fc_data, last_fc_data = dates.min(), dates.max()
    days_horizon = (last_fc_data - first_fc_data).days

    st_horizon_in_days = 7 * 6
    mt_horizon_in_days = 7 * 27  # half year

    if days_horizon <= st_horizon_in_days:
        horizon = Horizon.ST
    elif days_horizon <= mt_horizon_in_days:
        horizon = Horizon.MT
    else:
        horizon = Horizon.LT

    return horizon
