from service.swagger.description import SwaggerDescription
from service.swagger.schemas import JsonData, ParquetData, CSVData

description = SwaggerDescription()


class SwaggerRequest:
    fc_openapi_extra = {
        "header": {
            "accept": "application/gzip"
        },
        "requestBody": {
            "content": {
                "application/json": {
                    "schema": JsonData.schema(),
                    "examples": {
                        "json_1": {
                            "summary": "Json request data type example",
                            "description": description.example_desc.format(
                                "<p><span></span>: required columns: " +
                                "['key', 'date', 'partition', 'target']</p>" +
                                "<li>key - id временного ряда</li>" +
                                "<li>date - дата в формате 'YYYY-MM-dd'</li>" +
                                "<li>partition - разметка временного ряда" +
                                "<ul><li>'train' - данные для обучения модели</li>" +
                                "<li>'fc' - данные для построения прогноза</li></ul></li>" +
                                "<li>target - наша величина, которую нам нужно прогнозировать. " +
                                "Например, отгрузки</li>" +
                                "<p></p><p><b>Замечания:</b></p><p>* В данных не должно быть пропусков:</p>" +
                                "<p>Например, у нас не было отгрузок 1 января. " +
                                "В этом случае ожидается, что придет отгрузка равная нулю.</p>" +
                                "<p>* Даты должны быть сортированы по возрастанию.</p>" +
                                "<p>* Прогноз строится на все точки, где partition указан 'fc'</p>"
                            ),
                            "value": JsonData.Config().schema_extra["examples"][0],
                        },
                        "json_2": {
                            "summary": "Json request data with discount",
                            "description": description.example_desc.format(
                                "<p><span></span>: required columns: " +
                                "['key', 'date', 'partition', 'target', 'discount_promo']</p>" +
                                "<li>key - id временного ряда</li>" +
                                "<li>date - дата в формате 'YYYY-MM-dd'</li>" +
                                "<li>partition - разметка временного ряда</li>" +
                                "<li>'const' - колонка константного типа</li>" +
                                "<li>'discount_promo' - данные о скидках в процентах</li>" +
                                "<li>target - наша величина, которую нам нужно прогнозировать.</li>"
                            ),
                            "value": JsonData.Config().schema_extra["examples"][1],
                        },
                        "json_3": {
                            "summary": "Json request data with discount&stock",
                            "description": description.example_desc.format(
                                "<p><span></span>: required columns: " +
                                "['key', 'date', 'partition', 'target', 'stock', 'discount_promo']</p>" +
                                "<li>key - id временного ряда</li>" +
                                "<li>date - дата в формате 'YYYY-MM-dd'</li>" +
                                "<li>partition - разметка временного ряда</li>" +
                                "<li>'stock' - колонка stock</li>" +
                                "<li>'discount_promo' - данные о скидках в процентах</li>" +
                                "<li>target - наша величина, которую нам нужно прогнозировать.</li>"
                            ),
                            "value": JsonData.Config().schema_extra["examples"][2],
                        }
                    },
                },
                "application/gzip": {
                    "schema": ParquetData.schema(),
                    "examples": {
                        "parquet": {
                            "summary": "Parquet request data type example",
                            "description": "Parquet request data type example. " +
                                           "Use this example, when payload_type = 'parquet'",
                            "value": str(JsonData.Config().schema_extra["examples"][0])
                        },
                    },
                },
            },
            "required": True,
        },
    }

    act_decomposition_openapi_extra = {
        "header": {
            "accept": "application/gzip"
        },
        "requestBody": {
            "content": {
                "application/json": {
                    "schema": JsonData.schema(),
                    "examples": {
                        "json_1": {
                            "summary": "Json request example for actuals decomposition",
                            "description": description.example_desc.format(
                                "<p><span></span>: required columns: " +
                                "['key', 'date', 'partition', 'target']</p>" +
                                "<li>key - id временного ряда</li>" +
                                "<li>date - дата в формате 'YYYY-MM-dd'</li>" +
                                "<li>partition - разметка временного ряда" +
                                "<ul><li>'train' - данные для обучения модели</li>" +
                                "<li>'decompose' - данные для декомпозиции</li></ul></li>" +
                                "<li>target - величина, которую нужно декомпозировать. " +
                                "Например, отгрузки</li>" +
                                "<p></p><p><b>Замечания:</b></p><p>* В данных не должно быть пропусков:</p>" +
                                "<p>Например, у нас не было отгрузок 1 января. " +
                                "В этом случае ожидается, что придет отгрузка равная нулю.</p>" +
                                "<p>* В переданных feature_types обязательно должны быть указаны типы для " +
                                "декомпозиции. Например, если ни один feature_type, ассоциированный с промо, не " +
                                "указан, hecate не сможет сделать декомпозицию на baseline и uplift.</p>"
                                "<p>* Если передан пустой словарь feature_types, код упадет с ошибкой 400.</p>"
                                "<p>* Даты должны быть сортированы по возрастанию.</p>" +
                                "<p>* Декомпозиция строится на все точки, где partition указан 'decompose'.</p>" +
                                "<p>* Данные возвращаются только для точек, где partition указан 'decompose'.</p>"
                            ),
                            "value": JsonData.Config().schema_extra["examples"][3],
                        }
                    },
                },
                "application/gzip": {
                    "schema": ParquetData.schema(),
                    "examples": {
                        "parquet": {
                            "summary": "Parquet request data type example",
                            "description": "Parquet request data type example. " +
                                           "Use this example, when payload_type = 'parquet'",
                            "value": str(JsonData.Config().schema_extra["examples"][0])
                        },
                    },
                },
            },
            "required": True,
        },
    }

    target_outliers_openapi_extra = {
        "header": {
            "accept": "application/gzip"
        },
        "requestBody": {
            "content": {
                "application/json": {
                    "schema": JsonData.schema(),
                    "examples": {
                        "json_1": {
                            "summary": "Example with json data",
                            "description": "Let use format described for fc/ or fc_batch/ endpoints",
                            "value": JsonData.Config().schema_extra["examples"][0],
                        },
                        "json_2": {
                            "summary": "Example with json data",
                            "description": "Let use format described for fc/ or fc_batch/ endpoints",
                            "value": JsonData.Config().schema_extra["examples"][1],
                        },
                        "json_3": {
                            "summary": "Example with json data",
                            "description": "Let use format described for fc/ or fc_batch/ endpoints",
                            "value": JsonData.Config().schema_extra["examples"][2],
                        }

                    }
                },
                "application/gzip": {
                    "schema": CSVData.schema(),
                    "examples": {
                        "csv_1": {
                            "summary": "Example with csv data",
                            "description": "Let use format described for fc/ or fc_batch/ endpoints",
                            "value": CSVData.Config().schema_extra["examples"][0],
                        },
                        "csv_2": {
                            "summary": "Example with csv data",
                            "description": "Let use format described for fc/ or fc_batch/ endpoints",
                            "value": CSVData.Config().schema_extra["examples"][1],
                        },
                        "csv_3": {
                            "summary": "Example with csv data",
                            "description": "Let use format described for fc/ or fc_batch/ endpoints",
                            "value": CSVData.Config().schema_extra["examples"][2],
                        }

                    }
                },
            },
            "required": True,
        },
    }
