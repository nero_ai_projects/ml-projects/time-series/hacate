import pandas as pd
import numpy as np
import pytest

import hecate.forecasting.outliers.target


def test_mark_outliers_synth_data():
    df = pd.DataFrame({'date': pd.date_range('2020-01-02', '2021-12-31')})
    df['target'] = np.where(df['date'] == '2021-08-01', 50, df.index)
    df['target'] = np.where(df['date'] == '2020-02-01', 600, df['target'])
    df['target'] = np.where(df['date'] == '2020-06-01', 1000, df['target'])
    df['partition'] = 'train'

    df, col = hecate.forecasting.outliers.target.add_target_outliers_mark(df)
    col = col[0]

    assert np.sum(df[col] == 1) >= 3, 'missed outliers'
    assert np.sum(df[col] == 1) <= 3, 'marked normal points as outliers'
    if np.sum(df[col] == 1) == 3:
        assert (df[df[col] == 1].date.values == df[(df['date'] == '2021-08-01') |
                                                   (df['date'] == '2020-02-01') |
                                                   (df['date'] == '2020-06-01')].date.values).all(), \
            'marked wrong points as outliers'


@pytest.mark.parametrize('outliers_input_df', ['test_1'], indirect=True)
def test_mark_outliers_real_data_1(outliers_input_df):
    df, _, _ = outliers_input_df
    df['day'] = df.date.dt.dayofweek
    df.loc[df['day'] == 6, 'target'] = 0
    df['target'] *= 5
    df, col = hecate.forecasting.outliers.target.add_target_outliers_mark(df)
    col = col[0]
    assert np.sum(df[col] == 1) >= 4, 'missed outliers'
    assert np.sum(df[col] == 1) <= 4, 'marked normal points as outliers'
    if np.sum(df[col] == 1) == 4:
        assert (df[df[col] == 1].date.values == df[(df['date'] == '2020-03-16') |
                                                   (df['date'] == '2020-03-17') |
                                                   (df['date'] == '2020-03-23') |
                                                   (df['date'] == '2020-03-28')].date.values).all(), \
            'marked wrong points as outliers'


@pytest.mark.parametrize('outliers_input_df', ['test_2'], indirect=True)
def test_mark_outliers_real_data_2(outliers_input_df):
    df, _, _ = outliers_input_df
    df['target'] *= 10
    df.loc[df['date'] == '2021-05-15', 'target'] = 1

    df, col = hecate.forecasting.outliers.target.add_target_outliers_mark(df)
    col = col[0]

    assert np.sum(df[col] == 1) >= 2, 'missed outliers'
    assert np.sum(df[col] == 1) <= 2, 'marked normal points as outliers'
    if np.sum(df[col] == 1) == 2:
        assert (df[df[col] == 1].date.values == df[(df['date'] == '2020-03-15') |
                                                   (df['date'] == '2020-03-29')].date.values).all(), \
            'marked wrong points as outliers'


def test_mark_zero_periods_synth_data():
    df = pd.DataFrame({'date': pd.date_range('2020-01-02', '2021-12-31')})
    df['target'] = np.where(df['date'] >= '2021-01-01', 0,
                            [x + 4 for x in df.index])
    df['partition'] = np.where(df['date'] >= '2021-07-01', 'fc', 'train')
    df, col = hecate.forecasting.outliers.target.add_target_zero_periods_mark(df, max_percent_zeroes=None)
    col = col[0]
    assert np.sum(df[col] == 1) >= 181, 'missed zero region points'
    assert np.sum(df[col] == 1) <= 181, 'marked normal points as zero region'
    if np.sum(df[col] == 1) == 181:
        assert (df[df[col] == 1].index == df[(df['date'] >= '2021-01-01') &
                                             (df['date'] <= '2021-06-30')].index).all(), \
            'marked wrong points as zero_region'

    df['target'] = np.where(df['date'] >= '2021-11-01', 400, 0)
    df['partition'] = 'train'
    df, col = hecate.forecasting.outliers.target.add_target_zero_periods_mark(df, max_percent_zeroes=None)
    col = col[0]
    assert np.sum(df[col] == 1) >= 669, 'missed zero region points'
    assert np.sum(df[col] == 1) <= 669, 'marked normal points as zero region'
    if np.sum(df[col] == 1) == 669:
        assert (df[df[col] == 1].index == df[df['date'] < '2021-11-01'].index).all(), \
            'marked wrong points as zero region'


@pytest.mark.parametrize('outliers_input_df', ['test_1'], indirect=True)
def test_mark_zero_periods_real_data_1(outliers_input_df):
    df, _, _ = outliers_input_df
    df['day'] = df.date.dt.dayofweek
    df.loc[df['day'] == 6, 'target'] = 0
    df, col = hecate.forecasting.outliers.target.add_target_zero_periods_mark(df)
    col = col[0]
    assert np.sum(df[col] == 1) == 0, 'marked normal points as zero region'


@pytest.mark.parametrize('outliers_input_df', ['test_3'], indirect=True)
def test_mark_zero_periods_real_data_3(outliers_input_df):
    df, _, _ = outliers_input_df
    df = df[df['date'] > '2021-01-01']
    df, col = hecate.forecasting.outliers.target.add_target_zero_periods_mark(df)
    col = col[0]
    assert np.sum(df[col] == 1) >= 99, 'missed zero region points'
    assert np.sum(df[col] == 1) <= 99, 'marked normal points as zero region'
    if np.sum(df[col] == 1) == 99:
        assert (df[df[col] == 1].index == df[((df['date'] >= '2021-01-02') &
                                              (df['date'] <= '2021-02-11')) |
                                             ((df['date'] >= '2021-06-17') &
                                              (df['date'] <= '2021-08-13'))].index).all(), \
            'marked wrong points as zero period'


def test_leave_zero_periods():
    column = pd.Series([1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0,
                        1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1])
    new_column = hecate.forecasting.outliers.target.leave_zero_periods(column, 3)
    assert (new_column == pd.Series([1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1])).all(), \
        'wrong replacement ones with zeros'


def test_zero_periods_return_type() -> None:
    df = pd.DataFrame(
        {
            "date": pd.date_range('2023-01-01', periods=100),
            "key": ["key1"]*100,
            "partition": ["train"]*80 + ["fc"]*20,
            "target": [100]*50 + [1]*10 + [100]*40,
        }
    )
    df, cols = hecate.forecasting.outliers.target.add_target_zero_periods_mark(df)

    assert df[cols[0]].dtype == 'bool'
