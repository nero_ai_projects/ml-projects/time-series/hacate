import numpy as np
import pandas as pd
from tools.generate_time_series import TSGenerator


def test_ts_generator_y(promo_details, price_details):
    ts_gen = TSGenerator(formula='4 * d1 + 2 * d2 - 3 * p1',
                         bl_start_value=100,
                         trend_angle=0.02,
                         seasonality=['Y', 'M'],
                         freq='D', dur='2Y',
                         promo_details=promo_details,
                         price_details=price_details,
                         white_noise_sd=15)

    hc_out = ts_gen.get('hc')
    all_out = ts_gen.get('all')

    assert set(hc_out.columns) == set(['key', 'date', 'partition', 'target', 'discount', 'p1'])
    assert set(all_out.columns) == set(['date', 'promo_id', 'discount', 'profile_num', 'p1', 'bl', 'disc_uplift',
                                        'profile_effect', 'price_effect_p1', 'trend', 'Y_seasonality', 'M_seasonality',
                                        'target', 'white_noise', 'key', 'partition'])

    assert pd.infer_freq(hc_out.date) == 'D'

    assert (hc_out.shape[0] in [365 * 2 + i for i in list(range(3))])
    assert (all_out.shape[0] in [365 * 2 + i for i in list(range(3))])

    assert all_out.bl.unique()[0] == [100]

    assert np.round(hc_out.partition.value_counts()['train'] / hc_out.shape[0], 1) == 0.8


def test_ts_generator_w(promo_details, price_details):
    ts_gen = TSGenerator(formula='4 * d1 + 2 * d2 - 3 * p1',
                         bl_start_value=100,
                         trend_angle=0.10,
                         seasonality=['Y', 'M'],
                         freq='W', dur='Y',
                         promo_details=promo_details,
                         price_details=price_details,
                         white_noise_sd=20)
    hc_out = ts_gen.get('hc')

    assert pd.infer_freq(hc_out.date).split('-')[0] == 'W'
    assert hc_out.shape[0] in [52, 53]
    assert np.round(hc_out.partition.value_counts()['train'] / hc_out.shape[0], 1) == 0.8
