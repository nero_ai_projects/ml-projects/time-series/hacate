import pandas as pd
import numpy as np
import pytest

from hecate.features import FeatureType, get_cols_from_types
from hecate.utils.utils import get_frequency
from hecate.forecasting.models.utils import get_default_n_estimators
from hecate.forecasting.utils import mark_tiny_leading_values_non_relevant
from hecate.forecasting import trend
from hecate.forecasting.feature_generation import add_time_features
from hecate.utils.utils import round_cols
from tests.hecate.test_hecate import generate_df_with_tiny_leading_values


def test_trend_angle(hecate_input_df):
    data, feature_types, result = hecate_input_df
    alpha = trend.trend_angle(data[data['partition'] == 'train']['target'])
    assert alpha is not np.nan
    assert -180 < alpha < 180


def test_train_trend(hecate_input_df):
    data, feature_types, result = hecate_input_df
    train_data = data[data['partition'] == 'train'].drop('partition', axis=1)
    train_data.set_index('date', inplace=True)
    data = data[['date', 'target', 'partition']]
    time_features = ['time-year', 'time-month', 'time-week', 'time-dayofweek', 'time-weekofmonth']
    data, cols = add_time_features(data, time_features)
    data.set_index('date', inplace=True)
    train_data = data[data['partition'] == 'train'].drop('partition', axis=1)
    trend_params = trend.get_trend_params(train_data['target'])
    data['trend'] = trend_params[1] + trend_params[0] * np.arange(data.shape[0])
    assert data['trend'].isna().sum() == 0


def test_make_trend(hecate_input_df):
    data, feature_types, result = hecate_input_df

    promo_feature_types = [FeatureType.discount, FeatureType.promo, FeatureType.promo_budget]
    promo_features = get_cols_from_types(feature_types, promo_feature_types)

    if len(data[data['partition'] == 'train']) >= 365:
        data, col = trend.add_trend(data, promo_features)
        assert True not in data['trend']


def test_mark_tiny_leading_values_non_relevant():
    test_sample = generate_df_with_tiny_leading_values()

    marked_sample = mark_tiny_leading_values_non_relevant(test_sample)
    assert 'non_relevant' in marked_sample.partition.unique()

    # we don't mark non-relevant history if it constant
    const_sample = pd.DataFrame(pd.date_range('2020-01-01', '2020-12-31'), columns=['date'])
    const_sample['target'] = 100
    const_sample['partition'] = 'train'
    marked_sample = mark_tiny_leading_values_non_relevant(const_sample.copy(), percent_threshold=0.1)
    assert 'non_relevant' not in marked_sample.partition.unique()


@pytest.mark.parametrize('outliers_input_df', ['test_1'], indirect=True)
def test_utils(outliers_input_df):
    df, _, _ = outliers_input_df
    df['date'] = pd.to_datetime(df['date'], format='%Y-%m-%d')
    assert get_default_n_estimators(get_frequency(df['date'])) == 20

    df_w = pd.DataFrame(df.groupby(pd.Grouper(key='date',
                                              freq='W'))['target'].agg('sum'))
    assert get_default_n_estimators(get_frequency(df_w.reset_index()['date'])) == 10


def test_round_cols() -> None:
    df = pd.DataFrame({
        'a': [123.4567890123] * 20,
        'b': [987.6543210987] * 20
    })

    # Test 0
    data = df.copy(deep=True)
    answer = df.copy(deep=True)

    round_cols(data, ('a', 'b'), None)

    assert np.all(data == answer)
    # Test 1.1
    data = df.copy(deep=True)
    answer = pd.DataFrame({
        'a': [123.46] * 20,
        'b': [987.65] * 20
    })

    round_cols(data, ('a', 'b'), 2)
    assert np.all(data == answer)

    # Test 1.2
    data = df.copy(deep=True)
    answer = pd.DataFrame({
        'a': [123] * 20,
        'b': [988] * 20
    })

    round_cols(data, ('a', 'b'), 0)
    assert np.all(data == answer)
