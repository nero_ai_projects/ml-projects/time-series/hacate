import pandas as pd
import numpy as np

from hecate.forecasting.repartitions import repartition
import pytest


def create_template_for_test_df(one_year=False):
    data = pd.DataFrame({'key': ['key1']*1195,
                         'date': pd.date_range(start="2021-09-09", periods=1195),
                         'partition': ['train']*1095 + ['fc']*100
                         })
    if one_year:
        data = data.iloc[-465:]
    return data


@pytest.mark.parametrize('repartitions_input_df, answer',
                         [('test_1', True), ('test_2', False)], indirect=['repartitions_input_df'])
def test_is_extreme_changes_in_target_yoy(repartitions_input_df, answer):
    data, _, _ = repartitions_input_df

    result = repartition.is_extreme_changes_in_target_yoy(data, feature_types={})

    assert result == answer


test_data_is_extreme_changes_in_target_yoy_synthetic_data = [
        (
            [0.1]*365+[0.5]*365+[2]*465,
            0,
            False,
            False,
            {},
            1.5,
            True,
            "Basic test on rapidly increasing target",
        ),
        (
            [2]*365+[0.5]*365+[0.1]*465,
            0,
            False,
            False,
            {},
            1.5,
            True,
            "Basic test on rapidly decreasing target",
        ),
        (
            [0.15]*365+[0.2]*365+[0.3]*465,
            0,
            False,
            False,
            {},
            1.5,
            False,
            "Basic test on slowly increasing target",
        ),
        (
            [0.1]*365+[0.5]*365+[0.1]*465,
            [0]*365+[0.2]*365+[0]*465,
            False,
            False,
            {},
            1.5,
            False,
            "Test on increasing target with promo",
        ),
        (
            [0.1]*365+[0]*365+[0]*465,
            0,
            False,
            False,
            {},
            1.5,
            False,
            "Test on zero median-target",
        ),
        (
            [10]*365+[10]*365+[10]*465,
            0,
            False,
            False,
            {},
            1.5,
            False,
            "Test on constant",
        ),
]


@pytest.mark.parametrize("""target, discount, target_outlier, target_zero_period, feature_types,
                         threshold, answer, error_msg""",
                         test_data_is_extreme_changes_in_target_yoy_synthetic_data)
def test_is_extreme_changes_in_target_yoy_synthetic_data(target, discount, target_outlier, target_zero_period,
                                                         feature_types, threshold, answer, error_msg):
    data = create_template_for_test_df()

    data['target'] = target
    data['discount'] = discount
    data['target-outlier'] = target_outlier
    data['target-zero_period'] = target_zero_period
    assert repartition.is_extreme_changes_in_target_yoy(
        data, feature_types=feature_types, threshold=threshold) == answer, error_msg


test_data_is_extreme_changes_in_target_yoy_short_data = [
        (
            [0.5]*100+[2]*365,
            0,
            False,
            False,
            {},
            1.5,
            False,
            "Short test on rapidly increasing target",
        ),
        (
            [0.1]*100+[0.5]*100+[2]*265,
            0,
            False,
            False,
            {},
            1.5,
            False,
            "Short test on rapidly decreasing target",
        ),
]


@pytest.mark.parametrize("""target, discount, target_outlier, target_zero_period, feature_types,
                         threshold, answer, error_msg""",
                         test_data_is_extreme_changes_in_target_yoy_short_data)
def test_is_extreme_changes_in_target_yoy_short_data(target, discount, target_outlier, target_zero_period,
                                                     feature_types, threshold, answer, error_msg):
    data = create_template_for_test_df(one_year=True)

    data['target'] = target
    data['discount'] = discount
    data['target-outlier'] = target_outlier
    data['target-zero_period'] = target_zero_period

    assert repartition.is_extreme_changes_in_target_yoy(
        data, feature_types=feature_types, threshold=threshold) == answer, error_msg


@pytest.mark.parametrize('repartitions_input_df, train_start_date_range',
                         [('test_1', pd.date_range(start='2022-06-15', end='2022-06-21')),
                          ('test_2', pd.date_range('2021-04-01', periods=1))],
                         indirect=['repartitions_input_df'])
def test_mark_extreme_history_non_relevant(repartitions_input_df, train_start_date_range):
    data, _, _ = repartitions_input_df

    output_df = repartition.mark_extreme_history_non_relevant(data, feature_types={})

    assert output_df.loc[output_df.partition == 'train', 'date'].min() in train_start_date_range


test_data_mark_extreme_history_non_relevant_synthetic_data = [
        (
            [0.1]*365+[10]*365+[1000]*465,
            0,
            False,
            False,
            {},
            ['non_relevant']*365+['non_relevant']*364+['train']*366+['fc']*100,
            "Basic test on rapidly increasing target",
        ),
        (
            [0.2]*365+[0.2]*365+[0.2]*465,
            0,
            False,
            False,
            {},
            ['train']*1095+['fc']*100,
            "Basic test on constant target",
        ),
]


@pytest.mark.parametrize("""target, discount, target_outlier, target_zero_period, feature_types, answer, error_msg""",
                         test_data_mark_extreme_history_non_relevant_synthetic_data)
def test_mark_extreme_history_non_relevant_synthetic_data(target, discount, target_outlier,
                                                          target_zero_period, feature_types,
                                                          answer, error_msg):
    data = create_template_for_test_df()

    data['target'] = target
    data['discount'] = discount
    data['target-outlier'] = target_outlier
    data['target-zero_period'] = target_zero_period

    output_df = repartition.mark_extreme_history_non_relevant(data, feature_types=feature_types)
    assert np.all(output_df['partition'] == answer), error_msg
