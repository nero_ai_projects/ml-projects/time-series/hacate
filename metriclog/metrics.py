import pandas as pd


def calculate_forecast_bias(results_table: pd.DataFrame) -> float:
    """
    Function calculates forecast bias for results of one hecate run

    :param results_table: table of results of one run

    :return: forecast bias
    """
    return round((results_table.target - results_table.fc).sum() / results_table.fc.sum() * 100, 2)


def calculate_forecast_accuracy(results_table: pd.DataFrame) -> float:
    """
    Function calculates forecast accuracy for results of one hecate run.
    Absolute error is calculated for each key and date

    :param results_table: table of results of one run

    :return: forecast accuracy
    """
    return round((1 - (abs(results_table.target - results_table.fc)).sum() / results_table.target.sum()) * 100, 2)


def calculate_metrics(results_table: pd.DataFrame) -> dict:
    """
    Function runs metrics calculations

    :param results_table: table of results of one run

    :return: dictionary of the calculated metrics with their names
    """
    fb = calculate_forecast_bias(results_table[results_table.partition == 'fc'])
    fa = calculate_forecast_accuracy(results_table[results_table.partition == 'fc'])
    return {'FB': fb, 'FA': fa}


def create_metrics_table(project: str, commit_id: str, branch: str, horizon: str, fold: int, metrics: dict) \
        -> pd.DataFrame:
    """
    Function creates table with all the metrics for the one run results table

    :param project: project name
    :param commit_id: last commit id in teh current branch
    :param branch: current branch
    :param horizon: current horizon
    :param fold: current fold
    :param metrics: dictionary of calculated metrics

    :return: table with metrics for the current run results
    """
    result_table_name = 'results_' + project.lower()
    run_info = {'project': [project], 'commit_id': [commit_id], 'branch': [branch], 'horizon': [horizon],
                'fold': [fold], 'result_table': result_table_name}
    return pd.DataFrame.from_dict(dict(run_info, **metrics))
