import logging
from typing import Literal
from logging.handlers import TimedRotatingFileHandler

from hecate.config import LOGS_APP_PATH, LOGS_WORKER_PATH

loggers: dict[str, dict[Literal['app', 'worker'] | None, logging.Logger]] = {}


def get_logger(name: str, service_kind: Literal['app', 'worker'] | None = None) -> logging.Logger:
    """
    The function returns the logger by its name and type.
    The logger configured for the service (any service_kind) has priority.
    At the same time, loggers for different types of service are not interchangeable.

    :param name: name of logger
    :param service_kind: The type of logger for the service. If the logger is not for the service, the value is None.

    :return: logger
    """

    global loggers

    if name in loggers:
        if None not in loggers[name]:
            if service_kind in loggers[name]:
                logger = loggers[name][service_kind]
            else:
                logger = set_up_logger(name, service_kind=service_kind)
                loggers[name][service_kind] = logger
        else:
            if service_kind is not None:
                logger = set_up_logger(name, service_kind=service_kind)
                loggers[name][service_kind] = logger
            else:
                logger = loggers[name][service_kind]
    else:
        logger = set_up_logger(name, service_kind=service_kind)
        loggers[name] = dict()
        loggers[name][service_kind] = logger

    return logger


def set_up_logger(name: str, service_kind: Literal['app', 'worker'] | None = None) -> logging.Logger:
    """
    Setup logging with handlers

    :param name: name of logger
    :param service_kind: The type of logger for the service. If the logger is not for the service, the value is None.
    :return: logger
    """
    if service_kind is not None:
        log_format = '%(asctime)s - %(levelname)s - %(module)s - %(request_id)s - %(user_name)s - %(message)s'
    else:
        log_format = '%(asctime)s - %(levelname)s - %(module)s - %(message)s'

    log_formatter = logging.Formatter(log_format, datefmt="%Y-%m-%d %H:%M:%S")

    match service_kind:
        case "app" | None:
            log_name = LOGS_APP_PATH
        case "worker":
            log_name = LOGS_WORKER_PATH
        case _:
            raise ValueError(f'Unknown service_kind passed: {service_kind}')

    logger = logging.getLogger(name)
    logger.handlers.clear()
    logger.propagate = False
    logger.setLevel(logging.INFO)

    log_handler = TimedRotatingFileHandler(when='D', filename=log_name,
                                           backupCount=30, encoding="utf-8")
    log_handler.setLevel(logging.INFO)
    log_handler.setFormatter(log_formatter)

    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    console.setFormatter(log_formatter)

    logger.addHandler(console)
    logger.addHandler(log_handler)

    logger.filters.clear()
    logger.addFilter(LoggerRequestIDFilter())
    logger.addFilter(LoggerUserNameFilter())

    return logger


class LoggerRequestIDFilter(logging.Filter):

    def __init__(self, request_id: str = 'undefined_request_id'):
        super().__init__()
        self.request_id = request_id

    def filter(self, record: logging.LogRecord) -> bool:
        record.request_id = self.request_id
        return True


class LoggerUserNameFilter(logging.Filter):

    def __init__(self, user_name: str = 'undefined_user_name'):
        super().__init__()
        self.user_name = user_name

    def filter(self, record: logging.LogRecord) -> bool:
        record.user_name = self.user_name
        return True


def set_up_request_id(logger: logging.Logger, request_id: str) -> None:
    """
    The function determines the request_id value for the logger.
    """

    logger.filters[0].request_id = request_id  # type: ignore


def set_up_user_name(logger: logging.Logger, user_name: str) -> None:
    """
    The function determines the user_name value for the logger.
    """

    logger.filters[1].user_name = user_name  # type: ignore


def get_request_id(logger: logging.Logger) -> str:
    """
    The function returns the current value of the logger request_id.
    If it wasn't defined, it will return 'undefined_request_id' and send a warning.
    """

    request_id = logger.filters[0].request_id  # type: ignore

    if request_id == 'undefined_request_id':
        logger.warning("request_id was not defined")

    return request_id
