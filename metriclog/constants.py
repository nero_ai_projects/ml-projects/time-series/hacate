"""
Constants for hecate.metriclog
"""
from typing import Literal

HORIZONS = Literal['st', 'mt', 'lt', 'all']
MAIN_HECATE_FC_PARAMS = ['branch', 'commit_id', 'fold', 'focus', 'horizon']
MAIN_AB_HECATE_PARAMS = ['branch_a', 'commit_a_id', 'branch_b', 'commit_b_id', 'fold', 'focus', 'horizon']
