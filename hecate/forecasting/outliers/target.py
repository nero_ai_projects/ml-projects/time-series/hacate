import numpy as np
import pandas as pd
from typing import Literal
from sklearn.base import TransformerMixin
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import MinMaxScaler

from hecate.forecasting.utils import HorizonMixin
from hecate.forecasting.horizon import Horizon
from hecate.forecasting.outliers.base import DummyOutlierMarkGenerator
from hecate.constants import HALF_YEAR_LEN
from hecate.features import TypesOfFeatures
from hecate.types import TupleDataAndCols


class DefaultOutlierMarksGenerator(DummyOutlierMarkGenerator):
    def add_outlier_marks(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) -> TupleDataAndCols:
        # - outliers
        data, outlier_col = add_target_outliers_mark(data)
        # - zero periods
        data, zero_periods_col = add_target_zero_periods_mark(data)
        marks = outlier_col + zero_periods_col

        return data, marks


class STOutlierMarksGenerator(DefaultOutlierMarksGenerator):
    ...


class MTOutlierMarksGenerator(DefaultOutlierMarksGenerator):
    ...


class LTOutlierMarksGenerator(DefaultOutlierMarksGenerator):
    ...


class TargetOutlierGenerator(DummyOutlierMarkGenerator, HorizonMixin):

    def add_outlier_marks(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) -> TupleDataAndCols:
        generator_from_horizon = {
            Horizon.ST: STOutlierMarksGenerator,
            Horizon.MT: MTOutlierMarksGenerator,
            Horizon.LT: LTOutlierMarksGenerator,
        }
        generator = generator_from_horizon.get(self.horizon, DefaultOutlierMarksGenerator)

        data, added_marks = generator().add_outlier_marks(data)

        return data, added_marks


def add_target_outliers_mark(
        df: pd.DataFrame,
        scl: TransformerMixin | None = None,
        eps: float = 0.1,
        min_samples: int = 4
) -> tuple[pd.DataFrame, list[Literal['target-outlier']]]:
    """
    Function adds to dataset binary feature, that marks
    extreme high sales. Such points (outliers)
    are found with DBSCAN - clustering algorithm.
    If there are no outliers in dataset
    then will be added constant 0.

    :param df: pd.DataFrame - input dataframe
    :param scl: class - Scaler (minmax by default)
    :param eps: float - DBSCAN parameter (0.15 by default)
    :param min_samples: int - DBSCAN parameter (4 by default)

    :return: pd.DataFrame
    :return: list(test_outlier indicator)
    """

    if scl is None:
        scl = MinMaxScaler()

    df['target-outlier'] = False
    is_train = df.partition == 'train'
    train_target = df.loc[is_train, 'target'].to_numpy()

    if train_target.size > HALF_YEAR_LEN:
        idx = np.arange(train_target.size)
        model = DBSCAN(eps=eps, min_samples=min_samples) \
            .fit(scl.fit_transform(np.vstack((train_target, idx)).T))
        df.loc[is_train, 'target-outlier'] = model.labels_ == -1

    return df, ['target-outlier']


def leave_zero_periods(col: pd.Series, min_period_dur: int) -> np.ndarray:
    """
    Function marks zero sales periods, if a period is long enough (longer,
    than min_period_dur).

    :param col: pd.Series - input column
    :param min_period_dur: int - how long a period should be to be considered
    a zero-sales period

    :return: pd.Series
    """

    rol_mean = col.rolling(min_period_dur, min_periods=1).mean()
    rol_flag = rol_mean == 1

    shift_cols = []
    for i in range(1, min_period_dur):
        shift_cols.append(rol_flag.shift(-i, fill_value=False))

    return np.vstack(shift_cols + [rol_flag]).max(axis=0)


def add_target_zero_periods_mark(
        df: pd.DataFrame,
        scl: TransformerMixin | None = None,
        eps: float = 0.15,
        min_samples: int = 4,
        perc: float = 0.05,
        min_period_dur: int = 3,
        max_percent_zeroes: float | None = 30.0
) -> tuple[pd.DataFrame, list[Literal['target-zero_period']]]:
    """
    Function adds a binary feature to an input dataframe, which marks
    zero sales periods. Such periods are found with DBSCAN.
    If there are no zero sales periods, a column with zeroes is added.

    :param df: pd.DataFrame - input dataframe
    :param scl: class - Scaler (minmax by default)
    :param eps: float - DBSCAN parameter
    :param min_samples: int - DBSCAN parameter
    :param perc: float - percentage of target mean as a threshold
    :param min_period_dur: int - minimum amount of days for the period to
    be considered zero-sales period
    :param max_percent_zeroes: float - the maximum percentage
    of marking near-zero values (by the number of points).
    If None, the restriction does not apply.

    :return: pd.DataFrame
    :return: list(zero indicator)
    """

    if scl is None:
        scl = MinMaxScaler()

    df['target-zero_period'] = False

    train = df.loc[df.partition == 'train', ['target', 'date']]
    if (train.date.max() - train.date.min()).days <= HALF_YEAR_LEN:
        return df, ['target-zero_period']

    train = train[['target']]
    train_mean = train.target.mean()

    train['id'] = np.arange(train.shape[0])
    train['high_target'] = train['target'] > perc * train_mean
    model = DBSCAN(eps=eps, min_samples=min_samples).fit(
        scl.fit_transform(train[['high_target', 'id']]))
    train['dbscan'] = model.labels_

    train['dbscan_mean'] = train['target'].groupby(train['dbscan']).transform('mean')
    train['dbscan'] = ~((train.dbscan == -1) | (train.dbscan_mean > perc * train_mean))

    if train.dbscan.sum() == 0:
        return df, ['target-zero_period']

    train['dbscan'] = leave_zero_periods(train.dbscan, min_period_dur)
    df.loc[df.partition == 'train', 'target-zero_period'] = train.dbscan
    if max_percent_zeroes is not None:
        if df['target-zero_period'].mean() >= (max_percent_zeroes/100):
            df['target-zero_period'] = False

    return df, ['target-zero_period']
