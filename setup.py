import logging
import os


def set_up_api_keys():
    """
    Load API KEY from env file and parse it

    :return: dict of api_keys if API KEY exist else empty dict
    """
    api_key = os.getenv('API_KEY')
    api_keys_dict = {}
    if not api_key:
        return api_keys_dict
    list_keys = api_key.split(";")
    for item in list_keys:
        temp_list = item.split(":")
        api_keys_dict[temp_list[0]] = temp_list[1]
    return api_keys_dict


def set_up_logger(is_service: bool = True):
    """
    Setup logging with handlers
    :return: logger
    """
    if is_service:
        log_format = '%(asctime)s - %(levelname)s - %(module)s - %(request_id)s - %(message)s'
    else:
        log_format = '%(asctime)s - %(levelname)s - %(module)s - %(message)s'
    log_name = os.path.join('logs', 'app.log')
    file_path = os.path.dirname(os.path.abspath(__file__))
    if os.getcwd() != file_path:
        log_name = os.path.join(file_path, log_name)
    logging.basicConfig(
        level=logging.INFO,
        format=log_format,
        datefmt='%Y-%m-%d %H:%M:%S', )

    from logging.handlers import TimedRotatingFileHandler

    log_handler = TimedRotatingFileHandler(when='D', filename=log_name,
                                           backupCount=30, encoding="utf-8")
    log_handler.setLevel(logging.INFO)
    log_handler.setFormatter(logging.Formatter(log_format))

    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    console.setFormatter(logging.Formatter(log_format))

    logger = logging.getLogger('hecate')
    logging.getLogger('').handlers.clear()
    logger.handlers.clear()

    logger.addHandler(console)
    logger.addHandler(log_handler)


class LoggerRequestIDFilter(logging.Filter):

    def __init__(self, request_id: str = 'undefined_request_id'):
        super().__init__()
        self.request_id = request_id

    def filter(self, record: logging.LogRecord) -> bool:
        record.request_id = self.request_id
        return True
