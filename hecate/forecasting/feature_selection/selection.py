import pandas as pd
import numpy as np
from typing import Callable

from hecate.types import TupleDataAndCols
from hecate.forecasting.utils import HorizonMixin
from hecate.forecasting.feature_selection.base import DummyFeatureSelector


class DefaultFeatureSelector(DummyFeatureSelector):

    def drop_features(
            self,
            data: pd.DataFrame,
            business_features: list[str],
            time_features: list[str],
            period_features: list[str] | None = None,
            target_features: list[str] | None = None) -> list[str]:
        """
        Function select and delete constant features.
        All features drop which have unique one element in pd.Series.
        Data columns must contain features list.

        :param data: pd.DataFrame - input dataframe
        :param business_features: list of string - list of business features
        :param time_features: list of string - list of time features
        :param period_features: list of string - list of column names
        for outliers and zero-sales periods
        :param target_features: list of string - list of features generated
        from target

        :return: list - list of result features
        """
        period_features = [] if period_features is None else period_features
        target_features = [] if target_features is None else target_features

        feature_cols = business_features + time_features + period_features + target_features

        drop_feature = []

        params_from_func: dict[Callable, list] = {
            drop_constant_columns: feature_cols,
            drop_non_looped_time_features: time_features,
            drop_business_features: business_features,
        }
        for func, feature in params_from_func.items():
            data, drop_cols = func(data, feature)
            drop_feature += drop_cols

        # result features after feature generation and feature selection
        selected_features = list(set(feature_cols).difference(drop_feature))

        selected_features = selected_features if selected_features else time_features

        return selected_features


class STFeatureSelector(DefaultFeatureSelector):
    ...


class MTFeatureSelector(DefaultFeatureSelector):
    ...


class LTFeatureSelector(DefaultFeatureSelector):
    ...


class FeatureSelector(DummyFeatureSelector, HorizonMixin):

    def drop_features(
            self,
            data: pd.DataFrame,
            business_features: list[str],
            time_features: list[str],
            period_features: list[str] | None = None,
            target_features: list[str] | None = None) -> list[str]:

        generator_from_horizon = {
            'st': STFeatureSelector,
            'mt': MTFeatureSelector,
            'lt': LTFeatureSelector,
        }
        generator = generator_from_horizon.get(self.horizon, DefaultFeatureSelector)

        selected_features = \
            generator().drop_features(data, business_features, time_features, period_features, target_features)

        return selected_features


def drop_business_features(data: pd.DataFrame, features: list[str] | pd.Index, corr_rate=0.95
                           ) -> TupleDataAndCols:
    """
    The function filters business features:
        * by exceeding the correlation threshold
        ...
    :param train: pd.DataFrame - input dataframe
    :param features: list of string - list of features
    :param corr_rate: float - correlation rate to drop features

    :return: tuple with dataset and list of drop features
    """

    features = data.columns.intersection(features)

    train = data.loc[data.partition == 'train']
    drop_cols = drop_high_corr_features(train, features, corr_rate)

    return data, drop_cols


def drop_high_corr_features(
        train: pd.DataFrame,
        features: list[str] | pd.Index,
        corr_rate: float = 0.9
) -> list[str]:

    """
    Function select and delete business features according correlation rate.
    Default correlation rate is 0.9.

    :param train: pd.DataFrame - input dataframe
    :param features: list of string - list of features
    :param corr_rate: float - correlation rate to drop features

    :return: list of string - list of drop features
    """

    features = train.select_dtypes(["number", "bool"]).columns.intersection(features)

    matrix_corr_abs = np.abs(train[['target'] + list(features)].corr().to_numpy())
    cond = (matrix_corr_abs >= corr_rate) & ~np.isnan(matrix_corr_abs)
    features_mask = (~np.triu(cond, k=1)).prod(axis=0).astype(bool)
    selected_cols = features[features_mask[1:]]

    columns_to_drop = features.difference(selected_cols).to_list()

    return columns_to_drop


def drop_constant_columns(data: pd.DataFrame, features: list[str]) -> TupleDataAndCols:
    """
    Function select and delete constant features.
    All features drop which have unique one element in pd.Series.
    Data columns must contain features list.

    :param data: pd.DataFrame - input dataframe
    :param features: list of string - list of features

    :return: data - output dataframe with drop features
    :return: list - list of drop features
    """

    # select features only in column
    features = data.columns.intersection(features).to_list()
    nunique_features = data.loc[data['partition'] == 'train', features].nunique()

    constant_cols = nunique_features[nunique_features == 1].index.to_list()
    # data.drop(columns=constant_cols, inplace=True)

    return data, constant_cols


def drop_non_looped_time_features(data: pd.DataFrame, features: list[str]) -> TupleDataAndCols:
    """
    The function drop month and week columns if data contain less one-year history.

    :param data: pd.DataFrame - input dataframe
    :param features: list of time features
    :return: tuple (data, list with removed features)
    """
    dates = data.loc[data['partition'] == 'train', 'date']
    day_length = (dates.max() - dates.min()).days
    check_loop_for = {'time-month', 'time-week'}
    non_looped: set[str] = set() if day_length >= 365 else check_loop_for.intersection(features)

    to_drop = list(non_looped.intersection(data.columns))
    if to_drop:
        # data.drop(columns=to_drop, inplace=True)
        ...

    return data, to_drop
