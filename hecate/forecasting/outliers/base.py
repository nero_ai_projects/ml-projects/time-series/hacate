import pandas as pd

from hecate.features import TypesOfFeatures
from hecate.types import TupleDataAndCols
from hecate.forecasting.horizon import Horizon


class DummyOutlierMarkGenerator:
    def __init__(self) -> None:
        super().__init__()
        self.horizon = Horizon.UNDEFINED

    def add_outlier_marks(self, data: pd.DataFrame, feature_types: TypesOfFeatures | None = None) \
            -> TupleDataAndCols:
        return data, []
