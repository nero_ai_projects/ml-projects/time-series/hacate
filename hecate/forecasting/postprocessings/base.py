import pandas as pd

from hecate.features import TypesOfFeatures
from hecate.forecasting.horizon import Horizon


class DummyPostprocessor:
    def __init__(self, horizon: Horizon = Horizon.UNDEFINED):
        self.horizon = horizon

    def execute(self, data: pd.DataFrame, horizon: Horizon, output_columns: list[str],
                feature_types: TypesOfFeatures | None = None, decimals: int | None = 2) -> pd.DataFrame:
        """
        Method collect logic of postprocessing for sample.
        :param data: initial dataframe for postprocessing
        :param horizon: forecasting horizon
        :param output_columns: columns to add to the output dataframe apart from the mandatory ones
        :param feature_types: dictionary of feature types
        :param decimals: number of decimal places to round the output values to

        :return: dataframe after postprocessing
        """

        return data
