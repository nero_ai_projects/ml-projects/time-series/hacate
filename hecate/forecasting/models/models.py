from abc import abstractmethod
from typing import Protocol, Any

import numpy as np
import xgboost as xgb
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer, TransformedTargetRegressor

from hecate.forecasting.models.utils import (
    get_default_n_estimators,
    TargetEncoder,
    get_sample_importance,
)


class SklearnRegressor(Protocol):
    @abstractmethod
    def __init__(self, *args, **kwargs): ...
    @abstractmethod
    def fit(self, *args, **kwargs): ...
    @abstractmethod
    def predict(self, *args, **kwargs): ...
    @abstractmethod
    def get_feature_importance(self) -> dict: ...


class DefaultForecastingPipeline(SklearnRegressor):
    """
    Forecasting pipeline class for features transformation with OHE, MinMaxScaler and TransformedTargetRegressor.
    Supposed to be common for any model being used (whether it is XGB, random forest, or any other class of the model)
    """

    def __init__(self,
                 feature_cols: list[str],
                 freq: str | None = None,
                 constructor: Any | None = None,
                 params_model: dict | None = None,
                 ohe_cols: list[str] | None = None,
                 min_max_cols: list[str] | None = None,
                 base_score: float | None = None,
                 target_encoding_cols: list[str] | None = None):
        """
        Initialize parameters. In case not specified otherwise, takes XGB tree by default.

        :param feature_cols: list with feature column names
        :param freq: frequency of timeline to pick XGB parameters. Not required, if any other constructor is used.
        :param constructor: model constructor with sklearn API
        :param params_model: parameters for initializing model
        :param ohe_cols: list with feature column names for one hot encoding
        :param min_max_cols: list with feature column names for min max scaling
        :param base_score: model parameter
        :param target_encoding_cols: list of time feature columns for target encoding
        """

        self.constructor = xgb.XGBRegressor if constructor is None else constructor

        if params_model is None:
            params_model = {
                'eta': 0.3,
                'max_depth': 2,
                'n_estimators': get_default_n_estimators(freq),
                'min_child_weight': 2,
                'nthread': 1,
                'importance_type': 'gain',
                'base_score': base_score,
            }
        target_encoding_cols = [] if target_encoding_cols is None else target_encoding_cols
        ohe_cols = [] if ohe_cols is None else ohe_cols
        if min_max_cols is None:
            min_max_cols = [col for col in feature_cols if col not in target_encoding_cols + ohe_cols]

        # define model
        all_transformed_cols = min_max_cols + ohe_cols + target_encoding_cols
        other_columns = [col for col in feature_cols if col not in all_transformed_cols]

        target_encoder = TargetEncoder()

        numeric_transformer = Pipeline(
            steps=[('scaler', MinMaxScaler())]
        )

        cat_transformer = OneHotEncoder(handle_unknown='ignore')

        preprocessor = ColumnTransformer(
            sparse_threshold=0,
            transformers=[
                ('target_encoding', target_encoder, target_encoding_cols),
                ('num', numeric_transformer, min_max_cols),
                ('cat', cat_transformer, ohe_cols),
                ('other', 'passthrough', other_columns)
            ],
        )

        pipeline = Pipeline(steps=[('preprocessor', preprocessor),
                                   ('model', self.constructor(**params_model))])

        self.model = TransformedTargetRegressor(regressor=pipeline, transformer=MinMaxScaler())

    def fit(self, *args, **kwargs):
        """
        Fit the model.
        """
        if isinstance(self.constructor(), xgb.XGBRegressor):
            kwargs['model__sample_weight'] = get_sample_importance(args[0])
        self.model.fit(*args, **kwargs)

    def predict(self, *args, **kwargs):
        """
        Return predictions of the model.
        """
        return self.model.predict(*args, **kwargs)

    def get_feature_importance(self):
        """
        Return feature importance dictionary.
        """
        return self.model.regressor_.named_steps['model'].feature_importances_


class STForecastingPipeline(DefaultForecastingPipeline):
    ...


class MTForecastingPipeline(DefaultForecastingPipeline):
    ...


class LTForecastingPipeline(DefaultForecastingPipeline):
    ...


class ShortKeyForecastingPipeline(SklearnRegressor):
    """
    Forecasting pipeline class for keys with short train
    """
    def __init__(self, *args, **kwargs):
        self.mean_target = None

    def fit(self, train_features, train_target):
        """
        Fit method for short keys
        """

        self.mean_target = np.mean(train_target.values)

        return self

    def predict(self, data):
        """
        Predict method for short keys
        """
        mean_value = self.mean_target
        prediction = np.full(data.shape[0], mean_value)

        return prediction

    def get_feature_importance(self) -> dict:
        """
        Return feature importance dictionary.
        """
        return {}
