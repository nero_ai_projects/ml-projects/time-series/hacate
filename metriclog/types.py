"""
New classes required in hecate.metriclog
"""

from dataclasses import dataclass, replace
from typing import Literal, NamedTuple

from metriclog.constants import HORIZONS


@dataclass
class HecateVersionsParams:
    """
    Accumulates branch name and commit id of two hecate versions as well as hecate forecasting parameters

    :param project: project, on which the hecate forecast and AB tests to be passed. Can be 'all' for the forecast
    :param branch_a: branch name for the hecate version A
    :param commit_a_id: commit id for the hecate version A
    :param branch_b: branch name for the hecate version B
    :param commit_b_id: commit id for the hecate version B
    :param n_folds: desired number of folds to forecast on. Use -1 to choose all available folds
    :param fold: fold number. Fold 1 is the last fold in time
    :param focus: flag to use only most important products
    :param horizon: desired horizon. can use all
    :param bl: if True hecate calculates BL too. To calculate baseline every discount feature will be zero in forecast
               period. BL is set to be False until it's added as a particular feature column in datatables like 'focus'
    :param output_dir: path to directory for saving files
    :param save_locally: if True will save the results locally in 'output_dir' instead of into database
    :param save_input_file_locally: if True will save the input in hecate dataframe locally in folder 'output_dir'
    :param n_cores: number of threads to run in parallel for data uploading to db
    """

    branch_a: str = ''
    commit_a_id: str = ''
    branch_b: str = ''
    commit_b_id: str = ''
    branch: str = ''
    commit_id: str = ''
    project: str = 'all'
    fold: int = 0
    focus: bool = True
    horizon: HORIZONS = 'all'
    n_folds: int = 3
    bl: bool = False
    output_dir: str = './outputs/'
    save_locally: bool = False
    save_input_file_locally: bool = False
    n_cores: int = 1

    def set_main_version(self, version: Literal['A', 'a', 'B', 'b']):
        """
        Metod sets which hecate version will be fixed in arguments 'branch' and 'commit_id' to be passed in forecasting

        :param version: the name of the main version. If 'A' or 'a', 'branch_a' and 'commit_a_id' values will be used

        :return: an instance with the updated parameters
        """
        if version.lower() == 'a':
            new_instance = replace(self, branch=self.branch_a, commit_id=self.commit_a_id)
        elif version.lower() == 'b':
            new_instance = replace(self, branch=self.branch_b, commit_id=self.commit_b_id)
        else:
            raise ValueError(f"Get unexpected hecate version {version} instead of 'A', or 'B'.")
        return new_instance


class HecateVersion(NamedTuple):
    """
    Defines the version of hecate by branch and commit id
    """
    branch: str
    commit_id: str
