from typing import NamedTuple
from itertools import product

import pandas as pd

from hecate.log import get_logger
from hecate.features import FeatureSet
from hecate.utils.utils import get_frequency
from hecate.forecasting.utils import get_short_train_flag
from hecate.forecasting.models.utils import get_base_score
from hecate.forecasting.horizon import Horizon
from hecate.constants import SHORT_TRAIN_FLAGS, TARGET_ENCODING_COLS
from hecate.forecasting.models.base import DummyForecastingMachine, ModelAnswer
from hecate.forecasting.models.models import (
    DefaultForecastingPipeline,
    STForecastingPipeline,
    MTForecastingPipeline,
    LTForecastingPipeline,
    ShortKeyForecastingPipeline,
)

logger = get_logger('hecate')


class ForecastingMachine(DummyForecastingMachine):
    """
    Forecasting pipeline class.
    """

    @staticmethod
    def forecast(data: pd.DataFrame,
                 horizon: Horizon,
                 feature_set: FeatureSet,
                 freq: str | None = None,
                 base_score: float | None = None) -> ModelAnswer:
        """
        Get model answer for the input data and features.

        :param data: initial data with features added
        :param horizon: forecasting horizon
        :param feature_set: feature set to be used for forecasting
        :param freq: frequency of timeline
        :param base_score: model parameter

        :return: ModelAnswer class instance
        """
        data['target'] -= data['trend']
        train_partition = data[data['partition'] == 'train']

        if freq is None:
            freq = get_frequency(data['date'])

        if base_score is None:
            base_score = get_base_score(data)

        if (train_partition.date.max() - train_partition.date.min()).days >= 365:
            target_encoding_cols = [col for col in TARGET_ENCODING_COLS if col in feature_set.selected_features]
        else:
            target_encoding_cols = []

        short_train_flag = get_short_train_flag(data)

        pipeline_from_ts_metas = get_pipeline_from_ts_metas()
        ts_meta_i = TimeSeriesMeta(horizon, short_train_flag)

        fc_pipeline = pipeline_from_ts_metas.get(ts_meta_i, DefaultForecastingPipeline)
        fc_pipeline = fc_pipeline(
            feature_set.selected_features,
            freq,
            target_encoding_cols=target_encoding_cols,
            ohe_cols=list(set(feature_set.label_features).intersection(feature_set.selected_features)),
            base_score=base_score
        )
        fc_pipeline.fit(train_partition[feature_set.selected_features], train_partition['target'])

        prediction = fc_pipeline.predict(data[feature_set.selected_features])
        prediction += data['trend']
        prediction = prediction.clip(lower=0)

        data['target'] += data['trend']

        return ModelAnswer(prediction, fc_pipeline)


class TimeSeriesMeta(NamedTuple):
    horizon: Horizon
    short_train: bool


def get_all_ts_metas() -> list[TimeSeriesMeta]:
    """
    The function creates list with pairs of horizons and train lengths.
    return: list with pairs of horizon and train_length
    """
    ts_meta = TimeSeriesMeta
    all_ts_metas = [
        ts_meta(horizon=h, short_train=l) for h, l in product(Horizon, SHORT_TRAIN_FLAGS) if h != Horizon.UNDEFINED
    ]
    return all_ts_metas


def get_pipeline_from_ts_metas() -> dict:
    """
    The function creates pipeline dictionary.
    return: dictionary with pipeline for each pair horizon-train length
    """

    pipeline_from_ts_metas: dict = {}
    for item in get_all_ts_metas():
        if item.horizon == Horizon.ST:
            pipeline_from_ts_metas[item] = STForecastingPipeline
        elif item.horizon == Horizon.MT:
            pipeline_from_ts_metas[item] = MTForecastingPipeline
        elif item.horizon == Horizon.LT:
            pipeline_from_ts_metas[item] = LTForecastingPipeline
        else:
            logger.warning(f'Unknown horizon {item.horizon}: using Default forecasting pipeline')

    pipeline_from_ts_metas = \
        {k: ShortKeyForecastingPipeline if k.short_train else v for k, v in pipeline_from_ts_metas.items()}
    return pipeline_from_ts_metas
