This file collect a set of rules that everybody should keep in the mind during development.

# Feature naming convention

## Problem

We could re-write feature that was already created.
Example: I'm worked on discount and find that counting day in promo period will improve metrics.
So, I create a column ```count```

You got a task to improve price changes sensitivities and understood that counter before price increase will be good solution.
So, you add a column ```count```.

Boom! We lost one of the added column...

## Solution

Let add columns according to template: ```{parent_column}-{suffix}```

```python
    parent_column = 'date'
    suffix = 'year'
    name = f'{parent_column}-{suffix}'
```

Then for our initial example we will produce two cols: ```discount-count``` and ```price-count```

# Type hints

## Problem

Dynamic typing in python, on the one hand, gives you more flexibility, on the other hand, it hides an additional difficulty in keeping knowledge of the types of variables used in your head. This can generate subtle errors.

## Solution

To catch errors at the earliest stage - in the process of writing code - there is a [type annotation in python](https://docs.python.org/3.10/library/typing.html#relevant-peps).

By itself, the type annotation (or type hint) does not solve the problem (does not conduct testing), but additionally documents the code, which makes it possible to explicitly specify the desired types for any objects.

Static code analyzers (pyright, mypy, ...) can work on the basis of type annotations. They check the correspondence of the specified types and highlight the places of possible conflicts. Logical operations on types also work.

> [!Warning] The current project will use static typing everywhere after a while. In particular, checks at the CI/CD level will appear.

## Instructions

- Most of the most common types do not need to be imported from libraries. For example, `list[...]`, `str[...]`, `tuple[...]`, `dict[...]`.
- Prior to the `python 3.10` version, it was required to use the corresponding objects from the standard library `typing` (`List[...]`, `Std[...]`, `Tuple[...]`, `Dict[...]`). Now this step is undesirable.
- Similarly, type unions are now specified using a vertical bar (logical "or"). For example, `set | list`, instead of the obsolete `Union[set, list]'.
- Optional arguments of the function should also be specified through a vertical line: `Iterable[float] | None`
- Complex composite types are not mandatory yet, so if it is impossible to solve the problem quickly, you can do with specifying only the top level (for example, `dict` instead of `dict[str | int, float]')
- The `tuple` data structure with a large number of different elements should, if possible, be replaced with `NamedTuple` due to the difficulty in interpreting types.

## Tools

Recommended Type Analyzer - [pyright](https://github.com/microsoft/pyright)

Как пользоваться:
### Command line
```bash
pip install pyright
pyright hecate/
```
### IDE
For each IDE, there are extensions that allow you to highlight errors directly in the editing window. Install, configure and use.


# [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)

## Problem

Commit messages are often written in an arbitrary format and with varying degrees of informativeness. This makes it difficult to read the commit history and create `CHANGELOG.md `.

## Solution

A good solution to the problem is to use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).
This allows you to standardize the commit history and makes it possible to generate CHANGELOG.md automatically.

The commit message should be structured as follows:
```
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```

- build: changes that affect the build system or external dependencies
- ci: changes to our CI configuration files and scripts
- docs: documentation only changes
- feat: a new feature (this correlates with MINOR in [Semantic Versioning](https://semver.org))
- fix: a bug fix (this correlates with `PATCH` in Semantic Versioning)
- perf: a code change that improves performance
- refactor: A code change that neither fixes a bug nor adds a feature
- style: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- test: Adding missing tests or correcting existing tests
- chore: Other changes that don't modify src or test files
- revert: Reverts a previous commit
- BREAKING CHANGE: a commit that has a footer BREAKING CHANGE:, or appends a `!` after the type/scope, introduces a breaking API change (correlating with `MAJOR` in Semantic Versioning). A BREAKING CHANGE can be part of commits of any type.

## Tools

To check commit messages, we use the pre-commit hooks plugin [commitizen](https://github.com/commitizen-tools/commitizen).

To enable the hook, you need to run the command:
```
pre-commit install --hook-type commit-msg
```
The hook settings are in the file `.pre-commit-config.yaml`.


# Versioning and CHANGELOG

## Problem

External Hecate users should be able to capture the package status and update it as needed.
Using a commit hash to commit is an unacceptable luxury.

Each version change should be accompanied by an update of the change log to inform users.

## Solution

The versioning of the project takes place on the basis of an agreement [SemVer](https://semver.org) (`MAJOR`.`MINOR`.`PATCH`). When specifying the version (and tags), the prefix `v` is used (for example, `v1.3.1`), according to [documentation](https://git-scm.com/book/en/v2/Git-Basics-Tagging) git. The version is changed manually.

[GitLab API](https://docs.gitlab.com/ee/api/repositories.html#generate-changelog-data) provides the ability to update a file `CHANGELOG.md` based on the commit history.

The file is updated at one of the stages of the gitlab ci/cd pipeline.

## Instructions

1. Before you complete the merge of your changes in development, you decide to change the version:

    - Carefully read the source [SemVer](https://semver.org)
    - If you have removed some functionality or otherwise violated backward compatibility - increase `MAJOR`
    - If you have added new functionality without breaking backward compatibility - increase the `MINOR`
    - If the bug is fixed - increase the `PATCH`
    - Or we discuss on the status

2. Updating the version in `pyproject.tool`.
3. In one of the commits (or merge commit), you need to leave a brief description of the changes that affected the version.

    - the description is arbitrary
    - at the end of the commit message (after an empty line), you need to put [git trailer](https://docs.gitlab.com/ee/user/project/changelogs.html) (`Changelog: <trailer>`) so that the message gets into `CHANGELOG.md `. Valid keywords are specified in `.gitlab/changelog_config.yml` in the 'categories' block (a description of the category is specified after the colon, which will be reflected in `CHANGELOG.md `)

    Example:
    >\<message>
    >
    >Changelog: feat
4. Merge
5. Pipeline CI/CD will collect the tagged commit messages and update `CHANGELOG.md `
6. The tag with the new version will be added automatically.
