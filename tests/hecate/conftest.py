import pytest
import os
import json
import pandas as pd
import xgboost as xgb
from sklearn.linear_model import Ridge

PATH_DIR = os.path.join(os.getcwd().split('tests')[0], 'tests', 'tests')
HECATE_DIR = os.path.join(PATH_DIR, 'hecate')
OUTLIERS_DIR = os.path.join(PATH_DIR, 'outliers')
ROLLING_DIR = os.path.join(PATH_DIR, 'add_rolling')
PRICES_DIR = os.path.join(PATH_DIR, 'prices')
REPARTITIONS_DIR = os.path.join(PATH_DIR, 'repartitions')
PROFILE_DIR = os.path.join(PATH_DIR, 'add_profile')
PI_DIR = os.path.join(PATH_DIR, 'prediction_interval')


@pytest.fixture(params=os.listdir(HECATE_DIR), ids=os.listdir(HECATE_DIR))
def hecate_input_df(request):
    """
    Read data from the folder.
    """
    return get_data(os.path.join(HECATE_DIR, request.param))


@pytest.fixture(params=os.listdir(OUTLIERS_DIR), ids=os.listdir(OUTLIERS_DIR))
def outliers_input_df(request):
    """
    Read data from the folder.
    """
    return get_data(os.path.join(OUTLIERS_DIR, request.param))


@pytest.fixture(params=os.listdir(ROLLING_DIR), ids=os.listdir(ROLLING_DIR))
def rolling_input_df(request):
    """
    Read data from the folder.
    """
    return get_data(os.path.join(ROLLING_DIR, request.param))


@pytest.fixture(params=os.listdir(PRICES_DIR), ids=os.listdir(PRICES_DIR))
def prices_input_df(request):
    """
    Read data from the folder.
    """
    return get_data(os.path.join(PRICES_DIR, request.param))


@pytest.fixture(params=os.listdir(REPARTITIONS_DIR), ids=os.listdir(REPARTITIONS_DIR))
def repartitions_input_df(request):
    """
    Read data from the folder.
    """
    return get_data(os.path.join(REPARTITIONS_DIR, request.param))


@pytest.fixture(params=os.listdir(PROFILE_DIR), ids=os.listdir(PROFILE_DIR))
def profile_df(request):
    """
    Read data from the folder.
    """
    return get_data(os.path.join(PROFILE_DIR, request.param))


@pytest.fixture(params=os.listdir(PI_DIR), ids=os.listdir(PI_DIR))
def pi_input_df(request):
    """
    Read data from the folder.
    """
    return get_data(os.path.join(PI_DIR, request.param))


def get_data(abs_path_to_folder):
    """
    Get test from file. Tests` directory is 'tests'.
    Each test save in separate directory.
    Name of directory is the future name of test.
    Name of data file: data.csv.
    Name of dictionary file: dict.json.
    Name of result file: result.csv (default)
    """
    data = pd.read_csv(os.path.join(abs_path_to_folder, 'data.csv'),
                       parse_dates=['date'])
    try:
        with open(os.path.join(abs_path_to_folder, 'dict.json'), 'r') as file:
            feature_types = json.load(file)
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        feature_types = {}
    try:
        result = pd.read_csv(os.path.join(abs_path_to_folder, 'result.csv'))
    except (FileNotFoundError, pd.errors.EmptyDataError):
        result = pd.DataFrame()
    return data, feature_types, result


@pytest.fixture
def required_cols():
    return ['key', 'date', 'target']


@pytest.fixture
def columns_format():
    return ['column', 'type', 'subtype']


@pytest.fixture
def forecast_column():
    return 'fc'


@pytest.fixture
def date_column():
    return 'date'


@pytest.fixture(params=[['year', 'month', 'weekofyear'],
                        ['year', 'weekofyear', 'weekofmonth', 'dayofweek']])
def time_features(request):
    return request.param


@pytest.fixture(params=[['discount_promo', 'month', 'week']])
def promo_features(request):
    return request.param


@pytest.fixture(params=[('Ridge',
                         {'solver': ['svd', 'cholesky'],
                          'alpha': [0.1, 0.5, 1]},
                         Ridge,
                         ['column1', 'column2', 'column3'],
                         ['column1'],
                         ['column3']),
                        ('XGB',
                         {'eta': [0.1, 0.3],
                          'max_depth': [1, 2, 4]},
                         xgb.XGBRegressor,
                         ['col1'],
                         [],
                         ['col1'])])
def model_items(request):
    return request.param


@pytest.fixture
def mock_dev_env(monkeypatch):
    monkeypatch.setenv("HECATE_ENV", "dev")


@pytest.fixture
def mock_prod_env(monkeypatch):
    monkeypatch.setenv("HECATE_ENV", "prod")


@pytest.fixture(params=[None,
                        ['bl'],
                        ['bl', 'cn'],
                        ['cn']])
def drivers(request):
    return request.param
