import pytest
import os
import pandas as pd
import json
import sqlite3
from metriclog.database import Database

PATH_DIR = os.path.join(os.getcwd().split('tests')[0], 'tests', 'metriclog/tests')
TEST_PATHS = [f for f in os.listdir(PATH_DIR) if not f.startswith('.')]
AB_DIR = os.path.join(PATH_DIR, 'ab')


@pytest.fixture()
def session():
    connection = sqlite3.connect(':memory:')
    cursor = connection.cursor()
    cursor.execute("CREATE TABLE folds ("
                   "project VARCHAR NOT NULL, "
                   "horizon VARCHAR NOT NULL, "
                   "fold smallint NOT NULL, "
                   "forecast_start date NOT NULL, "
                   "forecast_end date NOT NULL);")
    cursor.execute('INSERT INTO folds VALUES ("apricot", "mt", 0, "2021-11-22", "2021-12-31")')
    cursor.execute('INSERT INTO folds VALUES ("apricot", "st", 1, "2021-11-22", "2021-12-31")')
    cursor.execute('INSERT INTO folds VALUES ("wings", "st", 0, "2021-11-22", "2021-12-31")')
    cursor.execute('INSERT INTO folds VALUES ("wings", "st", 0, "2021-11-23", "2021-12-23")')
    cursor.execute('INSERT INTO folds VALUES ("test", "lt", 1, "2022-05-01", "2022-12-31")')
    cursor.execute("CREATE TABLE wings ("
                   "key VARCHAR ( 30 ) NOT NULL, "
                   "date date NOT NULL, "
                   "partition PARTITION NOT NULL, "
                   "target FLOAT4 NOT NULL, "
                   "time_freq char NOT NULL, "
                   "focus smallint NOT NULL, "
                   "project PROJECT NOT NULL);")
    cursor.execute('INSERT INTO wings VALUES (1,"2019-01-01", 299007.55, "train", "D", 1, "wings")')
    cursor.execute('INSERT INTO wings VALUES (2,"2019-01-01", 299007.55, "train", "D", 0, "wings")')
    cursor.execute('INSERT INTO wings VALUES (3,"2019-01-01", 299007.55, "train", "D", 1, "wings")')
    cursor.execute("CREATE TABLE feature_types ("
                   "feature VARCHAR NOT NULL, "
                   "project VARCHAR NOT NULL, "
                   "type VARCHAR NOT NULL);")
    cursor.execute('INSERT INTO feature_types VALUES ("key", "wings", "dim")')
    cursor.execute('INSERT INTO feature_types VALUES ("date", "wings", "dim")')
    cursor.execute('INSERT INTO feature_types VALUES ("category", "apricot", "dim")')
    cursor.execute("CREATE TABLE test_table ("
                   "Name VARCHAR NOT NULL, "
                   "Age smallint NOT NULL);")
    cursor.execute("CREATE TABLE results_wings ("
                   "key VARCHAR ( 30 ) NOT NULL, "
                   "date date NOT NULL, "
                   "fc FLOAT4 NOT NULL, "
                   "target FLOAT4 NOT NULL, "
                   "focus boolean NOT NULL, "
                   "fold smallint NOT NULL, "
                   "horizon VARCHAR NOT NULL, "
                   "commit_id VARCHAR NOT NULL, "
                   "branch VARCHAR NOT NULL, "
                   "project PROJECT NOT NULL);")
    cursor.execute('INSERT INTO results_wings VALUES (1,"2019-01-01", 1, 1, False, 0, "mt", "id_1", "branch", "wings")')
    cursor.execute('INSERT INTO results_wings VALUES (1,"2019-01-01", 1, 1, False, 0, "mt", "id_2", "branch", "wings")')
    cursor.execute('INSERT INTO results_wings VALUES (1,"2019-01-01", 1, 1, False, 0, "st", "id_3", "branch", "wings")')
    cursor.execute("CREATE TABLE ab_results_test ("
                   "project VARCHAR NOT NULL, "
                   "horizon VARCHAR NOT NULL, "
                   "focus boolean NOT NULL, "
                   "fold smallint NOT NULL, "
                   "commit_a_id VARCHAR NOT NULL, "
                   "branch_a VARCHAR NOT NULL, "
                   "commit_b_id VARCHAR NOT NULL, "
                   "branch_b VARCHAR NOT NULL, "
                   "key VARCHAR NOT NULL, "
                   "result VARCHAR NOT NULL, "
                   "effect_abs FLOAT4 NOT NULL, "
                   "effect_percent FLOAT4 NOT NULL, "
                   "ci_abs_lower FLOAT4 NOT NULL, "
                   "ci_abs_upper FLOAT4 NOT NULL, "
                   "ci_percent_lower FLOAT4 NOT NULL, "
                   "ci_percent_upper FLOAT4 NOT NULL, "
                   "distribution VARCHAR NOT NULL, "
                   "mde FLOAT4 NOT NULL, "
                   "weight FLOAT4 NOT NULL);")
    cursor.execute('INSERT INTO ab_results_test VALUES ("test", "mt", False, 1, "commit_a_test", '
                   '"branch_a_test", "commit_b_test", "branch_b_test", "test_key", "A won", 1, 100, 0.5, 1.5, 50, 150, '
                   '"norm", 50, 1.0)')
    cursor.execute('INSERT INTO ab_results_test VALUES ("test", "mt", True, 1, "commit_a_test", '
                   '"branch_a_test", "commit_b_test", "branch_b_test", "test_key", "A won", 1.2, 120, 0.8, 1.6, 80, '
                   '160, "norm", 40, 1.0)')
    cursor.execute('INSERT INTO ab_results_test VALUES ("test", "st", False, 1, "commit_a_test", '
                   '"branch_a_test", "commit_b_test", "branch_b_test", "test_key", "A won", 1, 100, 0.3, 1.8, 30, 180, '
                   '"undefined", 80, 1.0)')
    cursor.execute('INSERT INTO ab_results_test VALUES ("test", "st", False, 2, "commit_a_test", '
                   '"branch_a_test", "commit_b_test", "branch_b_test", "test_key", "A won", 2.104, 210.4, 1.5, 2.7, '
                   '150, 270, "undefined", 60, 1.0)')
    cursor.execute("CREATE TABLE test ("
                   "key VARCHAR ( 30 ) NOT NULL, "
                   "date date NOT NULL, "
                   "fc FLOAT4 NOT NULL, "
                   "target FLOAT4 NOT NULL, "
                   "metrics FLOAT4 NOT NULL, "
                   "focus boolean NOT NULL, "
                   "fold smallint NOT NULL, "
                   "horizon VARCHAR NOT NULL, "
                   "commit_id VARCHAR NOT NULL, "
                   "branch VARCHAR NOT NULL, "
                   "product_group VARCHAR NOT NULL, "
                   "product_category VARCHAR NOT NULL, "
                   "partition VARCHAR NOT NULL, "
                   "project PROJECT NOT NULL);")
    cursor.execute("CREATE TABLE results_test ("
                   "key VARCHAR ( 30 ) NOT NULL, "
                   "date date NOT NULL, "
                   "fc FLOAT4 NOT NULL, "
                   "target FLOAT4 NOT NULL, "
                   "metrics FLOAT4 NOT NULL, "
                   "focus boolean NOT NULL, "
                   "fold smallint NOT NULL, "
                   "horizon VARCHAR NOT NULL, "
                   "commit_id VARCHAR NOT NULL, "
                   "branch VARCHAR NOT NULL, "
                   "product_group VARCHAR NOT NULL, "
                   "product_category VARCHAR NOT NULL, "
                   "partition VARCHAR NOT NULL, "
                   "project PROJECT NOT NULL);")
    connection.commit()
    cursor.close()
    yield connection
    connection.close()


class TestDatabase(Database):
    def __init__(self, session):
        self.conn = session
        self.cursor = self.conn.cursor()
        self.attempts_limit = 3

    def send_chunk(self, df: pd.DataFrame, table: str) -> bool:
        """
        Method loads tuples made of input dataframe rows as a batch to the database.
        Replaces send_chunk in the Database class to overcome difference in execute_many method
        :param df: dataframe to load
        :param table: name of table where the dataframe should be put

        :return: True if the load failed
        """
        tuples = [tuple(x) for x in df.to_numpy()]
        cols = ','.join(list(df.columns))
        n_cols = df.shape[1]
        query_start = "INSERT INTO %s(%s) VALUES(?" + ",?" * (n_cols - 1) + ");"
        query = query_start % (table, cols)
        self.cursor.executemany(query, tuples)
        return False

    def __del__(self):
        pass


@pytest.fixture()
def db_connection(session):
    return TestDatabase(session)


@pytest.fixture()
def partition_column():
    return 'partition'


@pytest.fixture()
def input_from_db():
    data = pd.read_csv(os.path.join(PATH_DIR, 'data_apricot.csv'), parse_dates=['date'])
    return data


@pytest.fixture()
def horizon_and_folds():
    horizon_and_folds_data = pd.read_csv(os.path.join(PATH_DIR, 'horizon_and_folds.csv'),
                                         parse_dates=['forecast_start', 'forecast_end'])
    return horizon_and_folds_data


@pytest.fixture()
def feature_types():
    with open(os.path.join(PATH_DIR, 'feature_types.json'), 'r') as file:
        feature_types = json.load(file)
    return feature_types


@pytest.fixture()
def hecate_input():
    data = pd.read_csv(os.path.join(PATH_DIR, 'input_apricot.csv'))
    return data


@pytest.fixture()
def hecate_output():
    data = pd.read_csv(os.path.join(PATH_DIR, 'results_apricot.csv'), parse_dates=['date'])
    return data


@pytest.fixture()
def final_table():
    data = pd.read_csv(os.path.join(PATH_DIR, 'final_table_apricot.csv'), parse_dates=['date'])
    return data


@pytest.fixture()
def final_table_columns():
    return {'key', 'date', 'fc', 'target', 'project', 'focus', 'fold', 'horizon', 'commit_id', 'branch'}


@pytest.fixture()
def ab_results_test():
    """
    Read AB_results_test report from the folder.
    """
    ab_results = pd.read_csv(os.path.join(AB_DIR, 'ab_results_test.csv'), index_col=0)

    return ab_results


@pytest.fixture()
def ab_datasets():
    """
    Read datasets for AB test from the folder.
    """
    data_a = pd.read_csv(os.path.join(AB_DIR, 'ab_data_a.csv'), index_col=0)
    data_b = pd.read_csv(os.path.join(AB_DIR, 'ab_data_b.csv'), index_col=0)

    return data_a, data_b


@pytest.fixture()
def ab_summary_test():
    """
    Read AB_summary report from the folder.
    """
    ab_summary = pd.read_csv(os.path.join(AB_DIR, 'ab_summary.csv'), index_col=0)

    return ab_summary
