from enum import Enum
from typing import TypeAlias, NamedTuple, final
from copy import deepcopy

import pandas as pd

from hecate.metrics.custom import fa, fb
from hecate.forecasting.utils import EnvVar, HorizonMixin
from hecate.forecasting.decompositions.decomposition import Decompositor, DummyDecompositor
from hecate.forecasting.decompositions.strategies import DecompositionStrategies
from hecate.forecasting.features.generators.base import DummyFGenerator
from hecate.forecasting.features.generators.business import BusinessFGenerator
from hecate.forecasting.features.generators.target import TargetFGenerator
from hecate.forecasting.features.generators.time import TimeFGenerator
from hecate.forecasting.feature_selection.selection import FeatureSelector
from hecate.forecasting.feature_selection.base import DummyFeatureSelector
from hecate.forecasting.horizon import get_horizon_from_sample, Horizon
from hecate.forecasting.outliers.base import DummyOutlierMarkGenerator
from hecate.forecasting.outliers.target import TargetOutlierGenerator
from hecate.forecasting.postprocessings.base import DummyPostprocessor
from hecate.forecasting.postprocessings.fc import \
    FCAnnihilator, DummyFCAnnihilator, DummyFCRounder, SimpleRounder, RollingRounder
from hecate.forecasting.repartitions.base import DummyRepartitionExecutor
from hecate.forecasting.repartitions.repartition import RepartitionExecutor
from hecate.constants import TIME_FEATURES_COLS, REQUIRED_COLS
from hecate.features import FeatureSet, FeatureType, TypesOfFeatures, get_cols_from_types
from hecate.forecasting.trend_generator.trend import TrendGenerator, DummyTrendGenerator
from hecate.forecasting.models.base import DummyForecastingMachine
from hecate.forecasting.models.model_selection import ForecastingMachine
from hecate.types import Drivers


@final
class StepName(str, Enum):
    PREPROCESSOR = 'preprocessor'
    FORECASTER = 'forecaster'
    POSTPROCESSOR = 'postprocessor'


PreprocessingStepExecutor: TypeAlias = TargetOutlierGenerator | RepartitionExecutor | TrendGenerator \
                                       | BusinessFGenerator | TimeFGenerator | TargetFGenerator | FeatureSelector
ForecastingStepExecutor: TypeAlias = ForecastingMachine | Decompositor
PostprocessingStepExecutor: TypeAlias = FCAnnihilator | SimpleRounder | RollingRounder


@final
class PreprocessingStepName(str, Enum):
    TARGET_OUTLIER_GENERATOR = 'target_outlier_generator'
    REPARTITION_EXECUTOR = 'repartition_executor'
    TREND_GENERATOR = 'trend_generator'
    BUSINESS_FEATURE_GENERATOR = 'business_feature_generator'
    TIME_FEATURE_GENERATOR = 'time_feature_generator'
    TARGET_FEATURE_GENERATOR = 'target_feature_generator'
    FEATURE_SELECTOR = 'feature_selector'


@final
class ForecastingStepName(str, Enum):
    MODEL = 'model'
    DECOMPOSITOR = 'decompositor'


@final
class PostprocessingStepName(str, Enum):
    FC_ANNIHILATOR = 'fc_annihilator'
    FC_SIMPLE_ROUNDER = 'fc_simple_rounder'
    FC_ROLLING_ROUNDER = 'fc_rolling_rounder'


@final
class TotalByPartition(NamedTuple):
    train_target: float
    train_fc: float
    fc_target: float
    fc_fc: float


@final
class MetricsByPartition(NamedTuple):
    train_fa: float
    train_fb: float
    fc_fa: float
    fc_fb: float


@final
class FCSampleResult:
    """
    Class implement to store forecasting result.
    """
    def __init__(self,
                 key: str,
                 data: pd.DataFrame | None = None,
                 output_columns: list[str] | None = None,
                 feature_importance: dict | None = None,
                 is_failed: bool = False,
                 trace: str | None = None) -> None:
        """
        Construct object that will be used for collecting result and meta info about forecasting one sample.

        :param key: string that allow to identify time series id
        :param data: pd.DataFrame with forecast (and forecast decomposition)
        :param output_columns: list of columns that contain fc result. Example: ['fc', 'bl', 'uplift']
        :param feature_importance: feature importance dictionary
        :param is_failed: mark if forecasting failed or not
        :param trace: string that contain information about exception's traceback
        """
        if is_failed and trace is None:
            raise ValueError('traceback must be passed if task was failed')

        self.is_failed = is_failed
        self.key = key
        self.traceback = trace
        self.data = pd.DataFrame() if data is None else data
        self.output_columns = ['fc'] if output_columns is None else output_columns

        self.total_from_partition = None
        self.metrics = None
        self.feature_importance = feature_importance

    def fillup_metrics(self) -> None:
        if not self.is_failed:
            train_part = self.data[self.data.partition == 'train']
            fc_part = self.data[self.data.partition == 'fc']

            self.total_from_partition = TotalByPartition(train_part.target.sum(),
                                                         train_part.fc.sum(),
                                                         fc_part.target.sum(),
                                                         fc_part.fc.sum())

            self.metrics = MetricsByPartition(fa(train_part.target, train_part.fc),
                                              fb(train_part.target, train_part.fc),
                                              fa(fc_part.target, fc_part.fc),
                                              fb(fc_part.target, fc_part.fc))


class DummyPreprocessor:
    def execute(self, data: pd.DataFrame, horizon: Horizon, feature_types: TypesOfFeatures, features: list[str]
                ) -> tuple[pd.DataFrame, FeatureSet]:
        data['trend'] = 0
        feature_set = FeatureSet()
        feature_set.feature_types = feature_types
        feature_set.features = features
        feature_set.added_business_features = []
        feature_set.time_features = []
        feature_set.added_target_features = []
        feature_set.period_features = []
        feature_set.selected_features = deepcopy(features)
        feature_set.label_features = get_cols_from_types(feature_types, [FeatureType.label])
        promo_feature_types = [FeatureType.discount, FeatureType.promo, FeatureType.promo_budget]
        promo_features = get_cols_from_types(feature_types, promo_feature_types)
        feature_set.promo_features = promo_features

        return data, feature_set


class PreprocessingStep:
    """
    Class implement management of execution separated step during forecasting.
    """

    def __init__(self, name: PreprocessingStepName, executor: PreprocessingStepExecutor, is_on: bool = True) -> None:
        self.name: PreprocessingStepName = name
        self.executor: PreprocessingStepExecutor = executor
        self.dummy_executor: DummyOutlierMarkGenerator | DummyRepartitionExecutor | DummyTrendGenerator \
            | DummyFGenerator | DummyFeatureSelector

        match self.executor:
            case TargetOutlierGenerator():
                self.dummy_executor = DummyOutlierMarkGenerator()
            case RepartitionExecutor():
                self.dummy_executor = DummyRepartitionExecutor()
            case TrendGenerator():
                self.dummy_executor = DummyTrendGenerator()
            case BusinessFGenerator() | TimeFGenerator() | TargetFGenerator():
                self.dummy_executor = DummyFGenerator()
            case FeatureSelector():
                self.dummy_executor = DummyFeatureSelector()
            case _:
                raise ValueError(f'Unknown executor type found {type(self.executor)}: impossible to set dammy executor')

        self.is_on: bool = is_on

    def get_executor(self):
        return self.executor if self.is_on else self.dummy_executor


@final
class PreprocessorConfig:

    def __init__(self, toggles: dict[PreprocessingStepName, bool]) -> None:
        """
        :param toggles: Dictionary for disabling individual preprocessing steps
            Names of preprocessing steps: 'target_outlier_generator', 'repartition_executor',
                'trend_generator', 'business_feature_generator', 'time_feature_generator',
                'target_feature_generator', 'feature_selector'.
        """
        unknown_steps = set(toggles.keys()).difference(PreprocessingStepName)
        if unknown_steps:
            raise ValueError(f'unknown steps found in toggles: {unknown_steps}')

        self.toggles = toggles


class Preprocessor(DummyPreprocessor):
    """
    Class implement a logic of preprocessing input data.
    All needed steps of repartition, data transformation and feature generation must be placed in the execute method
    """

    def __init__(self, config: PreprocessorConfig | None = None):
        _PreSN = PreprocessingStepName

        step_from_name: dict[PreprocessingStepName, PreprocessingStep] = {
            _PreSN.TARGET_OUTLIER_GENERATOR: PreprocessingStep(
                name=_PreSN.TARGET_OUTLIER_GENERATOR, executor=TargetOutlierGenerator()),
            _PreSN.REPARTITION_EXECUTOR: PreprocessingStep(
                name=_PreSN.REPARTITION_EXECUTOR, executor=RepartitionExecutor()),
            _PreSN.TREND_GENERATOR: PreprocessingStep(
                name=_PreSN.TREND_GENERATOR, executor=TrendGenerator()),
            _PreSN.BUSINESS_FEATURE_GENERATOR: PreprocessingStep(
                name=_PreSN.BUSINESS_FEATURE_GENERATOR, executor=BusinessFGenerator()),
            _PreSN.TIME_FEATURE_GENERATOR: PreprocessingStep(
                name=_PreSN.TIME_FEATURE_GENERATOR, executor=TimeFGenerator()),
            _PreSN.TARGET_FEATURE_GENERATOR: PreprocessingStep(
                name=_PreSN.TARGET_FEATURE_GENERATOR, executor=TargetFGenerator()),
            _PreSN.FEATURE_SELECTOR: PreprocessingStep(
                name=_PreSN.FEATURE_SELECTOR, executor=FeatureSelector()),
        }

        if config is not None:
            for step_name, is_on in config.toggles.items():
                step_from_name[step_name].is_on = is_on
        self.config = config

        self.target_outlier_generator = step_from_name[_PreSN.TARGET_OUTLIER_GENERATOR].get_executor()
        self.trend_generator = step_from_name[_PreSN.TREND_GENERATOR].get_executor()
        self.repartition_executor = step_from_name[_PreSN.REPARTITION_EXECUTOR].get_executor()
        self.business_feature_generator = step_from_name[_PreSN.BUSINESS_FEATURE_GENERATOR].get_executor()
        self.time_feature_generator = step_from_name[_PreSN.TIME_FEATURE_GENERATOR].get_executor()
        self.target_feature_generator = step_from_name[_PreSN.TARGET_FEATURE_GENERATOR].get_executor()
        self.feature_selector = step_from_name[_PreSN.FEATURE_SELECTOR].get_executor()

    @staticmethod
    def _set_horizon(preprocessing_step: HorizonMixin, horizon: Horizon) -> None:
        """
        Set horizon to a specified value for a passed preprocessing step.
        :param preprocessing_step: PreprocessingStep class instance to set horizon for. Supposed to have 'horizon' as
        an attribute
        :param horizon: horizon as a string
        :return: None
        """
        preprocessing_step.set_horizon(horizon)

    def execute(self, data: pd.DataFrame, horizon: Horizon, feature_types: TypesOfFeatures, features: list[str]
                ) -> tuple[pd.DataFrame, FeatureSet]:
        """
        Method collect logic of data preprocessing and feature engineering for sample.

        :param data: pandas dataframe with data
        :param horizon:
        :param feature_types:
        :param features:
        :return: (pd.DataFrame, FeatureSet)
        """
        # idea: combine feature_types and features to pass one argument

        horizon_dependent_steps = [step for step in self.__dict__.values() if isinstance(step, HorizonMixin)]
        for step in horizon_dependent_steps:
            self._set_horizon(step, horizon)

        # - outliers
        data, period_features = self.target_outlier_generator.add_outlier_marks(data, feature_types=feature_types)
        # preprocessing
        self.repartition_executor.update_partition(data, feature_types)
        # trend
        self.trend_generator.add_trend(data, feature_types=feature_types)

        # features generation:
        # - business feature
        data, added_business_features = self.business_feature_generator.add_features(data, feature_types=feature_types)
        # data, business_features = feature_generation.add_business_features(data, feature_types)
        business_features = features + added_business_features
        # - time feature
        data, time_features = self.time_feature_generator.add_features(data, time_features=list(TIME_FEATURES_COLS))

        # target features
        data, added_target_features = self.target_feature_generator.add_features(data, feature_types=feature_types)

        # features selection
        # TODO: move to FeatureSelector object
        selected_features = self.feature_selector.drop_features(data, business_features, time_features, period_features)
        selected_features.sort()

        feature_set = FeatureSet()
        feature_set.feature_types = feature_types
        feature_set.features = features
        feature_set.added_business_features = added_business_features
        feature_set.time_features = time_features
        feature_set.added_target_features = added_target_features
        feature_set.period_features = period_features
        feature_set.selected_features = selected_features
        feature_set.label_features = get_cols_from_types(feature_types, [FeatureType.label])

        promo_feature_types = [FeatureType.discount, FeatureType.promo, FeatureType.promo_budget]
        promo_features = get_cols_from_types(feature_types, promo_feature_types)
        feature_set.promo_features = promo_features

        return data, feature_set


class ForecastingStep:
    """
    Class implement management of execution separated step during forecasting.
    """

    def __init__(self, name: str, executor: ForecastingStepExecutor, is_on: bool = True) -> None:
        self.name: str = name
        self.executor: ForecastingStepExecutor = executor
        self.dummy_executor: DummyForecastingMachine | DummyDecompositor

        match self.executor:
            case ForecastingMachine():
                self.dummy_executor = DummyForecastingMachine()
            case Decompositor():
                self.dummy_executor = DummyDecompositor()
            case _:
                raise ValueError(f'Unknown executor type found {type(self.executor)}: impossible to set dummy executor')

        self.is_on: bool = is_on

    def get_executor(self):
        return self.executor if self.is_on else self.dummy_executor


@final
class ForecasterConfig:

    def __init__(self, toggles: dict[ForecastingStepName, bool]) -> None:
        """
        :param toggles: dictionary for disabling individual forecasting steps
            Names of forecasting steps: 'model', 'decompositor'.
        """
        unknown_steps = set(toggles.keys()).difference(ForecastingStepName)
        if unknown_steps:
            raise ValueError(f'unknown steps found in toggles: {unknown_steps}')

        self.toggles = toggles


class DummyForecaster:
    def execute(self, data: pd.DataFrame, horizon: Horizon, feature_set: FeatureSet,
                drivers: Drivers | None = None,
                dsd: DecompositionStrategies | None = None) -> FCSampleResult:
        data['fc'] = 0
        output_cols = ['key', 'date', 'fc']
        return FCSampleResult(key=data.key.values[0], data=data, output_columns=output_cols)


class Forecaster(DummyForecaster):

    def __init__(self, config: ForecasterConfig | None = None) -> None:

        _FcSN = ForecastingStepName

        step_from_name: dict[ForecastingStepName, ForecastingStep] = {
            _FcSN.MODEL: ForecastingStep(name=_FcSN.MODEL, executor=ForecastingMachine()),
            _FcSN.DECOMPOSITOR: ForecastingStep(name=_FcSN.DECOMPOSITOR, executor=Decompositor())
        }

        if config is not None:
            for step_name, is_on in config.toggles.items():
                step_from_name[step_name].is_on = is_on

        self.model = step_from_name[_FcSN.MODEL].get_executor()
        self.decompositor = step_from_name[_FcSN.DECOMPOSITOR].get_executor()
        self.config = config

    def execute(self,
                data: pd.DataFrame,
                horizon: Horizon,
                feature_set: FeatureSet,
                drivers: Drivers | None = None,
                dsd: DecompositionStrategies | None = None) -> FCSampleResult:
        """
        Executes forecaster.

        :param data: initial dataframe
        :param horizon: forecasting horizon
        :param feature_set: instance of FeatureSet class, containing lists of features of different type
        :param drivers: list of drivers for decomposition
        :param dsd: short for Decomposition Strategy Dictionary: dict for methods to use to calculate various figures

        :return: instance of FCSampleResult class
        """
        # model run
        model_out = self.model.forecast(data, horizon, feature_set)
        data['fc'] = model_out.prediction

        # decompositor run
        add_output_cols = self.decompositor.decompose(data, horizon, model_out.model, feature_set, drivers, dsd)
        output_cols = ['key', 'date', 'fc'] + add_output_cols

        # output
        out = FCSampleResult(key=data.key.values[0], data=data, output_columns=output_cols)
        if EnvVar.is_debug_environ():
            feat_imp = model_out.model.get_feature_importance()
            out.feature_importance = {k: v for k, v in zip(feature_set.selected_features, feat_imp)}
        return out


@final
class PostprocessorConfig:

    def __init__(self, toggles: dict[PostprocessingStepName, bool]):
        """
        :param toggles: Dictionary for disabling individual postprocessing steps
            Names of postprocessing steps: 'fc_annihilator'.
        """
        unknown_steps = set(toggles.keys()).difference(PostprocessingStepName)
        if unknown_steps:
            raise ValueError(f'unknown steps found in toggles: {unknown_steps}')

        self.toggles = toggles


class PostprocessingStep:

    def __init__(self, name: PostprocessingStepName, executor: PostprocessingStepExecutor, is_on: bool = True) -> None:
        self.name: PostprocessingStepName = name
        self.executor: PostprocessingStepExecutor = executor

        match self.executor:
            case FCAnnihilator():
                dummy_executor = DummyFCAnnihilator()
            case RollingRounder() | SimpleRounder():
                dummy_executor = DummyFCRounder()
            case _:
                raise ValueError(f'Unknown executor type found {type(self.executor)}: impossible to set dammy executor')

        self.dummy_executor = dummy_executor
        self.is_on: bool = is_on

    def get_executor(self):
        return self.executor if self.is_on else self.dummy_executor


class Postprocessor(DummyPostprocessor):
    """
    Class implement a logic of postprocessing input data.
    All needed logic of must be placed in the execute method.
    For example, logic of zeroing fc according to stop_fc column

    """

    def __init__(self, config: PostprocessorConfig | None = None) -> None:

        _PostSN = PostprocessingStepName

        step_from_name: dict[PostprocessingStepName, PostprocessingStep] = {
            _PostSN.FC_ANNIHILATOR: PostprocessingStep(name=_PostSN.FC_ANNIHILATOR, executor=FCAnnihilator()),
            _PostSN.FC_SIMPLE_ROUNDER: PostprocessingStep(name=_PostSN.FC_SIMPLE_ROUNDER, executor=SimpleRounder()),
            _PostSN.FC_ROLLING_ROUNDER: PostprocessingStep(name=_PostSN.FC_ROLLING_ROUNDER, executor=RollingRounder())
        }

        if config is not None:
            for step_name, is_on in config.toggles.items():
                step_from_name[step_name].is_on = is_on

        self.fc_annihilator = step_from_name[_PostSN.FC_ANNIHILATOR].get_executor()
        self.fc_simple_rounder = step_from_name[_PostSN.FC_SIMPLE_ROUNDER].get_executor()
        self.fc_rolling_rounder = step_from_name[_PostSN.FC_ROLLING_ROUNDER].get_executor()
        self.config = config

    def execute(self, data: pd.DataFrame, horizon: Horizon, output_columns: list[str],
                feature_types: TypesOfFeatures | None = None, decimals: int | None = 2) -> pd.DataFrame:
        """
        Method collect logic of postprocessing for sample.
        :param data: initial dataframe for postprocessing
        :param horizon: forecasting horizon
        :param output_columns: columns to add to the output dataframe apart from the mandatory ones
        :param feature_types: dictionary of feature types
        :param decimals: number of decimal places to round the output values to

        :return: dataframe after postprocessing
        """

        data = self.fc_rolling_rounder.postprocess(data=data, decimals=decimals)
        data = self.fc_simple_rounder.postprocess(data=data, decimals=decimals)
        self.fc_annihilator.horizon = horizon
        data = self.fc_annihilator.postprocess(data, output_columns, feature_types)

        return data


StepExecutor: TypeAlias = Preprocessor | Forecaster | Postprocessor
DummyStepExecutor: TypeAlias = DummyPreprocessor | DummyForecaster | DummyPostprocessor
StepConfig: TypeAlias = PreprocessorConfig | ForecasterConfig | PostprocessorConfig


class ForecastingTaskStep:
    """
    Class implement management of execution separated step during forecasting.
    """

    def __init__(self, name: StepName,
                 executor: StepExecutor,
                 is_on: bool = True) -> None:

        self.name: StepName = name
        self.executor: StepExecutor = executor
        self.dummy_executor: DummyStepExecutor

        match self.executor:
            case Preprocessor():
                self.dummy_executor = DummyPreprocessor()
            case Forecaster():
                self.dummy_executor = DummyForecaster()
            case Postprocessor():
                self.dummy_executor = DummyPostprocessor()
            case _:
                raise ValueError(f'Unknown executor type found {type(self.executor)}: impossible to set dammy executor')

        self.is_on: bool = is_on

    def get_executor(self):
        return self.executor if self.is_on else self.dummy_executor


@final
class ForecastingTaskConfig:

    def __init__(self, toggles: dict[StepName, bool] | None = None,
                 custom_executors: dict[StepName, StepExecutor] | None = None,
                 preprocessor_config: PreprocessorConfig | None = None,
                 forecaster_config: ForecasterConfig | None = None,
                 postprocessor_config: PostprocessorConfig | None = None,
                 ):
        is_step_configs = preprocessor_config is not None \
                           or forecaster_config is not None \
                           or postprocessor_config is not None

        if not is_step_configs and toggles is None and custom_executors is None:
            raise ValueError('toggles, custom_executors and every step config cant be None at same time')

        toggles = {} if toggles is None else toggles
        custom_executors = {} if custom_executors is None else custom_executors

        unknown_steps = set(toggles.keys()).difference(StepName)
        if unknown_steps:
            raise ValueError(f'unknown steps found in toggles: {unknown_steps}')

        unknown_steps = set(custom_executors.keys()).difference(StepName)
        if unknown_steps:
            raise ValueError(f'unknown steps found in updated_executors: {unknown_steps}')

        # impossible customize switched off step or set up another config
        switched_off_steps = (toggle for toggle, is_on in toggles.items() if not is_on)
        config_from_step: dict[StepName, StepConfig | None] = {
            StepName.PREPROCESSOR: preprocessor_config,
            StepName.FORECASTER: forecaster_config,
            StepName.POSTPROCESSOR: postprocessor_config,
        }
        for step in switched_off_steps:
            if step in custom_executors:
                raise ValueError(f'Step {step} was switched off in toggles, but custom_executor was found. ')
            if config_from_step[step] is not None:
                raise ValueError(f'Step {step} was switched off in toggles, but config was found')

        self.toggles = toggles
        self.updated_executors = custom_executors
        self.preprocessor_config = preprocessor_config
        self.forecaster_config = forecaster_config
        self.postprocessor_config = postprocessor_config


@final
class ForecastingTask:
    """
    Class ties every step of forecasting in one place.
    You need just create object and call execute method.
    """

    def __init__(self, config: ForecastingTaskConfig | None = None) -> None:
        if config is None:
            scfg_from_name = dict.fromkeys(StepName)
        else:
            scfg_from_name = {
                StepName.PREPROCESSOR: config.preprocessor_config,
                StepName.FORECASTER: config.forecaster_config,
                StepName.POSTPROCESSOR: config.postprocessor_config,
            }

        step_from_name: dict[StepName, ForecastingTaskStep] = {
            StepName.PREPROCESSOR: ForecastingTaskStep(
                name=StepName.PREPROCESSOR, executor=Preprocessor(config=scfg_from_name[StepName.PREPROCESSOR])),
            StepName.FORECASTER: ForecastingTaskStep(
                name=StepName.FORECASTER, executor=Forecaster(config=scfg_from_name[StepName.FORECASTER])),
            StepName.POSTPROCESSOR: ForecastingTaskStep(
                name=StepName.POSTPROCESSOR,
                executor=Postprocessor(config=scfg_from_name[StepName.POSTPROCESSOR])),
        }

        if config is not None:
            for step_name, is_on in config.toggles.items():
                step_from_name[step_name].is_on = is_on

            for step_name, custom_executor in config.updated_executors.items():
                step_from_name[step_name] = ForecastingTaskStep(step_name, executor=custom_executor)

        self.preprocessor = step_from_name[StepName.PREPROCESSOR].get_executor()
        self.forecaster = step_from_name[StepName.FORECASTER].get_executor()
        self.postprocessor = step_from_name[StepName.POSTPROCESSOR].get_executor()

    def execute(self,
                data: pd.DataFrame,
                feature_types: TypesOfFeatures,
                drivers: Drivers | None = None,
                dsd: DecompositionStrategies | None = None,
                add_columns: list[str] | None = None,
                decimals: int | None = 2
                ) -> FCSampleResult:
        """
        Executes forecasting task.

        :param add_columns: additional columns to return in result
        :param data: initial data
        :param feature_types: dictionary with feature types
        :param drivers: list of drivers for decomposition
        :param dsd: short for Decomposition Strategy Dictionary: dict for methods to use to calculate various figures
        :param decimals: number of decimal places for output data. Without rounding - None

        :return: FCSampleResult instance
        """

        features = [col for col in data.columns if all([col not in REQUIRED_COLS,
                                                        feature_types.get(col, '') != FeatureType.dim])]
        horizon = get_horizon_from_sample(data)

        data, feature_set = self.preprocessor.execute(data,
                                                      horizon=horizon,
                                                      feature_types=feature_types,
                                                      features=features)
        fc_result = self.forecaster.execute(data=data,
                                            horizon=horizon,
                                            feature_set=feature_set,
                                            drivers=drivers,
                                            dsd=dsd)

        fc_result.data = self.postprocessor.execute(data=data,
                                                    horizon=horizon,
                                                    output_columns=fc_result.output_columns,
                                                    feature_types=feature_types,
                                                    decimals=decimals)
        if EnvVar.is_debug_environ():
            fc_result.fillup_metrics()
        else:
            out_cols = fc_result.output_columns
            if add_columns:
                out_cols += add_columns
            fc_result.data = fc_result.data[out_cols]

        return fc_result
