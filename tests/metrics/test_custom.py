from hecate.metrics import custom
import pandas as pd


def test_fa():
    df = pd.DataFrame({'date': pd.date_range('2020-01-01', '2021-01-01')})

    df['target'] = 0
    df['fc'] = 0
    assert custom.fa(df['target'], df['fc']) == 1, \
        "Error with fa calculation: fc equals target, " \
        "but fa doesn't equal 1"

    df['target'] = 0
    df['fc'] = 1
    assert custom.fa(df['target'], df['fc']) == 0, \
        "Error with fa calculation: fc equals 1, " \
        "target equals 0, but fa doesn't equal 0"

    df['target'] = 2
    df['fc'] = 1
    assert custom.fa(df['target'], df['fc']) == 0.5, \
        "Error with fa calculation: fc equals 1, " \
        "target equals 2, but fa doesn't equal 0.5"


def test_fb():
    df = pd.DataFrame({'date': pd.date_range('2020-01-01', '2021-01-01')})

    df['target'] = 0
    df['fc'] = 0
    assert custom.fb(df['target'], df['fc']) == 0, \
        "Error with fb calculation: fc equals target, " \
        "but fb doesn't equal 0"

    df['target'] = 0
    df['fc'] = 1
    assert custom.fb(df['target'], df['fc']) == -1, \
        "Error with fb calculation: fc equals 1, " \
        "target equals 0, but fb doesn't equal -1"

    df['target'] = 2
    df['fc'] = 1
    assert custom.fb(df['target'], df['fc']) == 1, \
        "Error with fb calculation: fc equals 1, " \
        "target equals 2, but fb doesn't equal 1"
