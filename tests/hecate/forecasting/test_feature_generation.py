import pandas as pd
import numpy as np

from hecate.forecasting import feature_generation
import pytest


def create_template_for_test_df(full: bool = False):
    data = pd.DataFrame({'key': ['key1']*30,
                         'date': pd.date_range(start="2021-09-09", end="2021-10-08"),
                         'partition': ['train']*20 + ['fc']*10,
                         'target': [336.,   0.,   300.,   0.,   200.,   0.]*5
                         })
    if not full:
        data = data.iloc[16:22]
    return data


def test_add_time_features(hecate_input_df, time_features):
    data, feature_types, result = hecate_input_df
    time_features = [f"time-{feature}" for feature in time_features]
    data_output, cols = feature_generation.add_time_features(data.copy(), time_features)

    # empty output
    assert len(data_output) != 0
    # generate all time features
    assert set(data_output.columns) - set(data.columns) == set(time_features)


def test_add_promo_period_num(hecate_input_df):
    data, feature_types, result = hecate_input_df
    col = 'business-discount_promo'
    if col in data.columns:
        data, cols = feature_generation.add_promo_period_num(data.copy(), col)
        assert data[data['business-discount_promo'] == 0].equals(data[data[f'{col}_num'] == 0])
        assert data[data['business-discount_promo'] != 0].equals(data[data[f'{col}_num'] != 0])


def test_add_profile(hecate_input_df):
    data, feature_types, result = hecate_input_df
    col = 'business-discount_promo'
    if col in data.columns:
        data, cols = feature_generation.add_profile(data, col, profile_type='weighted')
        assert (set(data[data[col] == 0]['date']) - set(data[data[f'business-{col}_profile'] == 0]['date'])) == set()
        assert 0 not in data[data[f'business-{col}_profile'] > 0][col].unique()


@pytest.mark.parametrize("profile_type", ['mean', 'weighted'])
def test_add_profile_saving_order(profile_df: tuple[pd.DataFrame, dict, pd.DataFrame], profile_type: str) -> None:
    df, _, _ = profile_df
    df.sort_values('date', inplace=True)
    df, _ = feature_generation.add_profile(df, 'discount', profile_type=profile_type)
    assert df.date.is_monotonic_increasing


def test_add_promo_features(hecate_input_df):
    data, feature_types, result = hecate_input_df
    col = 'business-discount_promo'
    if col in data.columns:
        data, cols = feature_generation.add_promo_feature(data.copy(), col)
        assert 'business-discount_promo_profile' in list(data.columns)
        assert len(data[data['business-discount_promo_profile'].isna()]) == 0


def test_add_business_features(hecate_input_df):
    data, feature_types, result = hecate_input_df
    col = 'business-discount_promo'
    if col in data.columns:
        data, cols = feature_generation.add_business_features(data, feature_types, pd.infer_freq(data.date))
        assert 'business-discount_promo_profile' in list(data.columns)
        assert 'business-discount_promo_profile' in cols
        assert len(data[data['business-discount_promo_profile'].isna()]) == 0


def test_add_oos(hecate_input_df):
    data, feature_types, result = hecate_input_df
    col_stock = 'stock'
    exp_added_col = 'stock-oos'
    if col_stock in data.columns:
        data, col = feature_generation.add_oos(data, col_stock)
        assert exp_added_col in list(data.columns)
        assert [exp_added_col] == col
        assert len(data[data[exp_added_col].isna()]) == 0
        assert len(data[(data[col_stock] == 0) & (data['target'] == 0) & (data['partition'] == 'train')]) == \
               len(data[data[exp_added_col] == 1])


@pytest.mark.parametrize('prices_input_df', ['test_1'], indirect=True)
def test_prices(prices_input_df):
    data, _, _ = prices_input_df
    price_change_freq, price_effect_dur, price_col = 25, 21, 'p1'
    assert data[price_col].nunique() == 12

    new_price_col = feature_generation.remove_price_outliers(data[price_col])
    assert new_price_col.nunique() == 9
    assert new_price_col.min() == 75
    assert new_price_col.max() == 143

    feature_generation.smooth_values(data, price_col, change_freq=25)
    assert data[price_col].nunique() == 8
    assert data[price_col].min() == 100
    assert data[price_col].max() == 143

    price_change_col = price_col + '_change'
    data[price_change_col] = feature_generation.add_fractional_changes_of_col(data[price_col])
    assert price_change_col in data.columns
    assert data[price_change_col].nunique() == 9
    assert data[data[price_change_col].isna()].shape[0] == 0
    assert data[price_change_col].max() > 0.2
    assert data[price_change_col].min() < -0.1

    assert feature_generation.check_if_to_add_prices(data, price_col, price_effect_dur, ['discount'])

    feature_generation.add_reg_price_features(data, price_col, ['discount'], price_change_freq, price_effect_dur)
    for col in [f'business-{price_col}_price_period_num_before', f'business-{price_col}_price_period_num_after']:
        assert col in data.columns
        assert data[data[col].isna()].shape[0] == 0
        assert data[col].sum() > 0.6


@pytest.mark.parametrize('prices_input_df', ['test_2'], indirect=True)
def test_changing_prices(prices_input_df):
    data, _, _ = prices_input_df
    data['initial_price'] = data['price_reg']
    feature_generation.smooth_values(data, 'price_reg', change_freq=9)

    assert data.price_reg.nunique() == 3
    assert abs(data.initial_price - data.price_reg).sum() > 18
    assert abs(data.initial_price - data.price_reg).sum() < 19


test_data_calc_promo_id = [
        (
            np.array([0, 0.1, 0, 0.1, 0]),
            1,
            np.array([0, 1, 0, 2, 0]),
            "Basic test",
        ),
        (
            np.array([0.1, 0, 0.1, 0]),
            1,
            np.array([1, 0, 2, 0]),
            "Test with a promo slot on the left border",
        ),
        (
            np.array([0.1, 0, 0.1, 0]),
            2,
            np.array([0, 0, 0, 0]),
            "Test with too short promo slot on the left border",
        ),
        (
            np.array([0, 0.1, 0, 0.1]),
            1,
            np.array([0, 1, 0, 2]),
            "Test with a promo slot on the right border",
        ),
        (
            np.array([0, 0.1, 0, 0.1]),
            2,
            np.array([0, 0, 0, 0]),
            "Test with too short promo slot on the right border",
        ),
        (
            np.array([0, 0.1, 0, 0.1, 0]),
            2,
            np.array([0, 0, 0, 0, 0]),
            "Test with too short promo slots in the middle",
        ),
        (
            np.array([0, 0.1, 0.1, 0.2, 0.1, 0.1, 0]),
            2,
            np.array([0, 1, 1, 0, 2, 2, 0]),
            "A test with stuck promo slots in the middle",
        ),
        (
            np.array([0.1, 0.2, 0.2, 0, 0, 0.1, 0.2, 0.3, 0, 0.3, 0.3, 0.3]),
            3,
            np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1]),
            "Combined test",
        ),
        (
            np.arange(20),
            1,
            np.arange(20),
            "A simple test with a stairway",
        ),
        (
            np.array([np.nan, 0, 1, np.nan, 0, np.nan, np.nan, 1, 1, np.nan]),
            1,
            np.array([0, 0, 1, 0, 0, 0, 0, 2, 2, 0]),
            "Test with NaN values",
        ),
        (
            np.array([]),
            1,
            np.array([]),
            "Test with empty input",
        ),
]


@pytest.mark.parametrize("discount, minimal_length, answer, error_msg", test_data_calc_promo_id)
def test_calc_promo_id(discount: np.ndarray, minimal_length: int, answer: np.ndarray, error_msg: str) -> None:

    output = feature_generation.calc_promo_id(discount, minimal_length)

    assert np.all(output == answer), error_msg


@pytest.mark.parametrize("sample, col, minimal_length", [
    (pd.DataFrame(columns=['discount']), 'discount', 1),
    (pd.DataFrame({"discount1": [1, 2, 3]}), 'discount1', 1),
])
def test_add_promo_id_longer_then(sample: pd.DataFrame, col: str, minimal_length: int) -> None:
    output_df, output_columns = feature_generation.add_promo_id_longer_then(sample.copy(), col, minimal_length)

    assert set(output_df.columns.difference(sample.columns)) == set(output_columns)
    assert sample.equals(output_df[sample.columns])


@pytest.mark.parametrize('hecate_input_df', ['test_7'], indirect=True)
def test_calc_promo_day(hecate_input_df):
    """
    1) assert on promo not followed by another promo, Ex: [0,0.2,0.2,0]
    2) assert on promo followed by another promo, Ex: [0,0.2,0.25,0]
    """
    data, _, _ = hecate_input_df
    data_test = data.drop(columns=['orig_promo_day', 'orig_promo_day2'])

    column_out = feature_generation.calc_promo_day(data_test['discount'].copy())

    assert (column_out == data['orig_promo_day']).all()

    column_out = feature_generation.calc_promo_day(data_test['discount2'].copy())

    assert (column_out == data['orig_promo_day2']).all()


test_data_calc_promo_day_synthetic_data = [
        (
            np.array([0.2, 0.2, 0.2, 0, 0.25, 0.25]),
            np.array([1, 2, 3, 0, 1, 2]),
            "Basic test on promo not followed by another promo, Ex: [0,0.2,0.2,0]",
        ),
        (
            np.array([0.2, 0.2, 0.25, 0, 0.2, 0.25]),
            np.array([1, 2, 1, 0, 1, 1]),
            "Advanced test on promo followed by another promo, Ex: [0,0.2,0.25,0]",
        ),
        (
            np.array([], dtype=float),
            np.array([], dtype=float),
            "Test with empty input",
        ),
]


@pytest.mark.parametrize("discount, answer, error_msg", test_data_calc_promo_day_synthetic_data)
def test_calc_promo_day_synthetic_data(discount: pd.Series, answer: pd.Series, error_msg: str) -> None:
    """
    1) assert on promo not followed by another promo, Ex: [0,0.2,0.2,0]
    2) assert on promo followed by another promo, Ex: [0,0.2,0.25,0]
    3) assert on empty Series
    """

    output = feature_generation.calc_promo_day(discount)

    assert np.all(output == answer), error_msg


@pytest.mark.parametrize('hecate_input_df', ['test_7'], indirect=True)
def test_add_mark_first_promo_slot(hecate_input_df):
    """
    1) assert only expected column added
    2) assert on promo not followed by another promo, Ex: [0,0.2,0.2,0]
    3) assert only expected column added
    4) assert on promo followed by another promo, Ex: [0,0.2,0.25,0]
    """
    data, _, _ = hecate_input_df
    data_test = data.drop(columns=['orig_first_slot', 'orig_first_slot2'])

    data_output, feature = feature_generation.add_mark_first_promo_slot(data_test.copy(), 'discount')

    assert set(data_output.columns.difference(data_test.columns)) == set(feature)
    assert (data_output['business-discount_first_slot'] == data['orig_first_slot']).all()


@pytest.mark.parametrize('hecate_input_df', ['test_7'], indirect=True)
def test_add_mark_last_trained_slot_and_later(hecate_input_df):
    """
    1) assert feature name and column in df
    2) assert vs ideal column
    """
    data, _, _ = hecate_input_df
    data_test = data.drop(columns=['orig_last_and_later_slot', 'orig_last_and_later_slot_splitted_valid'])

    # splitted_not_valid = True
    data_output, feature = feature_generation.add_mark_last_trained_slot_and_later(data_test.copy(), 'discount')
    assert ('business-discount_last_and_later_slot' in
            feature) & ('business-discount_last_and_later_slot' in data_output.columns)
    assert (data_output['business-discount_last_and_later_slot'] ==
            data['orig_last_and_later_slot']).all()

    # splitted_not_valid = False
    data_output, feature = feature_generation.add_mark_last_trained_slot_and_later(
        data_test.copy(), 'discount', splitted_not_valid=False)

    assert ('business-discount_last_and_later_slot' in
            feature) & ('business-discount_last_and_later_slot' in data_output.columns)
    assert (data_output['business-discount_last_and_later_slot'] ==
            data['orig_last_and_later_slot_splitted_valid']).all()


@pytest.mark.parametrize('hecate_input_df', ['test_7'], indirect=True)
def test_add_promo_period_num_real_data(hecate_input_df):
    """
    1) assert only expected column added
    2) assert on promo not followed by another promo, Ex: [0,0.2,0.2,0]
    3) assert only expected column added
    4) assert on promo followed by another promo, Ex: [0,0.2,0.25,0]
    """
    data, _, _ = hecate_input_df
    data_test = data.drop(columns=['orig_promo_day', 'orig_promo_day2'])

    data_output, feature = feature_generation.add_promo_period_num(data_test.copy(), 'discount')

    assert set(data_output.columns.difference(data_test.columns)) == set([feature])
    assert (data_output['business-discount_num'] == data['orig_promo_day']).all()

    data_output, feature = feature_generation.add_promo_period_num(data_test.copy(), 'discount2')

    assert set(data_output.columns.difference(data_test.columns)) == set([feature])
    assert (data_output['business-discount2_num'] == data['orig_promo_day2']).all()


test_data_add_promo_period_num_synthetic_data = [
        (
            np.array([0.2, 0.2, 0.2, 0, 0.25, 0.25]),
            np.array([1, 2, 3, 0, 1, 2]),
            "Basic test on promo not followed by another promo, Ex: [0,0.2,0.2,0]",
        ),
        (
            np.array([0.2, 0.2, 0.25, 0, 0.2, 0.25]),
            np.array([1, 2, 1, 0, 1, 1]),
            "Advanced test on promo followed by another promo, Ex: [0,0.2,0.25,0]",
        ),
        (
            np.array([0, 0.2, 0.25, 0, 0.2, 0.25]),
            np.array([0, 1, 1, 0, 1, 1]),
            "Test on promo starting with 0",
        ),
]


@pytest.mark.parametrize("discount, answer, error_msg", test_data_add_promo_period_num_synthetic_data)
def test_add_promo_period_num_synthetic_data(discount: np.ndarray, answer: np.ndarray, error_msg: str) -> None:
    """
    1) assert on promo not followed by another promo, Ex: [0,0.2,0.2,0]
    2) assert on promo followed by another promo, Ex: [0,0.2,0.25,0]
    3) assert on promo starting with 0
    """
    data = create_template_for_test_df()
    data['discount'] = discount
    data_output, _ = feature_generation.add_promo_period_num(data.copy(), 'discount')

    assert np.all(data_output['business-discount_num'] == answer), error_msg


@pytest.mark.parametrize('hecate_input_df', ['test_7'], indirect=True)
def test_add_week_before_promo(hecate_input_df):
    """
    1) assert feature name and column in df
    2) assert vs real promo data
    """
    data, _, _ = hecate_input_df
    data_test = data.drop(columns=['orig_week_before_promo'])

    data_output, feature = feature_generation.add_week_before_promo(data_test.copy(), 'discount')

    assert ('business-discount_week_before_promo' in
            feature) & ('business-discount_week_before_promo' in data_output.columns)
    assert (data_output['business-discount_week_before_promo'] == data['orig_week_before_promo']).all()


test_add_week_before_promo_synthetic_data = [
        (
            np.array([0, 0, 0, 0, 0, 0, 0, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2,
                      0.2, 0, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0, 0, 0, 0, 0, 0, 0, 0]),
            'discount',
            7,
            np.array([True,  True,  True,  True,  True,  True,  True, False, False,
                      False, False, False, False, False,  True, False, False, False,
                      False, False, False, False, False, False, False, False, False,
                      False, False, False]),
            "Test on promo with non promo week & short distance between promo",
        ),
        (
            np.array([0, 0, 0, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0, 0.2,
                      0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.2]),
            'discount',
            7,
            np.array([True,  True,  True, False, False, False, False, False, False,
                      False,  True, False, False, False, False, False, False, False,
                      False, False, False, False,  False, False, False, False, False,
                      False, False, False]),
            "Test on promo with 3 non promo days & short distance between promo",
        ),
        (
            np.array([0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.2,
                      0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0]),
            'discount',
            7,
            np.array([False, False, False, False, False, False, False, False, False,
                      False,  False, False, False, False, False, True, True, True,
                      True, True, True, True,  False, False, False, False, False,
                      False, False, False]),
            "Test on promo with 0 non promo & long distance between promo",
        ),
        (
            np.array([0, 0, 0.2, 0.2, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                      0, 0, 0, 0, 0, 0.2, 0.2, 0, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2]),
            'discount',
            8,
            np.array([False, False, False, False, False, False, False, False, False,
                      False, False, False, False, False,  True,  True,  True,  True,
                      True,  True,  True, False, False, False, False, False, False,
                      False, False, False]),
            "Test on minimal_length=8",
        ),
]


@pytest.mark.parametrize("discount, column, minimal_length, answer, error_msg",
                         test_add_week_before_promo_synthetic_data)
def test_add_week_before_promo_synthetic_data(discount: np.ndarray, column: str, minimal_length: int,
                                              answer: np.ndarray, error_msg: str) -> None:
    """
    !!! Promo followed by another promo not tested, Ex: [0,0.2,0.25,0] !!!
    !!! Testing only for minimal_length>=7 !!!
    1) assert on promo with non promo week & short distance between promo
    2) assert on promo with 3 non promo days & short distance between promo
    3) assert on promo with 0 non promo & long distance between promo
    4) assert on minimal_length=8
    """
    data = create_template_for_test_df(full=True)
    data['discount'] = discount
    data_output, _ = feature_generation.add_week_before_promo(data.copy(), column, minimal_length=minimal_length)

    assert np.all(data_output[f'business-{column}_week_before_promo'] == answer), error_msg


@pytest.mark.parametrize('hecate_input_df', ['test_7'], indirect=True)
def test_cut_potential_pincs(hecate_input_df):
    """
    1) assert nan added
    2) assert vs real promo data
    """
    data, _, _ = hecate_input_df
    data_test = data.drop(columns=['orig_cut_potential_pincs'])

    column_out = feature_generation.cut_potential_pincs(data_test['price'], window=7)

    assert np.isnan(column_out).any()
    assert (column_out.equals(data['orig_cut_potential_pincs']))


test_data_cut_potential_pincs_synthetic_data = [
        (
            pd.Series([1, 1, 1, 1, 1, 1, 1, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8,
                       0.8, 1, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 1, 1, 1, 1,
                       1, 1, 1, 1]),
            7,
            pd.Series([np.nan, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                       1,  1,  1,  1,  1,  1, np.nan, np.nan,  1,  1,
                       1,  1,  1,  1, np.nan,  1,  1,  1,  1, 1,  1,  1,  1]),
            "Basic test on price with long graduate inscrease and small sharp increase",
        ),
        (
            pd.Series([0.8, 1, 1, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 1, 0.8, 0.8,
                       0.8, 0.8, 0.8, 0.8, 0.8, 1, 1, 1, 1, 1, 1, 1, 1,
                       1, 0.8, 1, 0.8]),
            7,
            pd.Series([1,  1,  1,  1,  1,  1,  1,  1,  1, np.nan, np.nan,  1,  1,
                       1,  1,  1,  1, np.nan,  1,  1,  1,  1,  1,  1,  1,  1,
                       1,  1, np.nan, np.nan]),
            "Advanced test on price with sharp increase and graduate later",
        ),
        (
            pd.Series([1, 1, 0.8, 0.8, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                       1, 1, 1, 1, 1, 0.8, 0.8, 1, 0.8, 0.8, 0.8, 0.8, 0.8,
                       0.8, 0.8, 0.8, 0.8]),
            7,
            pd.Series([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                       1, 1, np.nan, np.nan, np.nan, np.nan, np.nan, np.nan,
                       np.nan, 0.8, 0.8, 0.8]),
            "Advanced test on price with sharp increase and split graduate later",
        ),
        (
            pd.Series([], dtype=float),
            7,
            pd.Series([], dtype=float),
            "Test with empty input",
        ),
]


@pytest.mark.parametrize("price, window, answer, error_msg", test_data_cut_potential_pincs_synthetic_data)
def test_cut_potential_pincs_synthetic_data(price: pd.Series, window: int, answer: pd.Series, error_msg: str) -> None:
    """
    1) assert on price with long graduate inscrease and small sharp increase
    2) assert on price with sharp increase and graduate later
    3) assert on price with sharp increase and split graduate later
    4) assert on empty Series
    """

    output = feature_generation.cut_potential_pincs(price, window=window)

    assert np.allclose(output, answer, equal_nan=True), error_msg


@pytest.mark.parametrize('hecate_input_df', ['test_7'], indirect=True)
def test_approximate_by_step_function(hecate_input_df):
    """
    1) assert nan added
    2) assert vs real promo data
    """
    data, _, _ = hecate_input_df
    data_test = data.drop(columns=['orig_approximate_by_step_function'])

    column_out = feature_generation.approximate_by_step_function(data_test['price'], window=7)

    assert not np.isnan(column_out).any()
    assert (column_out == data['orig_approximate_by_step_function']).all()


test_data_approximate_by_step_function_synthetic_data = [
        (
            pd.Series([1, 1, 1, 1, 1, 1, 1, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8,
                       0.8, 1, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 1, 1, 1, 1,
                       1, 1, 1, 1]),
            7,
            pd.Series([1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.,
                       1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.]),
            "Basic test on price with long graduate inscrease and small sharp increase",
        ),
        (
            pd.Series([0.8, 1, 1, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 1, 0.8, 0.8,
                       0.8, 0.8, 0.8, 0.8, 0.8, 1, 1, 1, 1, 1, 1, 1, 1,
                       1, 0.8, 1, 0.8]),
            7,
            pd.Series([1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.,
                       1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.]),
            "Advanced test on price with sharp increase and graduate later",
        ),
        (
            pd.Series([1, 1, 0.8, 0.8, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                       1, 1, 1, 1, 1, 0.8, 0.8, 1, 0.8, 0.8, 0.8, 0.8, 0.8,
                       0.8, 0.8, 0.8, 0.8]),
            7,
            pd.Series([1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1., 1.,
                       1., 1., 1., 1., 1., 1., 1., 1., 0.8, 0.8, 0.8, 0.8, 0.8,
                       0.8, 0.8, 0.8, 0.8]),
            "Advanced test on price with sharp increase and split graduate later",
        ),
        (
            pd.Series([], dtype=float),
            7,
            pd.Series([], dtype=float),
            "Test with empty input",
        ),
]


@pytest.mark.parametrize("price, window, answer, error_msg", test_data_approximate_by_step_function_synthetic_data)
def test_approximate_by_step_function_synthetic_data(price: pd.Series, window: int, answer: pd.Series,
                                                     error_msg: str) -> None:
    """
    1) assert on price with long graduate inscrease and small sharp increase
    2) assert on price with sharp increase and graduate later
    3) assert on price with sharp increase and split graduate later
    4) assert on empty Series
    """

    output = feature_generation.approximate_by_step_function(price, window=window)

    assert np.allclose(output, answer, equal_nan=True), error_msg


@pytest.mark.parametrize('hecate_input_df', ['test_7'], indirect=True)
def test_add_fractional_changes_of_col(hecate_input_df):
    """
    1) assert nan added
    2) assert vs real promo data
    """
    data, _, _ = hecate_input_df
    data_test = data.drop(columns=['orig_add_fractional_changes_of_col'])

    column_out = feature_generation.add_fractional_changes_of_col(data_test['price'])

    assert not np.isnan(column_out).any()
    assert np.isclose(column_out, data['orig_add_fractional_changes_of_col']).all()


test_data_add_fractional_changes_of_col_synthetic_data = [
        (
            pd.Series([1, 1, 1, 1, 1, 1, 1, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8,
                       0.8, 1, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 1, 1, 1, 1,
                       1, 1, 1, 1]),
            pd.Series([0, 0, 0, 0, 0, 0,  0, -0.2, 0, 0, 0, 0, 0, 0, 0.25, -0.2, 0, 0,
                       0, 0, 0, 0, 0.25, 0, 0, 0, 0, 0, 0, 0]),
            "Basic test on price with long graduate inscrease and small sharp increase",
        ),
        (
            pd.Series([0.8, 1, 1, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 0.8, 1, 0.8, 0.8,
                       0.8, 0.8, 0.8, 0.8, 0.8, 1, 1, 1, 1, 1, 1, 1, 1,
                       1, 0.8, 1, 0.8]),
            pd.Series([0, 0.25, 0, -0.2,  0,  0,  0,  0,  0, 0, 0.25, -0.2,  0,  0,  0,
                       0,  0,  0, 0.25,  0,  0,  0,  0,  0,  0,  0,  0, -0.2,  0.25, -0.2]),
            "Advanced test on price with sharp increase and graduate later",
        ),
        (
            pd.Series([1, 1, 0.8, 0.8, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                       1, 1, 1, 1, 1, 0.8, 0.8, 1, 0.8, 0.8, 0.8, 0.8, 0.8,
                       0.8, 0.8, 0.8, 0.8]),
            pd.Series([0,  0, -0.2,  0,  0.25,  0,  0,  0,  0,  0,  0,  0,  0, 0,  0,  0,
                       0,  0, -0.2,  0,  0.25, -0.2,  0,  0,  0,  0,  0, 0,  0,  0]),
            "Advanced test on price with sharp increase and split graduate later",
        ),
]


@pytest.mark.parametrize("price, answer, error_msg", test_data_add_fractional_changes_of_col_synthetic_data)
def test_add_fractional_changes_of_col_synthetic_data(price: pd.Series, answer: pd.Series, error_msg: str) -> None:
    """
    !!! Not tested on empty Series !!!
    1) assert on price with long graduate inscrease and small sharp increase
    2) assert on price with sharp increase and graduate later
    3) assert on price with sharp increase and split graduate later
    """

    output = feature_generation.add_fractional_changes_of_col(price)

    assert np.allclose(output, answer, equal_nan=True), error_msg


def test_is_pinc_produce_target_changes() -> None:
    df = pd.DataFrame({
        'date': pd.date_range('2023-01-01', periods=47),
        'partition': ['train']*42 + ['fc']*5,
        "discount": [0]*47,
        'pinc_test': np.array([100]*21 +
                              [200]*21 +
                              [200]*5),
        "target": np.array([10, 11, 10, 9, 12, 10, 12, 8, 11, 10, 12, 9, 11, 8, 12, 10, 12, 9, 8, 10, 11,
                            20, 21, 20, 19, 22, 20, 22, 18, 21, 20, 22, 19, 21, 18, 22, 20, 22, 19, 18, 20, 21,
                            0, 0, 0, 0, 0]),
    })

    answer = True
    res = feature_generation.is_pinc_produce_target_changes(df, 'pinc_test', 21, ['discount'])

    assert res == answer, res


data_for_test_get_bound_indexes_of_n_points_after_pinc = [
    (np.array([1, 1, 1, 1]), 1, []),
    (np.array([1, 1, 2, 2]), 1, [(2, 2)]),
    (np.array([1, 1, 2, 2]), 2, [(2, 3)]),
    (np.array([1, 1, 2, 2]), 3, [(2, 3)]),
    (np.array([1, 1, 2, 2, 3, 3]), 3, [(2, 3), (4, 5)]),
    (np.array([1, 2, 3]), 3, [(1, 1), (2, 2)]),
]


@pytest.mark.parametrize(('array', 'n_points', 'bounds'), data_for_test_get_bound_indexes_of_n_points_after_pinc)
def test_get_bound_indexes_of_n_points_after_pinc(
    array: np.ndarray, n_points: int, bounds: list[tuple[int, int]]
) -> None:

    func = feature_generation.get_bound_indexes_of_n_points_after_pinc

    assert func(price_array=array, n_points=n_points) == bounds
