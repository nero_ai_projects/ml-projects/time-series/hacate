from fastapi.testclient import TestClient
import app
import pandas as pd
from io import BytesIO
import hashlib
import json


client = TestClient(app.app)

DATA_DICT = {
    'key': ['key-1', 'key-1', 'key-1', 'key-1'],
    'date': ['2022-07-04', '2022-07-11', '2022-07-18', '2022-07-25'],
    'discount': [10, 0, 0, 0],
    'partition': ['train', 'train', 'train', 'fc'],
    'target': [100.01, 1, 1, 0.0]
}
NECESSARY_DIMS = ['key', 'date', 'partition']
PARAMS_DICT = [
    {"url": "/fc", "payload_type": "json"},
    {"url": "/fc", "payload_type": "parquet"},
    {"url": "/fc_batch", "payload_type": "json"},
    {"url": "/fc_batch", "payload_type": "parquet"},
    {"url": "/get_can_pairs", "payload_type": "json"},
    {"url": "/get_can_pairs", "payload_type": "parquet"},
    {"url": "/decompose_act", "payload_type": "json"},
    {"url": "/decompose_act", "payload_type": "parquet"},
]

for i in PARAMS_DICT:
    i["feature_types"] = {"discount": "discount"}


def send_request(url, payload_type, feature_types, mocker, status_code, auth=False):
    if not auth:
        mocker.patch('app.is_authorized', return_value=(True, None))
    data = pd.DataFrame.from_dict(DATA_DICT)
    req_data = dict()
    if payload_type == "json":
        data_io = data.to_json()
        data_io = json.loads(data_io)
        req_data["json"] = data_io if status_code == 200 else dict({"data": "test"})
    else:
        data_io = BytesIO()
        for dim in NECESSARY_DIMS:
            data[dim] = pd.Categorical(data[dim])
        data.to_parquet(data_io, index=False, compression='gzip')
        req_data["data"] = data_io.getvalue() if status_code == 200 else bytes("test", "utf-8")
    response = client.post(url + f"?payload_type={payload_type}",
                           headers={"api_key": "test", "Feature-types": json.dumps(feature_types)},
                           **req_data)
    print(response._content)
    assert response.status_code == status_code


def test_routes(mocker):
    for param in PARAMS_DICT:
        send_request(mocker=mocker, status_code=200, **param)


def test_app_fail(mocker):
    for param in PARAMS_DICT:
        send_request(mocker=mocker, status_code=400, **param)


def test_auth(mocker):
    api_keys_dict = {"user_1": "pytest_key_1",
                     "user_2": "pytest_key_2"}
    app.app.api_keys_dict = api_keys_dict
    for key in api_keys_dict:
        is_auth, user = app.is_authorized(hashlib.sha512(
                                api_keys_dict[key].encode()
                            ).hexdigest())
        assert is_auth and user == key


def test_headers_correction(mocker):
    mocker.patch('app.is_authorized', return_value=(True, None))
    data = pd.DataFrame.from_dict(DATA_DICT)
    data["dim"] = ["2 Jan", "3 Jan",
                   "4 Jan", "5 Jan"]
    req_data = dict()
    headers = {"api_key": "test"}
    data_io = data.to_json()
    data_io = json.loads(data_io)
    req_data["json"] = data_io
    response = client.post("/fc?payload_type=json",
                           headers=headers,
                           **req_data)
    assert response.status_code == 500
    response = client.post("/fc_batch?payload_type=json",
                           headers=headers,
                           **req_data)
    assert response.status_code == 500
    response = client.post("/decompose_act?payload_type=json",
                           headers=headers,
                           **req_data)
    assert response.status_code == 400

    headers['feature-types'] = '{"dim": "dim", "discount": "discount"}'
    response = client.post("/fc?payload_type=json",
                           headers=headers,
                           **req_data)
    assert response.status_code == 200
    response = client.post("/fc_batch?payload_type=json",
                           headers=headers,
                           **req_data)
    assert response.status_code == 200
    response = client.post("/decompose_act?payload_type=json",
                           headers=headers,
                           **req_data)
    assert response.status_code == 200
